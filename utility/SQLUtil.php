<?php
/* Copyright (C) Medelice, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Jules Sanglier <jsgr@protonmail.ch>, January 2018
 */

require_once(__DIR__.'/../initializer.inc.php');
require_once(__DIR__.'/../exceptions/DatabaseException.php');
require_once(__DIR__.'/../exceptions/DatabaseConnectionException.php');
require_once(__DIR__.'/../exceptions/DatabaseInvalidQueryException.php');
require_once(__DIR__.'/../exceptions/DatabaseRequestException.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 26/10/2017
 * Time: 16:42
 *
 */
class SQLUtil
{
    public static function open($DB = ""){
        $sql = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, $DB);
        if ($sql->connect_error){
            throw new DatabaseConnectionException('Connection exception');
        }
        $sql->set_charset('utf8');
        return $sql;
    }

    public static function close($sql_instance){
        if (!$sql_instance instanceof mysqli){

        }

        return $sql_instance->close();
    }

    private static function get_db_from_table($table){
        $prefix_associated_with_db = [
            "products",
            "users",
            "config"
        ];

        foreach ($prefix_associated_with_db as $prefix){
            if (strpos($table, $prefix) !== false){
                //var_dump("medelice_".$prefix);
                return "medelice_".$prefix;
            }
        }

        return null;
    }

    public static function delete($table, $where, $options = null){
        $sql = self::open(self::get_db_from_table($table));
        $params = '';
        $select = 'delete ';
        $bind_parameters = [];

        $where_count = count($where);
        $select .= 'from '.$table.' where ';
        for ($j = 0; $j < 2; $j++) {
            $i = 0;
            foreach ($where as $column_with_instruction => $comparison_data) {
                switch ($j) {
                    case 0:
                        $w_splitted = explode(':', $column_with_instruction);
                        $column = $w_splitted[0];
                        $instruction = (count($w_splitted) > 1 ? $w_splitted[1] : "=");

                        $select .= $column . ' ' . $instruction . ' ?' . ($i < ($where_count - 1) ? ' and ' : '');
                        $params .= (is_float($comparison_data) ? 'd' : (is_int($comparison_data) ? 'i' : 's'));
                        break;

                    default:
                        if ($i === 0) {
                            $bind_parameters[] = $params;
                        }

                        $bind_parameters[] = &$where[$column_with_instruction];
                        break;
                }
                ++$i;
            }
        }

        if ($options != null){
            $select .= ' '.$options;
        }

        $stmt = $sql->prepare($select);

        if ($stmt){
            call_user_func_array(array($stmt, 'bind_param'), $bind_parameters);
            unset($bind_parameters);
            $stmt->execute();
            if ($stmt->error != ''){
                throw new DatabaseRequestException($stmt->error, $stmt);
            }

            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('Invalid query : '.$select);
        }

        self::close($sql);

        return true;
    }

    /**
     * @param $table
     * @param $columns array ["column1", "column2"]
     * @param $where
     * @param $options
     * @throws DatabaseConnectionException
     */
    public static function select($table, $columns, $where, $options = null){
        //var_dump(self::get_db_from_table($table));
        $sql = self::open(self::get_db_from_table($table));
        $params = '';
        $select = 'select ';
        $i = 0;
        $bind_parameters = [];

        $columns_count = count($columns);
        foreach ($columns as $column) {
            $select .= $column . ($i < ($columns_count - 1) ? ',' : ' ');
            ++$i;
        }
        //var_dump($table);
        //var_dump(self::get_db_from_table($table));
        $where_count = count($where);
        $where_check = $where_count > 0;
        $select .= 'from '.$table.($where_check ? ' where ' : '');
        if ($where_check) {
            for ($j = 0; $j < 2; $j++) {
                $i = 0;
                foreach ($where as $column_with_instruction => $comparison_data) {
                    switch ($j) {
                        case 0:
                            $w_splitted = explode(':', $column_with_instruction);
                            $column = $w_splitted[0];
                            $instruction = (count($w_splitted) > 1 ? $w_splitted[1] : "=");

                            $select .= $column . ' ' . $instruction . ' ?' . ($i < ($where_count - 1) ? ' and ' : '');
                            $params .= (is_float($comparison_data) ? 'd' : (is_int($comparison_data) ? 'i' : 's'));
                            break;

                        default:
                            if ($i === 0) {
                                $bind_parameters[] = $params;
                            }

                            $bind_parameters[] = &$where[$column_with_instruction];
                            break;
                    }
                    ++$i;
                }
            }
        }

        if ($options != null){
            $select .= ' '.$options;
        }
        $stmt = $sql->prepare($select);
        $returned_results = array();
        //var_dump($select);
        if ($stmt){
            if (count($bind_parameters) > 0) {
                call_user_func_array(array($stmt, 'bind_param'), $bind_parameters);
            }

            unset($bind_parameters);
            $stmt->execute();

            if ($stmt->error != ''){
                throw new DatabaseRequestException($stmt->error, $stmt);
            }

            $meta = $stmt->result_metadata();
            $result = array();

            while ($field = $meta->fetch_field()){
                $var = $field->name;
                $$var = null;
                $result[$var] = &$$var;
            }

            call_user_func_array(array($stmt, 'bind_result'), $result);
            while ($stmt->fetch()) {
                array_push($returned_results, array_map(create_function('$x', 'return $x;'), $result));
            }

            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        self::close($sql);

        return $returned_results;
    }

    //update table set x = y, z = alpha where blablabla
    public static function update($table, $sets, $where, $options = null){
        $sql = self::open(self::get_db_from_table($table));
        $params = '';
        $update = 'update '.$table.' ';
        $bind_parameters = [];

        $sets_count = count($sets);
        $i = 0;
        foreach ($sets as $column => $value) { //building prepared query (set part) and binding parameters types
            if ($i == 0) {
                $update .= 'set ';
            }
            $update .= $column . ' = ?' . ($i < ($sets_count - 1) ? ', ' : ' ');
            $params .= (is_float($value) ? 'd' : (is_int($value) ? 'i' : 's'));
            ++$i;
        }
        $update .= 'where ';

        $where_count = count($where);
        $i = 0;
        foreach ($where as $column_with_instruction => $comparison_data) { //building prepared query (where part) and binding parameters types
            $w_splitted = explode(':', $column_with_instruction);
            $column = $w_splitted[0];
            $instruction = (count($w_splitted) > 1 ? $w_splitted[1] : "=");

            $update .= $column . ' ' . $instruction . ' ?' . ($i < ($where_count - 1) ? ' and ' : '');
            $params .= (is_float($comparison_data) ? 'd' : (is_int($comparison_data) ? 'i' : 's'));
            ++$i;
        }


        $bind_parameters[] = $params;

        foreach ($sets as $column => $value) { //binding set parameters
            $bind_parameters[] = &$sets[$column];
        }

        foreach ($where as $column_with_instruction => $comparison_data) { //binding where parameters
            $bind_parameters[] = &$where[$column_with_instruction];
        }

        if ($options != null){
            $update .= ' '.$options;
        }

        $stmt = $sql->prepare($update);
        if ($stmt){
            call_user_func_array(array($stmt, 'bind_param'), $bind_parameters);
            unset($bind_parameters);
            $stmt->execute();


            if ($stmt->error != ''){
                throw new DatabaseRequestException($stmt->error, $stmt);
            }

            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('Invalid query : '.$update);
        }

        self::close($sql);

        return true;
    }

    /**
     * If exists, update otherwise insert
     */
    public static function set(){

    }

    /**
     * Insert values in a table. I think, I'll call this function the unreadable,
     * yeah it's fucking incomprehensible except for the dev hihi (it's me Jules), but I truly love her, call me if u need
     * @param $table string The insertion table.
     * @param $columns_values_array array As key : the column name, and as value : value ftw
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function insert($table, $columns_values_array)
    {
        $sql = self::open(self::get_db_from_table($table));
        $params = '';
        $insert = 'insert into ' . $table . ' (';
        $args = [];

        $count_columns_values_array = count($columns_values_array);
        for ($j = 0; $j < 4; $j++) {
            $i = 0;

            if ($j < 3) {
                foreach ($columns_values_array as $key => $value) {
                    switch ($j) {
                        case 0:
                            $insert .= $key . (($i < ($count_columns_values_array - 1)) ? ',' : ') values(');
                            break;
                        case 1:
                            $insert .= '?' . (($i < ($count_columns_values_array - 1)) ? ',' : ')');
                            $params .= (is_float($value) ? 'd' : (is_int($value) ? 'i' : 's'));
                            break;
                        case 2:
                            if ($i === 0) {
                                $args[] = $params;
                            }

                            $args[] = &$columns_values_array[$key];
                            break;
                    }
                    $i++;
                }
            }
            else {
                //var_dump($insert);
                $stmt = $sql->prepare($insert);
                if ($stmt) {
                    call_user_func_array(array($stmt, 'bind_param'), $args);

                    $stmt->execute();
                    //var_dump($stmt);
                    if ($stmt->error !== ''){
                        throw new DatabaseRequestException($stmt->error, $stmt);
                    }

                    $stmt->close();
                } else {
                    throw new DatabaseInvalidQueryException('');
                }

                self::close($sql);
            }
        }
    }
}