<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/08/2018
 * Time: 18:16
 *
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\PHPMailerException;

require_once(__DIR__.'/../vendor/autoload.php');

interface IMailConfiguration{
    public function getHost();
    public function getUsername();
    public function getPassword();
}

abstract class MailConfiguration{
    public $host;
    public $port;
    public $username;
    public $password;
}

class DevteamMailConfiguration extends MailConfiguration implements IMailConfiguration {

    public function __construct(){
        $this->host = "ssl0.ovh.net";
        $this->port = 587;
        $this->username = "devteam@medelice.com";
        $this->password = "Dj74xwpMazj87!";
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort(){
        return $this->port;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }
}

class RobotMailConfiguration extends MailConfiguration implements IMailConfiguration{

    public function __construct(){
        $this->host = "ssl0.ovh.net";
        $this->port = 587;
        $this->username = "robot@medelice.com";
        $this->password = "Ad54x83hdX2&br";
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort(){
        return $this->port;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }
}

class ContactClientMailConfiguration extends MailConfiguration implements IMailConfiguration{
    public function __construct(){
        $this->host = "ssl0.ovh.net";
        $this->port = 587;
        $this->username = "contact-client@medelice.fr";
        $this->password = "ZjhDjcb54SJxds";
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPort(){
        return $this->port;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getPassword()
    {
        return $this->password;
    }
}

class Mail
{
    private $mail_configuration;
    private $to;
    private $subject;
    private $body;

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo(string $to)
    {
        $this->to = $to;
    }

    public function addTo(string $mail){
        if (!is_array($this->to))
            $this->to = array();

        array_push($this->to, $mail);
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getMailConfiguration()
    {
        return $this->mail_configuration;
    }

    /**
     * @param mixed $mail_configuration
     */
    public function setMailConfiguration(MailConfiguration $mail_configuration)
    {
        $this->mail_configuration = $mail_configuration;
    }
}

interface IMailSender{
    public function send(Mail $mail);
}

class MailSender implements IMailSender{

    //TODO mailer exception
    public function send(Mail $mail, array $patterns = array("/{content}/"), array $replacements = array(), bool $without_background_template = false)
    {
        $pmail = new PHPMailer(true);
        $configuration = $mail->getMailConfiguration();
        if (!$configuration instanceof MailConfiguration){
            throw new MedeliceException("");
        }

        try {
            $pmail->isSMTP();
            $pmail->Host = $configuration->host;
            $pmail->SMTPAuth = true;
            $pmail->Username = $configuration->username;
            $pmail->Password = $configuration->password;
            $pmail->SMTPSecure = 'tls';
            $pmail->Port = $configuration->port;

            $pmail->setFrom($configuration->username, $mail->getSubject());
            if (is_array($mail->getTo())){
                $pmail->SMTPKeepAlive = true;

                foreach ($mail->getTo() as $mail_) {
                    $pmail->addAddress($mail_);
                }
            }else{
                $pmail->addAddress($mail->getTo());
            }


            $pmail->CharSet = 'UTF-8';
            $pmail->isHTML(true);
            $pmail->Subject = $mail->getSubject();

            if (!$without_background_template) {
                $template = file_get_contents(__DIR__ . '/mail_template.html');

                $pmail->Body = preg_replace($patterns, $replacements, $template);
            }
            else{
                $pmail->Body = $mail->getBody();
            }

            $pmail->setFrom($configuration->username, "Médélice");

            $pmail->send();

        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $e->getMessage();
        }

        return true;
    }
}