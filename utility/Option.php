<?php
/* Copyright (C) Medelice, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Jules Sanglier <jsgr@protonmail.ch>, January 2018
 */

require_once(__DIR__.'/../initializer.inc.php');

interface OptionProvider
{
    public function get($option_key);
    public function update_value($option);
    public function insert($option);
    public function remove($option_key);
}

class DBOptionProvider implements OptionProvider
{
    public function get($option_key){
        $raw_option = SQLUtil::select(OPTIONS_TABLE,
            ['*'],
            [Option::$COLUMNS[0] => $option_key]);

        if (count($raw_option) == 0){
            throw new MedeliceException("Option doesn't exists");
        }

        $option = new Option();
        $option->setKey($option_key);
        $option->setValue($raw_option[0]["value"]);

        return $option;
    }

    public function update_value($option){
        if (!$option instanceof Option){
            throw new MedeliceException("option must be an instanceof Option");
        }

        SQLUtil::update(OPTIONS_TABLE,
            [Option::$COLUMNS[1] => $option->getValue()],
            [Option::$COLUMNS[0] => $option->getKey()]);

        return true;
    }

    public function insert($option){
        if (!$option instanceof Option){
            throw new MedeliceException("option must be an instanceof Option");
        }

        SQLUtil::insert(OPTIONS_TABLE,
            [
                Option::$COLUMNS[0] => $option->getKey(),
                Option::$COLUMNS[1] => $option->getValue()
            ]);
        return true;
    }

    public function remove($option_key){
        SQLUtil::delete(OPTIONS_TABLE, [Option::$COLUMNS[0] => $option_key]);
        return true;
    }
}

class Option{
    private $key;
    private $value;

    public static $COLUMNS = array("name", "value");

    /**
     * @param $opt_key
     */
    public function __construct(){
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param mixed $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }
}