<?php
/* Copyright (C) Medelice, All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Jules Sanglier <jsgr@protonmail.ch>, January 2018
 */

class RegexUtil
{
    //Faked filter_in
    public static function filter_in_var_with_regex($in, $variable, $regex, $throw_error_if_failed = true, $throw_error_if_not_exists = true){
        $in_var = null;

        if ($in == INPUT_GET ? !isset($_GET[$variable]) : !isset($_POST[$variable])){
            if ($throw_error_if_not_exists){
                throw new MedeliceException("Une erreur interne due à une valeur inexistante à eu lieu");
            }
            return false;
        }else{
            $in_var = $in == INPUT_GET ? $_GET[$variable] : $_POST[$variable];
        }
        //throw new MedeliceException($_POST);

        return self::filter_var_with_regex($in_var, $regex, $throw_error_if_failed);
    }

    public static function filter_var_with_regex($variable, $regex, $throw_error_if_failed = true, $check_if_exists = false){
        if ($check_if_exists){
            if (!isset($variable)){
                throw new MedeliceException("Une erreur interne due à une valeur inexistante à eu lieu");
            }
        }

        $filtered = filter_var($variable, FILTER_VALIDATE_REGEXP,
            array(
                "options" => array("regexp" => $regex)
            )
        );

        if ($filtered == false && $throw_error_if_failed){
            if (strcmp($variable, "") == 0){
                throw new MedeliceException("Veuillez remplir le(s) champ(s) manquant(s).");
            }
            throw new MedeliceException("Veuillez verifier les données saisies.");
        }

        return $filtered;
    }

    //"classic" means without any default parameter like a regex
    public static function classic_filter_in_with_error_thrower($in, $variable, $filter, $throw_error_if_not_exists = false)
    {
        if ($throw_error_if_not_exists){
            if ($in == INPUT_GET ? !isset($_GET[$variable]) : !isset($_POST[$variable]))
                throw new MedeliceException('Filter thrown');
        }

        $filtered = filter_input($in, $variable, $filter);
        //var_dump($filter);
        if (!$filtered && $throw_error_if_not_exists){
            if (strcmp($variable, "") == 0 || $variable == null){
                throw new MedeliceException("Veuillez remplir le(s) champ(s) manquant(s).");
            }
            throw new MedeliceException("Veuillez verifier les données saisies.");
        }
        return $filtered;
    }

    public static function classic_filter_var_with_error_thrower($variable, $filter, $throw_error_if_not_exists = false)
    {
        if ($throw_error_if_not_exists){
            if (!isset($variable))
                throw new MedeliceException('Filter thrown');
        }

        $filtered = filter_var($variable, $filter);
        if (!$filtered && $throw_error_if_not_exists){
            if (strcmp($variable, "") == 0 || $variable == null){
                throw new MedeliceException("Veuillez remplir le(s) champ(s) manquant(s).");
            }
            throw new MedeliceException("Veuillez verifier les données saisies.".$filtered.$filter);
        }
        return $filtered;
    }

    public static function uid(){
        return '/^[a-zA-Z\d]{13}$/';
    }

    public static function order_uid(){
        return '/^[a-zA-Z\d]{8}$/';
    }

    public static function stripe_source_token(){
        return '/^\A(src_){1}[a-zA-Z\d]{24}$/';
    }

    public static function stripe_source_secret_token(){
        return '/^\A(src_client_secret_){1}[a-zA-Z\d]{24}$/';
    }

    public static function token(){
        return '/^[0-9-a-f]{24}[.]{1}[0-9]{8}$/';
    }

    public static function token_hash(){
        return '/^[0-9-a-f]{32}$/';
    }

    public static function password(){
        return '/^(?=.*[&~@!:;(){}\-\|_])(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,64}$/';
    }

    public static function email(){
        return '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/iD';
    }

    public static function name(){
        return '/^[a-zA-ZÀ-ÿ -]{2,50}$/';
    }

    public static function input_birth(){
        return '/^\d{4}-\d{2}-\d{2}$/';
    }

    public static function birth(){
        return '/^\d{2}\/\d{2}\/\d{4}$/';
    }

    public static function international_phone(){
        return '/((?:\+|00)[17](?: |\-)?|(?:\+|00)[1-9]\d{0,2}(?: |\-)?|(?:\+|00)1\-\d{3}(?: |\-)?)?(0\d|\([0-9]{3}\)|[1-9]{0,3})(?:((?: |\-)[0-9]{2}){4}|((?:[0-9]{2}){4})|((?: |\-)[0-9]{3}(?: |\-)[0-9]{4})|([0-9]{7}))/';
    }

    public static function box_ref(){
        return '/^(V_){1}[a-zA-Z-0-9]{3,50}$/';
    }

    public static function intersect_product_and_box_ref()
    {
        return '/(^[a-zA-Z_]{3,50}\d*$)|(^(V_){1}[a-zA-Z-0-9]{3,50}$)/';
    }

    public static function region_id(){
        return '/^[A-Z]{3}$/';
    }

    public static function product_ref(){
        return '/^[a-zA-Z_]{3,50}\d*$/';
    }

    public static function product_name(){
        return '/^[a-zA-Z0-9_àéèç()!\'-]+( [a-zA-Z0-9_àéèç()!\'-]+)*$/';
    }

    public static function product_quantity_unity(){
        return '/^[a-zA-Z]{1,3}$/';
    }

    public static function product_quantity_value(){
        return '/^\d{1,6}$/';
    }

    public static function product_category(){
        return '/^\d{1,11}$/';
    }

    public static function product_description(){
        return '/^([a-zA-Z])*(\.)*(\,)*(\!)*(\?)*(\;)*(\:)*$/';
    }

    public static function delivery_mode(){
        return '/^(normal|express){1}$/';
    }

    public static function tracking()
    {
        return '/^(\w|\d)+$/';
    }

    public static function crc32(){
        return '/[a-f0-9]{8}/';
    }

    public static function json()
    {
        return '/\{.*\:\{.*\:.*\}\}/';
    }

    public static function sha1(){
        return '/\b[0-9a-f]{5,40}\b/';
    }

    public static function sha256(){
        return '/\b[A-Fa-f0-9]{64}\b/';
    }

    public static function iso3166_country(){
        return '/^[A-Z]{2}$/';
    }

    public static function checkbox(){
        return '/^(on|off)$/';
    }

    public static function int(){
        return '/^\d*$/';
    }

    public static function order_status(){
        return '/^\-[1]{1}$|^[0-5]{1}$/';
    }

    private static function to_HTML_Regex($regex)
    {
        $regex = substr($regex, 1, (strlen($regex) - 2));
        return $regex;
    }

    public static function output2HTML($regex)
    {
        echo self::to_HTML_Regex($regex);
    }
}