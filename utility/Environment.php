<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/08/2018
 * Time: 02:04
 */
include(__DIR__.'/../initializer.inc.php');

class Environment
{
    public static function get(){
        return DEBUG ? "production" : "deployment";
    }

    public static function get_server_name(){
        return DEBUG ? "medev:7890" : "medelice.com";
    }
}