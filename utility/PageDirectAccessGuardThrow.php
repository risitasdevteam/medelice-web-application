<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 28/08/2018
 * Time: 16:57
 */
require_once(__DIR__."/../exceptions/MedeliceException.php");
require_once(__DIR__."/../exceptions/PageDirectAccessException.php");
require_once(__DIR__."/../utility.inc.php");

try{
    if (!defined("GUARD")){
        throw new PageDirectAccessException("");
    }
}
catch(Exception $e){
    redirect("/index.php?page=home");
    exit;
}
