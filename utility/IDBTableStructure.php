<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/08/2018
 * Time: 20:45
 */
interface IDBTableStructure{
    static function getDatabase();
    static function getColumns();
    static function getTable();
}
