<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/08/2018
 * Time: 03:12
 */
require_once(__DIR__."/utility/PageDirectAccessGuard.php");
include(__DIR__.'/includes/data_handler.php');

try{
    switch ($_SERVER['REQUEST_METHOD']){
        case 'GET':
            $page = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, "page", FILTER_SANITIZE_STRING, true);
            break;
        case 'POST':
            $page = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "page", FILTER_SANITIZE_STRING, true);
            break;

        default:
            throw new MedeliceException("Wrong method");
            break;
    }

    switch ($page){
        case "connexion":
            include(__DIR__."/customer/auth/REST_connect.php");
            break;

        case "confirmation-mdp-oublie":
            include(__DIR__ . '/customer/account/forgotten_password/REST_confirmation-mot-de-passe-oublie.php');
            break;

        case "envoi-lien-mdp-oublie":
            include(__DIR__.'/customer/account/forgotten_password/REST_send_forgotten_password_url.php');
            break;

        case "inscription":
            include(__DIR__.'/customer/auth/REST_register.php');
            break;

        case "existence-compte":
            include(__DIR__.'/customer/auth/REST_username_check.php');
            break;

        case "ajout-panier":
            include(__DIR__.'/customer/purchase/REST_ajouter-au-panier.php');
            break;

        case "suppression-panier":
            include(__DIR__.'/customer/purchase/REST_supprimer-du-panier.php');
            break;

        case "confirmation-compte":
            include_once(__DIR__.'/customer/auth/REST_verify.php');
            break;

        case "contact-client":
            include_once(__DIR__.'/customer/contact/REST_send_contact_mail.php');
            break;

        case "verfication-connexion":
            include_once(__DIR__.'/customer/auth/REST_is_connected.php');
            break;

        case "recuperation-adresses":
            include_once(__DIR__."/customer/shipping/REST_get_addresses.php");
            break;

        case "passer-commande":
            include_once(__DIR__."/customer/purchase/passer-commande/REST_passer-commande.php");
            break;

        case "poll-3dsecure":
            include_once(__DIR__."/customer/purchase/passer-commande/REST_3d-secure-handler.php");
            break;

        case "commande-annulee":
            include(__DIR__."/customer/purchase/passer-commande/REST_order_cancelled.php");
            break;

        case "edition-adresse":
            include(__DIR__."/customer/account/REST_my_addresses_edit.php");
            break;

        case "ajout-adresse":
            include(__DIR__ . "/customer/account/REST_my_addresses_add.php");
            break;

        case "suppression-adresse":
            include(__DIR__."/customer/account/REST_my_addresses_remove.php");
            break;

        case "generation-sitemap":
            include(__DIR__."/php-sitemap-generator-master/REST_generate_sitemap.php");
            break;

        default:
            throw new MedeliceException("Wrong method");
            break;
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}