<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/08/2017
 * Time: 00:32
 */
require_once(__DIR__.'/exceptions/BadRequestMethodException.php');
require_once(__DIR__.'/utility/JsonResponse.class.php');
require_once(__DIR__.'/utility/JsonResponseStatusType.enum.php');
require_once(__DIR__.'/initializer.inc.php');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\PHPMailerException;

require_once('vendor/autoload.php');

function range_array_by_value($array, $condition){
    $matrix = array();
    foreach ($array as $data_id => $data_value){
        if ($data_value == $condition){
            $matrix[$data_id] = $data_value;
        }
    }

    return $matrix;
}

function br(){
    return '<br>';
}

function redirect_to_error_page($error_code){
    if (!is_int($error_code)){
        throw new MedeliceException("error code must be an integer");
    }

    //http_response_code($error_code);
    redirect("/index.php?page=erreur&code=".strval($error_code));
}


/**
 * @param Exception $e
 * @param null $redirection
 * @throws MedeliceException
 */
function exception_to_json_payload(Exception $e, $redirection = null)
{
    $traits = class_uses_deep($e);
    $response = new JsonResponse(JsonResponseStatusType::ERROR);

    if (DEV){
        $response->add_payload(new JsonResponsePayload("trace", $e->getTraceAsString()));
        $response->add_payload(new JsonResponsePayload("error_msg", $e->getMessage()));
    }

    if (isset($traits['ShowableException']))
    {
        $response->add_payload(new JsonResponsePayload("error", $e->getMessage()));
    }
    else if (isset($traits['ConfidentialException']))
    {
        $response->add_payload(new JsonResponsePayload("error",  "Veuillez vérifier que les données saisies soient correctes et uniques, une erreur a été généré.")); //confidential error, must send an email to dev, "Don't panic" returned by default.
        //technical_mail($e);
    }
    else{
        if ($e instanceof MedeliceException)
        { //we might have forgotten to implement a specific trait or just mean there's no reason to create a specific exception
            $response->add_payload(new JsonResponsePayload("error", $e->getMessage()));
            if ($redirection != null){
                //redirect("/index.php?page=".$redirection);
            }else{
                //redirect_to_error_page(400);
            }
        }
        else
        {
            $response->add_payload(new JsonResponsePayload("error", $e->getMessage()));
            //technical_mail($e);
        }
    }
    $response->transmit();
}

/**
 * Throw a BadRequestMethodException if server request method's not equals to a POST.
 * @throws BadRequestMethodException
 */
function post_thrown(){
    if ($_SERVER['REQUEST_METHOD'] != 'POST'){
        http_response_code(405);
        throw new BadRequestMethodException("It's not working like that...");
    }
}

/**
 * Throw a BadRequestMethodException if server request method's not equals to a GET.
 * @throws BadRequestMethodException
 */
function get_thrown(){
    if ($_SERVER['REQUEST_METHOD'] != 'GET'){
        http_response_code(405);
        throw new BadRequestMethodException("It's not working like that...");
    }
}

function console($data) {
    $output = $data;
    if (is_array( $output )) {
        $output = implode(',', $output);
    }

    echo $data;
    echo "<script>alert( 'Debug Objects: " . $output . "' );</script>";
}

function getSMTP($mail){
    return explode("@", $mail)[1];
}

function is_a_fake_mail($mail){
    $fp = file('fakemails.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $mail = getSMTP($mail);

    return $fp && in_array($mail, $fp);
}

/**
 * Fonction mère pour tous les envois de mails.
 * @param $to
 * @param $subject
 * @param $msg
 */
function custom_mail($to, $subject, $msg, $from_dev = false, $altbody = "", $is_html = false){
    $mail = new PHPMailer(true);
    try {
        $mail->isSMTP();
        $mail->Host = 'ssl0.ovh.net';
        $mail->SMTPAuth = true;
        $mail->Username = ($from_dev ? 'devteam@medelice.com' : 'noreply@medelice.com');
        $mail->Password = ($from_dev ? 'Dj74xwpMazj87!' : 'Ad54x83hdX2&br');
        $mail->SMTPSecure = 'tls';
        $mail->Port = ($from_dev ? 587 : 0);

        $mail->setFrom(($from_dev ? 'devteam@medelice.com' : 'noreply@medelice.com'), 'Médélice');
        $mail->addAddress($to);

        $mail->CharSet = 'UTF-8';
        $mail->isHTML($is_html);
        $mail->Subject = $subject;
        $mail->Body    = $msg;
        $mail->AltBody = ($is_html ? $altbody : $msg);

        $mail->send();
        return true;
    } catch (Exception $e) {
        echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    }
}

/**
 * Mail de vérification lors de la création du compte.
 * @param $to
 * @param $surname
 * @param $link
 */
function verificationMail($to, $surname, $link){
    $subject = 'Medelice - Verification du compte';
    $message = 'Bonjour '.$surname.',\nNous vous remercions de votre inscription, voici le lien : '.$link;

    custom_mail($to, $subject, $message);
}

/**
 * Envoi d'un message de rapport d'erreur(s) vers les développeurs.
 * @param PHPMailerException $e
 */
function technical_mail(\Exception $e){
    $msg = 'Médélice - Houston we have a problem'."\r\n".
        'Location : '.$e->getFile() ."\r\n".
        'Line : '.$e->getLine()."\r\n".
        'Message : '.$e->getMessage() ."\r\n".
        'Trace : '.$e->getTraceAsString()."\r\n".
        'Occurred at : ' .time()."\r\n";

    foreach (DEV_MAILS as $mail){
        custom_mail($mail, "Médélice - Houston we have a problem", $msg, true);
    }
}

/**
 * Redirect by js and php by the way.
 * @param $page
 */
function redirect($page){
    if (!headers_sent()) {
        header('Location: ' . $page);
    }else{
        echo '<script>document.location.href="'.$page.'"</script>';
    }
}

/**
 * Send a notification with js.
 * @param $type
 * @param $message
 */
function showNotification($type, $message){
    echo '<script>printNotification("'.$type.'","'.$message.'");</script>';
}

function swap(&$a, &$b){
    $t = $a;
    $a = $b;
    $b = $t;
}

function partition(&$array, $low, $high){
    $pivot = $array[$high];
    $i = ($low - 1);

    for ($j = $low; $j <= $high - 1; $j++){
        if ($array[$j] <= $pivot){
            $i++;
            swap($array[$i], $array[$j]);
        }
    }
    swap($array[$i + 1], $array[$high]);
    return ($i + 1);
}

function quick_sort(&$array, $low, $high){
    if ($low < $high){
        $pi = partition($array, $low, $high);
        quick_sort($array, $low, ($pi - 1));
        quick_sort($array, ($pi + 1), $high);
    }
}

function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}


function get_countries_zipcode_regex(){
  $ZIPREG=array(
      "US"=>"^\\d{5}([\\-]?\\d{4})?$",
      "UK"=>"^(GIR|[A-Z]\\d[A-Z\\d]??|[A-Z]{2}\\d[A-Z\\d]??)[ ]??(\\d[A-Z]{2})$",
      "DE"=>"\\b((?:0[1-46-9]\\d{3})|(?:[1-357-9]\\d{4})|(?:[4][0-24-9]\\d{3})|(?:[6][013-9]\\d{3}))\\b",
      "CA"=>"^([ABCEGHJKLMNPRSTVXY]\\d[ABCEGHJKLMNPRSTVWXYZ])\\ {0,1}(\\d[ABCEGHJKLMNPRSTVWXYZ]\\d)$",
      "FR"=>"^(F-)?((2[A|B])|[0-9]{2})[0-9]{3}$",
      "IT"=>"^(V-|I-)?[0-9]{5}$",
      "AU"=>"^(0[289][0-9]{2})|([1345689][0-9]{3})|(2[0-8][0-9]{2})|(290[0-9])|(291[0-4])|(7[0-4][0-9]{2})|(7[8-9][0-9]{2})$",
      "NL"=>"^[1-9][0-9]{3}\\s?([a-zA-Z]{2})?$",
      "ES"=>"^([1-9]{2}|[0-9][1-9]|[1-9][0-9])[0-9]{3}$",
      "DK"=>"^([D|d][K|k]( |-))?[1-9]{1}[0-9]{3}$",
      "SE"=>"^(s-|S-){0,1}[0-9]{3}\\s?[0-9]{2}$",
      "BE"=>"^[1-9]{1}[0-9]{3}$",
      "IN"=>"^\\d{6}$"
  );
  return $ZIPREG;
}

function get_countries(){
    $countries = array(
      /*"AF" => "Afghanistan",
      "AL" => "Albania",
      "DZ" => "Algeria",
      "AS" => "American Samoa",
      "AD" => "Andorra",
      "AO" => "Angola",
      "AI" => "Anguilla",
      "AQ" => "Antarctica",
      "AG" => "Antigua and Barbuda",
      "AR" => "Argentina",
      "AM" => "Armenia",
      "AW" => "Aruba",
      "AU" => "Australia",
      "AT" => "Austria",
      "AZ" => "Azerbaijan",
      "BS" => "Bahamas",
      "BH" => "Bahrain",
      "BD" => "Bangladesh",
      "BB" => "Barbados",
      "BY" => "Belarus",
      "BE" => "Belgium",
      "BZ" => "Belize",
      "BJ" => "Benin",
      "BM" => "Bermuda",
      "BT" => "Bhutan",
      "BO" => "Bolivia",
      "BA" => "Bosnia and Herzegovina",
      "BW" => "Botswana",
      "BV" => "Bouvet Island",
      "BR" => "Brazil",
      "IO" => "British Indian Ocean Territory",
      "BN" => "Brunei Darussalam",
      "BG" => "Bulgaria",
      "BF" => "Burkina Faso",
      "BI" => "Burundi",
      "KH" => "Cambodia",
      "CM" => "Cameroon",
      "CA" => "Canada",
      "CV" => "Cape Verde",
      "KY" => "Cayman Islands",
      "CF" => "Central African Republic",
      "TD" => "Chad",
      "CL" => "Chile",
      "CN" => "China",
      "CX" => "Christmas Island",
      "CC" => "Cocos (Keeling) Islands",
      "CO" => "Colombia",
      "KM" => "Comoros",
      "CG" => "Congo",
      "CD" => "Congo, the Democratic Republic of the",
      "CK" => "Cook Islands",
      "CR" => "Costa Rica",
      "CI" => "Cote D'Ivoire",
      "HR" => "Croatia",
      "CU" => "Cuba",
      "CY" => "Cyprus",
      "CZ" => "Czech Republic",
      "DK" => "Denmark",
      "DJ" => "Djibouti",
      "DM" => "Dominica",
      "DO" => "Dominican Republic",
      "EC" => "Ecuador",
      "EG" => "Egypt",
      "SV" => "El Salvador",
      "GQ" => "Equatorial Guinea",
      "ER" => "Eritrea",
      "EE" => "Estonia",
      "ET" => "Ethiopia",
      "FK" => "Falkland Islands (Malvinas)",
      "FO" => "Faroe Islands",
      "FJ" => "Fiji",
      "FI" => "Finland",*/
      "FR" => "France",
      /*"GF" => "French Guiana",
      "PF" => "French Polynesia",
      "TF" => "French Southern Territories",
      "GA" => "Gabon",
      "GM" => "Gambia",
      "GE" => "Georgia",
      "DE" => "Germany",
      "GH" => "Ghana",
      "GI" => "Gibraltar",
      "GR" => "Greece",
      "GL" => "Greenland",
      "GD" => "Grenada",
      "GP" => "Guadeloupe",
      "GU" => "Guam",
      "GT" => "Guatemala",
      "GN" => "Guinea",
      "GW" => "Guinea-Bissau",
      "GY" => "Guyana",
      "HT" => "Haiti",
      "HM" => "Heard Island and Mcdonald Islands",
      "VA" => "Holy See (Vatican City State)",
      "HN" => "Honduras",
      "HK" => "Hong Kong",
      "HU" => "Hungary",
      "IS" => "Iceland",
      "IN" => "India",
      "ID" => "Indonesia",
      "IR" => "Iran, Islamic Republic of",
      "IQ" => "Iraq",
      "IE" => "Ireland",
      "IL" => "Israel",
      "IT" => "Italy",
      "JM" => "Jamaica",
      "JP" => "Japan",
      "JO" => "Jordan",
      "KZ" => "Kazakhstan",
      "KE" => "Kenya",
      "KI" => "Kiribati",
      "KP" => "Korea, Democratic People's Republic of",
      "KR" => "Korea, Republic of",
      "KW" => "Kuwait",
      "KG" => "Kyrgyzstan",
      "LA" => "Lao People's Democratic Republic",
      "LV" => "Latvia",
      "LB" => "Lebanon",
      "LS" => "Lesotho",
      "LR" => "Liberia",
      "LY" => "Libyan Arab Jamahiriya",
      "LI" => "Liechtenstein",
      "LT" => "Lithuania",
      "LU" => "Luxembourg",
      "MO" => "Macao",
      "MK" => "Macedonia, the Former Yugoslav Republic of",
      "MG" => "Madagascar",
      "MW" => "Malawi",
      "MY" => "Malaysia",
      "MV" => "Maldives",
      "ML" => "Mali",
      "MT" => "Malta",
      "MH" => "Marshall Islands",
      "MQ" => "Martinique",
      "MR" => "Mauritania",
      "MU" => "Mauritius",
      "YT" => "Mayotte",
      "MX" => "Mexico",
      "FM" => "Micronesia, Federated States of",
      "MD" => "Moldova, Republic of",
      "MC" => "Monaco",
      "MN" => "Mongolia",
      "MS" => "Montserrat",
      "MA" => "Morocco",
      "MZ" => "Mozambique",
      "MM" => "Myanmar",
      "NA" => "Namibia",
      "NR" => "Nauru",
      "NP" => "Nepal",
      "NL" => "Netherlands",
      "AN" => "Netherlands Antilles",
      "NC" => "New Caledonia",
      "NZ" => "New Zealand",
      "NI" => "Nicaragua",
      "NE" => "Niger",
      "NG" => "Nigeria",
      "NU" => "Niue",
      "NF" => "Norfolk Island",
      "MP" => "Northern Mariana Islands",
      "NO" => "Norway",
      "OM" => "Oman",
      "PK" => "Pakistan",
      "PW" => "Palau",
      "PS" => "Palestinian Territory, Occupied",
      "PA" => "Panama",
      "PG" => "Papua New Guinea",
      "PY" => "Paraguay",
      "PE" => "Peru",
      "PH" => "Philippines",
      "PN" => "Pitcairn",
      "PL" => "Poland",
      "PT" => "Portugal",
      "PR" => "Puerto Rico",
      "QA" => "Qatar",
      "RE" => "Reunion",
      "RO" => "Romania",
      "RU" => "Russian Federation",
      "RW" => "Rwanda",
      "SH" => "Saint Helena",
      "KN" => "Saint Kitts and Nevis",
      "LC" => "Saint Lucia",
      "PM" => "Saint Pierre and Miquelon",
      "VC" => "Saint Vincent and the Grenadines",
      "WS" => "Samoa",
      "SM" => "San Marino",
      "ST" => "Sao Tome and Principe",
      "SA" => "Saudi Arabia",
      "SN" => "Senegal",
      "CS" => "Serbia and Montenegro",
      "SC" => "Seychelles",
      "SL" => "Sierra Leone",
      "SG" => "Singapore",
      "SK" => "Slovakia",
      "SI" => "Slovenia",
      "SB" => "Solomon Islands",
      "SO" => "Somalia",
      "ZA" => "South Africa",
      "GS" => "South Georgia and the South Sandwich Islands",
      "ES" => "Spain",
      "LK" => "Sri Lanka",
      "SD" => "Sudan",
      "SR" => "Suriname",
      "SJ" => "Svalbard and Jan Mayen",
      "SZ" => "Swaziland",
      "SE" => "Sweden",
      "CH" => "Switzerland",
      "SY" => "Syrian Arab Republic",
      "TW" => "Taiwan, Province of China",
      "TJ" => "Tajikistan",
      "TZ" => "Tanzania, United Republic of",
      "TH" => "Thailand",
      "TL" => "Timor-Leste",
      "TG" => "Togo",
      "TK" => "Tokelau",
      "TO" => "Tonga",
      "TT" => "Trinidad and Tobago",
      "TN" => "Tunisia",
      "TR" => "Turkey",
      "TM" => "Turkmenistan",
      "TC" => "Turks and Caicos Islands",
      "TV" => "Tuvalu",
      "UG" => "Uganda",
      "UA" => "Ukraine",
      "AE" => "United Arab Emirates",
      "GB" => "United Kingdom",
      "US" => "United States",
      "UM" => "United States Minor Outlying Islands",
      "UY" => "Uruguay",
      "UZ" => "Uzbekistan",
      "VU" => "Vanuatu",
      "VE" => "Venezuela",
      "VN" => "Viet Nam",
      "VG" => "Virgin Islands, British",
      "VI" => "Virgin Islands, U.s.",
      "WF" => "Wallis and Futuna",
      "EH" => "Western Sahara",
      "YE" => "Yemen",
      "ZM" => "Zambia",
      "ZW" => "Zimbabwe"*/
    );
    return $countries;
}

function country_exist($country){
  $country = ucfirst(strtolower($country));
  $countries = get_countries();
  foreach ($countries as $key){
    if ($country == $key){
        return $country;
    }
  }
  return false;
}

function erase_special_char($str){
  $spec_chars = array('!', '#', '$', '%', '&', '*', '+', '-', '=', '?', '^', '_', '`', '{', '|', '}', '~', '@', '.', '[', ']', '<', '>');
  for ($i = 0; $i < count($str); $i++){
    for ($si = 0; $si < count($spec_chars); $si++){
        if ($str[$i] == $spec_chars[$si]){
          $str[$i] = "";
        }
    }
  }
  return $str;
}

function post($url, $headers = [], $postParameters = [])
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_POST => true,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => true,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 120,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_POST => true,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_HEADER => false,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_POSTFIELDS => http_build_query($postParameters)
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
}

function get($url, $headers = [])
{
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_URL => $url,
        CURLOPT_HEADER => false,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_CONNECTTIMEOUT => 120,
        CURLOPT_TIMEOUT        => 120,
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    return $result;
}

/**
 * Get local timestamp.
 * @return int
 */
function getLocalTimestamp(){
    return time() + (3600 * (localtime()[8] == 1 ? 2 : 1));
}

function force2UsedDefinedArgs($args){
    foreach ($_POST as $post_value => $post_name){
        if (!in_array($post_value, $args)){
            throw new DataBadException('Les données entrées sont incorrectes.');
        }
    }
}

function class_uses_deep($class, $autoload = true) {
    $traits = [];
    do {
        $traits = array_merge(class_uses($class, $autoload), $traits);
    } while($class = get_parent_class($class));
    foreach ($traits as $trait => $same) {
        $traits = array_merge(class_uses($trait, $autoload), $traits);
    }
    return array_unique($traits);
}

function toBool($var) {
    if (!is_string($var)) return (bool) $var;
    //var_dump($var);
    switch (strtolower($var)) {
        case '1':
        case 'true':
        case 'on':
        case 'yes':
        case 'y':
            return true;
        default:
            return false;
    }
}

function toStripePrice($price)
{
    return $price * 100; //stripe uses money lowest unity, cents here
}

class ObjectUtil{

    /**
     * Class casting
     *
     * @param string|object $destination
     * @param object $sourceObject
     * @return object
     */
    public static function cast($destination, $sourceObject)
    {
        if (is_string($destination)) {
            $destination = new $destination();
        }
        $sourceReflection = new ReflectionObject($sourceObject);
        $destinationReflection = new ReflectionObject($destination);
        $sourceProperties = $sourceReflection->getProperties();
        foreach ($sourceProperties as $sourceProperty) {
            $sourceProperty->setAccessible(true);
            $name = $sourceProperty->getName();
            $value = $sourceProperty->getValue($sourceObject);
            if ($destinationReflection->hasProperty($name)) {
                $propDest = $destinationReflection->getProperty($name);
                $propDest->setAccessible(true);
                $propDest->setValue($destination,$value);
            } else {
                $destination->$name = $value;
            }
        }
        return $destination;
    }

}