<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 30/08/2018
 * Time: 11:08
 */

?>

<html lang="fr">
    <head>
        <title>Médélice - Maintenance</title>

        <meta charset="UTF-8">
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1, width=device-width" />

        <link rel="icon" type="image/png" href="/images/favicon-medelice.png">
        <link rel="apple-touch-icon" href="/images/apple-favicon.png">
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">

        <style>
            *
            {
                margin: 0;
                padding: 0;
            }
        </style>
    </head>
    <body>
        <div style="width: 100%; height: 100%; display: flex; align-items: center; justify-content: center;">
            <div style="text-align: center">
                <img src="/images/logo.jpg"><br>
                <div style="font-family: Montserrat, sans-serif; font-weight: bold; font-size: 18pt; margin-top: 2em;">Le site est actuellement en maintenance, <br>veuillez revenir plus tard !</div>
            </div>
        </div>
    </body>
</html>
