<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 24/11/2017
 * Time: 20:33
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
?>


<div id="topsection" style="display: inline-block">
    <div class="medelice-miss-404-container">
        <!--div class="medelice-miss-404"></div-->
        <img src="/images/medelice_404.png" class="medelice-miss-404">
    </div>
    <div class="medelice-404-text-container">
        <span class="medelice-404-text-a poiret bold">Oups... <span class="bromello" style="color: #8c7748; font-weight: normal">Vous êtes vous perdu ?</span></span><br>
        <span class="medelice-404-text-b quicksand">Nous sommes désolés, <br><span style="margin-left: 10%">cette page n'existe pas.</span></span>
    </div>
</div>