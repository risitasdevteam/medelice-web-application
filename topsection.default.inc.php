<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/10/2017
 * Time: 18:13
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");

echo '
		<div id="topsection">
	        <div id="topsection-texttop">
	            <div class="poiret bold" style="float: right; margin-right: 2%">
                    <div style="font-size: 1.9vw; text-align: justify; text-align-last: justify;">DE L\'ÉPICERIE FINE RÉGIONALE</div>
                    <div style="font-size: 1.4vw; text-align: justify; text-align-last: justify;">en valisettes prêtes à offrir ou au détail</div>
                </div>
	            <div class="bromello colored-font" style="float: right; font-size: 3vw; margin-right: 2%">
	                le meilleur
                </div>
            </div>
            <div id="topsection-imgzone">
                <!-- ici mettre une image avec width: 100% et height: auto ; mettre une img de background à topsection-imgzone (et la modifier en fonction de la région) -->
                <img style="width: 100%; height: auto" src="/images/photo_banniere.jpg"/>
            </div>
            <div style="min-height: 1.8vw"></div>

            <div id="topsection-textbottom">
                <span id="topsection-textbottom-span">
';
                switch ($current_page){
                    case 'produits':
                        $name = Regions::getRegionNameFromId($current_region_id);
                        echo '<span class="colored-font">'.$name.'</span>';
                        break;
                    default:
                        break;
                }
echo '
                </span>
            </div>
		</div>
	';