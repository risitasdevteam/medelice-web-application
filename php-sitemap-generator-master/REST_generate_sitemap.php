<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/09/2018
 * Time: 17:27
 */

require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__."/MedeliceSitemapGenerator.php");
require_once(__DIR__."/../includes/data_handler.php");

try{
    post_thrown();
    $key = RegexUtil::filter_in_var_with_regex(INPUT_POST, "key", "/e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855/");

    MedeliceSitemapGenerator::generate();
}
catch(Exception $e){
    exception_to_json_payload($e);
}