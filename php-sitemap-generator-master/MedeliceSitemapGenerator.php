<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/09/2018
 * Time: 16:42
 */
include 'SitemapGenerator.php';
require_once(__DIR__."/../produits/Product.php");
require_once(__DIR__."/../produits/Valisette.php");

class MedeliceSitemapGenerator{
    public static function generate(){
        $site = "https://medelice.com";
        $sitemap = new SitemapGenerator($site);
        $sitemap->createGZipFile = true;
        $sitemap->maxURLsPerSitemap = 10000;
        $sitemap->sitemapFileName = "sitemap.xml";
        $sitemap->sitemapIndexFileName = "sitemap-index.xml";

        $product_provider = new DBProductProvider();
        $products_ref = $product_provider->get_all_refs();
        $count_products_ref = count($products_ref);
        for ($i = 0; $i < $count_products_ref; $i++){
            $sitemap->addUrl($site.'/produit/'.$products_ref[$i], null, 'weekly', '0.7');
        }

        $products_regions_and_categories = $product_provider->get_regions_and_categories();
        foreach ($products_regions_and_categories as $region_id => $category){
                $sitemap->addUrl($site.'/produits/'.$region_id."/".$category, null, 'weekly', '0.9');
        }

        $valisettes_ref = Valisette::get_all_refs();
        $count_valisettes_ref = count($valisettes_ref);
        for ($i = 0; $i < $count_valisettes_ref; $i++){
            $sitemap->addUrl($site.'/valisettes/'.$valisettes_ref[$i], null, "weekly", "0.9");
        }

        $sitemap->addUrl($site."/maintenance", date('c'),  'yearly',    '0.1');
        $sitemap->addUrl($site."/boutique", date('c'), 'monthly', '0.2');
        $sitemap->addUrl($site."/accueil", date("c"), "weekly", '1');
        $sitemap->addUrl($site."/about", date("c"), "monthly", "0.2");

        try {
            $sitemap->createSitemap();
            $sitemap->writeSitemap();
        }
        catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }

    }
}