<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 28/08/2017
 * Time: 17:46
 */

//require_once('initializer.inc.php');
require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/utility/Option.php');
require_once(__DIR__.'/customer/misc/UserUtil.php');

$option_provider = new DBOptionProvider();
$title = $option_provider->get('website_title');

echo '<title>'.$title->getValue().'</title>';

$maintenance = boolval($option_provider->get('maintenance')->getValue());
if ($maintenance && !UserUtil::isLogged()){
    redirect('/maintenance');
}

?>

<!--META-->
<meta charset="UTF-8">
<meta name="description" content="">
<meta name="viewport" content="initial-scale=1, width=device-width" />


<link rel="icon" type="image/png" href="/images/favicon-medelice.png">
<link rel="apple-touch-icon" href="/images/apple-favicon.png">

<link rel="stylesheet" type="text/css" href="/css/style.css">
<link rel="stylesheet" type="text/css" href="/css/fontawesome.min.css">
<link rel="stylesheet" type="text/css" href="/css/new_fonts.css">
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Lobster|Roboto:300,400,600,700">
<link rel="stylesheet" type="text/css" href="/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="/slick/slick-theme.css"/>
<link rel="stylesheet" type="text/css" href="/css/notyf.min.css"/>
<link rel="stylesheet" type="text/css" href="/css/spinner_wave.css"/>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/modernizr.custom.js"></script>
<script type="text/javascript" src="/js/classie.js"></script>
<script type="text/javascript" src="/js/notificationFx.js"></script>
<script type="text/javascript" src="/js/loginFx.js"></script>
<script type="text/javascript" src="/customer/auth/connect.js"></script>
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js"></script>
<script type="text/javascript" src="/js/search.js"></script>
<script type="text/javascript" src="/slick/slick.js"></script>
<script type="text/javascript" src="/js/notyf.min.js"></script>

<script src="/dashboard/js/RESTResponseProvider.js"></script>
<script type="text/javascript" src="/js/NotificationFactory.js"></script>
<script type="text/javascript" src="/js/Notification.js"></script>

<script type="text/javascript" src="/js/medelice.js"></script>