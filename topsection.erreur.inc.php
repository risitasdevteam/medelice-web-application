<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 12/07/2018
 * Time: 20:35
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");

    if(isset($_GET['code']))
    {
        $code = $_GET['code'];
    }
    else
    {
        $code = -1;
    }

?>

<div id="topsection" style="display: inline-block">
    <div class="medelice-miss-404-container">
        <img src="/images/medelice_404.png" class="medelice-miss-404">
    </div>
    <div class="medelice-404-text-container">
        <span class="medelice-404-text-a poiret bold">Oups... <span class="bromello" style="color: #8c7748; font-weight: normal"><?php echo ($code == 403 ? 'Accès refusé' : 'Une erreur est survenue'); ?></span></span><br>
        <span class="medelice-404-text-b quicksand">Si cette erreur se répète,<br><span style="margin-left: 42%">contactez-nous.</span></span>
        <div class="quicksand" style="font-size: 2vw; font-style: italic; margin: 7vw 18% 0 0; text-align: right">Code d'erreur : <?php echo $code; ?></div>
    </div>
</div>
