<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/10/2017
 * Time: 12:38
 */
class ProductBoxesJson
{
    public $boxes = array();

    public function __construct(...$box_ref){
        foreach ($box_ref as $ref){
            array_push($this->boxes, $ref);
        }
    }

    public function encode(){
        return json_encode(get_object_vars($this));
    }
}