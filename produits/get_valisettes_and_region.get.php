<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/07/2018
 * Time: 23:11
 */
require_once(__DIR__.'/../utility.inc.php');
require_once(__DIR__.'/../produits/Regions.class.php');
require_once(__DIR__.'/../produits/Box.class.php');

try{
    get_thrown();

    $valisettes = Box::getBoxes("", "");
    $regions = Regions::getRegions();

    $x = base64_encode(serialize($valisettes));
    $y = base64_encode(serialize($regions));
    echo $x."\r\n";
    echo $y;

    $decoded_valisettes = unserialize(base64_decode($x));
    $decoded_regions = unserialize(base64_decode($y));
}
catch(Exception $e){

}