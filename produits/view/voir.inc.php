<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/08/2017
 * Time: 15:58
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../Product.php');
require_once(__DIR__ . '/../Regions.class.php');
include(__DIR__ . '/../../includes/data_handler.php');

try
{
    $ref = RegexUtil::filter_in_var_with_regex(INPUT_GET, 'ref', RegexUtil::product_ref());
    $product = (new DBProductProvider())->get($ref);
    if ($product === null){
        redirect_to_error_page(500);
        exit;
    }

    $colors = Regions::getColorsFromRegionId($product->getRegion());

    //Ajouter un nUDown
    echo '
        <script src="/js/Cart.js"></script>
        <div id="pattern-header"></div>
        <div class="product-path">
            <span class="product-path-text">
                <a href="/index.php?page=home">accueil</a> >
                <a href="/index.php?page=produits&reg='.$product->getRegion().'&cat=0"> les produits '.Regions::getRegionNameFromId($product->getRegion()).'</a> >
                <a href="/index.php?page=produits&reg='.$product->getRegion().'&cat='.$product->getCategory().'">'.ProductsCategories::get_category_name($product->getCategory()).'</a> >
                <a href="/index.php?page=produits&reg='.$product->getRegion().'&cat='.$product->getCategory().'#subcat-'.$product->getSubCategory().'">'.ProductsCategories::getSubcategoryName($product->getSubCategory()).'</a>
            </span>
        </div>
        <div id="product-display-container">
            <div class="product-image-container" style="display: flex">';

                $io_provider = new FileIOProductProvider();
                $images = $io_provider->get_images($product->getRef());
                foreach ($images as $path)
                {
                    echo '
                        <img class="fill-image" src="'.$path.'"/>
                    ';
                }

    echo '  
            </div>
            <div class="product-info-container">
                <div class="product-title-section">
                    <span class="product-title quicksand bold">'.$product->getName().'</span>
                    '.($product->getIsvegan() ? '<i class="vegan product-title-icon"></i>' : '').'
                    '.($product->getIsbio() ? '<i class="bio product-title-icon"></i>' : '').'
                    '.(CartUtil::is_alcohol($product->getRef()) ? '<i class="alcool product-title-icon"></i>' : '').'
                </div>
                <div class="product-description-container">';
                    echo $product->getQuantityValue() . ' ' . $product->getQuantityUnity() .'<br>';
                    echo $product->getBrand().'<br><br>';
                    echo $product->getDescription();
                    echo '<br><br> Disponibilité : '.($product->getStock() > 10 ? 'en stock' : ($product->getStock() <= 0 ? 'épuisé' : $product->getStock().' restant(s) !'));
                    if (CartUtil::is_alcohol($product->getRef())){
                        echo '<br><br><i>L\'abus d\'alcool est dangereux pour la santé, à consommer avec modération.</i>';
                    }
    echo '      </div>
                <div class="product-buy-container">
                    <span class="product-buy-price bold colored-font">'.$product->getPrice().'€ <span style="vertical-align: super; font-size: 0.95vw;">TTC</span></span>
                    <br>
                    <span class="product-buy-ref bold">Réf : '.$product->getRef().'</span>
                    <br>
                    <br>
                    <form class="product-buy-form" id="add2cart" method="post">
                        <input type="hidden" name="ref" value="'.$product->getRef().'">
                        <button type="submit" class="product-addtocart-btn colored-bg">
                            <div class="product-addtocart-btn-icon">
    ';
                                    include(__DIR__ . '/../../images/logo_icon.svg.php'); //<img src="/images/logo_icon.svg.php" style="width: 10%; height: auto"/>

    echo '          
                            </div>
                            <span class="product-addtocart-btn-text quicksand">Ajouter au panier</span>
                        </button>
                        <div class="product-selector">
                            <input onmouseover="this.style.borderColor=\''.$colors[0].'\'" onmouseout="this.style.borderColor=\'gray\'" id="quantity" type="number" name="quant" value="1" min="1"">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    ';

    echo '
        <script>
            $("#add2cart").on("submit", function(e)
            {
                e.preventDefault();

                var ref = $("[name=\'ref\']").val();
                var quantity = $("[name=\'quant\']").val();

                CartUtil.insert(ref, quantity);
            });

            $("#quantity").on("keyup keydown change", function(e)
                {
                    if (this.value < 1 || this.value > 10000){
                        this.value = 1;
                    }
                }
            );
            
            $(\'#pattern-header\').css(
                {
                    "background-image":"url(\'/images/regions/patterns/'.$product->getRegion().'.png\')"
                }
            );

            $(\'.product-image-container\').slick({
                accessibility: true,
                arrows: true,
                dots: true,
                slidesToShow: 1,
                swipe: true,
                swipeToSlide: true,
                adaptativeHeight: true
            });
        </script>
    ';
}
catch(Exception $e){
    //exception_to_json_payload($e);
}
