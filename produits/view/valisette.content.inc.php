<?php
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../Valisette.php');
require_once(__DIR__ . '/../Product.php');

try
{
    if($_current_product != null)
    {
        $box = $_current_product;

        $products = $box->get_products_for_display();

        if (count($products) == 0){
            //redirect_to_error_page(500);
            //exit;
        }

        $n = 0;

        foreach ($products as $product_ref => $product_colors) // $product_colors as array
        {
            $product = (new DBProductProvider)->get($product_ref);
            if ($product === null){
                throw new MedeliceException("The specified product doesn't exists");
            }

            $io_provider = new FileIOProductProvider();
            $first_image = $io_provider->get_images($product->getRef())[0];


            echo '
                <a href="/index.php?page=voir&ref='.$product->getRef().'" class="valisette-product" style="float: ' . ($n % 2 == 0 ? 'left' : 'right') . '">
                    <div class="valisette-product-vignette">
                        <img class="fill-image" src="' . $first_image . '">
                    </div>
                    <div class="valisette-product-content">
                        <div class="valisette-product-text quicksand">
                            <div style="width: 100%; height: auto">
                                <span style="width: 100%">' . $product->getName() . '</span><br>
                                <div style="height: 0.7vw"></div>
                                <span style="width: 100%">' . $product->getBrand(). ', ' . $product->getQuantityValue() . ' ' . $product->getQuantityUnity() . '</span>
                            </div>
                        </div>
                        <div class="valisette-markers-container">';

                            foreach ($product_colors as $color)
                            {
                                echo '<div class="valisette-marker" style="background-color: #' . $color . '"></div>';
                            }

            echo ' 
                        </div>
                    </div>
                </a>
            ';

            $n++;
        }
    }
}
catch(Exception $e)
{
    technical_mail($e);
}