<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 31/10/2017
 * Time: 14:37
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../Regions.class.php');
require_once(__DIR__ . '/../ProductsCategories.class.php');

echo '
		<div id="topsection" style="min-height: 100%">
	        <div id="topsection-texttop">
	            <div class="poiret bold" style="float: right; margin-right: 2%">
                    <div style="font-size: 1.9vw; text-align: justify; text-align-last: justify;">DE L\'ÉPICERIE FINE RÉGIONALE</div>
                    <div style="font-size: 1.4vw; text-align: justify; text-align-last: justify;">en valisettes prêtes à offrir ou au détail</div>
                </div>
	            <div class="bromello colored-font" style="float: right; font-size: 3vw; margin-right: 2%">
	                le meilleur
                </div>
            </div>
            <div id="topsection-imgzone">
';
                include(__DIR__ . '/voir.inc.php');
echo '
            </div>
		</div>
';
