<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/10/2017
 * Time: 18:13
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../Regions.class.php');
?>

<div id="topsection">
    <div id="topsection-texttop">
        <div class="poiret bold" style="float: right; margin-right: 2%">
            <div style="font-size: 1.9vw; text-align: justify; text-align-last: justify;">DE L'ÉPICERIE FINE RÉGIONALE</div>
            <div style="font-size: 1.4vw; text-align: justify; text-align-last: justify;">en valisettes prêtes à offrir ou au détail</div>
        </div>
        <div class="bromello colored-font" style="float: right; font-size: 3vw; margin-right: 2%">
            le meilleur
        </div>
    </div>
    <div id="topsection-imgzone" style="margin-top: 1vw">
        <div id="topsection-val-bg" class="valisette-header-bg">
            <img src="/images/valisettes/<?php echo $current_region_id ?>.png"/>
            <div class="valisette-header-text">
                <div>
                    <span class="bromello valisette-header-text-valisette">La Valisette</span><br>
                    <span class="colored-font valisette-header-text-region"><?php echo $region_name = Regions::getRegionNameFromId($current_region_id) ?></span>
                </div>
            </div>
        </div>
        <div style="height: 4.5vw"></div>
        <div class="valisette-ourselection-container">
            <span class="poiret valisette-ourselection-text">notre sélection <?php echo $region_name ?></span>
        </div>
        <div style="height: 3vw"></div>
        <div class="valisette-content">
                    <?php include("produits/view/valisette.content.inc.php"); ?>
        </div>
    </div>
</div>
<script>
    $("#topsection-val-bg").css(
        {
            "background-image":'linear-gradient(to left, rgba(255, 255, 255, 1) 0%, rgba(255, 255, 255, 1) 20%, rgba(0, 0, 0, 0) 100%), url("/images/regions/patterns/<?php echo $current_region_id ?>.png")'
        }
    );
</script>
