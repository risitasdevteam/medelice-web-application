<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 01/10/2017
 * Time: 20:06
 */

require_once(__DIR__ . '/../Regions.class.php');
require_once(__DIR__ . '/../ProductsCategories.class.php');

echo '
    <hr>
    <div class="filter-box">
        <br>
        <span class="filter-box-title">'.L::products_filters.'</span>
        <br><br><br>
        <div class="filter-box-sections">
            <b class="filter-category">'.L::products_f_regions.'</b>
            <div class="checkboxes-container">
';

$regions = Regions::getRegions();

for($i = 0; $i < count($regions); $i++)
{
    $id = $regions[$i][0];
    $name = $regions[$i][1];
    echo '<div class="filter-checkbox-acc">';
    echo '<input type="checkbox" id="Reg'.$id.'" name="RegCheckbox'.$id.'" class="custom-checkbox"/><label for="Reg'.$id.'">'.$name.'</label>';
    echo '</div>';
}

echo '      
            </div>
            <br>
            <b class="filter-category">'.L::products_f_types.'</b><br>
            <div class="checkboxes-container">
';

$categories = ProductsCategories::get_all_categories_as_array();

for($i = 0; $i < count($categories); $i++)
{
    echo '
            <div class="filter-checkbox-acc">
                <input type="checkbox" id="Cat'.$i.'" name="CatCheckbox'.$i.'" class="custom-checkbox"/><label for="Cat'.$i.'">'.$categories[$i].'</label>
            </div>
    ';
}

echo '
            </div>
            <br>
            <button class="filters-button" onclick="location.href = applyFilters()">'.L::products_apply_filters.'</button>
        </div>
    </div>';