<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/08/2017
 * Time: 18:49
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../Product.php');
require_once(__DIR__ . '/../ProductsCategories.class.php');

try {
    echo '<script src="/js/Cart.js"></script>';
    
    $provider = new DBProductProvider();
    $products = $provider->get_by_region_and_category($current_region_id, $current_category); //$current_category is a global var from header.inc

    echo '
        <div class="product-listing-container">
            <div class="product-list-box">
            <b class="category-name">'.ProductsCategories::get_category_name($current_category).'</b><br>
    ';

    $count_products = count($products);
    $i = 0;
    if($count_products < 1)
    {
        echo '<div class="no-products">Aucun produit trouvé pour la catégorie sélectionnée.</div>';
    }
    else
    {
        $last_sub_category_inserted = "";
        $i = 0; 
        $n_products_in_sub_category = 0;
        foreach ($products as $product){
            if (!$product instanceof Product){
                break;
            }
            
            if ($last_sub_category_inserted !== $product->getSubCategory()){ //insert sub category container
              if ($i != 0){
                echo '
                        </div>
                    </div>
                    <br>
                ';

                $n_products_in_sub_category = 1;
              }else{
                $n_products_in_sub_category++;
              }

                echo '
                    <div class="subcat-container"">
                        <span class="subcat-name colored-font quicksand bold">'.$product->getSubCategory().'</span><br>
                        <div class="subcat-products-container">
                ';
                $last_sub_category_inserted = $product->getSubCategory();
            }else{
              $n_products_in_sub_category++;
            }

            $product_io_provider = new FileIOProductProvider();
            $image = $product_io_provider->get_images($product->getRef())[0];

            echo '
                <div class="product-box" style="float: ' . ($count_products > 1 ? ($n_products_in_sub_category % 2 == 1 ? 'left' : 'right') : 'left') . '">
                    <a class="button-link product-preview" href="/produit/'. $product->getRef() .'">
                        <img class="fill-image" src="' . $image . '" />
                    </a>
                    <div class="product-text quicksand">
                        <a class="product-name quicksand bold" href="/produit/'. $product->getRef() .'">' . $product->getName() . '</a><br>
                        <i>' . $product->getBrand() . '</i><br>
                        <span>' . $product->getQuantityValue() . ' ' . $product->getQuantityUnity() .'</span><br>
                        <br>
                        <span>' . ($product->getStock() > 10 ? 'En stock' : ($product->getStock() <= 0 ? 'Épuisé' : $product->getStock(). ' restants !')) .'</span><br>
                        <div class="product-icons" style="position: absolute; bottom: 40%; width: 100%; display: inline-block; right: 20px;">
                            '.($product->getIsvegan() ? '<i class="vegan right"></i>' : '').'
                            '.($product->getIsbio() ? '<i class="bio right" style="margin-right: 0.5vw;"></i>' : '').'
                            '.(CartUtil::is_alcohol($product->getRef()) ? '<i class="alcool right" style="margin-right: 0.5vw"></i>' : '').'
                        </div>
                        <span class="product-price colored-font abril">' . $product->getPrice() . '€ <span style="vertical-align: super; font-size: 0.95vw;">TTC</span></span>
                        <form class="product-addtocart-section">
                            <button type="submit" class="product-addtocart-btn colored-bg">
                                <div class="product-addtocart-btn-icon">
                ';
                                    include(__DIR__ . '/../../images/logo_icon.svg.php'); //Add to cart icon !
    echo '
                                </div>
                                <span class="product-addtocart-btn-text quicksand">Ajouter au panier</span>
                            </button>
                            <input type="hidden" name="ref" value="'.$product->getRef().'">
                            <div class="product-selector">
                                <input onmouseover="this.style.borderColor=\''.$colors[0].'\'" onmouseout="this.style.borderColor=\'gray\'" id="quant-'.$i.'" type="number" name="quant" value="1" min="1" max="10000">
                            </div>
                        </form>
                    </div>
                </div>';

            $i++;
        }
    }

    echo '
            </div>
        </div>
    ';

    echo ' <script>
           function add_to_cart_handler(){
                $(".product-addtocart-section").submit(function(e){
                    e.preventDefault();

                    var ref = $(this).find(\'[name="ref"]\').val();
                    var quantity = $(this).find(\'[name="quant"]\').val();

                    CartUtil.insert(ref, quantity);
                });
           }

            function numeric_handler() {
                $("input[name=\'quant\']").on("keyup keydown change", function()
                {
                    if ($(this).val() < 1 || $(this).val() > 99999){
                        $(this).val(1);
                    }
                });
            }

            $(document).ready(function(){
                add_to_cart_handler();
                numeric_handler();
            });
        </script>';

    //TODO pagination
}
catch (Exception $e){
    exception_to_json_payload($e);
}
