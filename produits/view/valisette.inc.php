<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 04/11/2017
 * Time: 21:16
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../Valisette.php');
$valisette = $_current_product;

if($valisette == null || !$valisette instanceof Valisette) {
}

$data = $valisette->get_sub_valisettes_data_as_array(true, true, true, false);
$count = count($data["colors"] ?? array()); //can be anything
if ($count < 1){
    redirect_to_error_page(500);
    echo "Aucune sous-valisette n'a été trouvée. Veuillez revenir plus tard.";
    return;
}

$current_ref = ($valisette->ref . Valisette::SUB_VALISETTE_DELIMITER . $data["var"][0]); //why non truncated reference ?
?>
    <script src="/js/Cart.js"></script>
    <script>
        var current_ref = "<?php echo $current_ref ?>";
        var colors = [];

        <?php
            $i = 0;
            foreach($data["colors"] as $color)
            {
                echo 'colors["'.$data["var"][$i].'"] = "'.$color.'";';
                $i++;
            }
        ?>

        function selectBox(ref)
        {
            current_ref = "<?php echo $valisette->ref.Valisette::SUB_VALISETTE_DELIMITER ?>" + ref;
            document.getElementById("ref").setAttribute("value", current_ref);
            $('#subbox-indicator').css(
                {
                    "background-color": "#" + colors[ref],
                    "box-shadow":"0 0 2px #" + colors[ref]
                }
            );
        }
    </script>
    <div style="width: 100%; height: 4vw; display: flex; align-items: center"><span class="quicksand" style="width: 100%; text-align: center; font-size: 1.2vw">Vous avez la possibilité d'acheter tous ces produits à l'unité dans la rubrique <a href="/index.php?page=produits">NOS PRODUITS AU DÉTAIL</a></span></div>
    <div style="height: 1.2vw"></div>
    <div class="valisette-ourselection-container" style="height: 0.8vw">
        <span class="valisette-ourselection-text poiret" style="font-size: 1.6vw; padding: 0 4vw"><?php echo $count.' POSSIBILITÉ'.($count > 1 ? 'S' : '') ?> DE COMPOSITION</span>
    </div>
    <div style="height: 4vw"></div>
    <form class="product-addtocart-section" style="position: relative; right: 0; bottom: 0; width: 20vw; margin-left: 40.25vw" id="addbox">
        <button type="submit" class="product-addtocart-btn colored-bg">
            <span class="quicksand product-addtocart-btn-text" style="width: 100%; margin-top: 0">Ajouter au panier</span>
        </button>
        <input type="hidden" id="ref" name="ref" value="<?php echo $current_ref ?>">
        <div class="product-selector" style="margin-right: 1.5vw; width: 4.5vw">
            <input onmouseover="this.style.borderColor=<?php echo $colors[0] ?>'" onmouseout="this.style.borderColor = 'gray'" id="quantity" type="number" name="quant" value="1" min="1" max="999999">
        </div>
        <div id="subbox-indicator" style="height: 1.5vw; width: 1.5vw; border-radius: 0.75vw; background-color: <?php echo '#'.$data["colors"][0].'; box-shadow: 0 0 2px #'.$data["colors"][0] ?>; margin: 0.25vw"></div>
    </form>
    <div class="quicksand" style="width: 100%; text-align: center; font-size: 1vw">Cliquez sur un prix pour choisir quelle valisette ajouter au panier</div>
    <div style="height: 3vw"></div>
    <form class="valisette-vignettes-prices">
        <?php
                //TODO
              $vignettes_prices_inserted = false;
              for ($i = 0; $i < count($data["colors"]); $i++) {
                  $color = $data["colors"][$i];
                  $price = $data["prices"][$i];


                  $ref = $data["var"][$i];

                  echo '<div class="valisette-addtocart-btn abril" onclick="selectBox(\'' . $ref . '\')" style="background-color: #' . $color . '">
                    <span class="valisette-addtocart-btn-text noselect" style="position: relative">';
                  $new_price = explode('.', $price);
                  echo $new_price[0];
                  if (count($new_price) > 1)
                      echo '<span style="font-size: 5vw">.' . $new_price[1] . '€</span>';
                  else
                      echo '<span style="font-size: 4.5vw">€</span>';
                  echo '
                    </span>
                </div>';
              }
        ?>
    </form>


<div style="height: 3vw"></div>
<div style="width: 100%; text-align: center; font-size: 1vw">
    Vous référer aux pastilles de couleurs présentes à côté des produits<br>
    <div class="valisette-color-refs-container">
        <span style="display: flex; align-items: center; margin: 0.1vw auto 0">
            <span class="valisette-color-ref">
                <?php
                    for ($i = 0; $i < count($data["colors"]); $i++) {
                        $color = $data["colors"][$i];
                        $price = $data["prices"][$i];

                        echo '<div class="valisette-color-ref-sticker" style="background-color: #' . $color . '"></div>
                        <span class="valisette-color-ref-text">produits de la valisette à ' . $price . '€</span>';
                    }
                ?>
                </span>
        </span>
    </div>
        Tous les prix indiqués incluent la TVA
</div>
<div style="height: 3vw"></div>
<?php include(__DIR__ . '/../../medelice-bottom.inc.php');?>
<div style="height: 2vw"></div>

<script>
    $("#addbox").submit(function(e)
    {
        e.preventDefault();

        var ref = $("[name='ref']").val();
        var quantity = $("#quantity").val();

        CartUtil.insert(ref, quantity);
    });

    $("#quantity").on("keyup keydown change", function()
    {
        if (this.value < 1 || this.value > 999999){
            this.value = 1;
        }
    });
</script>
