<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 08/08/2018
 * Time: 18:51
 */

//namespace Products;

require_once(__DIR__.'/../exceptions/MedeliceException.php');
require_once(__DIR__.'/../utility/SQLUtil.php');
require_once(__DIR__.'/../utility.inc.php');
require_once(__DIR__."/Valisette.php");

abstract class DBProductsStructure{
    const columns_array = ["ref",
        "name",
        "brand",
        "quantity_unity",
        "quantity_value",
        "total_mass",
        "category",
        "sub_category",
        "region",
        "description",
        "price",
        "isbio",
        "isvegan",
        "vat_type",
        "stock"];

    const table = 'products';
}

abstract class ElasticSearchProductsStructure{
    const index = 'products';
    const type = 'product';

    const meaningful_attributes_array = ["ref",
        "name",
        "brand",
        "description",
        "price"];
}

interface ProductIOProvider{
    public function get_images($ref);
    public function upload_images($ref);
}

class FileIOProductProvider implements ProductIOProvider{

    public function get_images($ref)
    {
        $images = array();
        $relative_path = $_SERVER['DOCUMENT_ROOT'];
        $images_path = '/images/produits';
        $scan = scandir($relative_path . $images_path);
        if (count($scan) > 0) {
            foreach ($scan as $file_name) {
                if (!is_dir($file_name)) {
                    $exploded_file_name = strrpos($file_name, "-") !== false ? explode("-", $file_name) : explode(".", $file_name);
                    if (isset($exploded_file_name[0])) {
                        $reference_from_file_name = $exploded_file_name[0];

                        if (strcmp($reference_from_file_name, $ref) == 0) {
                            array_push($images, $images_path . DIRECTORY_SEPARATOR . $file_name);
                        }
                    }
                }
            }
        }

        if (count($images) === 0){
            array_push($images, $images_path."/default_product.png");
        }

        return $images;
    }

    public function upload_images($ref)
    {
        $files_global_name = 'products_upload_images';

        if (count($_FILES[$files_global_name]) < 1){
            throw new MedeliceException("No file were uploaded");
        }

        $upload_path = $_SERVER["DOCUMENT_ROOT"]."/images/produits/";
        foreach ($_FILES[$files_global_name]["error"] as $key => $error){
            if ($error !== UPLOAD_ERR_OK) {
                throw new MedeliceException("Upload failed with error code " . $error);
            }

            $info = getimagesize($_FILES[$files_global_name]['tmp_name'][$key]);
            if ($info === false) {
                throw new MedeliceException("Unable to determine image type of uploaded file");
            }

            if (($info[2] !== IMAGETYPE_GIF) && ($info[2] !== IMAGETYPE_JPEG) && ($info[2] !== IMAGETYPE_PNG)) {
                throw new MedeliceException("Not a gif/jpeg/png");
            }

            $file_extension = explode(".", $_FILES[$files_global_name]["name"][$key])[1];
            $file_name = $ref;

            $iterator = 0;
            while (file_exists($upload_path.basename($file_name.".".$file_extension))){
                if ($iterator == 0){
                    $file_name .= "-";
                }

                $file_name = substr($file_name, 0, (strlen($file_name) - ($iterator > 0 ? 1 : 0))).strval($iterator);
                $iterator++;
            }

            $full_upload_path = $upload_path.basename($file_name.".".$file_extension);

            if (!move_uploaded_file($_FILES[$files_global_name]["tmp_name"][$key], $full_upload_path)){
                throw new MedeliceException("File with ".$key. " key was not uploaded due to an error");
            }
        }

        return true;
    }

}

interface ProductProvider{
    /**
     * @param string $ref
     * @return mixed
     */
    public function get(string $ref);

    public function get_all();

    /**
     * @param string $region
     * @param int $category
     * @return mixed
     */
    public function get_by_region_and_category(string $region, int $category);

    /**
     * @param string $region
     * @return mixed
     */
    public function get_by_region(string $region);

    /**
     * @param $product
     * @return mixed
     */
    public function insert(Product $product);

    /**
     * @param string $spreadsheet_path
     * @return mixed
     */
    public function insert_from_spreadsheet(string $spreadsheet_path);

    /**
     * @param string $ref
     * @return mixed
     */
    public function remove(string $ref);

    /**
     * @param string $old_ref
     * @param Product $product
     * @return mixed
     */
    public function edit(string $old_ref, Product $product);

    public function get_stock_gaps(int $minimum = 5);

    public function get_regions_where_products_exists();

    public function decrement_stock_for_specific_order(array $cart_content_array);
    public function get_all_refs();
    public function get_regions_and_categories();
}

class DBProductProvider implements ProductProvider{

    /**
     * @param $ref
     * @return Product
     * @throws DatabaseConnectionException
     */
    public function get(string $ref)
    {
        $product = new Product();
        $product->setRef($ref);

        $sql = SQLUtil::open("medelice_products");
        $table = DBProductsStructure::table;

        $prepared_query = "select p.name,
            p.quantity_unity,
              p.quantity_value,
                p.total_mass,
                  p.category,
                    p.sub_category,
                      p.region,
                        p.description,
                          p.price,
                            p.isbio,
                              p.isvegan,
                                b.brand,
                                  p.vat_type,
                                  p.stock
            from " . $table . " p
            join products_brands b where p.ref = ? and p.brand = b.id";

        $found = false;
        if ($prepared_statement = $sql->prepare($prepared_query)){
            $prepared_statement->bind_param("s", $ref);
            $prepared_statement->execute();
            $prepared_statement->bind_result($name, $quantity_unity, $quantity_value, $total_mass, $category, $sub_category, $region, $description, $price, $is_bio, $is_vegan, $brand, $vat, $stock);


            while ($row = $prepared_statement->fetch()){
                $product->setName($name);
                $product->setQuantityUnity($quantity_unity);
                $product->setQuantityValue($quantity_value);
                $product->setTotalMass($total_mass);
                $product->setCategory($category);
                $product->setSubCategory($sub_category);
                $product->setRegion($region);
                $product->setDescription($description);
                $product->setPrice($price);
                $product->setIsbio($is_bio);
                $product->setIsvegan($is_vegan);
                $product->setBrand($brand);
                $product->setVatType($vat);
                $product->setStock($stock);

                $found = true;
            }

            $prepared_statement->close();
        }

        SQLUtil::close($sql);

        return ($found ? $product : null);
    }


    /**
     * @param array $columns
     * @return array
     * @throws DatabaseInvalidQueryException
     * @throws DatabaseRequestException
     */
    private static function get_from_specific_columns(array $columns)
    {
        $raw_products = SQLUtil::select(DBProductsStructure::table,
            ['*'],
            $columns);

        $products = array();
        if (count($raw_products) > 0){
            foreach ($raw_products as $raw_product){
                $product = Product::from_array($raw_product);
                array_push($products, $product);
            }
        }

        return $products;
    }

    public function get_all(){
        $products = array();

        $sql = SQLUtil::open("medelice_products");
        $table = DBProductsStructure::table;

        $prepared_query = "select p.ref,
          p.name,
            p.quantity_unity,
              p.quantity_value,
                p.total_mass,
                   sc.subc_name,
                      p.region,
                        p.description,
                          p.price,
                            p.isbio,
                              p.isvegan,
                                b.brand,
                                  p.vat_type,
                                  p.stock
            from ".$table." p
            join products_brands b on p.brand = b.id
            join products_sub_categories sc on sc.c_id = p.category and sc.subc_id = p.sub_category
            order by subc_name";

        if ($prepared_statement = $sql->prepare($prepared_query)){
            $prepared_statement->execute();
            $prepared_statement->bind_result($ref, $name, $quantity_unity, $quantity_value, $total_mass, $sub_category, $region, $description, $price, $is_bio, $is_vegan, $brand, $vat, $stock);


            while ($row = $prepared_statement->fetch()){
                $product = new Product();
                $product->setRef($ref);
                $product->setName($name);
                $product->setQuantityUnity($quantity_unity);
                $product->setQuantityValue($quantity_value);
                $product->setTotalMass($total_mass);
                $product->setSubCategory($sub_category);
                $product->setRegion($region);
                $product->setDescription($description);
                $product->setPrice($price);
                $product->setIsbio($is_bio);
                $product->setIsvegan($is_vegan);
                $product->setBrand($brand);
                $product->setVatType($vat);
                $product->setStock($stock);

                array_push($products, $product);
            }

            $prepared_statement->close();
        }

        SQLUtil::close($sql);

        return $products;
    }

    public function get_all_refs(){
        $refs = array();

        $raw = SQLUtil::select(DBProductsStructure::table, ["ref"], []);
        $count = count($raw);
        if ($count > 0){
            for ($i = 0; $i < $count; $i++){
                $pointer = $raw[$i]["ref"];
                array_push($refs, $pointer);
            }
        }

        return $refs;
    }

    public function get_regions_and_categories(){
        $regions_and_categories = array();
        $sql = SQLUtil::open("medelice_products");
        $query = "select distinct region, category from ".DBProductsStructure::table;

        if ($stmt = $sql->prepare($query)){
            $stmt->bind_result($region, $category);
            $stmt->execute();

            while ($stmt->fetch()){
                $regions_and_categories[$region] = $category;
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $regions_and_categories;
    }

    /**
     * @param string $region
     * @param int $category
     * @return array
     */
    public function get_by_region_and_category(string $region, int $category)
    {
        $products = array();

        $sql = SQLUtil::open("medelice_products");
        $table = DBProductsStructure::table;

        $prepared_query = "select p.ref,
          p.name,
            p.quantity_unity,
              p.quantity_value,
                p.total_mass,
                   sc.subc_name,
                      p.region,
                        p.description,
                          p.price,
                            p.isbio,
                              p.isvegan,
                                b.brand,
                                  p.vat_type,
                                  p.stock
            from ".$table." p
            join products_brands b on p.brand = b.id
            join products_sub_categories sc on sc.c_id = p.category and sc.subc_id = p.sub_category
            where p.region = ? and p.category = ? order by subc_name";

        if ($prepared_statement = $sql->prepare($prepared_query)){
            $prepared_statement->bind_param("ss", $region, $category);
            $prepared_statement->execute();
            $prepared_statement->bind_result($ref, $name, $quantity_unity, $quantity_value, $total_mass, $sub_category, $region, $description, $price, $is_bio, $is_vegan, $brand, $vat, $stock);


            while ($row = $prepared_statement->fetch()){
                $product = new Product();
                $product->setRef($ref);
                $product->setName($name);
                $product->setQuantityUnity($quantity_unity);
                $product->setQuantityValue($quantity_value);
                $product->setTotalMass($total_mass);
                $product->setSubCategory($sub_category);
                $product->setRegion($region);
                $product->setDescription($description);
                $product->setPrice($price);
                $product->setIsbio($is_bio);
                $product->setIsvegan($is_vegan);
                $product->setBrand($brand);
                $product->setVatType($vat);
                $product->setStock($stock);

                array_push($products, $product);
            }

            $prepared_statement->close();
        }

        SQLUtil::close($sql);

        return $products;
    }

    /**
     * @param string $region
     * @return array
     */
    public function get_by_region(string $region)
    {
        $region_column = array_values(DBProductsStructure::columns_array)[8];

        return self::get_from_specific_columns(array_combine(array($region_column), array($region)));
    }

    /**
     * @param $product
     * @return bool
     * @throws DatabaseInvalidQueryException
     * @throws DatabaseRequestException
     * @throws MedeliceException
     */
    public function insert(Product $product)
    {
        SQLUtil::insert(DBProductsStructure::table, array_combine(DBProductsStructure::columns_array, (array)$product));

        $indexer = new ElasticSearchProductIndexer();
        $indexer->index($product);

        return true;
    }

    /**
     * @param string $spreadsheet_path
     * @return int
     * @throws DataBadException
     */
    public function insert_from_spreadsheet(string $spreadsheet_path)
    {
        $reader = new SpreadsheetReader($spreadsheet_path);
        $iterator = 0;
        foreach ($reader as $row)
        {
            if ($iterator == 0){
                if (count(array_intersect_key($row, array_flip(DBProductsStructure::table))) !== count(DBProductsStructure::table)) {
                    throw new DataBadException("false arrays intersection");
                }
            }
            else {
                $product_as_array = array_combine(DBProductsStructure::table, $row);
                $product_as_obj = Product::from_array($product_as_array);

                self::insert($product_as_obj);
            }
            $iterator++;
        }

        return $iterator;
    }

    /**
     * @param string $ref
     * @return bool
     * @throws DatabaseInvalidQueryException
     * @throws DatabaseRequestException
     */
    public function remove(string $ref)
    {
        $column = array_values(DBProductsStructure::columns_array)[0];
        SQLUtil::delete(DBProductsStructure::table,
            [
                $column => $ref
            ]
        );

        $indexer = new ElasticSearchProductIndexer();
        $indexer->unindex($ref);

        return true;
    }

    /**
     * @param string $old_ref
     * @param Product $product
     * @return bool
     * @throws DataBadException
     * @throws DatabaseInvalidQueryException
     * @throws DatabaseRequestException
     */
    public function edit(string $old_ref, Product $product)
    {
        $old_product = $this->get($old_ref);

        $sets = Product::array_difference($old_product, $product);
        $ref_column = array_values(DBProductsStructure::columns_array)[0];
        SQLUtil::update(DBProductsStructure::table,
            $sets,
            [
                $ref_column => $old_ref
            ]
        );

        if ($old_ref !== $product->getRef()) {
            SQLUtil::update(Valisette::SUB_VALISETTES_CONTENT_TABLE,
                [$ref_column => $product->getRef()],
                [$ref_column => $old_ref]);
        }

        $edit_elastic_indexation = false;
        foreach (ElasticSearchProductsStructure::meaningful_attributes_array as $column){
            if (array_key_exists($column, $sets)){
                $edit_elastic_indexation = true;
            }
        }

        if ($edit_elastic_indexation) {
            $indexer = new ElasticSearchProductIndexer();
            $indexer->update($old_ref, $product);
        }

        return true;
    }

    public function get_stock_gaps(int $minimum = 5)
    {
        $sql = SQLUtil::open("medelice_products");
        $query = "select ref, stock from ".DBProductsStructure::table." where stock <= ? order by stock";

        $gaps = array();
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("d", $minimum);
            $stmt->bind_result($ref, $stock);
            $stmt->execute();

            if ($stmt->error !== ''){
                $stmt->close();
                SQLUtil::close($sql);

                throw new MedeliceException("Stock gaps bug");
            }

            while ($stmt->fetch()){
                array_push($gaps, array("ref" => $ref, "stock" => $stock));
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $gaps;
    }

    public function get_regions_where_products_exists()
    {
        $regions = array();
        $sql = SQLUtil::open("medelice_products");
        $query = 'select distinct p.region, r.region_name, r.hex, r.neg_hex from '.DBProductsStructure::table.' p join products_regions r where p.region = r.id';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_result($region_id, $region_name, $region_hex, $region_neg_hex);
            $stmt->execute();

            if ($stmt->error !== ''){
                $stmt->close();
                SQLUtil::close($sql);

                throw new MedeliceException("Can't retrieve region where products exists");
            }

            while ($stmt->fetch()){
                array_push($regions, array("id" => $region_id, "name" => $region_name, "hex" => $region_hex, "neg_hex" => $region_neg_hex));
            }

            $stmt->close();
        }

        SQLUtil::close($sql);
        return $regions;
    }

    public function decrement_stock_for_specific_order(array $cart_content_array)
    {
        foreach ($cart_content_array as $ref => $quantity){
            if (CartUtil::is_a_valisette($ref)){
                $b = new Valisette($ref);
                if (!$b->exists){
                    throw new MedeliceException('Box doesn\'t exists');
                }

                $valisette_id = explode(Valisette::SUB_VALISETTE_DELIMITER, $ref)[1];
                $sub_valisette = $b->get_specific_sub_valisette_from_id($valisette_id);
                if (!$sub_valisette instanceof SubValisette){
                    throw new MedeliceException("");
                }

                foreach ($sub_valisette->products as $sub_ref){
                    $product = $this->get($sub_ref);
                    $product->setStock(($product->getStock() - $quantity));

                    $this->edit($sub_ref, $product);
                }
            }else{
                $product = $this->get($ref);
                $product->setStock(($product->getStock() - $quantity));

                $this->edit($ref, $product);
            }
        }
    }
}

interface ProductIndexer{
    public function index(Product $product);
    public function update(string $old_ref, Product $product);
    public function unindex(string $ref);
    public function to_elasticsearch_array(Product $product);
}

class ElasticSearchProductIndexer implements ProductIndexer{

    public function index(Product $product)
    {
        /*$client = ClientBuilder::create()->build();

        $params = [
            'index' => 'products',
            'type' => 'product',
            'id' => $product->getRef(),
            'body' => self::to_elasticsearch_array($product)
        ];

        return $client->index($params);*/
        return true;
    }

    public function update(string $old_ref, Product $product)
    {
        /*$client = ClientBuilder::create()->build();

        $old_product = (new DBProductProvider())->get($old_ref);

        $params = [
            'index' => 'products',
            'type' => 'product',
            'id' => $old_product->getRef(),
            'body' => ['doc' => Product::array_difference($old_product, $product, true)],
            'refresh' => true
        ];

        return $client->update($params);*/
        return true;
    }

    public function unindex(string $ref)
    {
        // TODO: Implement unindex() method.
    }

    public function to_elasticsearch_array(Product $product){
        $ref = $product->getRef();
        $product_as_array = (array)$product;
        $merged_array = array_merge(array_intersect_key($product_as_array, array_flip(ElasticsearchProductsStructure::meaningful_attributes_array)));
        $merged_array["img"] = (new FileIOProductProvider())->get_images($ref)[0];
        return $merged_array;
    }

}

class Product
{
    //TODO public -> private
    public $ref;
    public $name;
    public $brand;
    public $quantity_unity;
    public $quantity_value;
    public $total_mass;
    public $category;
    public $sub_category;
    public $region;
    public $description;
    public $price;
    public $isbio;
    public $isvegan;
    public $vat_type;
    public $stock;

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getQuantityUnity()
    {
        return $this->quantity_unity;
    }

    /**
     * @param mixed $quantity_unity
     */
    public function setQuantityUnity($quantity_unity)
    {
        $this->quantity_unity = $quantity_unity;
    }

    /**
     * @return mixed
     */
    public function getQuantityValue()
    {
        return $this->quantity_value;
    }

    /**
     * @param mixed $quantity_value
     */
    public function setQuantityValue($quantity_value)
    {
        $this->quantity_value = $quantity_value;
    }

    /**
     * @return mixed
     */
    public function getTotalMass()
    {
        return $this->total_mass;
    }

    /**
     * @param mixed $total_mass
     */
    public function setTotalMass($total_mass)
    {
        $this->total_mass = $total_mass;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getSubCategory()
    {
        return $this->sub_category;
    }

    /**
     * @param mixed $sub_category
     */
    public function setSubCategory($sub_category)
    {
        $this->sub_category = $sub_category;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getIsbio()
    {
        return $this->isbio;
    }

    /**
     * @param mixed $isbio
     */
    public function setIsbio($isbio)
    {
        $this->isbio = boolval($isbio);
    }

    /**
     * @return mixed
     */
    public function getIsvegan()
    {
        return $this->isvegan;
    }

    /**
     * @param mixed $isvegan
     */
    public function setIsvegan($isvegan)
    {
        $this->isvegan = boolval($isvegan);
    }

    /**
     * @return mixed
     */
    public function getVatType()
    {
        return $this->vat_type;
    }

    /**
     * @param mixed $vat_type
     */
    public function setVatType($vat_type)
    {
        $this->vat_type = $vat_type;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }


    public static function from_array(array $product_array){
        $product = new Product();

        $mapped_numerically_columns_array = array_values(DBProductsStructure::columns_array);
        $get_column = function($index) use ($product_array, $mapped_numerically_columns_array){
            return $product_array[$mapped_numerically_columns_array[$index]];
        };

        $product->setRef($get_column(0));
        $product->setName($get_column(1));
        $product->setBrand($get_column(2));
        $product->setQuantityUnity($get_column(3));
        $product->setQuantityValue($get_column(4));
        $product->setTotalMass($get_column(5));
        $product->setCategory($get_column(6));
        $product->setSubCategory($get_column(7));
        $product->setRegion($get_column(8));
        $product->setDescription($get_column(9));
        $product->setPrice($get_column(10));
        $product->setIsbio($get_column(11));
        $product->setIsvegan($get_column(12));
        $product->setVatType($get_column(13));
        $product->setStock($get_column(14));

        return $product;
    }

    public function to_array(){
        return array_merge(DBProductsStructure::columns_array,
            $this->getRef(),
            $this->getName(),
            $this->getBrand(),
            $this->getQuantityUnity(),
            $this->getQuantityValue(),
            $this->getTotalMass(),
            $this->getCategory(),
            $this->getSubCategory(),
            $this->getRegion(),
            $this->getDescription(),
            $this->getPrice(),
            $this->getIsbio(),
            $this->getIsvegan(),
            $this->getStock());
    }

    public static function array_difference(Product $product1, Product $product2, $for_elastic_search = false){
        $product1_matrix = (!$for_elastic_search ? (array)$product1 : (new ElasticSearchProductIndexer())->to_elasticsearch_array($product1));
        $product2_matrix = (!$for_elastic_search ? (array)$product2 : (new ElasticSearchProductIndexer())->to_elasticsearch_array($product2));

        $sql_update_sets_matrix = [];

        foreach($product1_matrix as $key => $value){
            $product2_matrix_value = $product2_matrix[$key];

            if ($value != $product2_matrix_value){
                $sql_update_sets_matrix[$key] = $product2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }
}