<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/07/2018
 * Time: 18:16
 */
require_once(__DIR__.'/../exceptions/data/DataException.php');
require_once(__DIR__.'/../exceptions/data/DataBadException.php');

class SubValisette
{
    public $id;
    public $title;
    public $color;
    public $products; //array of SubValisetteProduct
    public $price;

    public static $COLUMNS = ["id", "title", "color", "products", "price"];

    public function __construct($id, $title, $color, $products, $price)
    {
        $this->id = $id;
        $this->title = $title;
        $this->color = $color;

        if (!is_array($products)) {
            try {
                $decoded_array = json_decode($products, true)[0][self::$COLUMNS[3]];
                if (is_array($decoded_array)) {
                    $this->products = $decoded_array;
                }
            } catch (Exception $e) {
            }
        }
        else
            $this->products = $products;

        $this->price = $price;
    }

    public function to_array(){
        return get_object_vars($this);
    }

    public static function from_array($array){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        if (count(array_intersect_key($array, array_flip(self::$COLUMNS))) != count(self::$COLUMNS)) {
            throw new DataBadException("false arrays intersection");
        }

        //fuzzy way to cast
        $sub_valisette = new SubValisette(
            $array[self::$COLUMNS[0]],
            $array[self::$COLUMNS[1]],
            $array[self::$COLUMNS[2]],
            $array[self::$COLUMNS[3]],
            $array[self::$COLUMNS[4]]
        );

        return $sub_valisette;
    }
}