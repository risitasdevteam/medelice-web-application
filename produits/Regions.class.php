<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 16/09/2017
 * Time: 17:37
 */
require_once(__DIR__.'/../utility/SQLUtil.php');
require_once(__DIR__.'/../exceptions/DatabaseException.php');
require_once(__DIR__.'/../exceptions/DatabaseInvalidQueryException.php');

class Regions
{
    //column "departements" is deprecated
    public static $COLUMNS = array("id", "region_name", "departements", "hex", "neg_hex");

    /**
     * Récupération de l'identifiant et du nom de toutes les régions dans un concaténation d'array.
     * @return array
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function getRegions()
    {
        $sql = SQLUtil::open("medelice_products");

        $sql->set_charset('utf8');
        $query = 'select id,region_name from '.PRODUCTS_REGIONS_TABLE;
        $results = array();
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_result($id, $region_name);
            $stmt->execute();

            while ($stmt->fetch()){
                $region = array();
                $region[0] = $id;
                $region[1] = $region_name;

                array_push($results, $region);
            }

            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }
        return $results;
    }

    /**
     * Récupérer le nom d'une région en fonction de son identifiant.
     * @param $id L'identifiant régional, representé sous 3 lettres ABC.
     * @return null|string null si la region n'existe pas
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function getRegionNameFromId($id)
    {
        $sql = SQLUtil::open("medelice_products");
        $sql->set_charset("utf8");
        $query = 'select region_name from '.PRODUCTS_REGIONS_TABLE.' where id = ?';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param('s', $id);
            $stmt->bind_result($name);
            $stmt->execute();

            while ($stmt->fetch()){
                $stmt->close();
                SQLUtil::close($sql);
                return $name;
            }
        }else{
            throw new DatabaseInvalidQueryException('');
        }
        return null;
    }

    /**
     * Vérification de l'existence d'une région en fonction de son identifiant.
     * @param $id L'identifiant régional, representé sous 3 lettres ABC.
     * @return bool
     * @throws DatabaseInvalidQueryException
     */
    public static function exists($id)
    {
        return self::getRegionNameFromId($id) != null;
    }

    /**
     * Récupérer les couleurs (hex) d'une région en fonction de son identifiant sous forme d'un tableau.
     * @param $id L'identifiant régional, representé sous 3 lettres ABC.
     * @return array
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function getColorsFromRegionId($id)
    {
        $sql = SQLUtil::open("medelice_products");

        $colors = array();
        $query = 'select hex,neg_hex from '.PRODUCTS_REGIONS_TABLE.' where id = ?';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param('s', $id);
            $stmt->bind_result($hex, $neg_hex);
            $stmt->execute();

            while ($stmt->fetch()){
                $colors[0] = $hex;
                $colors[1] = $neg_hex;

                $stmt->close();
                SQLUtil::close($sql);
                return $colors;
            }
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        SQLUtil::close($sql);

        $colors[0] = self::getDefaultColor();
        $colors[1] = "FFFFFF";
        return $colors;
    }

    /**
     * Récupérer l'identifiant régional unique par défaut.
     * @return string
     */
    public static function getDefaultRegionId()
    {
        return "ARA";
    }

    /**
     * Récupérer la couleur en hexadécimal par défaut.
     * @return string
     */
    public static function getDefaultColor()
    {
        return "8c7748";
    }

    public static function add($id, $region_name, $hex, $neg_hex){
        SQLUtil::insert(PRODUCTS_REGIONS_TABLE,
            [
                self::$COLUMNS[0] => $id,
                self::$COLUMNS[1] => $region_name,
                self::$COLUMNS[3] => $hex,
                self::$COLUMNS[4] => $neg_hex
            ]);
        return true;
    }

    public static function remove($id){
        SQLUtil::delete(PRODUCTS_REGIONS_TABLE,
            [self::$COLUMNS[0] => $id]);
        return true;
    }

    public static function edit($id, $name){
        SQLUtil::update(PRODUCTS_REGIONS_TABLE,
            [self::$COLUMNS[1] => $name], [self::$COLUMNS[0] => $id]);
        return true;
    }
}
