<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 16:39
 */

class Category{
    public $id;
    public $name;

    public function __construct($id, $name){
        $this->id = $id;
        $this->name = $name;
    }
}