<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/10/2017
 * Time: 12:38
 */
class ProductRecipesJson
{
    public $recipes = array();

    public function __construct(...$recipes_id){
        foreach ($recipes_id as $id){
            array_push($recipes, $id);
        }
    }

    public function encode(){
        return json_encode(get_object_vars($this));
    }
}