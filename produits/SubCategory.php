<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 16:39
 */
interface SubCategoryProvider{
    public function get($id);
    public function add($subcategory);
    public function remove($id);
    public function update_name($subcategory);
}

class DBSubCategoryProvider implements SubCategoryProvider{

    public function get($id){
        $raw_subcategory = SQLUtil::select(PRODUCTS_SUBC_TABLE,
            ['*'],
            [SubCategory::$COLUMNS[0] => $id]);

        if (count($raw_subcategory) == 0){
            throw new MedeliceException("SubCategory doesn't exists");
        }

        $subcategory = new SubCategory();
        $subcategory->setId($id);
        $subcategory->setName($raw_subcategory[0]["subc_name"]);
        $subcategory->setParentCategoryId($raw_subcategory[0]["c_id"]);

        return $subcategory;
    }

    public function add($subcategory){
        if (!$subcategory instanceof SubCategory){
            throw new MedeliceException("subcategory must be an instanceof SubCategory obj");
        }

        SQLUtil::insert(PRODUCTS_SUBC_TABLE,
            [
                SubCategory::$COLUMNS[1] => $subcategory->getParentCategoryId(),
                SubCategory::$COLUMNS[2] => $subcategory->getName()
            ]);
        return true;
    }

    public function remove($id){
        SQLUtil::delete(PRODUCTS_SUBC_TABLE,
            [
                SubCategory::$COLUMNS[0] => $id,
            ]);
        return true;
    }

    public function update_name($subcategory){
        if (!$subcategory instanceof SubCategory){
            throw new MedeliceException("subcategory must be an instanceof SubCategory obj");
        }

        SQLUtil::update(PRODUCTS_SUBC_TABLE,
            [
                SubCategory::$COLUMNS[2] => $subcategory->getName()
            ],
            [
                SubCategory::$COLUMNS[0] => $subcategory->getId()
            ]);
        return true;
    }
}

class SubCategory{
    private $id;
    private $parent_category_id;
    private $name;

    public static $COLUMNS = array("subc_id", "c_id", "subc_name");

    public function __construct(){
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getParentCategoryId()
    {
        return $this->parent_category_id;
    }

    /**
     * @param mixed $parent_category_id
     */
    public function setParentCategoryId($parent_category_id)
    {
        $this->parent_category_id = $parent_category_id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function toArray(){
        return get_object_vars($this);
    }
}