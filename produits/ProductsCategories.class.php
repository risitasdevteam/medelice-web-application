<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/08/2017
 * Time: 17:26
 */
require_once(__DIR__ . '/../initializer.inc.php');

//NEED TO BE REFURBISHED TODO
class ProductsCategories
{
    public $id;
    public $name;

    public $exists = false;

    /*public function __construct($c_id){
        $sql = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, "medelice_products");
        if ($sql->connect_error){
            die($sql->connect_error);
        }
        $this->_c_id = $sql->real_escape_string($c_id);

        $select = 'select * from '.PRODUCTS_CATEGORIES_TABLE.' where c_id = \''.$this->_c_id.'\'';
        $result = $sql->query($select);
        if ($result->num_rows > 0){
            while ($row = $result->fetch_assoc()){
                $this->_c_name = $row['c_name'];
            }
            $this->_c_exist = true;
        }else{
            $this->_c_exist = false;
        }
        $sql->close();
    }*/
    public static $COLUMNS = array("id", "name");
    public static $SUB_COLUMNS = array("subc_id", "c_id", "subc_name");

    public function __construct($id){
            $this->id = $id;

            $raw_result = SQLUtil::select(PRODUCTS_CATEGORIES_TABLE, ['*'], ['id' => $id]);
            if (count($raw_result) > 0){
                $this->name = $raw_result[0]["name"];
                $this->exists = true;
            }
    }

    public static function add($category_name){
        SQLUtil::insert(PRODUCTS_CATEGORIES_TABLE,
            [
                self::$COLUMNS[1] => $category_name
            ]);

        return true;
    }

    public static function add_sub($name, $category_id){
        SQLUtil::insert(PRODUCTS_SUBC_TABLE,
        [
            self::$SUB_COLUMNS[1] => $category_id,
            self::$SUB_COLUMNS[2] => $name
        ]);
        return true;
    }

    public static function remove($category){
        if (!$category instanceof ProductsCategories){
            throw new MedeliceException("category is not an instance of Category obj");
        }

        if (!$category->exists){
            throw new MedeliceException("Category does not exists");
        }

        SQLUtil::delete(PRODUCTS_CATEGORIES_TABLE,
            [
                self::$COLUMNS[0] => $category->id,
            ]);
        return true;
    }

    public static function remove_sub($id){
        SQLUtil::delete(PRODUCTS_SUBC_TABLE,
            [
                self::$SUB_COLUMNS[0] => $id,
            ]);
        return true;
    }

    public static function update_name($id, $name){
        SQLUtil::update(PRODUCTS_CATEGORIES_TABLE,
            [
               self::$COLUMNS[1] => $name
            ],
            [
                self::$COLUMNS[0] => $id
            ]);
        return true;
    }

    public static function update_sub_name($id, $name){
        SQLUtil::update(PRODUCTS_SUBC_TABLE,
            [
                self::$SUB_COLUMNS[2] => $name
            ],
            [
                self::$SUB_COLUMNS[0] => $id
            ]);
        return true;
    }

    public static function get_all_categories_as_array()
    {
        $categories = array();
        $raw_categories = SQLUtil::select(PRODUCTS_CATEGORIES_TABLE,
            ['*'],
            []);

        $raw_categories_count = count($raw_categories);
        if ($raw_categories_count > 0){
            for ($i = 0; $i < $raw_categories_count; $i++){
                $raw_category = $raw_categories[$i];

                $id = $raw_category["id"];
                $name = $raw_category["name"];

                $categories[$id] = $name;
            }
        }
        return $categories;
    }

    public static function get_category_name($id)
    {
        /*$sql = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, "medelice_products");
        if (mysqli_connect_error())
        {
            throw new DatabaseConnectionException(L::db_connection_error);
        }
        $sql->set_charset("utf8");

        $select = 'select c_name from '.PRODUCTS_CATEGORIES_TABLE.' where (c_id like "'.$id.'")';

        $result = $sql->query($select);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                return $row['c_name'];
            }
        }*/

        $raw_name = SQLUtil::select(PRODUCTS_CATEGORIES_TABLE, [self::$COLUMNS[1]], [self::$COLUMNS[0] => $id]);
        return count($raw_name) > 0 ? $raw_name[0]["name"] : null;
    }

    public static function getSubcategoryName($id)
    {
        $sql = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, "medelice_products");
        if (mysqli_connect_error())
        {
            throw new DatabaseConnectionException(L::db_connection_error);
        }
        $sql->set_charset("utf8");

        $select = 'select subc_name from '.PRODUCTS_SUBC_TABLE.' where (subc_id like "'.$id.'")';

        $result = $sql->query($select);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                return $row['subc_name'];
            }
        }
    }

    public static function doesExists($id)
    {
        return (new ProductsCategories($id))->exists;
    }

    public static function getSubcategoriesFromCategory($category_id)
    {
        $sql = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, "medelice_products");
        if (mysqli_connect_error())
        {
            throw new DatabaseConnectionException(L::db_connection_error);
        }
        $sql->set_charset("utf8");

        $subcategories = array();
        $select = 'select subc_id,subc_name from '.PRODUCTS_SUBC_TABLE. ' where c_id = ?';
        if ($stmt = $sql->prepare($select)){
            $stmt->bind_param('d', $category_id);
            $stmt->bind_result($subc_id, $subc_name);
            $stmt->execute();

            while($stmt->fetch()){
                $subcategories[$subc_id] = $subc_name;
            }

            $stmt->close();
        }

        return $subcategories;
    }
}
