<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/07/2018
 * Time: 18:16
 */
//use \produits\SubValisette;
require_once(__DIR__.'/SubValisette.php');
require_once(__DIR__.'/../vendor/autoload.php');

use Elasticsearch\ClientBuilder;


class ValisetteNew{
    private $ref;
    private $region;
    private $title;

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }
}

class Valisette
{
    public $ref;
    public $region;
    public $title;
    public $sub_valisettes = array();

    public $exists = false;

    const SUB_VALISETTE_DELIMITER = ':'; //will be changed in the future

    const TABLE = "products_valisettes";
    const SUB_VALISETTES_TABLE = "products_sub_valisettes";
    const SUB_VALISETTES_CONTENT_TABLE = "products_sub_valisettes_content";

    public static $COLUMNS = ["ref", "region", "title"];

    public function __construct($ref)
    {
        if ($ref != null) {
            //check if a specified "under-box" exists
            $ref_with_sub = strpos($ref, self::SUB_VALISETTE_DELIMITER);
            //$sub_ref = "";

            if ($ref_with_sub) {
                $exploded_ref = explode(self::SUB_VALISETTE_DELIMITER, $ref);
                $primary_ref = $exploded_ref[0];
                //$sub_ref = $exploded_ref[1];

                $this->ref = $primary_ref;
            } else {
                $this->ref = $ref;
            }

            $mysql = SQLUtil::open("medelice_products");
            $valisettes_prepared_query = 'SELECT c.product_ref, c.id, sv.color, sv.title, sv.price, v.region, v.title
                                          FROM products_sub_valisettes_content c
                                          JOIN products_sub_valisettes sv ON c.id=sv.id
                                          JOIN products_valisettes v ON v.ref=sv.ref and v.ref=c.ref
                                          WHERE c.ref = ?';

            if ($valisettes_prepared_statement = $mysql->prepare($valisettes_prepared_query)){
                $valisettes_prepared_statement->bind_param("s", $this->ref);
                $valisettes_prepared_statement->execute();
                $valisettes_prepared_statement->bind_result($product_ref, $sub_valisette_id, $sub_valisette_color, $sub_valisette_title, $sub_valisette_price, $valisette_region, $valisette_title);

                $iterator = 0;
                $give_me_back_the_content = function($product_ref, $sub_valisette_id, $sub_valisette_color, $sub_valisette_title, $sub_valisette_price) {

                    if (!array_key_exists($sub_valisette_id, $this->sub_valisettes)){
                        $sub_valisette = new SubValisette($sub_valisette_id, $sub_valisette_title, $sub_valisette_color, array($product_ref), $sub_valisette_price);
                        $this->sub_valisettes[$sub_valisette_id] = $sub_valisette;
                    }else{
                        $sub_valisette = $this->sub_valisettes[$sub_valisette_id];
                        if ($sub_valisette instanceof SubValisette){
                            array_push($sub_valisette->products, $product_ref);
                        }
                    }
                };

                while ($row = $valisettes_prepared_statement->fetch())
                {
                    if ($iterator == 0) {
                        $this->region = $valisette_region;
                        $this->title = $valisette_title;
                        $this->exists = true;
                    }

                    $give_me_back_the_content($product_ref, $sub_valisette_id, $sub_valisette_color, $sub_valisette_title, $sub_valisette_price);
                    $iterator++;
                }
            }

            $mysql->close();
        }
    }

    /**
     * @return mixed
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * @param mixed $ref
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return array
     */
    public function getSubValisettes()
    {
        return $this->sub_valisettes;
    }

    /**
     * @param array $sub_valisettes
     */
    public function setSubValisettes($sub_valisettes)
    {
        $this->sub_valisettes = $sub_valisettes;
    }


    public function to_array(){
        return get_object_vars($this);
    }

    public static function from_array($array, $directly_from_raw_sql = false){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        if (count(array_intersect_key($array, array_flip(self::$COLUMNS))) != count(self::$COLUMNS)) {
            throw new DataBadException("false arrays intersection");
        }

        //fuzzy way to cast
        $valisette = new Valisette(null);
        $valisette->ref = $array[self::$COLUMNS[0]];
        $valisette->region = $array[self::$COLUMNS[1]];
        $valisette->title = $array[self::$COLUMNS[2]];
        if (!$directly_from_raw_sql)
            $valisette->sub_valisettes = $array[self::$COLUMNS[2]];
        $valisette->exists = true;

        return $valisette;
    }

    public function get_products_for_display()
    {
        $products_list = array();
        foreach($this->sub_valisettes as $sub_valisette)
        {
            if ($sub_valisette instanceof SubValisette) {
                foreach ($sub_valisette->products as $product) {
                    if (!isset($products_list[$product]))
                        $products_list[$product] = array();
                    array_push($products_list[$product], $sub_valisette->color);
                }
            }
        }

        return $products_list;
    }

    /**
     * @return array
     */
    public function get_sub_valisettes_data_as_array($variables = true, $prices_array = true, $colors_array = true, $products_array = true){

        $variables_ref = array();
        $prices = array();
        $colors = array();
        $products = array();

        foreach($this->sub_valisettes as $sub_valisette)
        {
            if (!$sub_valisette instanceof SubValisette){

            }

            if ($variables)
                array_push($variables_ref, $sub_valisette->id);
            if ($colors_array)
                array_push($colors, $sub_valisette->color);
            if ($prices_array)
                array_push($prices, $sub_valisette->price);

            if ($products_array) {
                $local_products = array();
                foreach ($sub_valisette->products as $product) {
                    array_push($local_products, $product);
                }
                array_push($products, $local_products);
            }
        }

        return array("var" => $variables_ref, "prices" => $prices, "colors" => $colors, "products" => $products);
    }

    public function image($from_relative_path = false)
    {
        $relative_path = __DIR__ . '/..';
        $image_path = ($from_relative_path ? $relative_path : '').'/images/valisettes/thumbnails/'.$this->region.'.png';

        if (!file_exists(($from_relative_path ? $image_path : ($relative_path.$image_path))))
        {
            throw new MedeliceException("The preview image of this valisette doesn't exists", 500);
        }

        return $image_path;
    }

    public static function get_preview_img_from_region($region){
        $valisette = new Valisette(null);
        $valisette->region = $region;

        return $valisette->image();
    }

    public static function add($valisette){
        if (!$valisette instanceof Valisette){
            throw new DataBadException('$valisette must be an instance of Valisette object');
        }

        $valisette_array = $valisette->to_array();
        unset($valisette_array['exists']);
        unset($valisette_array['sub_valisettes']);
        //var_dump(array_combine(self::$COLUMNS, $valisette_array));
        SQLUtil::insert(self::TABLE, array_combine(self::$COLUMNS, $valisette_array));
        return true;
    }

    public function modify($new_valisette){
        if (!$new_valisette instanceof Valisette){

        }

        $sets = self::array_difference($new_valisette->to_array(), $this->to_array());

        SQLUtil::update(self::TABLE,
            $sets
            , [self::$COLUMNS[0] => $new_valisette->ref]);

        return true;
    }

    public static function array_difference($valisette1, $valisette2){
        if (!$valisette1 instanceof Valisette || !$valisette2 instanceof Valisette){
            throw new DataBadException("Valisette 1 or 2 must be Valisette objects");
        }

        $valisette1_matrix = $valisette1->to_array();
        $valisette2_matrix = $valisette2->to_array();

        $sql_update_sets_matrix = [];

        foreach($valisette1_matrix as $key => $value){
            $valisette2_matrix_value = $valisette2_matrix[$key];

            if ($value != $valisette2_matrix_value){
                $sql_update_sets_matrix[$key] = $valisette2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }

    public function get_specific_sub_valisette_from_id($var_id)
    {
        foreach($this->sub_valisettes as $sub_valisette)
        {
            if (!$sub_valisette instanceof SubValisette){

            }

            if (strcmp($var_id, $sub_valisette->id) == 0)
                return $sub_valisette;
        }
        return null;
    }

    public static function get_filled_valisettes(){
        $valisettes = array();
        $sql = SQLUtil::open("medelice_products");
        $query = 'select distinct v.ref, v.region, v.title from products_valisettes v join products_sub_valisettes sv on sv.ref = v.ref join products_sub_valisettes_content svc on svc.ref = v.ref';

        if ($stmt = $sql->prepare($query)){
            $stmt->bind_result($ref, $region, $title);
            $stmt->execute();

            while ($stmt->fetch()){
                $valisette = new Valisette(null);
                $valisette->setRef($ref);
                $valisette->setRegion($region);
                $valisette->setTitle($title);

                array_push($valisettes, $valisette);
            }

            $stmt->close();
        }

        SQLUtil::close($sql);
        return $valisettes;
    }

    public static function get_valisettes()
    {
        $raw_valisettes = SQLUtil::select(self::TABLE, ['*'], []);
        $valisettes = array();
        if (count($raw_valisettes) > 0){
            foreach ($raw_valisettes as $raw_valisette){
                array_push($valisettes, Valisette::from_array($raw_valisette, true));
            }
        }else{
            throw new MedeliceException("");
        }

        return $valisettes;
    }

    public static function get_all_refs(){
        $refs = array();

        $raw = SQLUtil::select(self::TABLE, ["ref"], []);
        $count = count($raw);
        if ($count > 0){
            for ($i = 0; $i < $count; $i++){
                $pointer = $raw[$i]["ref"];
                array_push($refs, $pointer);
            }
        }

        return $refs;
    }

    public function to_elasticsearch_array(){
        $product = $this->to_array();
        $meaningful_fields_indexes = ["ref", "sub_valisettes"];

        $merged_array = array_merge(array_intersect_key($product, array_flip($meaningful_fields_indexes)));
        $merged_array["img"] = $this->image();

        return $merged_array;
    }

    /**
     * Index the product to our elasticsearch db
     * @return array
     */
    public function index_it(){
        $client = ClientBuilder::create()->build();

        $params = [
            'index' => 'products',
            'type' => 'product',
            'id' => $this->ref,
            'body' => $this->to_elasticsearch_array()
        ];

        return $client->index($params);
    }

    public function update_indexed($new_product){
        if (!$new_product instanceof Product){
            return false; //throw TODO
        }

        $product1_matrix = $this->to_elasticsearch_array();
        $product2_matrix = $new_product->to_elasticsearch_array();

        $update_sets_matrix = [];

        foreach($product1_matrix as $key => $value){
            $product2_matrix_value = $product2_matrix[$key];

            if ($value != $product2_matrix_value){
                $update_sets_matrix[$key] = $product2_matrix_value;
            }
        }

        $client = ClientBuilder::create()->build();

        $params = [
            'index' => 'products',
            'type' => 'product',
            'id' => $this->ref,
            'body' => $update_sets_matrix
        ];

        return $client->update($params);
    }

    public function deindex(){
        $client = ClientBuilder::create()->build();

        $params = [
            'index' => 'products',
            'type' => 'product',
            'id' => $this->ref
        ];

        return $client->delete($params);
    }
}