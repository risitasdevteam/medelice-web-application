<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/08/2018
 * Time: 20:05
 */

class DatabaseRequestException extends DatabaseException
{
    public function __construct($msg, $stmt){
        parent::__construct($msg, 500);

        if ($stmt != null && $stmt instanceof mysqli_stmt){
            $stmt->close();
        }
    }
}
