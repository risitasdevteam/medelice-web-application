<?php
require_once(__DIR__.'/../MedeliceException.php');
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 27/10/2017
 * Time: 10:58
 */
abstract class ProductException extends MedeliceException
{
    use ShowableException;
}