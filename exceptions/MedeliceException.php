<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/08/2017
 * Time: 15:26
 */
require_once(__DIR__.'/ShowableException.php');
require_once(__DIR__.'/ConfidentialException.php');

class MedeliceException extends Exception
{
    public function __construct($message, $code = 0)
    {
        parent::__construct($message, $code);
        //http_response_code($code);
        //motherofhandlerexceptions
    }

    public function __toString()
    {
        return $this->message;
    }
}
