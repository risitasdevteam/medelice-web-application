<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/08/2017
 * Time: 15:08
 */
require_once(__DIR__ . '/../MedeliceException.php');

abstract class UserException extends MedeliceException
{
    use ShowableException;
}