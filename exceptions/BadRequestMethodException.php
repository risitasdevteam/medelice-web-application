<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/11/2017
 * Time: 09:24
 */
require_once(__DIR__.'/MedeliceException.php');

class BadRequestMethodException extends MedeliceException
{
    use ConfidentialException;
}