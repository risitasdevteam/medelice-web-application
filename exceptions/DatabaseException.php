<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/08/2017
 * Time: 15:55
 */
require_once(__DIR__ . '/MedeliceException.php');

abstract class DatabaseException extends MedeliceException
{
    use ConfidentialException;
}