<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 17/08/2018
 * Time: 16:53
 */
require_once(__DIR__.'/../orders/OrderStatus.enum.php');

interface ModalProvider{
    function get_current_month_balance();
    function get_current_month_sales();
    function get_last_five_minutes_cpu_overload();
}

class DBModalProvider implements ModalProvider {

    function get_current_month_balance()
    {
        $balance = 0;
        $sql = SQLUtil::open("medelice_users");
        $select = 'select coalesce(sum(total_bill),0) as total from users_orders where status != ? and status != ? and account_time >= ?';

        if ($stmt = $sql->prepare($select)){
            $undesired_status1 = OrderStatus::PENDING_3D_SECURE;
            $undesired_status2 = OrderStatus::ERROR;
            $day = date("j");
            $monthly_account_time = (time() - (86400*($day > 1 ? $day : 0)));

            $stmt->bind_param("ddd", $undesired_status1, $undesired_status2, $monthly_account_time);
            $stmt->bind_result($total);
            $stmt->execute();

            while ($stmt->fetch()){
                $balance = $total;
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $balance;
    }

    function get_current_month_sales()
    {
        $sales = 0;
        $sql = SQLUtil::open("medelice_users");
        $select = 'select count(id) from users_orders where status != ? and status != ? and account_time >= ?';

        if ($stmt = $sql->prepare($select)){
            $undesired_status1 = OrderStatus::PENDING_3D_SECURE;
            $undesired_status2 = OrderStatus::ERROR;
            $day = date("j");
            $monthly_account_time = (time() - (86400*($day > 1 ? $day : 0)));

            $stmt->bind_param("ddd", $undesired_status1, $undesired_status2, $monthly_account_time);
            $stmt->bind_result($total);
            $stmt->execute();

            while ($stmt->fetch()){
                $sales = $total;
            }

            $stmt->close();
        }
        SQLUtil::close($sql);

        return $sales;
    }

    function get_last_five_minutes_cpu_overload()
    {
        return strcmp(php_uname("s"), "Windows NT") != 0 ? round(sys_getloadavg()[0]) : 99;
    }
}