<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/08/2018
 * Time: 14:45
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/orders.php');

try{
    $order_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'order_id', RegexUtil::order_uid());
    $status = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'status', RegexUtil::order_status());
    $tracking = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "tracking", FILTER_SANITIZE_STRING, true);

    $order_provider = new DBOrderProvider();
    $order = $order_provider->get_on_unique_id($order_id);
    $order->setStatus(intval($status));
    $order->setStatusLastUpdate(time());
    $order->setTracking($tracking);

    if (!$order_provider->update($order_id, $order)){
        throw new MedeliceException("Impossible de mettre à jour le statut de la commande ".$order_id);
    }

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Statut de la commande ".$order_id." édité avec succès !"));
    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}