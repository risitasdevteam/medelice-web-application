<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 05/08/2018
 * Time: 16:15
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

$sub_page = isset($_GET['sub']) ?  $_GET['sub'] : 'en_cours';

switch($sub_page)
{
    case 'nouvelles':
        $name = "Nouvelles commandes";
        $desc = "Cette page regroupe toutes les commandes qui n'ont pas encore été prises en charge par l'équipe.";
        $function = "OrdersUtil.get_unprocessed(function(response){
                var table = $('.orders-table tbody');
                var orders = response.payload.unprepared_orders;
                if (Object.keys(orders).length > 0){

                    $.each(orders, function(index, item){
                        var id = item['id'];
                        var customer_addresses = item['customer_addresses'];
                        var billing = customer_addresses['billing'];
                        var emitter_name = billing['emitter_name'];
                        var status = item['status'];
                        var delivery_mode = item['delivery_mode'];
                        var total_bill = item['total_bill'];

                        table.append('<tr style=\"height: 2vw; background-color: #eaeaea\">'+
                        '<td style=\"padding-left: 2%;\">' + id + '</td>'+
                        '<td>' + emitter_name + '</td>'+
                        '<td>' + status + '</td>'+
                        '<td>' + delivery_mode + '</td>'+
                        '<td>88 888</td>'+
                        '<td>' + total_bill + '€</td>'+
                        '<td><a href=\"/dashboard/index.php?page=commande&ref=' + id + '\" class=\"normal-link\">détails...</a></td>'+
                    '</tr>');
                });
            }
        }, function(){

        });";
        break;

    case 'en_cours':
        $name = "Commandes en cours";
        $desc = "Cette page regroupe toutes les commandes en cours de traitement par l'équipe.";
        $function = "OrdersUtil.get_processing(function(response){
                var table = $('.orders-table tbody');
                var orders = response.payload.processing_orders;
                if (Object.keys(orders).length > 0){

                    $.each(orders, function(index, item){
                        var id = item['id'];
                        var customer_addresses = item['customer_addresses'];
                        var billing = customer_addresses['billing'];
                        var emitter_name = billing['emitter_name'];
                        var status = item['status'];
                        var delivery_mode = item['delivery_mode'];
                        var total_bill = item['total_bill'];

                        table.append('<tr style=\"height: 2vw; background-color: #eaeaea\">'+
                        '<td style=\"padding-left: 2%;\">' + id + '</td>'+
                        '<td>' + emitter_name + '</td>'+
                        '<td>' + status + '</td>'+
                        '<td>' + delivery_mode + '</td>'+
                        '<td>88 888</td>'+
                        '<td>' + total_bill + '€</td>'+
                        '<td><a href=\"/dashboard/index.php?page=commande&ref=' + id + '\" class=\"normal-link\">détails...</a></td>'+
                    '</tr>');
                });
            }
        }, function(){

        });";
        break;

    case 'terminees':
        $name = "Commandes terminées";
        $desc = "Cette page regroupe toutes les commandes qui ont été reçues par nos clients.";
        $function = "OrdersUtil.get_done(function(response){
                var table = $('.orders-table tbody');
                var orders = response.payload.done_orders;
                if (Object.keys(orders).length > 0){

                    $.each(orders, function(index, item){
                        var id = item['id'];
                        var customer_addresses = item['customer_addresses'];
                        var billing = customer_addresses['billing'];
                        var emitter_name = billing['emitter_name'];
                        var status = item['status'];
                        var delivery_mode = item['delivery_mode'];
                        var total_bill = item['total_bill'];

                        table.append('<tr style=\"height: 2vw; background-color: #eaeaea\">'+
                        '<td style=\"padding-left: 2%;\">' + id + '</td>'+
                        '<td>' + emitter_name + '</td>'+
                        '<td>' + status + '</td>'+
                        '<td>' + delivery_mode + '</td>'+
                        '<td>88 888</td>'+
                        '<td>' + total_bill + '€</td>'+
                        '<td><a href=\"/dashboard/index.php?page=commande&ref=' + id + '\" class=\"normal-link\">détails...</a></td>'+
                    '</tr>');
                });
            }
        }, function(){

        });";
        break;

    default:
        $name = "Commandes en cours";
        $desc = "Cette page regroupe toutes les commandes en cours de traitement par l'équipe.";
}

?>

<script src="/dashboard/js/Orders.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ <?php echo $name; ?></span>
</div>
<div class="orders-topmsg-container quicksand">
    <?php echo $desc; ?>
</div>
<div class="orders-container">
    <table class="orders-table roboto">
        <tr class="orders-tablerow bold" style="">
            <td class="orders-td-ref" style="width: 14%; padding-left: 2%;">Réf. commande</td>
            <td class="orders-td-client" style="width: 25%;">Client</td>
            <td class="orders-td-status" style="width: 22%;">Statut</td>
            <td class="orders-td-delmode" style="width: 12%;">Livraison</td>
            <td class="orders-td-prods" style="width: 11%;">Nb. produits</td>
            <td class="orders-td-value" style="width: 10%;">Valeur</td>
            <td class="orders-td-details" style="width: 7%;"></td>
        </tr>
    </table>
</div>

<script>
    $(document).ready(function(){
        <?php echo $function ?>
    });
</script>

