<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 25/08/2018
 * Time: 14:02
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once (__DIR__ . '/../../orders/Order.class.php');
require_once (__DIR__ . '/../../produits/Product.php');

if(!isset($_GET['ref']))
{
    echo 'Aucune commande sélectionnée';
    exit;
}

$ref = $_GET['ref'];

$provider = new DBOrderProvider();
$order = $provider->get_on_unique_id($ref);

if($order == null)
{
    echo 'La commande sélectionnée n\'existe pas.';
    exit;
}



?>

<div class="order-sum-container">
    <div class="order-sum">
        <div class="order-sum-title bold">Commande : <?php echo $order->getId() ?></div>
        <?php

            $products = CartUtil::decode_cart_from_json($order->getProducts());

            foreach($products as $ref => $quantity)
            {
                echo '
                    <ul class="list-group">
                        <li class="order-sum-entry list-group-item d-flex justify-content-between align-items-center">
                            '.$ref.'
                            <span class="order-sum-entry-qsection">
                                <i class="fa fa-minus order-sum-entry-qsection-btn"></i>
                                <span class="badge badge-primary badge-pill"><span class="amount">0</span> / <span class="max-value">'.$quantity.'</span></span>
                                <i class="fa fa-plus order-sum-entry-qsection-btn"></i>
                            </span>
                        </li>
                    </ul>
                ';
            }

        ?>
        <script>
            $('.fa-minus').click(function (event) {
                var amountSpan = $(event.target).parent().find('.amount');
                var amount = parseInt(amountSpan.text());

                if(amount > 0)
                {
                    amount--;
                    amountSpan.text(amount);
                }
            });

            $('.fa-plus').click(function (event) {
                var parent = $(event.target).parent().parent();
                var amountSpan = $(event.target).parent().find('.amount');
                var amount = parseInt(amountSpan.text());
                var maxValue = parseInt($(event.target).parent().find('.max-value').text());

                if(amount < maxValue)
                {
                    amount++;
                    amountSpan.text(amount);
                }

                if(amount === maxValue)
                {
                    validateItem(parent);
                }
            });

            function validateItem(parent) // parent must be .order-sum-entry
            {
                parent.addClass("active");

                parent.find('.fa-minus').css(
                    {
                        "display":"none"
                    }
                );

                var plus = parent.find('.fa-plus');
                plus.removeClass("fa-plus order-sum-entry-qsection-btn");
                plus.addClass("fa-check order-sum-entry-confirmed");
            }
        </script>
    </div>
</div>
