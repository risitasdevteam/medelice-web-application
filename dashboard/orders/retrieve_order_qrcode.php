<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/08/2018
 * Time: 16:21
 */
require_once(__DIR__.'/../../vendor/autoload.php');
require_once(__DIR__.'/../../utility/RegexUtil.php');
require_once(__DIR__.'/../../utility/Environment.php');

try{
    $order_id = RegexUtil::filter_in_var_with_regex(INPUT_GET, "id", RegexUtil::order_uid());

    $data = "https://".Environment::get_server_name()."/dashboard/orders/order_summarize.inc.php";
    $size = '500x500';
    $logo = "https://lh3.googleusercontent.com/5lJcNEXUEXz9UNBDRQQ7hbVXnJz9s3D9KfDPUyCx18mhW45eJOl-kZa5KIbq1r2lY7Va_EIN2YqqZWmRD4VmYJPpkzo9MWmqRwmLarlKhqHA0sP-Nf6pxqbZNkxer6ucoCP1MZC5UW8u-yXtdgATnjS5E12v1D5CfZFT8og8sJzoWpTR6WRg0ckbxGskeN7aA2812C9pmdzsBJPw6IK8HdZ5QDM_sstTAZpC0O4ONrv74ItzPJVhWJbu9LR3t7JFS4kuWIdYRvd4uLvlMVmzLJXIv-EHpneDGITCkoH-vwT3MBQ2AqWBwG8oApz3JnxVzPVBFVFIz0r4wbv2tFnhsNwmv9mxIipNQ8vuZlajEcWjmsfgLIRAuGBNPREJgrQt5kX9qPCMyRJ3J_91sUXVTBe551uGrbx2-0T1-smMzDYL-G7ZxfXbpudU51t-WVJY4ma_2U0AXao-rcFdyxHpnlwEebvMqg1fCoKw_zX5_aRkR2wGaMfRJpmNSZEfcpBAtFVdc8mg2EzkNdkOanV03kOvQHjxpFUHhAXErme8iIO-lNC0JVd7jmA3hP5Mm2z0UtioB09mFs6IRkyPGiqYzjjxYV1N=w1920-h910";

    $QR = imagecreatefrompng('https://chart.googleapis.com/chart?cht=qr&chld=H|1&chs='.$size.'&chl='.urlencode($data));
    $logo = imagecreatefromstring(file_get_contents($logo));
    $QR_width = imagesx($QR);
    $QR_height = imagesy($QR);
    $logo_width = imagesx($logo);
    $logo_height = imagesy($logo);
    $logo_qr_width = $QR_width/3;
    $scale = $logo_width/$logo_qr_width;
    $logo_qr_height = $logo_height/$scale;

    imagecopyresampled($QR, $logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

    header('Content-type: image/png');
    imagepng($QR);
    imagedestroy($QR);
}
catch(Exception $e){
    exception_to_json_payload($e);
}

