<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 21/08/2018
 * Time: 21:10
 */


require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__ . '/../../includes/orders.php');
include(__DIR__ . '/../../includes/data_handler.php');
require_once(__DIR__.'/../../vendor/autoload.php');

//>50 valisettes, le service clientèle vous contactera pour la livraison **MENTION IMPORTANTE pour cause de rupture**
$express_delivery_delay = 2; //j
$standard_delivery_delay = 7;

$delivery_progress = 0;
$delivery_progress_interval = array(
    OrderStatus::RECEIVED => 5,
    OrderStatus::PROCESSING => 45,
    OrderStatus::SENT => 75,
    OrderStatus::DELIVERED => 100,
    OrderStatus::ERROR => 0,
    OrderStatus::PENDING_3D_SECURE => 0
);

$order_id = null;
$order = null;

try {
    $order_id = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'ref', FILTER_SANITIZE_STRING, true);

    $provider = new DBOrderProvider();
    $order = $provider->get_on_unique_id($order_id);

    if ($order === null) {
        throw new MedeliceException("Cette commande n'existe pas.");
    }

    $delivery_progress = $delivery_progress_interval[$order->getStatus()];
}
catch(Exception $e){

}

$account_time = $order->getAccountTime();
$delivery_mode = $order->getDeliveryMode();
$delivery_time_estimation = date("d/m/Y", $account_time + ((strcmp($delivery_mode, "normal") == 0) ? $standard_delivery_delay : $express_delivery_delay) * 86400);
$decoded_address = OrderCustomerAddresses::decode_from_json($order->getCustomerAddresses());

?>

<script src="/dashboard/js/Orders.js"></script>
<div id="topsection">
    <div class="dashboard-order-container">
        <div class="quicksand bold" style="font-size: 2vw">Commande n°<?php echo $order_id ?></div>
        <div class="order-admin-section">
            <div class="field-container">
                <label class="field-descriptor" for="switch_status">Changer le statut de la commande</label><br>
                <select class="custom-select custom-select-responsive" id="status_change_select" style="width: 16vw; margin-left: 0.5vw;">
                    <option value="0" <?php echo $order->getStatus() === OrderStatus::RECEIVED ? "selected" : "" ?>>Reçue chez nous</option>
                    <option value="1" <?php echo $order->getStatus() === OrderStatus::PROCESSING ? "selected" : "" ?>>En cours de traitement</option>
                    <option value="2" <?php echo $order->getStatus() === OrderStatus::SENT ? "selected" : "" ?>>Remis au transporteur</option>
                    <option value="3" <?php echo $order->getStatus() === OrderStatus::DELIVERED ? "selected" : "" ?>>Livrée chez le client</option>
                    <option value="4" <?php echo $order->getStatus() === OrderStatus::ERROR ? "selected" : "" ?>>Erreur</option>
                    <option value="5" <?php echo $order->getStatus() === OrderStatus::CANCELED ? "selected" : "" ?>>Annulée</option>
                </select>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="tracking">N° de tracking</label><br>
                <input class="field-input" id="tracking" type="text" name="tracking" placeholder="000000000000" value="<?php echo $order->getTracking() ?>">
            </div>
            <div class="field-container" style="display: flex; align-items: center;">
                <button class="field-button button order-admin-btn" id="order_status_submit">Valider</button>
                <button class="field-button button order-admin-btn" onclick="showQRCodesMenu()"><i class="fa fa-qrcode" style="margin-right: 0.5vw"></i>Codes QR</button>
            </div>
        </div>
        <script>
            function showQRCodesMenu()
            {
                $('.dashboard-qrcodes-overlay').css(
                    {
                        "display":"flex"
                    }
                );
            }

            function hideQRCodesMenu()
            {
                $('.dashboard-qrcodes-overlay').css(
                    {
                        "display":"none"
                    }
                );
            }
        </script>
        <div class="dashboard-qrcodes-overlay overlay-container">
            <div class="dashboard-qrcodes-menu">
                <select class="custom-select custom-select-responsive" style="width: 20vw">
                    <option value="">Afficher les produits</option>
                </select>
                <div class="fill-image-container dashboard-qrcode-display">
                    <?php
                        echo  '<img class="fill-image" src="/dashboard/orders/retrieve_order_qrcode.php?id='.$order->getId().'">';
                    ?>
                    <!--<img class="fill-image" src="">-->
                </div>
                <button class="button orange-btn quicksand bold dashboard-qrcodes-menu-btn" onclick="hideQRCodesMenu()">Fermer</button>
            </div>
        </div>
        <div class="order-status-container quicksand bold">
            <div class="order-status-progress-bar">
                <div class="order-progress-bar">
                    <span style="width: <?php echo ($delivery_progress < 31 ? 0 : ($delivery_progress < 65 ? 33 : ($delivery_progress < 98 ? 66 : 100))); ?>%"></span>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-1 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::RECEIVED] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande prise en compte
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-2 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::PROCESSING] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-archive"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande en préparation
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-3 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::SENT] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande expédiée
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-4 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::DELIVERED] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-flag"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande reçue
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        setTimeout(function() {
                            $(".order-progress-bar > span").css(
                                {
                                    "width":"<?php echo $delivery_progress ?>%"
                                }
                            );
                        }, 200);
                    });
                </script>
            </div>
            <div class="order-status-description">
                <?php
                $msg = "";
                $msg_indication = "";
                $msg_indication_color = "";

                switch ($order->getStatus()){
                    case OrderStatus::RECEIVED:
                        $msg = "Votre commande a été réçue, elle sera traitée très vite.";
                        $msg_indication = "Date de livraison estimée : ".$delivery_time_estimation." (Livraison ".$delivery_mode.')';
                        $msg_indication_color = "#009700";
                        break;

                    case OrderStatus::PROCESSING:
                        $msg = "Votre commande est en cours de préparation.";
                        $msg_indication = "Date de livraison estimée : ".$delivery_time_estimation." (Livraison ".$delivery_mode.')';
                        $msg_indication_color = "#009700";
                        break;

                    case OrderStatus::SENT:
                        $msg = "Votre commande a été envoyée, elle est prise en charge par nos pigeons voyageurs.";
                        $msg_indication = "N° de tracking : ".$order->getTracking().". Date de livraison estimée : ".$delivery_time_estimation." (Livraison ".$delivery_mode.')';
                        $msg_indication_color = "#009700";
                        break;

                    case OrderStatus::DELIVERED:
                        $msg = "Votre commande a été livrée, régalez-vous !";
                        $msg_indication = "Commande livrée le : ".date("d/m/Y", $order->getStatusLastUpdate());
                        $msg_indication_color = "#009700";
                        break;

                    case OrderStatus::ERROR:
                        $msg = "Une erreur s'est dissimulée dans votre commande, nos épicuriens font le nécessaire afin de débloquer la situation.";
                        $msg_indication = "<span style='text-decoration: line-through'>Date de livraison estimée : ".$delivery_time_estimation." (Livraison ".$delivery_mode.')</span>';
                        $msg_indication_color = "#db0f0f";
                        break;
                }

                echo $msg." ";
                echo '<span style="color: '.$msg_indication_color.'">'.$msg_indication.'</span>';
                ?>
            </div>
        </div>
        <div class="order-addresses-container quicksand">
            <div class="order-address-container order-delivery-address-container">
                <div class="order-address-title bold">Adresse de livraison</div>
                <div style="margin-left: 0.2vw">
                    <?php
                    $delivery_address = $decoded_address->delivery;
                    if (!$delivery_address instanceof Address){

                    }

                    $full_address = $delivery_address->emitter_name.' ('.$delivery_address->phone.')'.
                        br().
                        $delivery_address->address1.
                        br().
                        ($delivery_address->address2 != '' ? $delivery_address->address2.br() : '').
                        $delivery_address->zip.', '.$delivery_address->city.
                        br().
                        $delivery_address->state.
                        br().
                        $delivery_address->country;

                    echo $full_address;
                    ?>
                </div>
            </div>
            <div class="order-address-container order-billing-address-container">
                <div class="order-address-title bold">Adresse de facturation</div>
                <div style="margin-left: 0.2vw">
                    <?php
                    $billing_address = $decoded_address->billing;

                    if (!$billing_address instanceof Address){

                    }

                    $full_address = $billing_address->emitter_name.' ('.$delivery_address->phone.')'.
                        br().
                        $billing_address->address1.
                        br().
                        ($billing_address->address2 != '' ? $billing_address->address2.br() : '').
                        $billing_address->zip.', '.$billing_address->city.
                        br().
                        $billing_address->state.
                        br().
                        $billing_address->country;

                    echo $full_address;
                    ?>
                </div>
            </div>
        </div>
        <div class="order-products-container quicksand">
            <div class="order-products-title bold">Contenu de votre commande</div>
            <div class="order-products-subcontainer">
                <div class="order-products-header">
                    <?php
                    $products = CartUtil::decode_cart_from_json($order->getProducts());
                    $products_objects = array();
                    $valisettes_within_objects = array();

                    $total_mass = 0;

                    $add_product_mass = function($product_reference, $come_from_valisette = false) use (&$total_mass, &$products_objects){
                        $product = (new DBProductProvider)->get($product_reference);
                        if ($product === null){
                            throw new MedeliceException("The specified product doesn't exists");
                        }

                        if (!$come_from_valisette) {
                            array_push($products_objects, $product);
                        }

                        $total_mass += $product->getTotalMass();
                    };

                    if ($products == null || count($products) < 1){
                        echo "Panier vide, don't panic !";
                        return;
                    }

                    foreach ($products as $product_reference => $product_quantity)
                    {
                        if (CartUtil::is_a_valisette($product_reference))
                        {
                            $valisette_mother = new Valisette($product_reference);
                            $sub_valisette = $valisette_mother->get_specific_sub_valisette_from_id(explode(Valisette::SUB_VALISETTE_DELIMITER, $product_reference)[1]);
                            if (!$sub_valisette instanceof SubValisette){
                                throw new MedeliceException("");
                            }

                            $products_within = $sub_valisette->products;

                            foreach ($products_within as $product_reference_within){
                                $add_product_mass($product_reference_within, true);
                            }

                            $valisettes_within_objects[$product_reference] = $valisette_mother;
                            $total_mass += CartUtil::valisette_default_mass();
                        }
                        else
                        {
                            $add_product_mass($product_reference);
                        }
                    }

                    echo 'Masse totale : '.($total_mass * 10**(-3)).'kg';
                    ?>

                    <span style="float: right">Prix total : <?php echo $order->getTotalBill() ?>€</span>
                </div>
                <div class="order-products-container">

                    <?php
                    $iterator = 0;
                    $print_product_individually = function($product, $product_ref = "") use (&$products_objects, &$products, &$iterator){
                        $brand = "";
                        $url = 'index.php?page=';

                        if ($product instanceof Valisette){
                            $sub_valisette = $product->get_specific_sub_valisette_from_id(explode(Valisette::SUB_VALISETTE_DELIMITER, $product_ref)[1]);
                            if (!$sub_valisette instanceof SubValisette){
                                throw new MedeliceException("");
                            }

                            $ref = $product_ref;
                            $quantity = $products[$ref];

                            $preview_image_path = $product->image();
                            $name = $sub_valisette->title;
                            $price = ($sub_valisette->price * $quantity);

                            $url .= 'valisette&ref='.$ref;
                        }
                        else if ($product instanceof Product){
                            $images = (new FileIOProductProvider())->get_images($product->getRef());

                            $ref = $product->ref;
                            $quantity = $products[$product->ref];
                            $preview_image_path = $images[0];
                            $name = $product->name;
                            $brand = $product->brand;
                            $price = ($product->price * $quantity);
                            $url .= 'voir&ref='.$ref;
                        }
                        else{
                            throw new MedeliceException("");
                        }

                        echo '
                                <div class="order-product-display" style="float: '.($iterator % 2 == 0 ? "left" : "right").';">
                                    <a class="button-link order-product-picture" href="'.$url.'">
                                        <img class="fill-image" src="' . $preview_image_path . '"/>
                                    </a>
                                    <div class="order-product-infos">
                                        <span class="order-product-title">'.$name.'</span><br>
                                        <span class="order-product-brand">'.$brand.'</span><br>
                                        <div style="height: 0.3vw; width: 100%"></div>
                                        <span class="order-product-qty">Qté : '.$quantity.'</span>
                                        <span class="order-product-totalprice">'.($price * $quantity).'€</span>
                                    </div>
                                </div>
                            ';

                        $iterator++;
                    };

                    foreach ($products_objects as $product){
                        $print_product_individually($product);
                    }

                    foreach ($valisettes_within_objects as $product_reference => $sub_valisette){
                        $print_product_individually($sub_valisette, $product_reference);
                    }
                    ?>
                </div>
            </div>
        </div>

        <button style="height: 2vw; width: 100%; background-color: #8c7748; border: none; color: white; cursor: pointer;" onclick="location.href = '/index.php?page=ma-commande&ref=<?php echo $order_id ?>'">Voir la facture en PDF</button>
    </div>
</div>

<script>
    function change_status_handler(){
        var $button = $('#order_status_submit');

        $button.on("click", function(){
            var status = $('#status_change_select').val();
            var tracking = $('#tracking').val();

            var url = new URL(location.href);
            var order_id = url.searchParams.get("ref");

            OrdersUtil.edit(order_id, status, tracking, function(){
                setTimeout(function(){
                    location.reload();
                }, 3500);
            }, function(){

            });
        });
    }

    $(document).ready(function(){
        change_status_handler();
    });
</script>