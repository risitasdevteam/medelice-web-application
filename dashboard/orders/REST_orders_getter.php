<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 05/07/2018
 * Time: 14:45
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/orders.php');

function get_alone(string $unique_id){
    return (new DBOrderProvider())->get_on_unique_id($unique_id);
}

function get_unprepared(){
    return (new DBOrderProvider())->retrieve_unprepared_orders();
}

function get_done(){
    return (new DBOrderProvider())->retrieve_done_orders();
}

function get_processing(){
    return (new DBOrderProvider())->retrieve_processing_orders();
}

try{
    $mode = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'mode', '/^(alone|unprepared|done|processing)$/');

    $response = new JsonResponse(JsonResponseStatusType::OK);

    switch ($mode){
        case "alone":
            $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'unique_id', RegexUtil::crc32());
            $response->add_payload(new JsonResponsePayload("order", get_alone($id)));
            break;

        case "unprepared":
            $response->add_payload(new JsonResponsePayload("unprepared_orders", get_unprepared()));
            break;

        case "done":
            //var_dump(get_done());
            $response->add_payload(new JsonResponsePayload("done_orders", get_done()));
            break;

        case "processing":
            $response->add_payload(new JsonResponsePayload("processing_orders", get_processing()));
            break;

        default:
            throw new BadRequestMethodException("Wrong method"); //TODO require
            break;
    }

    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}