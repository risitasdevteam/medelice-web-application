/**
 * Created by jsanglier on 30/08/2018.
 */

class RegionsUtil{
    static retrieve(callback, error_callback){
        var rest_response_provider = new RESTResponseProvider();
        rest_response_provider.url = RouterUtil.get_url() + "?page=retrieve-regions";
        rest_response_provider.classic_callback = false;

        rest_response_provider.http_get(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }
}