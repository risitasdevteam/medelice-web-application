/**
 * Created by jsanglier on 09/08/2018.
 */

class ProductsUtil{
    static retrieve_stock_gaps(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url() + "?page=retrieve_stock_gaps";

        provider.classic_callback = false;
        provider.http_get(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static retrieve_by_region(region, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url() + "?page=retrieve-products-by-region&region="+region;

        provider.classic_callback = false;
        provider.http_get(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static retrieve_brands(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url() + "?page=retrieve-brands";

        provider.classic_callback = false;
        provider.http_get(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static add_brand(name, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "add-brand",
            new_brand_name: name
        };
        provider.classic_callback = true;

        provider.http_post(function (response) {
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static remove_brand(brand_id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "remove-brand",
            brand_id: brand_id
        };
        provider.classic_callback = true;

        provider.http_post(function (response) {
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }
}

class ValisettesUtil{
    static retrieve_alone(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url() + "?page=retrieve-valisettes&mode=alone";
        provider.classic_callback = false;
        provider.http_get(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static retrieve_associated(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url() + "?page=retrieve-valisettes&mode=associated";
        provider.classic_callback = false;
        provider.http_get(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static add(ref, name, region, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "add-valisette",
            ref: ref,
            name: name,
            region: region
        };
        provider.classic_callback = false;

        provider.http_post(function (response) {
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }
}