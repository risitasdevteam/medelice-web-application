/**
 * Created by jsanglier on 09/08/2018.
 */
class CustomerProvider{
    static login(user, password, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.classic_callback = true;
        provider.data = {
          page: 'login',
          mail: user,
            password: password
        };

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static retrieve(page, step, callback){
        var users = [];
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php?page=retrieve-users&page_n=" + page + "&step=" + step;
        provider.classic_callback = false;

        provider.http_get(function(response){
            var users_response = response.payload.users;
            if (Object.keys(users_response).length > 0){
                users_response.forEach(function(item){
                    users.push(item);
                });
            }
            callback.call(self, users);
        });
    }

    static edit(id, email, name, type, birthday, phone, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php";
        provider.classic_callback = true;

        provider.data = {
            page: 'user_edit',
            id: id,
            email: email,
            user_name: name,
            user_type: type,
            birthday: birthday,
            phone: phone
        };

        provider.http_post(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static remove(id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php";
        provider.classic_callback = true;

        provider.data = {
            page: 'user_remove',
            id: id
        };

        provider.http_post(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static send_forgotten_password_url(id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php";
        provider.classic_callback = true;

        provider.data = {
            page: 'user_password_reset',
            id: id
        };

        provider.http_post(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }

    static get_login_attempts(id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php?page=user_retrieve_login_attempts&id=" + id;
        provider.classic_callback = false;

        provider.http_get(function(response){
            callback(response);
        }, function(response){
            error_callback(response);
        });
    }
}

class CustomerUtil{
    static mail(mail, subject, content, ok_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/users/REST_send_mail.php";
        provider.classic_callback = true;
        provider.data = {mail: mail, subject: subject, content: content};

        provider.http_post(ok_callback);
    }
}