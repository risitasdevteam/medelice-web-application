/**
 * Created by jsanglier on 09/08/2018.
 */

class RESTResponseProvider{
    constructor(){
    }

    get url(){
        return this.rest_url;
    }

    set url(url){
        this.rest_url = url;
    }

    get classicCallback(){
        return this.classic_callback;
    }

    set classicCallback(classic_callback){
        this.classic_callback = classic_callback;
    }

    get data(){
        return this.form_data;
    }

    set data(response){
        this.form_data = response;
    }

    http_get(ok_callback, error_callback){
        var self = this;
        $.get(this.url, function(response){
            response = JSON.parse(response);
            if (self.classicCallback) {
                var factory = new NotificationFactory();
            }

            switch (response.status){
                case 'ok':
                    if (typeof ok_callback !== 'undefined')
                        ok_callback.call(self, response);

                    if (self.classicCallback){
                        var notification = new Notification();
                        notification.message = response.payload.msg;
                        notification.style_type = "info";

                        factory.show(notification);
                        break;
                    }

                    break;
                case 'error':
                    if (typeof error_callback !== 'undefined')
                        error_callback.call(self, response);

                    if (self.classicCallback){
                        var notification = new Notification();
                        notification.message = response.payload.error;
                        notification.style_type = "alert";

                        factory.show(notification);
                        break;
                    }

                    break;
            }
        });
    }

    http_post(ok_callback, error_callback){
        var self = this;
        $.post(this.url, this.data,
        function(response){
            response = JSON.parse(response);
            if (self.classicCallback) {
                var factory = new NotificationFactory();
            }
            switch (response.status){
                case 'ok':
                    if (typeof ok_callback !== 'undefined')
                        ok_callback.call(self, response);

                    if (self.classicCallback){
                        var notification = new Notification();
                        notification.message = response.payload.msg;
                        notification.style_type = "info";

                        factory.show(notification);
                        break;
                    }
                    break;
                case 'error':
                    if (typeof error_callback !== 'undefined')
                        error_callback.call(self, response);

                    if (self.classicCallback){
                        var notification = new Notification();
                        notification.message = response.payload.error;
                        notification.style_type = "alert";

                        factory.show(notification);
                        break;
                    }

                    break;
            }
        });
    }
}