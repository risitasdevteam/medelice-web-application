/**
 * Created by jsanglier on 13/08/2018.
 */
class ConfigUtil{

    static toggle_maintenance(reload_page = false){
        showLoadingScreen();
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.classic_callback = true;

        provider.data = {
            page: "toggle-maintenance"
        };

        provider.http_post(function(response){
            if(reload_page)
            {
                setTimeout(function(){
                    location.reload();
                }, 1500);
            }
            hideLoadingScreen();
        }, function (response) {
            hideLoadingScreen();
            error_notification("Erreur : " + response);
        });
    }

    static get_generic_pages(ok_callback, error_callback) {
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url()+"?page=retrieve-generic-pages";
        provider.classic_callback = false;

        provider.http_get(ok_callback, error_callback);
    }
}