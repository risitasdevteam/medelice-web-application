/**
 * Created by jsanglier on 22/08/2018.
 */

class OrdersUtil{
    static get_alone(unique_id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            'page':'orders_getter',
          'mode': 'alone',
            'unique_id': unique_id
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback(response)
        }, function(response){
            error_callback(response);
        });
    }

    static get_unprocessed(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            'page':'orders_getter',
            'mode': 'unprepared'
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback(response)
        }, function(response){
            error_callback(response);
        });
    }

    static get_done(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            'page':'orders_getter',
            'mode': 'done'
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback(response)
        }, function(response){
            error_callback(response);
        });
    }

    static get_processing(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            'page':'orders_getter',
            'mode': 'processing'
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback(response)
        }, function(response){
            error_callback(response);
        });
    }

    static edit(id, status, tracking, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();

        provider.data = {
            page:'order_status_edit',
            order_id: id,
            status: status,
            tracking: tracking
        };

        provider.classic_callback = true;
        provider.http_post(function(response){
            callback(response)
        }, function(response){
            error_callback(response);
        });
    }
}