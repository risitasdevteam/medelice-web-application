
function get_regions(){
    RegionsUtil.retrieve(function(response){
        var $regions_select = $("#region-select");
        if (response.payload.regions != null){
            $.each(response.payload.regions, function(index, item){
                $regions_select.append($('<option>', {
                    value: index,
                    text : item
                }));
            });
        }
    }, function(response){

    });
}

$(document).ready(function(){
    get_regions();
});
