/**
 * Created by jsanglier on 14/08/2018.
 */

class HTMLUtil{
    //from  https://stackoverflow.com/questions/1147359/how-to-decode-html-entities-using-jquery/1395954#1395954
   static decodeEntities(encodedString) {
        var textArea = document.createElement('textarea');
        textArea.innerHTML = encodedString;
        return textArea.value;
    }
}