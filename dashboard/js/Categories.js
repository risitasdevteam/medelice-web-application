/**
 * Created by jsanglier on 30/08/2018.
 */

class CategoriesUtil{
    static remove(id, is_a_subcategory, callback, error_callback){
       var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "category-editor",
            mode: 'remove',
            type: (is_a_subcategory ? 'subcategory' : 'category'),
            id: id
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static edit_name(id, is_a_subcategory, name, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "category-editor",
            mode: 'edit',
            type: (is_a_subcategory ? 'subcategory' : 'category'),
            id: id,
            name: name
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static add_category(name, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "category-editor",
            mode: 'add',
            type: 'category',
            name: name
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static add_sub_category(name, parent_id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url();
        provider.data = {
            page: "category-editor",
            mode: 'add',
            type: 'subcategory',
            name: name,
            id: parent_id
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static retrieve(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url()+"?page=retrieve-categories&mode=alone";

        provider.classic_callback = false;
        provider.http_get(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static retrieve_associated(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = RouterUtil.get_url()+"?page=retrieve-categories";

        provider.classic_callback = false;
        provider.http_get(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }
}