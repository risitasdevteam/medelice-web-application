<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 25/07/2018
 * Time: 10:27
 */
require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
?>

<div class="sidebar">
    <script>
        function toggleDropdown(id)
        {
            var parent = $('#' + id);
            var select = parent.find('.sidebar-element-dropdown-content');
            var icon = parent.find('.sidebar-element').find('.sidebar-element-dropdown-icon');

            if(select.css('display') === "block")
            {
                select.css(
                    {
                        "display":"none"
                    }
                );

                icon.removeClass('fa-angle-up');
                icon.addClass('fa-angle-down');
            }
            else
            {
                select.css(
                    {
                        "display":"block"
                    }
                );

                icon.removeClass('fa-angle-down');
                icon.addClass('fa-angle-up');
            }
        }
    </script>
    <div class="sidebar-topzone">
        <div class="bromello" style="font-size: 2.5vw; margin-top: 1.5vw"><span style="color: #8c7748">Mé</span>délice</div>
        <div style="margin-bottom: 1vw"><a href="/index.php?page=home"><i class="fa fa-sign-out sidebar-leave-icon"></i>Retour au site</a></div>
    </div>
    <a href="/dashboard/index.php?page=home" class="sidebar-element"><i class="fa fa-home sidebar-element-icon"></i>Accueil</a>
    <div class="sidebar-element-dropdown" id="orders-dropdown">
        <div class="sidebar-element" onclick="toggleDropdown('orders-dropdown')">
            <i class="fa fa-truck sidebar-element-icon"></i>Commandes <i class="fa fa-angle-down sidebar-element-dropdown-icon"></i>
        </div>
        <div class="sidebar-element-dropdown-content">
            <a href="/dashboard/index.php?page=commandes&sub=nouvelles" class="sidebar-element sidebar-subelement"><i class="fa fa-exclamation sidebar-element-icon" style="margin-left: 0.2vw"></i>Nouvelles commandes</a>
            <a href="/dashboard/index.php?page=commandes&sub=en_cours" class="sidebar-element sidebar-subelement"><i class="fa fa-tasks sidebar-element-icon"></i>Commandes en cours</a>
            <a href="/dashboard/index.php?page=commandes&sub=terminees" class="sidebar-element sidebar-subelement"><i class="fa fa-check sidebar-element-icon"></i>Commandes terminées</a>
        </div>
    </div>
    <div class="sidebar-element-dropdown" id="products-dropdown">
        <div class="sidebar-element" onclick="toggleDropdown('products-dropdown')">
            <i class="fa fa-shopping-basket sidebar-element-icon"></i>Produits <i class="fa fa-angle-down sidebar-element-dropdown-icon"></i>
        </div>
        <div class="sidebar-element-dropdown-content">
            <a href="/dashboard/index.php?page=nouveau-produit" class="sidebar-element sidebar-subelement"><i class="fa fa-plus sidebar-element-icon"></i>Ajouter un produit</a>
            <a href="/dashboard/index.php?page=produits" class="sidebar-element sidebar-subelement"><i class="fa fa-cog sidebar-element-icon"></i>Gérer les produits</a>
            <a href="/dashboard/index.php?page=categories" class="sidebar-element sidebar-subelement"><i class="fa fa-folder-open sidebar-element-icon"></i>Gérer les catégories</a>
            <a href="/dashboard/index.php?page=import" class="sidebar-element sidebar-subelement"><i class="fa fa-upload sidebar-element-icon"></i>Importer des produits</a>
        </div>
    </div>
    <div class="sidebar-element-dropdown" id="valisettes-dropdown">
        <div class="sidebar-element" onclick="toggleDropdown('valisettes-dropdown')">
            <i class="fa fa-suitcase sidebar-element-icon"></i>Valisettes <i class="fa fa-angle-down sidebar-element-dropdown-icon"></i>
        </div>
        <div class="sidebar-element-dropdown-content">
            <a href="/dashboard/index.php?page=nouvelle-valisette" class="sidebar-element sidebar-subelement"><i class="fa fa-plus sidebar-element-icon"></i>Ajouter une valisette</a>
            <a href="/dashboard/index.php?page=valisettes" class="sidebar-element sidebar-subelement"><i class="fa fa-cog sidebar-element-icon"></i>Gérer les valisettes</a>
        </div>
    </div>
    <div class="sidebar-element-dropdown" id="users-dropdown">
        <div class="sidebar-element" onclick="toggleDropdown('users-dropdown')">
            <i class="fa fa-user sidebar-element-icon"></i>Utilisateurs <i class="fa fa-angle-down sidebar-element-dropdown-icon"></i>
        </div>
        <div class="sidebar-element-dropdown-content">
            <a href="/dashboard/index.php?page=mailing" class="sidebar-element sidebar-subelement"><i class="fa fa-envelope sidebar-element-icon"></i>Mailing</a>
            <a href="/dashboard/index.php?page=utilisateurs" class="sidebar-element sidebar-subelement"><i class="fa fa-cog sidebar-element-icon"></i>Gérer les utilisateurs</a>
        </div>
    </div>
    <div class="sidebar-element-dropdown" id="config-dropdown">
        <div class="sidebar-element" onclick="toggleDropdown('config-dropdown')">
            <i class="fa fa-cogs sidebar-element-icon"></i>Configuration <i class="fa fa-angle-down sidebar-element-dropdown-icon"></i>
        </div>
        <div class="sidebar-element-dropdown-content">
            <a href="/dashboard/index.php?page=pages-generiques" class="sidebar-element sidebar-subelement"><i class="fa fa-file sidebar-element-icon"></i>Pages génériques</a>
            <a href="#" class="sidebar-element sidebar-subelement"><i class="fa fa-database sidebar-element-icon"></i>Base de données</a>
            <a href="https://www.ovh.com/auth/?action=gotomanager&from=https://www.ovh.com/fr/&ovhSubsidiary=fr" class="sidebar-element sidebar-subelement"><i class="fa fa-server sidebar-element-icon"></i>Hébergement du site</a>
            <a href="/dashboard/index.php?page=maintenance" class="sidebar-element sidebar-subelement"><i class="fa fa-fire-extinguisher sidebar-element-icon"></i>Mode maintenance</a>
        </div>
    </div>
    <a href="/dashboard/index.php?page=stock" class="sidebar-element"><i class="fa fa-heartbeat sidebar-element-icon"></i>Stocks</a>
    <a href="/dashboard/index.php?page=regions" class="sidebar-element"><i class="fa fa-map sidebar-element-icon"></i>Régions</a>
    <a href="/dashboard/index.php?page=stats" class="sidebar-element"><i class="fa fa-pie-chart sidebar-element-icon"></i>Statistiques</a>
</div>
