<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 28/10/2017
 * Time: 22:10
 */
?>

<title>Médélice - Dashboard</title>

<meta charset="utf-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
<link rel="stylesheet" href="/css/fontawesome.min.css">
<link rel="stylesheet" href="/dashboard/css/style.css">
<link rel="stylesheet" href="/dashboard/css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="/css/fontawesome.min.css">
<link rel="stylesheet" href="/css/new_fonts.css">
<link rel="stylesheet" href="/css/notyf.min.css">
<link rel="stylesheet" href="/dashboard/css/jquery-jvectormap-2.0.3.css" type="text/css" media="screen">
<link rel="stylesheet" href="/dashboard/config/trumbowyg/dist/ui/trumbowyg.min.css" type="text/css">
<link rel="stylesheet" href="/dashboard/config/trumbowyg/dist/plugins/colors/ui/trumbowyg.colors.css">
<link href="https://fonts.googleapis.com/css?family=Lobster|Montserrat|Roboto:400,700" rel="stylesheet">

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script src="/dashboard/js/bootstrap-colorpicker.min.js"></script>
<script src="/dashboard/js/jquery-jvectormap-2.0.3.min.js"></script>
<script src="/dashboard/js/jquery-jvectormap-world-mill.js"></script>
<script src="/js/notyf.min.js"></script>

<script src="/dashboard/config/trumbowyg/dist/trumbowyg.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/langs/fr.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/upload/trumbowyg.upload.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/table/trumbowyg.table.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/pasteimage/trumbowyg.pasteimage.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/lineheight/trumbowyg.lineheight.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/fontsize/trumbowyg.fontsize.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/fontfamily/trumbowyg.fontfamily.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/colors/trumbowyg.colors.min.js"></script>
<script src="/dashboard/config/trumbowyg/dist/plugins/base64/trumbowyg.base64.min.js"></script>

<script src="/dashboard/js/Router.js"></script>
<script src="/dashboard/js/RESTResponseProvider.js"></script>
<script src="/js/Notification.js"></script>
<script src="/js/NotificationFactory.js"></script>
<script src="/js/medelice.js"></script>