<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 13/08/2018
 * Time: 11:49
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<script src="/dashboard/js/Customer.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Mailing</span>
</div>
<div class="mailing-container">
    <div class="mailing-addresses-book">
        <div class="mailing-addresses-book-title quicksand bold">Carnet d'adresses</div>
        <div class="mailing-addresses-book-entries">
            <div class="mailing-addresses-book-entries-initial-border"></div>
            <div class="mailing-addresses-book-entry">
                <div class="bold">Tout le monde</div>
                <span class="user-book-entry">[everyone]</span>
            </div>
            <div class="mailing-addresses-book-entry">
                <div class="bold">Tout les clients standard</div>
                <span class="user-book-entry">[std_users]</span>
            </div>
            <div class="mailing-addresses-book-entry">
                <div class="bold">Tous les comptes non validés</div>
                <span class="user-book-entry">[unvalidated_users]</span>
            </div>
            <div class="mailing-addresses-book-entry">
                <div class="bold">L'équipe</div>
                <span class="user-book-entry">[staff]</span>
            </div>
            <div class="mailing-addresses-book-entry">
                <div class="bold">L'administation</div>
                <span class="user-book-entry">[admins]</span>
            </div>
            <div class="mailing-addresses-book-entry">
                <div class="bold"">Les modérateurs</div>
                <span class="user-book-entry">[moderators]</span>
            </div>
        </div>
    </div>
    <div class="contact-mail-form" style="background-color: white; border-radius: 0.2vw; padding: 1vw 1.2vw; width: 46vw; margin-right: 10vw">
        <div class="quicksand bold" style="width: 100%; font-size: 1.2vw; margin-bottom: 1vw; display: inline-block;">
            <span class="left">Envoyer un mail</span>
            <i class="fa fa-close right button" onclick="hideMailForm()"></i>
        </div>
        <div style="width: 97%; margin-left: 3%; display: flex; align-items: center; justify-content: space-between">
            <label class="field-descriptor" for="dest_mail" style="margin: 0;">À</label>
            <input type="text" class="field-input" id="dest_mail" style="width: 90%;">
        </div>
        <br>
        <div style="width: 97%; margin-left: 3%; display: flex; align-items: center; justify-content: space-between">
            <label class="field-descriptor" for="topic" style="margin: 0;">Sujet</label>
            <input type="text" class="field-input" id="topic" style="width: 90%;">
        </div>
        <div class="contact-mail-content-container" style="width: 97%; margin-left: 3%; height: 20.2vw;">
            <div class="editor"></div>
            <script>
                $('.editor').trumbowyg({
                    btns: [
                        ['viewHTML'],
                        ['undo', 'redo'],
                        ['formatting'],
                        ['bold', 'italic', 'strikethrough', 'underline'],
                        ['fontfamily', 'fontsize', 'lineheight'],
                        ['foreColor', 'backColor'],
                        ['superscript', 'subscript'],
                        ['link'],
                        ['insertImage', 'upload', 'base64'],
                        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['unorderedList', 'orderedList'],
                        ['table'],
                        ['horizontalRule'],
                        ['removeformat'],
                        ['fullscreen']
                    ],
                    semantic: false,
                    imageWidthModalEdit: true,
                    lang: 'fr'
                });
            </script>
        </div>
        <button class="button orange-btn quicksand bold" style="width: 40%; margin: 0.5vw 30% 0; height: 2.5vw; font-size: 1vw; border: none;" onclick="sendMail()">Envoyer</button>
    </div>
</div>

<script>
    function user_entry_click_handler(){
        var $entry = $('.mailing-addresses-book-entry');
        $entry.on("click", function(){
           var book_entry = $(this).find(".user-book-entry").text();

           var $dest_mail = $('#dest_mail');

            $dest_mail.val(book_entry);
        });
    }

    function sendMail(){
        var dest = $('#dest_mail').val();
        var subject = $('#topic').val();
        var content = $('.editor').trumbowyg("html");

        CustomerUtil.mail(dest, subject, content, function(){
        }, function(){
        });
    }

    $(document).ready(function(){
        user_entry_click_handler();
    });
</script>