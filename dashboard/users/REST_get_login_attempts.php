<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/08/2018
 * Time: 21:19
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try{
    $id = RegexUtil::filter_in_var_with_regex(INPUT_GET, "id", RegexUtil::uid());

    $provider = new DBCustomerProvider();
    $attempts = $provider->get_login_attempts($id);

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("attempts", $attempts)))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}