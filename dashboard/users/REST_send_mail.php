<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/08/2018
 * Time: 19:22
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include_once(__DIR__ . '/../../utility/Mail.php');

try{
    $email = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "mail", FILTER_SANITIZE_STRING, true);
    $subject = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "subject", FILTER_SANITIZE_STRING, true);
    $content = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "content", FILTER_SANITIZE_STRING, true);

    $mail_group = array("[everyone]", "[std_users]", "[unvalidated_users]", "[staff]", "[admins]", "[moderators]");
    if (in_array($email, $mail_group)){
        $key = array_search($email, $mail_group);
        $customers_provider = new DBCustomerProvider();

        $get_customers = function(int $rank) use($customers_provider){
            return $customers_provider->get_on_rank($rank, array(DBCustomerStructure::getColumns()[4]));
        };

        switch ($key){
            case 0:
                $users = array_merge($get_customers(UserType::administrator), $get_customers(UserType::banned_user), $get_customers(UserType::moderator), $get_customers(UserType::user), $get_customers(UserType::user_not_verified));
                break;

            case 1:
                $users = $get_customers(UserType::user);
                break;

            case 2:
                $users = $get_customers(UserType::user_not_verified);
                break;

            case 3:
                $users = array_merge($get_customers(UserType::moderator), $get_customers(UserType::administrator));
                break;

            case 4:
                $users = $get_customers(UserType::administrator);
                break;

            case 5:
                $users = $get_customers(UserType::moderator);
                break;

            default:
                $users = array();
                break;
        }

        $mail = new Mail();
        $mail->setBody($content);
        $mail->setSubject("Médélice - " . $subject);
        $mail->setMailConfiguration(new RobotMailConfiguration());

        foreach ($users as $customer){
            if (!$customer instanceof Customer){
                throw new MedeliceException("Customer in group is not an instanceof Customer obj");
            }

            $mail->addTo($customer->getMail());
        }

        $sender = new MailSender();
        $sender->send($mail, array("/{content}/"), array($mail->getBody()));
    }
    else {
        $mail = new Mail();
        $mail->setBody($content);
        $mail->setSubject("Médélice - " . $subject);
        $mail->setTo($email);
        $mail->setMailConfiguration(new RobotMailConfiguration());

        $sender = new MailSender();
        $sender->send($mail, array("/{content}/"), array($mail->getBody()));
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Mail envoyé avec succès !")))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}