<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 08/08/2018
 * Time: 13:24
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

$customer_provider = new DBCustomerProvider();
$max_page = ceil($customer_provider->get_total() / 40);
//TODO finir système powerlevel + searchbar via indéxation elasticsearch
?>

<script src="/dashboard/js/Customer.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Utilisateurs</span>
</div>
<div class="users-container">
    <div class="fields-line no-top-margin">
        <div class="field-container" style="display: flex; align-items: center;">
            <label class="field-descriptor" for="select_powerlevel" style="margin-bottom: 0; width: 19.5vw;">Sélectionnez une catégorie de comptes :</label>
            <select class="custom-select custom-select-responsive" id="select_powerlevel" style="width: 15vw;">
                <option value="0">Utilisateurs standard</option>
                <option value="-1">Administrateurs</option>
                <option value="-2">Modérateurs</option>
                <option value="1">Comptes en attente de validation</option>
                <option value="2">Comptes bannis</option>
                <option value="3" selected>Tous</option>
            </select>
        </div>
        <div class="field-container last-container">
            <input type="text" class="field-input" id="user_searchbar" placeholder="Rechercher">
        </div>
    </div>
    <table class="users-table roboto">
        <tr class="users-tablerow bold">
            <td class="users-td-id">Identifiant</td>
            <td class="users-td-name">Nom complet</td>
            <td class="users-td-mail">Mail</td>
            <td class="users-td-phone">Téléphone</td>
            <td class="users-td-lastco">Dernière connexion</td>
            <td class="users-td-more"></td>
        </tr>
    </table>
    <div class="page-select-bottom-container">
        <div class="page-select-zone">
            <i class="fa fa-angle-left button switch-page-button"></i>
            <input type="number" class="field-input page-select-field" id="page_number" min="1" max="<?php echo $max_page; ?>" value="1">
            <span>/ <?php echo $max_page; ?></span>
            <i class="fa fa-angle-right button switch-page-button"></i>
        </div>
    </div>
</div>

<script>
    function retrieve_users(page, step){
        var $table = $('.users-table tbody');

        var users = CustomerProvider.retrieve(page, step, function(response){
            $.each(response, function(index, item){
                var id = item["id"];
                var name = item["firstname"] + " " + item["surname"];
                var email = item["mail"];
                var phone = item["phone"];
                var last_connection = new Date(item["last_connection"]).toLocaleString();
                var ip = item["ip"];
                var rank = item["user_type"];

                $table.append('<tr class="users-tablerow" name="user-data" style="background-color: ' + (index % 2 == 0 ? '#eeeeee' : '#eaeaea') + ';" data-rank="' + rank + '">'+
                    '<td class="users-td-id">' + id + '</td>'+
                    '<td class="users-td-name">' + name + '</td>'+
                    '<td class="users-td-mail">' + email + '</td>'+
                    '<td class="users-td-phone">' + phone + '</td>'+
                    '<td class="users-td-lastco">' + last_connection + ' (' + ip + ')</td>'+
                    '<td class="users-td-more"><a class="normal-link" href="/dashboard/index.php?page=utilisateur&id=' + id + '">plus...</a></td>'+
                    '</tr>');
            });
        });
    }

    function power_level_change_handler(){
        var $select = $('#select_powerlevel');
        $select.on("change", function(){
            var rank = $(this).val();
            if (rank != 3) {
                $("[name='user-data'][data-rank!='" + rank + "']").hide();
                $("[name='user-data'][data-rank='" + rank + "']").show();
            }
            else{
                $("[name='user-data']").show();
            }
        });
    }

    function search_bar_handler(){
        $("#user_searchbar").on('keyup', function() {
            var value = $(this).val();
            var patt = new RegExp(value, "i");

            $('.users-table').find('[name="user-data"]').each(function() {
                if (!($(this).find('td').text().search(patt) >= 0)) {
                    $(this).not('.table-head').hide();
                }
                if (($(this).find('td').text().search(patt) >= 0)) {
                    $(this).show();
                }
            });

        });
    }

    function pagination_numeric_handler(){
        var $pagination_numeric_input = $('#page_number');
        $pagination_numeric_input.on("change", function(){
            var value = $(this).val();
            $("[name='user-data']").remove();

            retrieve_users((value - 1), 40);
        });
    }

    $(document).ready(function(){
        retrieve_users(0, 40);
        power_level_change_handler();
        search_bar_handler();
        pagination_numeric_handler();
    });
</script>
