<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/08/2018
 * Time: 00:06
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/customers.php');

try{
    $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "id", RegexUtil::uid());
    $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, true);
    $name = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'user_name', RegexUtil::name());
    $user_type = filter_input(INPUT_POST, 'user_type', FILTER_VALIDATE_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));
    $birthday = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'birthday', RegexUtil::input_birth()); //time to unix nworking?
    $phone = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'phone', RegexUtil::international_phone());

    $exploded_name = explode(" ", $name);

    $provider = new DBCustomerProvider();
    $customer = $provider->get($id);

    if (count($exploded_name) > 1){
        $customer->setSurname($exploded_name[1]);
    }

    $customer->setFirstname($exploded_name[0]);
    $customer->setBirthday(strtotime($birthday));
    $customer->setUserType($user_type);
    $customer->setPhone($phone);
    $customer->setMail($mail);

    if (!$provider->edit($customer)){
        throw new MedeliceException("Error");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Utilisateur édité avec succès !")))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}