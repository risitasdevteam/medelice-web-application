<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 15/08/2018
 * Time: 11:04
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/customers.php');

$columns = DBCustomerStructure::getColumns();
$retrieved_columns = [$columns[0], $columns[2], $columns[3], $columns[4], $columns[6], $columns[7], $columns[9], $columns[1]];
$users_per_page_default = 40;

try{
    $users_per_page = filter_input(INPUT_GET, 'step', FILTER_SANITIZE_NUMBER_INT) ?? $users_per_page_default;
    $page = filter_input(INPUT_GET, 'page_n', FILTER_SANITIZE_NUMBER_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL)) ?? 0;
    $upper = $users_per_page*($page > 0 ? $page + 1 : 1);
    $lower = $upper - $users_per_page;

    $user_provider = new DBCustomerProvider();
    $users = $user_provider->get_in_interval($lower, $users_per_page_default, $retrieved_columns);

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("users", $users));
    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}