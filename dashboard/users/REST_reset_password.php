<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/08/2018
 * Time: 02:08
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/customers.php');

try{
    $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "id", RegexUtil::uid());

    $provider = new DBCustomerProvider();
    $customer = $provider->get($id);
    if ($customer === null){
        throw new MedeliceException("Error user not found");
    }

    if (!$provider->generate_forgotten_password_url($customer, true)){
        throw new MedeliceException("Error");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Un mail de réinitialisation a été envoyé avec succès !")))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}