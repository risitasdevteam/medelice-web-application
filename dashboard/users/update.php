<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 15:28
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/customers.php');
include(__DIR__.'/../includes/data_handler.php');
//TODO
try
{
    $user_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'id', RegexUtil::uid());

    $firstname = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'firstname', RegexUtil::name(), true, false);
    $surname = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'surname', RegexUtil::name(), true, false);
    $mail = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'mail', RegexUtil::email(), true, false);
    $un_hashed_password = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'password', RegexUtil::password(), true, false);


    $hashed_password = User::generate_most_optimal_password_hash($un_hashed_password);
}
catch(Exception $e)
{
    exception_to_json_payload($e);
}