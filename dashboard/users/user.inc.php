<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 08/08/2018
 * Time: 13:47
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/customers.php');
include(__DIR__.'/../includes/data_handler.php');


try{
    $user_id = RegexUtil::filter_in_var_with_regex(INPUT_GET, "id", RegexUtil::uid());
    $user_provider = new DBCustomerProvider();
    $user = $user_provider->get($user_id);
    if ($user === null){
        throw new MedeliceException("Utilisateur inexistant");
    }
}
catch (Exception $e){
    echo 'Une erreur est survenue ...';
    exit;
}

?>

<script src="/dashboard/js/Customer.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Utilisateur individuel</span>
</div>
<div class="user-page-container">
    <div class="user-page-fields-container">
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="user_id">Identifiant</label><br>
                <input type="text" class="field-input" id="user_id" name="id" value="<?php echo $user->getId() ?>" disabled>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="user_name">Nom complet</label><br>
                <input type="text" class="field-input" id="user_name" name="user_name" value="<?php echo $user->getFirstname().' '.$user->getSurname() ?>">
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="user_dob">Naissance</label><br>
                <input type="date" class="field-input field-date" id="user_dob" name="birthday" value="<?php echo date("Y-m-j", $user->getBirthday()) ?>">
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="user_email">Email</label><br>
                <input type="email" class="field-input" id="user_email" name="email" value="<?php echo $user->getMail() ?>">
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="user_phone">Téléphone</label><br>
                <input type="text" class="field-input" id="user_phone" name="phone" value="<?php echo $user->getPhone() ?>">
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="user_type">Type de compte</label><br>
                <select class="custom-select custom-select-responsive" id="user_type" name="user_type" style="width: 15vw;">
                    <option value="0" <?php echo ($user->getUserType() === UserType::user ? "selected" : "")?>>Utilisateur standard</option>
                    <option value="-1" <?php echo ($user->getUserType() === UserType::administrator ? "selected" : "")?>>Administrateur</option>
                    <option value="-2" <?php echo ($user->getUserType() === UserType::moderator ? "selected" : "")?>>Modérateur</option>
                    <option value="1" <?php echo ($user->getUserType() === UserType::user_not_verified ? "selected" : "")?>>Compte en attente de validation</option>
                    <option value="2" <?php echo ($user->getUserType() === UserType::banned_user ? "selected" : "")?>>Compte banni</option>
                </select>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="user_joined">Inscription</label><br>
                <div class="field-text" id="user_joined"><?php echo date("j/m/Y h:i:s", $user->getSince()) ?></div>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="user_lastconnection">Dernière connexion</label><br>
                <div class="field-text" id="user_lastconnection" style="font-size: 0.95vw; margin-left: 0.5vw;"><?php echo date("j/m/Y h:i:s", $user->getLastConnection()).' @'.$user->getIp()?></div>
            </div>
        </div>
        <button class="user-page-save-button button darkgray-btn quicksand bold" id="save-button">
            <i class="fa fa-save" style="margin-right: 0.5vw;"></i>Sauvegarder les modifications
        </button>
    </div>
    <div class="user-page-buttons-container">
        <button class="indiv-user-form-button button darkgray-btn quicksand bold" onclick="showMailForm()">Contacter par mail</button>
        <hr class="user-page-buttons-separator">
        <a href="/index.php?page=mes-commandes&user=1f56q149f6" class="indiv-user-form-button button darkgray-btn quicksand bold button-link"><span class="button-link-text">Commandes passées</span></a>
        <a href="/index.php?page=mes-adresses&user=1f56q149f6" class="indiv-user-form-button button darkgray-btn quicksand bold button-link"><span class="button-link-text">Adresses enregistrées</span></a>
        <button class="indiv-user-form-button button darkgray-btn quicksand bold">Coupons</button>
        <hr class="user-page-buttons-separator">
        <button class="indiv-user-form-button button darkgray-btn quicksand bold" onclick="alertPasswordReset()">Réinit. mot de passe</button>
        <button class="indiv-user-form-button button darkgray-btn quicksand bold" onclick="showConnTrialsDisplay()">Tentatives de connexion</button>
        <button class="indiv-user-form-button button darkgray-btn quicksand bold" onclick="alertAccountRemoval()">Supprimer le compte</button>
    </div>
    <div class="contact-mail-form-container overlay-container">
        <div class="contact-mail-form" style="background-color: white; border-radius: 0.2vw; padding: 1vw 1.2vw; width: 46vw; margin: 8vw 27vw;">
            <div class="quicksand bold" style="width: 100%; font-size: 1.2vw; margin-bottom: 1vw; display: inline-block;">
                <span class="left">Envoyer un mail</span>
                <i class="fa fa-close right button" onclick="hideMailForm()"></i>
            </div>
            <div style="width: 97%; margin-left: 3%; display: flex; align-items: center; justify-content: space-between">
                <label class="field-descriptor" for="dest_mail" style="margin: 0;">À</label>
                <input type="text" class="field-input" id="dest_mail" style="width: 90%;" value="<?php echo $user->getMail() ?>">
            </div>
            <br>
            <div style="width: 97%; margin-left: 3%; display: flex; align-items: center; justify-content: space-between">
                <label class="field-descriptor" for="topic" style="margin: 0;">Sujet</label>
                <input type="text" class="field-input" id="topic" style="width: 90%;">
            </div>
            <div class="contact-mail-content-container" style="width: 97%; margin-left: 3%; height: 20.2vw;">
                <div class="editor"></div>
                <script>
                    $('.editor').trumbowyg({
                        btns: [
                            ['viewHTML'],
                            ['undo', 'redo'],
                            ['formatting'],
                            ['bold', 'italic', 'strikethrough', 'underline'],
                            ['fontfamily', 'fontsize', 'lineheight'],
                            ['foreColor', 'backColor'],
                            ['superscript', 'subscript'],
                            ['link'],
                            ['insertImage', 'upload', 'base64'],
                            ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                            ['unorderedList', 'orderedList'],
                            ['table'],
                            ['horizontalRule'],
                            ['removeformat'],
                            ['fullscreen']
                        ],
                        semantic: false,
                        imageWidthModalEdit: true,
                        lang: 'fr'
                    });
                </script>
            </div>
            <button class="button orange-btn quicksand bold" style="width: 40%; margin: 0.5vw 30% 0; height: 2.5vw; font-size: 1vw; border: none;" onclick="sendMail()">Envoyer</button>
        </div>
        <script>
            function showMailForm()
            {
                $('.contact-mail-form-container').css(
                    {
                        "display":"block"
                    }
                );
            }

            function hideMailForm()
            {
                $('.contact-mail-form-container').css(
                    {
                        "display":"none"
                    }
                );
            }

            function sendMail()
            {
                var mail = $("#dest_mail").val();
                var subject = $("#topic").val();
                var content = $('.editor').trumbowyg("html");

                CustomerUtil.mail(mail, subject, content, function(){
                    hideMailForm();
                });
            }

            function save_button_handler(){
                var $button = $('#save-button');
                $button.on("click", function(){
                    var id = $('#user_id').val();
                    var name = $('#user_name').val();
                    var user_type = $('#user_type').val();
                    var birthday = $('#user_dob').val();
                    var email = $('#user_email').val();
                    var phone = $('#user_phone').val();

                    CustomerProvider.edit(id, email, name, user_type, birthday, phone, function(response){

                    }, function(response){

                    });
                });
            }

            $(document).ready(function(){
                save_button_handler();
            });
        </script>
    </div>
    <div class="connection-trials-display-container overlay-container" style="align-items: center; justify-content: center">
        <div class="connection-trials-display">
            <div class="connection-trials-display-title quicksand bold">
                <span class="left">Tentatives de connexion (échecs)</span>
                <i class="fa fa-close right button" onclick="hideConnTrialsDisplay()"></i>
            </div>
            <div class="connection-trials-table-container">
                <table class="connection-trials-table roboto">
                    <tr class="connection-trials-tablerow bold">
                        <td class="connection-trials-datetime">Date et heure</td>
                        <td class="connection-trials-ip">IP de connexion</td>
                        <td class="connection-trials-country">Pays de connexion</td>
                    </tr>
                    <tr class="connection-trials-tablerow" style="background-color: #eeeeee">
                        <td class="connection-trials-datetime">07/08/2018 13:47:39</td>
                        <td class="connection-trials-ip">192.168.10.201</td>
                        <td class="connection-trials-country"><img class="connection-trials-country-flag" src="/images/flags/fr.png">France</td>
                    </tr>
                </table>
            </div>

        </div>
        <script>
            function showConnTrialsDisplay()
            {
                $('.connection-trials-display-container').css(
                    {
                        "display":"flex"
                    }
                );
            }

            function hideConnTrialsDisplay()
            {
                $('.connection-trials-display-container').css(
                    {
                        "display":"none"
                    }
                );
            }

            function retrieve_connections_trials(){
                var id = $('#user_id').val();
                var table = $('.connection-trials-table tbody');

                CustomerProvider.get_login_attempts(id, function(response){
                    if (Object.keys(response.payload.attempts).length > 0){
                        $.each(response.payload.attempts, function(index, item){

                            var time = new Date(item["time"]).toLocaleString();
                            var ip = item["ip"];

                            table.append('<tr class="connection-trials-tablerow"' + (index % 2 == 0 ? ' style="background-color: #eeeeee"' : '' ) + '>'+
                            '<td class="connection-trials-datetime">' + time + '</td>'+
                            '<td class="connection-trials-ip">' + ip + '</td>'+
                            '<td class="connection-trials-country"><img class="connection-trials-country-flag" src="/images/flags/fr.png">France</td>'+
                                '</tr>');
                        });
                    }
                }, function(response){

                });
            }
        </script>
    </div>
    <div class="alert-password-reset-container overlay-container">
        <div class="alert-form quicksand bold">
            <div class="alert-form-text-container">
                Voulez-vous vraiment <span style="color: red">RÉINITIALISER</span> le mot de passe de ce compte ?<br>
                <br>
                Un email d'avertissement sera envoyé à son détenteur.
            </div>
            <div class="alert-form-buttons-container">
                <button class="alert-form-button button green-btn left quicksand bold" onclick="discardPasswordReset()">Annuler</button>
                <button class="alert-form-button button red-btn right quicksand bold" onclick="confirmPasswordReset()">Réinitialiser le mot de passe</button>
            </div>
        </div>
        <script>
            function alertPasswordReset()
            {
                $('.alert-password-reset-container').css(
                    {
                        "display":"block"
                    }
                );
            }

            function discardPasswordReset()
            {
                $('.alert-password-reset-container').css(
                    {
                        "display":"none"
                    }
                );
            }

            function confirmPasswordReset()
            {
                var id = $('#user_id').val();
                CustomerProvider.send_forgotten_password_url(id, function(){
                    discardPasswordReset();
                }, function(response){
                });
            }
        </script>
    </div>
    <div class="alert-account-removal-container overlay-container">
        <div class="alert-form quicksand bold">
            <div class="alert-form-text-container">
                Vous aller <span style="color: red">SUPPRIMER</span> un compte utilisateur. Cette action est irréversible, et ne doit être entreprise qu'avec un <span style="color: red">accord express de la direction</span>.<br>
                <br>
                Après la suppression, un email d'avertissement sera envoyé à l'ancien détenteur du compte.
            </div>
            <div class="alert-form-buttons-container">
                <button class="alert-form-button button green-btn left quicksand bold" onclick="discardAccountRemoval()">Annuler</button>
                <button class="alert-form-button button red-btn right quicksand bold" onclick="confirmAccountRemoval()">Confirmer la suppression</button>
            </div>
        </div>
        <script>
            function alertAccountRemoval()
            {
                $('.alert-account-removal-container').css(
                    {
                        "display":"block"
                    }
                );
            }

            function discardAccountRemoval()
            {
                $('.alert-account-removal-container').css(
                    {
                        "display":"none"
                    }
                );
            }

            function confirmAccountRemoval()
            {
                var id = $('#user_id').val();

                CustomerProvider.remove(id, function(response){
                    discardAccountRemoval();
                    setTimeout(function(){
                        location.href = "/dashboard/index.php?page=utilisateurs";
                    }, 2500);
                }, function(response){

                });
            }

            $(document).ready(function(){
               retrieve_connections_trials();
            });
        </script>
    </div>
</div>

