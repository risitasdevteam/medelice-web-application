<?php
require_once(__DIR__.'/../utility/SQLUtil.php');
require_once(__DIR__ . '/../customer/misc/UserUtil.php');
require_once(__DIR__.'/includes/customers.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 28/10/2017
 * Time: 23:55
 */
class DashboardUtil
{
    /**
     * Vérifier si l'customer est logé sur
     * @return bool
     */
    public static function userLegallyLogged(){
        return UserUtil::isLogged() && UserUtil::is_a_data_manager();
    }

    public static function loginCheck()
    {
        //if (DEV) return true;

        if (!self::userLegallyLogged()) {
            http_response_code(403);
            throw new MedeliceException('Forbidden.');
        }
        return true;
    }
    /**
     * Récupérer le nombre de commandes effectuées dans les 24h.
     * @return mixed
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function totalOrdersIn24h(){
        $sql = SQLUtil::open("medelice_users");
        $back_24h_before = (time() - (60*60*24));

       $query = 'select count(order_id) as total from '.ORDERS_TABLE.' where order_date > ?';
        if ($stmt = $sql->prepare($query)){

            $stmt->bind_param('d', $back_24h_before);
            $stmt->bind_result($total);
            $stmt->execute();

            $stmt->fetch();
            $stmt->close();
            SQLUtil::close($sql);

            return $total;
        }else {
            SQLUtil::close($sql);
            throw new DatabaseInvalidQueryException('');
        }
    }

    /**
     * Récupérer le nombre de commandes effectuées dans la journée.
     * @return mixed
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function todayTotalOrders(){
        $sql = SQLUtil::open("medelice_users");
        $query = 'select count(order_id) as total from '.ORDERS_TABLE.' where order_date >= ?';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param('d', self::get_midnight_millis());
            $stmt->bind_result($total);
            $stmt->execute();

            $stmt->fetch();
            $stmt->close();
            SQLUtil::close($sql);

            return $total;
        }else{
            SQLUtil::close($sql);
            throw new DatabaseInvalidQueryException('');
        }
    }

    public static function get_midnight_millis(){
        $isdst = localtime()[8]; //heure d'été = 1
        $now = time() + (3600 * ($isdst == 1 ? 2 : 1));
        $h = ($now / 3600) % 24;
        $m = ($now / 60) % 60;
        $s = ($now % 60);
        return $now - (3600 * $h + 60 * $m + $s);
    }
}