<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 19/07/2018
 * Time: 18:40
 */

require_once(__DIR__.'/../../exceptions/data/DataException.php');
require_once(__DIR__.'/../../exceptions/data/DataBadException.php');

class Stat
{
    public $id;
    public $type;
    public $value;
    public $timestamp;

    public static $columns = array("id", "type", "value", "value", "timestamp");

    public function __construct($id, $type, $value, $timestamp){
        $this->id = $id;
        $this->type = $type;
        $this->value = $value;
        $this->timestamp = $timestamp;
    }

    public function toArray(){
        return get_object_vars($this);
    }

    public static function from_array($array){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        if (count(array_intersect_key($array, self::$columns)) != count(self::$columns)) {
            throw new DataBadException("false arrays intersection");
        }

        //fuzzy way to cast
        $stat = new Stat(
            $array["id"],
            $array["type"],
            $array["value"],
            $array["timestamp"]
        );

        return $stat;
    }
}