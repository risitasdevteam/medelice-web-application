<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 19/07/2018
 * Time: 18:35
 */

require_once(__DIR__.'/Stat.php');

class StatsManager
{
    const TABLE = "stats";

    public static function get_from_type_and_timestamp($type, $timestamp_operator, $timestamp){
        if (!is_string($type) || !is_string($timestamp_operator) || !is_numeric($timestamp)){
            throw new DataBadException("");
        }

        $raw_stats = SQLUtil::select(self::TABLE, ['*'], ['type' => $type, 'timestamp'.$timestamp_operator => $timestamp]);
        $total = count($raw_stats);
        if ($total < 1){
            throw new MedeliceException("Aucune statisitique n'a été trouvé pour les conditions données");
        }

        if ($total > 1){
            $stats_array = [];
            foreach ($raw_stats as $stat_array){
                $stat = Stat::from_array($stat_array);
                array_push($stats_array, $stat);
            }
            return $stats_array;
        }

        return Stat::from_array($raw_stats[0]);
    }

    public static function push($stat_obj){
        if (!$stat_obj instanceof Stat){
            throw new DataBadException("");
        }

        SQLUtil::insert(self::TABLE, $stat_obj->toArray());
        return true;
    }
}