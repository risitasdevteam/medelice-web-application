<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 19/07/2018
 * Time: 19:04
 */
require_once(__DIR__.'/Stat.php');
require_once(__DIR__.'/StatsManager.php');
//require_once(__DIR__.'/')
require_once(__DIR__.'/../../initializer.inc.php'); //because this file will be executed as a poor standalone one we need to give himself a full package of things

$super_secret_passphrase_hex = "441dbb897cd4ec5b6c427fbf1771080bce4e6616ea502af73ecc6e8acf15ce43ae4e6785b0ddf0bc5faffb1b4b38963c816fd5a03edb61b9a92cf7d5d9334e47c71c13065c4f02aa97bd89dad9e3cb11036eef3506eda4908014f3a579ed41dab14aeadb564e0b951a52338588682e9e6a028239735fc822960849ce823915bcb0efeac8607b06975b5578b4c77694af188e2a7fd57ac340a9ad253ca8428d71552430040fe4fb007a0b8291b36bd535205c5d8067a597dd013cbf172639974e2fbc53c7f3386b2e215dde49e2c05d91f18c16dbc2e2cd16ba46b3122f2d09fdabfafaa5e16e111374287fd5379a7101ff3c3edf274030441715e72cff74ce9e14463223459b2bfa5ca20caaf51b863983ed67ba6e5457c84252e41f9bb12a2ee5c63a6b59e971f8e8b751fa493a1178e38e8669c61a5ead85a12d5f8a8b1d8e9fc39f7e3751ddb6717d7cf3eaa42a31f1df8cd2afa716345e6a759c66f5584ee1206bb56858e02b0d0d701f2e33e499f71e404765db7788b58432a8492fe7edbb8ce2fe4244e80c0676853f09ac76b80b04ed3cdcb4356c2b8d4320073cf2e9814bfadfcc9bd99a8f7e29cdc92b2ef2dbde37047eca536d6f2e0e938cafc158aeff72e964b0a827065ac088c875e5bd737f5d4b9914445d9d8e99a4be678e92d1bd701b69c8099b3be1d469b3bd26473c56353b90e8f2faec47f2132754e86b";

try{
    post_thrown();

    $closure = function($var_name, $filter = FILTER_SANITIZE_STRING){
       return RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, $var_name, $filter, true);
    };

    $secret_pass = $closure('secret_pass');
    $type = $closure('type');
    $value = $closure('value', FILTER_SANITIZE_NUMBER_INT);

    if (strcmp($secret_pass, $super_secret_passphrase_hex) != 0){
        throw new MedeliceException("invalid pass faggot");
    }

    $stat = new Stat(null, $type, $value, time());
    if (!StatsManager::push($stat)){

    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}