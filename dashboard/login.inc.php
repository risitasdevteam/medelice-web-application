<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2018
 * Time: 21:15
 */
require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__."/../utility.inc.php");
require_once(__DIR__."/DashboardUtil.php");

if (DashboardUtil::userLegallyLogged()){
    redirect("/dashboard/index.php?page=home");
}
?>

<script src="/dashboard/js/Customer.js"></script>
<script>
    $('.content').css(
        {
            "width":"100%"
        }
    );
</script>
<div class="dashboard-login-container">
    <div class="dashboard-login-form">
        <div class="dashboard-login-top">
            <span class="bromello" style="font-size: 36pt;">
                <span style="color: #8c7748;">Mé</span>délice
            </span><br>
            <a href="/index.php?page=home" style="color: black">
                <i class="fa fa-sign-out sidebar-leave-icon" style="margin-right: 0.5em;"></i>Retour au site
            </a>
        </div>
        <div class="dashboard-login-bottom">
            <input type="email" id="mail" class="field-input dashboard-login-field" placeholder="Utilisateur">
            <input type="password" id="password" class="field-input dashboard-login-field" placeholder="Mot de passe">
            <button class="dashboard-login-button button quicksand bold orange-btn">Connexion</button>
        </div>
    </div>
</div>
<script>
    function handler(){
        var $button = $('.dashboard-login-button');
        $button.on("click", function(){
            var mail = $('#mail').val();
            var password = $('#password').val();

           CustomerProvider.login(mail, password, function(response){
               location.href = "/dashboard/index.php?page=home";
           }, function(response){

           });
        });
    }

    $(document).ready(function(){
        handler();
    });
</script>