<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 16:49
 */

abstract class Privileges
{
    const ADMIN = 0;
    const MODERATOR = 1;
}