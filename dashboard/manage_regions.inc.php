<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 04/08/2018
 * Time: 18:52
 */

require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
?>

<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Régions</span>
</div>
<div class="regions-container">
    <div class="regions-title-container">
        <span class="roboto bold left" style="font-size: 1.2vw;">Régions existantes</span>
        <button class="regions-add-reg-btn field-button button roboto" onclick="showAddRegionForm()"><i class="fa fa-plus" style="margin-right: 0.6vw"></i>Ajouter une région</button>
    </div>
    <table class="regions-table roboto">
        <tr class="regions-tablerow bold">
            <td class="regions-td-id">ID</td>
            <td class="regions-td-name">Nom</td>
            <td class="regions-td-color">Couleur d'arrière-plan</td>
            <td class="regions-td-negcolor">Couleur de texte</td>
            <td class="regions-td-edit"></td>
            <td class="regions-td-remove"></td>
        </tr>
    </table>
</div>
<div class="new-region-form-container">
    <div class="new-region-form">
        <div class="new-region-form-title bold">
            <span class="left">Ajouter une région</span>
            <i class="fa fa-close right button" onclick="hideAddRegionForm()"></i>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="reg_id">Identifiant</label><br>
                <input class="field-input" id="reg_id" placeholder="ex: BRE, ARA..." style="width: 10vw;">
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="reg_name">Nom</label><br>
                <input class="field-input" id="reg_name" style="width: 20.5vw;">
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="reg_color">Couleur d'arrière-plan</label><br>
                <input class="field-colorpicker" type="color" id="reg_color" value="#ffffff">
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="reg_negcolor">Couleur de texte</label><br>
                <input class="field-colorpicker" type="color" id="reg_negcolor" value="#ffffff">
            </div>
        </div>
        <button class="new-region-confirm-btn button orange-btn quicksand bold" onclick="add_region()">Ajouter cette région</button>
    </div>
</div>
<script>
    function get_regions(){
        var url = '/dashboard/products/REST_get_regions.php?mode=all';

        $.get(url, function(response){
            response = JSON.parse(response);

               switch (response.status){
                   case 'ok':
                       if (Object.keys(response.payload.regions).length > 0){
                           var $table_tbody = $('.regions-table tbody');

                           $('.regions-table tr[id="content"]').remove();

                           var iterator = 0;
                           $.each(response.payload.regions, function(index, item){
                               var region_id = index;
                               var name = item["region_name"];
                               var hex = item["hex"];
                               var neg_hex =item["neg_gex"];

                               $table_tbody.append('<tr class="regions-tablerow" id="content" data-id="' + region_id + '" ' + (iterator % 2 == 0 ? 'style="background-color: #eaeaea;"' : '')+ '>'+
                                   '<td style="padding-left: 2%;">' + region_id + '</td>'+
                                   '<td>' + name + '</td>'+
                                   '<td><input type="color" class="field-colorpicker" id="' + region_id + '_color" value="#' + hex + '" disabled="true"></td>'+
                                   '<td><input type="color" class="field-colorpicker" id="' + region_id + '_negcolor" value="#' + neg_hex +'" disabled="true"></td>'+
                                   '<td><button class="edit-button" onclick=""><i class="fa fa-pencil"></i></button></td>'+
                                    '<td><button class="remove-button"><i class="fa fa-close"></i></button></td>'+
                               '</tr>');

                               iterator++;
                           });
                           remove_button_click_register();
                       }
                       break;

                   case 'error':
                       break;
               }
        });
    }

    function add_region(){
        var data = {
          'mode':'add',
            'id':$('#reg_id').val(),
            'region_name':$('#reg_name').val(),
            'hex':$('#reg_color').val().replace("#", ""),
            'neg_hex':$('#reg_negcolor').val().replace("#", "")
        };

        var provider = new RESTResponseProvider();
        provider.url = '/dashboard/products/REST_edit_regions.php';
        provider.data = response;
        provider.classic_callback = true;
        provider.http_post(function(){
            hideAddRegionForm();
            get_regions();
        });
    }

    function edit_region(){

    }

    function remove_region(id){
        var provider = new RESTResponseProvider();
        provider.url = '/dashboard/products/REST_edit_regions.php';
        provider.data = {
            'mode':'remove',
            'id': id
        };
        provider.classic_callback = true;

        provider.http_post(function(){
            get_regions();
        });
    }

    function remove_button_click_register(){
        $('.remove-button').on("click", function(){
            var remove = confirm("Souhaitez vous vraiment supprimer cette région ?");
            if (remove) {
                var region_id = $(this).closest('tr').data("id");
                remove_region(region_id);
            }
        });
    }

    function showAddRegionForm()
    {
        $('.new-region-form-container').css(
            {
                "display":"block"
            }
        );
    }

    function hideAddRegionForm()
    {
        $('.new-region-form-container').css(
            {
                "display":"none"
            }
        );
    }

    $(document).ready(function(){
        get_regions();
        remove_button_click_register();
    });
</script>
