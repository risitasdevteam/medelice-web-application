<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 15:23
 */

require_once(__DIR__.'/../../utility.inc.php');
require_once(__DIR__.'/../../utility/RegexUtil.php');
require_once(__DIR__.'/../../exceptions/MedeliceException.php');
require_once(__DIR__.'/../../exceptions/BadRequestMethodException.php');