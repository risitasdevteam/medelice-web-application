<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 19:17
 */

require_once(__DIR__.'/../../orders/Order.class.php');
require_once(__DIR__.'/../../orders/OrderDeliveryMode.php');
require_once(__DIR__.'/../../orders/OrderProduct.php');
require_once(__DIR__.'/../../orders/OrderProperty.php');
require_once(__DIR__.'/../../orders/OrderStatus.enum.php');