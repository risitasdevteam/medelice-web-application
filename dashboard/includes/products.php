<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 20:05
 */

require_once(__DIR__ . '/../../produits/Product.php');
require_once(__DIR__.'/../../produits/Valisette.php');
require_once(__DIR__.'/../../produits/Regions.class.php');
require_once(__DIR__.'/../../produits/Category.php');
require_once(__DIR__.'/../../produits/SubCategory.php');
require_once(__DIR__.'/../../produits/ProductsCategories.class.php');