<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 14:59
 *
 * File to put in every dashboard header
 */
require_once(__DIR__.'/../DashboardUtil.php');
require_once(__DIR__.'/../Privileges.enum.php');

try {
    DashboardUtil::loginCheck();
}
catch(Exception $e){
    redirect("/");
    exit; //prevent any data leak
}

