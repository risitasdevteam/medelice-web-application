<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 28/10/2017
 * Time: 21:46
 */

require_once(__DIR__."/../utility/PageDirectAccessGuard.php");
require_once(__DIR__ . '/../customer/misc/UserUtil.php');

// TODO insérer un check de permission (+ un check de online) (pour les accès directs)

require_once(__DIR__.'/../initializer.inc.php');
?>

<html>
    <head>
        <?php include(__DIR__.'/head_data.inc.php'); ?>
    </head>
    <body>
        <header>
            <?php include(__DIR__.'/body_header.inc.php'); ?>
        </header>
        <?php

            if($current_page != "login")
            {
                include(__DIR__."/includes/admin_min.php");
                include(__DIR__ . '/sidebar.inc.php');
            }

        ?>
        <div class="content">
            <?php

                $include = '';
                switch ($current_page)
                {
                    case 'nouveau-produit':
                        $include = 'products/topsection.product_editor.php';
                        break;

                    case 'importation':
                        $include = 'products/topsection.import_products.php';
                        break;

                    case 'nouvelle-valisette':
                        $include = 'products/topsection.add_valisette.php';
                        break;

                    case 'produits':
                        $include = 'products/topsection.manage_products.php';
                        break;

                    case 'valisettes':
                        $include = 'products/topsection.manage_boxes.php';
                        break;

                    case 'categories':
                        $include = 'products/topsection.manage_categories.php';
                        break;

                    case 'home':
                        $include = "home.php";
                        break;

                    case 'regions':
                        $include = "manage_regions.inc.php";
                        break;

                    case 'commandes':
                        $include = "orders/display_orders.inc.php";
                        break;

                    case 'commande':
                        $include = "orders/display_order.inc.php";
                        break;

                    case 'pages-generiques':
                        $include = "config/generic_pages.inc.php";
                        break;

                    case 'maintenance':
                        $include = "config/maintenance_mode.inc.php";
                        break;

                    case 'utilisateurs':
                        $include = "users/users.inc.php";
                        break;

                    case 'utilisateur':
                        $include = "users/user.inc.php";
                        break;

                    case 'mailing':
                        $include = "users/mailing.inc.php";
                        break;

                    case 'stock':
                        $include = "products/topsection.show_stock.php";
                        break;

                    case 'order-sum':
                        $include = "orders/order_summarize.php";
                        break;

                    case 'import':
                        $include = "products/topsection.import_products.php";
                        break;

                    case 'login':
                        $include = "login.inc.php";
                        break;

                    default:
                        $include = null;
                        $current_page = '404';
                        break;
                }

                if ($include != null)
                    include($include);

                include (__DIR__ . '/../loading_overlay.inc.php');
            ?>
        </div>
        <script>
            var notyf = new Notyf({ // mis à la fin pour permettre ses appels depuis toute la page (sauf footer...). De plus, notyf ne peut être instancié avant le chargement de body (par l expérience => a besoin de divs hôtes dès son instanciation)
                delay:5000
            });
        </script>
    </body>
</html>
