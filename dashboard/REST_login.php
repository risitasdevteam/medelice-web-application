<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 27/08/2018
 * Time: 03:44
 */
require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
include(__DIR__."/includes/data_handler.php");
include(__DIR__."/includes/customers.php");

try{
    $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL, true);
    $password = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'password', FILTER_SANITIZE_STRING, true);

    $result = User::login($mail, $password, false);
    if ($result){
        $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Connecté, redirection en cours..."));
        $response->transmit();
    }else{
        throw new MedeliceException("Une erreur est survenue lors de la connexion");
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}