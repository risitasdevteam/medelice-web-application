<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 07/08/2018
 * Time: 12:45
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/products.php');

try{
    post_thrown();

    $mode = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'mode', FILTER_SANITIZE_STRING, true);
    $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'id', RegexUtil::region_id());

    switch ($mode){
        case 'edit':
            $name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'name', FILTER_SANITIZE_STRING, true);
            Regions::edit($id, $name);
            (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Région ".$name." editée avec succès !")))->transmit();
            break;

        case 'remove':
            Regions::remove($id);
            (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Région ".$id." supprimée avec succès !")))->transmit();
            break;

        case 'add':
            $name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'region_name', FILTER_SANITIZE_STRING, true);
            $hex = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'hex', '/^[0-9a-fA-F]{6}$/');
            $neg_hex = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'neg_hex', '/^[0-9a-fA-F]{6}$/');

            Regions::add($id, $name, $hex, $neg_hex);
            (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Région ".$name." ajoutée avec succès !")))->transmit();
            break;

        default:
            throw new MedeliceException("Bad method");
            break;
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}