<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 22:51
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try{
    post_thrown();

    $brand_name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'new_brand_name', FILTER_SANITIZE_STRING, true);

    $sql = SQLUtil::open("medelice_products");
    $prepared_query = 'insert into products_brands (brand) values (?)';
    if ($stmt = $sql->prepare($prepared_query)){
        $stmt->bind_param("s", $brand_name);
        $stmt->execute();
        $stmt->close();
    }

    $response = new JsonResponse(JsonResponseStatusType::OK);
    $response->add_payload(new JsonResponsePayload("id", $sql->insert_id));
    SQLUtil::close($sql);

    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}