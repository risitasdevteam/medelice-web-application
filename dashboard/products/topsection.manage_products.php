<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 26/07/2018
 * Time: 20:30
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<script src="/dashboard/js/Products.js"></script>
<script src="/dashboard/js/Regions.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Produits</span>
</div>
<div class="products-info-container">
    <div class="products-info-header">
        <div class="field-container" style="display: flex; align-items: center;">
            <label class="left" for="region-select" style="margin-bottom: 0; margin-right: 0.5vw;">Choisissez une région :</label>
            <select class="custom-select custom-select-responsive" id="region-select" style="width: 15vw;">
            </select>
        </div>
        <div class="field-container last-container">
            <label for="search">Recherche par référence</label>
            <input class="field-input" name="search" type="text">
        </div>
    </div>

    <table class="products-info-table">
        <tr class="bold table-head">
            <td class="products-info-td-ref">Réf.</td>
            <td class="products-info-td-name">Nom</td>
            <td class="products-info-td-brand">Marque</td>
            <td class="products-info-td-cat">Catégorie</td>
            <td class="products-info-td-subcat">Sous-catégorie</td>
            <td class="products-info-td-mass">Masse</td>
            <td class="products-info-td-price">Prix</td>
            <td class="products-info-td-mod"></td>
            <td class="products-info-td-more"></td>
        </tr>
    </table>
    <div id="products-info-more-display">
        <div id="product-info-image" class="fill-image-container">
            <img class="fill-image" src="/images/produits/default_product.png">
        </div>
        <div id="product-info-description">
        </div>
        <div id="product-info-tags">
            <i class="vegan"></i>
            <i class="bio"></i>
        </div>
        <i class="fa fa-angle-down" id="product-info-more-hide" onclick="hideDescForm()"></i>
    </div>
    <script>
        function get_regions(){
            RegionsUtil.retrieve(function(response){
                var $regions_select = $("#region-select");
                if (response.payload.regions != null){
                    $.each(response.payload.regions, function(index, item){
                        $regions_select.append($('<option>', {
                            value: index,
                            text : item
                        }));
                    });
                    $regions_select.trigger("change");
                }
            }, function(response){

            });
        }


        function description_form_handler(ref, desc, image, isbio, isvegan){
            var icon = $('#icon-' + ref);
            if(icon.hasClass('fa-angle-up'))
            {
                resetProductListButtons();
                hideDescForm();
            }
        else
            {
                $('#product-info-description').html(desc);
                $('#product-info-image').find('img').attr('src', image);
                resetProductListButtons();
                showDescForm();
                icon.removeClass('fa-angle-down');
                icon.addClass('fa-angle-up');

                if (isbio)
                    $('#product-info-tags').append('<i class="bio"></i>');
                else
                    $('#product-info-tags').find(".bio").remove();

                if (isvegan)
                    $('#product-info-tags').append('<i class="vegan"></i>');
                else
                    $('#product-info-tags').find(".vegan").remove();
            }
        }

        function get_products_by_regions(region){
            var $table = $('.products-info-table tbody');
            ProductsUtil.retrieve_by_region(region, function(response){
                if (Object.keys(response.payload.products).length > 0){
                    $.each(response.payload.products, function(index, item){
                        var product_array = item;

                        var ref = product_array["ref"];
                        var name = product_array["name"];
                        var brand = product_array["brand"];
                        var category = product_array["category"];
                        var sub_category = product_array["sub_category"];
                        var quantity_value = product_array["quantity_value"];
                        var total_mass = product_array["total_mass"];
                        var price = product_array["price"];
                        var isbio = product_array["isbio"];
                        var isvegan = product_array["isvegan"];
                        var desc = product_array["description"];
                        var img = product_array["img"];

                        $table.append('<tr style="background-color: ' + (index % 2 == 0 ? '#eaeaea' : '#e0e0e0') + '" id="tr-' + ref + '">'+
                            '<td class="products-info-td-ref">' + ref + '</td>'+
                            '<td class="products-info-td-name">' + name + '</td>'+
                            '<td class="products-info-td-brand">' + brand + '</td>'+
                            '<td class="products-info-td-cat">' + category + '</td>'+
                            '<td class="products-info-td-subcat">' + sub_category + '</td>'+
                            '<td class="products-info-td-mass">' + quantity_value  + ' / ' + total_mass + '</td>'+
                            '<td class="products-info-td-price">' + price + '€</td>'+
                            '<td class="products-info-td-mod"><a class="normal-link" href="/dashboard/index.php?page=nouveau-produit&mode=edit&ref=' + ref + '">modifier...</a></td>'+
                            '<td class="products-info-td-more" id="more-button-' + ref + '" onclick="description_form_handler(\'' + ref + '\',\'' + desc.replace(/\"/g,'\\"') + '\',\'' + img + '\',' + isbio + ',' + isvegan + ');"><i class="product-info-more-icon fa fa-angle-down" id="icon-' + ref + '"></i></td>'+
                            '</tr>');
                    });
                }
            }, function(response){

            });
        }

        function select_region_onchange_handler(){
            var $regions_select = $("#region-select");

            $regions_select.on("change", function(){
                var region_id = this.value;
                $('tr[id*="tr-"]').remove();

                get_products_by_regions(region_id);
            });
        }

        function search(){
            $("[name='search']").on('keyup', function() {
                var value = $(this).val();
                var patt = new RegExp(value, "i");

                $('.products-info-table').find('tr').each(function() {
                    if (!($(this).find('td').text().search(patt) >= 0)) {
                        $(this).not('.table-head').hide();
                    }
                    if (($(this).find('td').text().search(patt) >= 0)) {
                        $(this).show();
                    }
                });

            });
        }

        function showDescForm()
        {
            $('#products-info-more-display').css(
                {
                    "bottom":"2vw"
                }
            );
        }

        function hideDescForm()
        {
            $('#products-info-more-display').css(
                {
                    "bottom":"-13vw"
                }
            );
            resetProductListButtons();
        }

        function resetProductListButtons()
        {
            $('.product-info-more-icon').removeClass('fa-angle-up');
            $('.product-info-more-icon').addClass('fa-angle-down');
        }

        $(document).ready(function(){
            get_regions();
            select_region_onchange_handler();
            search()
        });
    </script>
</div>