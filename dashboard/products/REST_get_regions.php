<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 15:31
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try{
    get_thrown();

    $mode = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'mode', FILTER_SANITIZE_STRING);

    $raw_regions = SQLUtil::select("products_regions", ($mode === 'all' ? ['*'] : ["id", "region_name"]), []);

    $raw_regions_count = count($raw_regions);
    $regions = array();
    for ($i = 0; $i < $raw_regions_count; $i++){
        $raw_region = $raw_regions[$i];

        $id = $raw_region["id"];

        if ($mode === 'all'){
            unset($raw_region["id"]);
            $regions[$id] = $raw_region;
        }
        else {
            $region = $raw_region["region_name"];

            $regions[$id] = $region;
        }
    }

    $response = new JsonResponse(JsonResponseStatusType::OK);
    $response->add_payload(new JsonResponsePayload("regions", $regions));


    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}