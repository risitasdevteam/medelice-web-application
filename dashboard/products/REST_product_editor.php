<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 21:37
 */
require_once(__DIR__ . "/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__ . '/../includes/data_handler.php');
include(__DIR__ . '/../includes/products.php');
include(__DIR__ . '/../includes/admin_min.php');

try{
    post_thrown();

    $mode = RegexUtil::filter_in_var_with_regex(INPUT_POST, "mode", "/^(edit|add)$/");

    $ref = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'ref', RegexUtil::product_ref());
    $name = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'name', RegexUtil::product_name());
    $brand_id = filter_input(INPUT_POST, 'brand_id', FILTER_VALIDATE_INT, array('flags' => FILTER_FLAG_ALLOW_OCTAL));
    $quantity_unity = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'quantity_unity', FILTER_SANITIZE_STRING, true);
    $quantity_value = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'quantity_value', FILTER_VALIDATE_FLOAT   , true);
    $total_mass = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'total_mass', FILTER_VALIDATE_FLOAT, true);
    $region_id = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'region_id', FILTER_SANITIZE_STRING, true);
    $category_id = filter_input(INPUT_POST, 'category_id', FILTER_VALIDATE_INT, array('flags' => FILTER_FLAG_ALLOW_OCTAL));
    $subcategory_id = filter_input(INPUT_POST, 'subcategory_id', FILTER_VALIDATE_INT, array('flags' => FILTER_FLAG_ALLOW_OCTAL));
    $price = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'price', FILTER_VALIDATE_FLOAT, true);
    $description = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'description', FILTER_SANITIZE_STRING);
    $vegan = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'vegan', RegexUtil::checkbox(), true, false);
    $bio = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'bio', RegexUtil::checkbox(), true, false);
    $vat = filter_input(INPUT_POST, 'vat', FILTER_VALIDATE_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));
    $stock = filter_input(INPUT_POST, 'stock', FILTER_VALIDATE_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));

    $product_obj = Product::from_array(array_combine(DBProductsStructure::columns_array,
        [
            $ref,
            $name,
            $brand_id,
            $quantity_unity,
            $quantity_value,
            $total_mass,
            $category_id,
            $subcategory_id,
            $region_id,
            $description,
            $price,
            $bio,
            $vegan,
            $vat,
            $stock
        ]
    ));

    $product_provider = new DBProductProvider();
    switch ($mode){
        case 'edit':
            $payload = new JsonResponsePayload("msg", $product_obj);
            if (!$product_provider->edit($ref, $product_obj)){
                throw new MedeliceException("s");
            }
            $payload = new JsonResponsePayload("msg", "Produit édité avec succès !");
            break;

        case 'add':
            $io = new FileIOProductProvider();
            if (!$io->upload_images($ref) || !$product_provider->insert($product_obj)){
                throw new MedeliceException("");
            }
            $payload = new JsonResponsePayload("msg", "Produit ajouté avec succès !");
            break;

        default:
            throw new MedeliceException("");
            break;
    }

    (new JsonResponse(JsonResponseStatusType::OK, $payload))->transmit();
}
catch(Exception $e){
    //echo $e->getMessage();
   exception_to_json_payload($e);
}