<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/08/2018
 * Time: 19:19
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__."/../includes/admin_min.php");
require_once(__DIR__."/../../utility/SQLUtil.php");

function add(string $product_ref, string $sub_valisette_id, string $valisette_id){
    $sql = SQLUtil::open("medelice_products");
    $query = "insert into products_sub_valisettes_content values(?,?,?)";
    if ($stmt = $sql->prepare($query)){
        $stmt->bind_param("sss", $product_ref, $sub_valisette_id, $valisette_id);
        $stmt->execute();

        if ($stmt->error !== ""){
            $stmt->close();
            throw new MedeliceException("Erreur, veuillez verifier que le produit ne soit pas déjà contenu dans cette sous valisette.");
        }

        $stmt->close();
    }

    SQLUtil::close($sql);
    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Produit ajouté avec succès dans la sous valisette ".$sub_valisette_id)))->transmit();
}

function remove(string $product_ref, string $sub_valisette_id, string $valisette_id){
    $sql = SQLUtil::open("medelice_products");
    $query = "delete from products_sub_valisettes_content where product_ref = ? and id = ? and ref = ?";
    if ($stmt = $sql->prepare($query)){
        $stmt->bind_param("sss", $product_ref, $sub_valisette_id, $valisette_id);
        $stmt->execute();

        if ($stmt->error !== ""){
            $stmt->close();
            throw new MedeliceException("Erreur, veuillez verifier que le produit existe déjà dans cette sous valisette.");
        }

        $stmt->close();
    }

    SQLUtil::close($sql);
    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Produit supprimé avec succès de la sous valisette ".$sub_valisette_id)))->transmit();
}

try{
    $mode = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'mode', '/^(remove|add){1}$/');
    $product_ref = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'ref', RegexUtil::product_ref());
    $sub_valisette_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'sub_valisette_id', "/^[a-zA-Z]{2}$/");
    $valisette_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "valisette_id", RegexUtil::box_ref());

    switch ($mode){
        case "remove":
            remove($product_ref, $sub_valisette_id, $valisette_id);
            break;
        case "add":
            add($product_ref, $sub_valisette_id, $valisette_id);
            break;

        default:
            throw new MedeliceException("Wrong methode");
    }
}catch(Exception $e){
    exception_to_json_payload($e);
}