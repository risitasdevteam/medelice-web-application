<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 15:11
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try{
    get_thrown();

    $response = new JsonResponse(JsonResponseStatusType::OK, null);

    $raw_brands = SQLUtil::select("products_brands", ["*"], []);

    $raw_brands_count = count($raw_brands);
    $brands = array();
    for ($i = 0; $i < $raw_brands_count; $i++){
        $raw_brand = $raw_brands[$i];

        $id = $raw_brand["id"];
        $brand = $raw_brand["brand"];

        $brands[$id] = $brand;
    }

    $response->add_payload(new JsonResponsePayload("brands", $brands));
    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}