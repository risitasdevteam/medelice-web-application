<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 03/08/2018
 * Time: 17:42
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/products.php');
try{
    get_thrown();

    $region = RegexUtil::filter_in_var_with_regex(INPUT_GET, 'region', RegexUtil::region_id());

    $sql = SQLUtil::open("medelice_products");

    $query = 'select
        p.ref,
        p.name,
        b.brand,
        c.name,
        sc.subc_name,
        p.quantity_value,
        p.total_mass,
        p.price,
        p.isbio,
        p.isvegan,
        p.description
    from
        products p
    join
        products_sub_categories sc
            on p.sub_category = sc.subc_id
    join
    	products_categories c
    		on p.category = c.id
    join
        products_brands b
            on p.brand = b.id
    where p.region = ?';

    $products = array();

    if ($stmt = $sql->prepare($query)){
        $stmt->bind_param("s", $region);
        $stmt->execute();
        $stmt->bind_result($ref, $name, $brand, $category_name, $sub_category_name, $quantity_value, $total_mass, $price, $isbio, $isvegan, $description);
        $keys = ["ref", "name", "brand", "category", "sub_category", "quantity_value", "total_mass", "price", "isbio", "isvegan", "description", "img"];

        while ($row = $stmt->fetch()){
            $io_provider = new FileIOProductProvider();
            array_push($products, array_combine($keys, [$ref, $name, $brand, $category_name, $sub_category_name, $quantity_value, $total_mass, $price, $isbio, $isvegan, $description, $io_provider->get_images($ref)[0]]));
        }
    }

    SQLUtil::close($sql);

    $response = new JsonResponse(JsonResponseStatusType::OK, null);
    $response->add_payload(new JsonResponsePayload("products", $products));
    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}