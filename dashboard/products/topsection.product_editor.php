<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/10/2017
 * Time: 23:18
 */

require_once(__DIR__ . "/../../utility/PageDirectAccessGuardThrow.php");

$token_prefix = 'product-add';
$token_alive = 600; //10mn

$edition_mode = false;
$mode = isset($_GET['mode']) ? $_GET['mode'] : 'add';
if($mode != "add" && $mode != "edit")
{
    $mode = "add";
}

if($mode == "edit")
{
    if (!isset($_GET['ref'])){
        throw new MedeliceException("");
    }
    $ref = $_GET['ref'];

    $edition_mode = true;
    $product_provider = new DBProductProvider();
    $product = $product_provider->get($ref);
}

?>

<script src="/dashboard/js/Products.js"></script>
<script src="/dashboard/js/Regions.js"></script>
<script src="/dashboard/js/Categories.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ <?php echo !$edition_mode ?'Nouveau produit' : 'Modifier un produit' ?> </span>
</div>
<form class="add-product-form" enctype="multipart/form-data">
    <input type="hidden" name="page" value="product-editor">
    <input type="hidden" value="<?php echo $mode ?>" name="mode">
    <div class="new-product-fields-container">
        <div class="fields-line no-top-margin">
            <div class="field-container"">
                <label class="field-descriptor" for="ref">Référence</label><br>
                <input type="text" class="field-input" name="ref" <?php echo ($edition_mode ? 'value="'.$product->getRef().'"' : '')?>>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="name">Nom</label><br>
                <input type="text" class="field-input" name="name" <?php echo ($edition_mode ? 'value="'.$product->getName().'"' : '')?>>
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="brand-select">Marque</label><br>
                <select class="product-brand-select custom-select custom-select-responsive" id="brand-select" name="brand_id">
                </select>
                <button class="field-button button" type="button" onclick="showNewBrandForm()"><i class="fa fa-pencil"></i></button>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="measure">Masse / Volume du contenu</label><br>
                <input type="number" step="0.01" class="field-input new-product-numeric-value left" name="quantity_value" <?php echo ($edition_mode ? 'value="'.$product->getQuantityValue().'"' : '')?>>
                <select class="product-measure-unit-select custom-select custom-select-responsive" name="quantity_unity">
                    <option value="g">g</option>
                    <option value="mL">mL</option>
                </select>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="total-mass">Masse totale du produit</label><br>
                <input type="number" step="0.01" class="field-input new-product-numeric-value left" name="total_mass" <?php echo ($edition_mode ? 'value="'.$product->getTotalMass().'"' : '')?>>
                <select class="product-measure-unit-select custom-select custom-select-responsive" name="totalmass-unit-select">
                    <option value="g">g</option>
                    <option value="kg">kg</option>
                </select>
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="region-select">Région</label><br>
                <select class="new-product-region-select custom-select custom-select-responsive" id="region-select" name="region_id">
                </select>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="category-select">Catégorie</label><br>
                <select class="new-product-select custom-select custom-select-responsive" id="category-select" name="category_id">
                </select>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="subcategory-select">Sous-catégorie</label><br>
                <select class="new-product-select custom-select custom-select-responsive" id="subcategory-select" name="subcategory_id">
                </select>
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="price">Prix (€)</label><br>
                <input class="field-input" type="number" name="price" step="0.01" style="width: 14vw" <?php echo ($edition_mode ? 'value="'.$product->getPrice().'"' : '')?>>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="vat">TVA</label><br>
                <select class="new-product-select custom-select custom-select-responsive" id="vat-select" name="vat">
                    <option value="0" <?php echo ($edition_mode ? 'selected' : '')?>>20%</option>
                    <option value="1" <?php echo ($edition_mode ? 'selected' : '')?>>5.5%</option>
                </select>
            </div>
            <div class="field-container">
                <label class="field-descriptor" for="stock">Stock</label><br>
                <input class="field-input" type="number" name="stock" step="1" style="width: 14vw" <?php echo ($edition_mode ? 'value="'.$product->getStock().'"' : '')?>>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container last-container" style="width: 100%">
                <label class="field-descriptor" for="description">Description</label><br>
                <textarea class="new-product-description-textarea" name="description"><?php echo ($edition_mode ? $product->getDescription() : '')?></textarea>
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <input class="field-checkbox" type="checkbox" name="vegan" <?php echo ($edition_mode && $product->getIsvegan() ? 'checked' : '')?>><label class="field-descriptor" for="vegan">Vegan</label>
        </div>
            <div class="field-container">
                <input class="field-checkbox" type="checkbox" name="bio" <?php echo ($edition_mode && $product->getIsbio() ? 'checked' : '')?>><label class="field-descriptor" for="bio">Bio</label>
            </div>
        </div>
    </div>
    <div class="new-product-right-container">
        <div class="new-product-images-container">
            <div class="new-product-images-title roboto bold">Images</div>
            <input class="new-product-images-input" type="file" name="products_upload_images[]" multiple="multiple">
        </div>
        <button type="submit" class="new-product-add-button button quicksand bold"><?php echo ($mode == "edit" ? "Modifier" : "Ajouter"); ?> ce produit</button>
    </div>
    <div id="new-brand-form-container">
        <div id="new-brand-form">
            <div class="new-brand-form-title quicksand bold">
                <span style="float: left">Nouvelle marque</span><i class="fa fa-close" style="float: right; cursor: pointer" onclick="hideNewBrandForm()"></i>
            </div>
            <input class="new-brand-field roboto field-input" type="text" name="new_brand_name" placeholder="Nom">
            <button type="button" class="new-brand-button button quicksand bold" onclick="confirmNewBrand()">Ajouter cette marque</button>
            <div class="brands-edit-separator"></div>
            <div class="new-brand-form-title quicksand bold">
                <span style="float: left">Marques enregistrées</span>
            </div>
            <div class="brands-edit-table-container">
                <table class="brands-edit-table roboto">
                    <tr class="brands-edit-tablerow bold">
                        <td class="brands-edit-id">ID</td>
                        <td class="brands-edit-name">Nom</td>
                        <td class="brands-edit-edit"></td>
                        <td class="brands-edit-remove"></td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

    <script>
        function get_brands(){
            ProductsUtil.retrieve_brands(function(response){
                var $brands_select = $("#brand-select");
                var $brands_table = $('.brands-edit-table tbody');
                $('[name="brands-edit-tablerow-data"]').remove();

                if (response.payload.brands != null){
                    $.each(response.payload.brands, function(index, item){
                        $brands_select.append($('<option>', {
                            value: index,
                            text : item
                        }));

                        $brands_table.append('<tr class="brands-edit-tablerow" name="brands-edit-tablerow-data">'+
                            '<td class="brands-edit-id">' + index + '</td>'+
                            '<td class="brands-edit-name">' + item + '</td>'+
                            '<td class="brands-edit-edit"><button name="edit-brand"><i class="fa fa-pencil"</button></td>'+
                            '<td class="brands-edit-remove"><button name="remove-brand" onclick="remove_brand_button_handler(' + index + ')"><i class="fa fa-close"></button></i></td>'+
                            '</tr>');
                    });

                    <?php echo $edition_mode ? '$(\'option:contains("'.$product->getBrand(). '")\').attr("selected", true);' : ''?>
                }
            }, function(response){

            });
        }

        function get_categories(){
            CategoriesUtil.retrieve_associated(function(response){
                var $category_select = $("#category-select");
                var $sub_category_select = $("#subcategory-select");

                if (response.payload.categories != null){
                    $.each(response.payload.categories, function(index, item){
                        var category_id = index;
                        var item_length = Object.keys(item).length;
                        var category_name = item["name"];

                        for (var i = 0; i < (item_length - 1); i++){
                            var sub_category_array = item[i];
                            var sub_category_id = sub_category_array["id"];
                            var sub_category_name = sub_category_array["name"];

                            $sub_category_select.append('<option value="' + sub_category_id + '" data-category_id="' + category_id + '" >' + sub_category_name + '</option>');
                        }

                        $category_select.append($('<option>', {
                            value: category_id,
                            text : category_name
                        }));

                        if ((parseInt(index) + 1) == Object.keys(response.payload.categories).length){
                            <?php
                            echo $edition_mode ? '$(\'#category-select\').find(\'option[value="'.$product->getCategory().'"]\').attr("selected", true);' : '';
                            echo $edition_mode ? '$(\'#subcategory-select\').find(\'option[value="'.$product->getSubCategory(). '"]\').attr("selected", true);' : '';
                            ?>
                        }
                    });
                }

                $category_select.append($('<option>', {
                    value: -1,
                    text : "Non référencé"
                }));
            }, function(response){

            });
        }

        function get_regions(){
            RegionsUtil.retrieve(function(response){
                var $regions_select = $("#region-select");
                if (response.payload.regions != null){
                    var iterator = 0;
                    $.each(response.payload.regions, function(index, item){
                        $regions_select.append($('<option>', {
                            value: index,
                            text : item
                        }));

                        <?php
                            if ($edition_mode){
                                echo 'if ((iterator + 1) == Object.keys(response.payload.regions).length){
                                    $(\'#region-select\').find(\'option[value="'.$product->getRegion().'"]\').attr("selected", true);
                                }';
                            }
                        ?>
                        iterator++;
                    });
                }
            }, function(response){

            });
        }


        function on_change_select_category(){
            var $category_select = $("#category-select");

            $category_select.change(function(){
                var category_id = this.value;

                $("#subcategory-select").attr("disabled", category_id == -1);

                var $eq_subs = $("#subcategory-select > option[data-category_id='" + category_id + "']");
                if ($eq_subs.length > 0) {
                    $eq_subs[0].selected = true;
                }

                $eq_subs.css("display", "block");

                $("#subcategory-select > option[data-category_id!='" + category_id + "']").css("display", "none");
            });
        }

        function trigger_init_category(){
            var $cat_options = $("#category-select > option");
            if ($cat_options.length > 0){
                $cat_options.parent().val($cat_options[0].value).trigger("change");
            }
        }

        function add_product_form_handler(){
            var $add_product_form = $(".add-product-form");

            $add_product_form.submit(function(e){
                e.preventDefault();

                var url = "/dashboard/router.php";

                $.ajax({
                    url:url,
                    data:new FormData($add_product_form[0]),
                    type:'POST',
                    contentType: false,
                    processData: false
                }).done(function(response){
                    response = JSON.parse(response);
                    switch (response.status){
                        case "ok":
                            info_notification(response.payload.msg);
                            break;

                        case "error":
                            error_notification(response.payload.error);
                            break;
                    }
                });
            });
        }

        function showNewBrandForm()
        {
            $("#new-brand-form-container").css(
                {
                    "display":"block"
                }
            );
        }

        function hideNewBrandForm()
        {
            $("#new-brand-form-container").css(
                {
                    "display":"none"
                }
            );
        }

        function confirmNewBrand()
        {
            var url = "/dashboard/products/REST_add_brand.php";
            var $new_brand_name = $('[name="new_brand_name"]');
            var $brands_select = $("#brand-select");

            hideNewBrandForm();

            ProductsUtil.add_brand($new_brand_name.val(), function(response){
                info_notification("Marque ajoutée avec succès !");

                var generated_id = response.payload.id;
                $brands_select.append($('<option>', {
                    value: generated_id,
                    text : $new_brand_name.val()
                }));
            }, function(response){
                error_notification(response.payload.error);
            });
        }

        function remove_brand_button_handler(brand_id){
            var confirmation = confirm("Voulez vous supprimer cette marque ?");
            if (confirmation) {
                ProductsUtil.remove_brand(brand_id, function(response){
                    get_brands();
                }, function(response){

                });
            }
        }

        $(document).ready(function(){
            get_brands();
            get_categories();
            get_regions();
            on_change_select_category();

            <?php
                if (!$edition_mode){
                    echo 'setTimeout(function(){
                        trigger_init_category();
                    }, 1000);';
                }
            ?>

            add_product_form_handler();
        });
    </script>
</form>
