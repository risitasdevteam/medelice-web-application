<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 07/11/2017
 * Time: 09:41
 */

require_once(__DIR__ . "/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__ . '/../includes/admin_min.php');
include(__DIR__ . '/../includes/data_handler.php');
include(__DIR__ . '/../includes/products.php');

try{
    post_thrown();

    $valisette_ref = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'ref', RegexUtil::box_ref());
    $valisette_name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    $valisette_region = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'region', RegexUtil::region_id());

    $valisette = new Valisette($valisette_ref);
    $valisette->region = $valisette_region;
    $valisette->title = $valisette_name;

    if (Valisette::add($valisette)){
        $response = new JsonResponse(JsonResponseStatusType::OK);
        $response->transmit();
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}