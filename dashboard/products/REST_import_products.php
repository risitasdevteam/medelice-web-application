<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 27/08/2018
 * Time: 04:03
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

try
{
    $provider = new DBProductProvider();

    $provider->insert_from_spreadsheet();
}
catch(Exception $e){
    exception_to_json_payload($e);
}