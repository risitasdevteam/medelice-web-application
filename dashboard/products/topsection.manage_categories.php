<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 25/07/2018
 * Time: 10:13
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>


<script src="/dashboard/js/Categories.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Catégories</span>
</div>
<div class="manage-categories-container">
    <div class="categories-container">
        <div class="manage-cat-title-container">
            <div class="manage-cat-title roboto bold">Gérer les catégories globales</div>
            <button class="manage-cat-add-btn button roboto" onclick="showNewCategoryForm()"><i class="fa fa-plus manage-cat-btn-icon"></i>Ajouter une catégorie</button>
        </div>
        <div class="manage-cat-display-container">
            <table class="manage-cat-table">
                <tr class="manage-cat-tablerow roboto bold">
                    <td class="manage-cat-tabledata manage-cat-id">ID</td>
                    <td class="manage-cat-tabledata manage-cat-name">Nom</td>
                    <td class="manage-cat-tabledata manage-cat-edit"></td>
                    <td class="manage-cat-tabledata manage-cat-remove"></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="add-cat-form-container" id="categories-form">
        <div class="add-category-form">
            <div class="add-category-form-title-container roboto bold">
                <span class="add-category-form-title">Ajouter une nouvelle catégorie</span>
                <i class="add-category-form-close fa fa-close" onclick="hideNewCategoryForm()"></i>
            </div>
            <div class="add-category-form-field-container">
                <span class="add-category-form-field-title roboto bold">Nom</span>
                <input class="add-category-form-field" id="newcat_name" type="text">
            </div>
            <div class="add-category-form-confirm-container">
                <button type="button" class="add-category-form-confirm-button button roboto bold" onclick="addCategory()">Valider</button>
            </div>
        </div>
    </div>
    <script>

    </script>
    <div class="subcategories-container">
        <div class="manage-cat-title-container">
            <div class="manage-cat-title roboto bold">Gérer les sous-catégories</div>
            <button class="manage-cat-add-btn roboto button" onclick="showNewSubcatForm()"><i class="fa fa-plus manage-cat-btn-icon"></i>Ajouter une sous-catégorie</button>
        </div>
        <div class="manage-subcat-choosecat-container roboto">
            <span class="manage-subcat-choosecat-title" for="subdisplay-choose-cat">Choisissez une catégorie mère :</span>
            <select class="manage-subcat-choosecat-select" id="subdisplay-choose-cat">
            </select>
        </div>
        <div class="manage-cat-display-container subcat-display-container">
            <table class="manage-cat-table" id="subcat_table">
                <tr class="manage-cat-tablerow roboto bold">
                    <td class="manage-cat-tabledata manage-cat-id">ID</td>
                    <td class="manage-cat-tabledata manage-cat-name">Nom</td>
                    <td class="manage-cat-tabledata manage-cat-edit"></td>
                    <td class="manage-cat-tabledata manage-cat-remove"></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="add-cat-form-container" id="subcategories-form"> <!-- TODO WIP -->
        <div class="add-category-form">
            <div class="add-category-form-title-container roboto bold">
                <span class="add-category-form-title">Ajouter une nouvelle sous-catégorie</span>
                <i class="add-category-form-close fa fa-close" onclick="hideNewSubcatForm()"></i>
            </div>
            <div class="add-category-form-field-container">
                <span class="add-category-form-field-title">Catégorie mère : </span>
                <select id="addsub-choose-cat">
                </select>
            </div>
            <div class="add-category-form-field-container">
                <span class="add-category-form-field-title roboto bold">Nom</span>
                <input class="add-category-form-field" id="newsubcat_name" type="text">
            </div>
            <div class="add-category-form-confirm-container">
                <button type="button" class="add-category-form-confirm-button button roboto bold" onclick="addSubcategory()">Valider</button>
            </div>
        </div>
    </div>
    <script>
        function get_categories_alone(){
            var $add_sub_categories_select = $('#addsub-choose-cat');

            CategoriesUtil.retrieve(function(response){
                var $categories_table = $('.manage-cat-table[id!="subcat_table"] tbody');
                $('.manage-cat-table[id!="subcat_table"] tr').remove();
                $add_sub_categories_select.find("option").remove();

                if (response.payload.categories != null){
                    $.each(response.payload.categories, function(index, item){
                        var category_id = index;
                        var category_name = item;

                        $categories_table.append('<tr class="manage-cat-tablerow roboto">'+
                            '<td class="manage-cat-tabledata manage-cat-id">' +  category_id + '</td>'+
                            '<td class="manage-cat-tabledata manage-cat-name">' + category_name + '</td>'+
                            '<td class="manage-cat-tabledata manage-cat-edit"><button class="manage-cat-tablebutton" onclick="edit_category(' + category_id + ', false, \'' + category_name + '\')"><div class="fa fa-pencil manage-cat-table-btn-icon"></button></td>"' +
                            '<td class="manage-cat-tabledata manage-cat-remove"><button class="manage-cat-tablebutton" onclick="remove_category(' + category_id + ', false)"><div class="fa fa-close manage-cat-table-btn-icon"></div></button></td>'+
                            '</tr>');

                        $add_sub_categories_select.append($('<option>', {
                            value: category_id,
                            text : category_id + ' - ' + category_name
                        }));
                    });
                }
            }, function(response){
                error_notification(response.payload.error);
            });
        }

        function get_categories(){
            var url = "/dashboard/products/REST_get_categories_associated_with_sub.php";
            var $category_select = $("#subdisplay-choose-cat");
            var $sub_categories_table = $('.manage-cat-table[id="subcat_table"] tbody');

            CategoriesUtil.retrieve_associated(function(response){
                $('.manage-cat-table[id="subcat_table"] tr').remove();
                $category_select.find("option").remove();

                if (response.payload.categories != null){
                    $.each(response.payload.categories, function(index, item){
                        var category_id = index;
                        var item_length = Object.keys(item).length;
                        var category_name = item["name"];

                        for (var i = 0; i < (item_length - 1); i++){
                            var sub_category_array = item[i];
                            var sub_category_id = sub_category_array["id"];
                            var sub_category_name = sub_category_array["name"];

                            $sub_categories_table.append('<tr class="manage-cat-tablerow roboto" id="subcat_tr" data-category-id="' + category_id + '">'+
                                '<td class="manage-cat-tabledata manage-cat-id">' + sub_category_id + '</td>'+
                                '<td class="manage-cat-tabledata manage-cat-name">' + sub_category_name + '</td>'+
                                '<td class="manage-cat-tabledata manage-cat-edit"><button class="manage-cat-tablebutton" onclick="edit_category(' + sub_category_id + ', true, \'' + sub_category_name + '\')"><i class="fa fa-pencil manage-cat-table-btn-icon"></i></button></td>'+
                                '<td class="manage-cat-tabledata manage-cat-remove"><button class="manage-cat-tablebutton" onclick="remove_category(' + sub_category_id + ', true)"><div class="fa fa-close manage-cat-table-btn-icon"></div></button></td>'+
                                '</tr>');
                        }

                        $category_select.append($('<option>', {
                            value: category_id,
                            text : category_id + ' - ' + category_name
                        }));
                    });
                }

                $category_select.trigger("change");
            }, function(response){

            });

            $category_select.on("change", function(){
                var category_id = this.value;

                var $eq_subs = $("tr[data-category-id='" + category_id + "'][id='subcat_tr']");
                $eq_subs.show();

                $("tr[data-category-id!='" + category_id + "'][id='subcat_tr']").hide();
            });
        }

        function edit_category(id, sub, actual_name){
            var name = prompt("Veuillez saisir le nouveau nom", actual_name);
            if (name != null){
                CategoriesUtil.edit_name(id, sub, name, function(response){
                    info_notification("Catégorie renommée avec succès !");
                    refresh_tables();
                }, function(response){
                    error_notification(response.payload.error);
                });
            }
        }

        function remove_category(id, sub){
            var continue_remove = confirm('Attention ! Vous allez supprimer une catégorie, êtes vous sûrs ?');

            if (continue_remove) {
                CategoriesUtil.remove(id, sub, function(response){
                    info_notification("Catégorie supprimée avec succès !");
                    refresh_tables();
                }, function(response){
                    error_notification(response.payload.error);
                });
            }
        }

        function showNewCategoryForm()
        {
            $('#categories-form').css(
                {
                    "display":"block"
                }
            );
        }

        function hideNewCategoryForm()
        {
            $('#categories-form').css(
                {
                    "display":"none"
                }
            );
        }

        function addCategory()
        {
            var nameField = $('#newcat_name');

            CategoriesUtil.add_category(nameField.val(), function(response){
                info_notification("Catégorie ajoutée avec succès !");
                refresh_tables();
            }, function(response){
                error_notification(response.payload.error);
            });

            hideNewCategoryForm();
        }

        function showNewSubcatForm()
        {
            $('#subcategories-form').css(
                {
                    "display":"block"
                }
            );
        }

        function hideNewSubcatForm()
        {
            $('#subcategories-form').css(
                {
                    "display":"none"
                }
            );
        }

        function addSubcategory()
        {
            var parentSelect = $('#addsub-choose-cat');
            var nameField = $('#newsubcat_name');

            CategoriesUtil.add_sub_category(nameField.val(), parentSelect.val(), function(response){
                info_notification("Sous-catégorie ajoutée avec succès !");
                refresh_tables();
            }, function(response){
                error_notification(response.payload.error);
            });

            hideNewSubcatForm();
        }

        function refresh_tables(){
            get_categories_alone();
            get_categories();
        }

        $(document).ready(function() {
            refresh_tables();
        });

    </script>
</div>