<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 07/08/2018
 * Time: 01:20
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try {
    get_thrown();

    $mode = RegexUtil::filter_in_var_with_regex(INPUT_GET, 'mode', '/^(alone|associated){1}$/');

    if ($mode === "associated") {
        $query = 'select
            v.ref,
            v.region,
            v.title,
            sv.id,
            sv.title,
            sv.color,
            sv.price,
            svc.product_ref,
            p.name,
            p.price
          from products_valisettes v
            join products_sub_valisettes sv
              on v.ref = sv.ref
            join products_sub_valisettes_content svc
              on svc.ref = v.ref and svc.id = sv.id
            join products p
              on p.ref = svc.product_ref';

        $sql = SQLUtil::open("medelice_products");
        $valisettes = array();
        if ($stmt = $sql->prepare($query)) {
            $stmt->bind_result($valisette_ref,
                $valisette_region,
                $valisette_title,
                $sub_valisette_id,
                $sub_valisette_title,
                $sub_valisette_color,
                $sub_valisette_price,
                $sub_valisette_product_ref,
                $sub_valisette_product_name,
                $sub_valisette_product_price);

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                if (array_key_exists($valisette_ref, $valisettes)) {

                    if (!array_key_exists($sub_valisette_id, $valisettes[$valisette_ref]["sub"])) {
                        $valisettes[$valisette_ref]["sub"][$sub_valisette_id] = array();
                        //can't use a closure ... :/
                        //$valisettes[$valisette_ref]["title"] = $valisette_title;
                        $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["title"] = $sub_valisette_title;
                        $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["color"] = $sub_valisette_color;
                        $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["price"] = $sub_valisette_price;
                        $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"] = array();
                    }


                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref] = array();
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["name"] = $sub_valisette_product_name;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["price"] = $sub_valisette_product_price;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["img"] = (new FileIOProductProvider())->get_images($sub_valisette_product_ref)[0];

                } else {
                    $valisettes[$valisette_ref] = array();
                    $valisettes[$valisette_ref]["title"] = $valisette_title;
                    $valisettes[$valisette_ref]["sub"] = array();

                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id] = array();

                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["title"] = $sub_valisette_title;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["color"] = $sub_valisette_color;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["price"] = $sub_valisette_price;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"] = array();

                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref] = array();
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["name"] = $sub_valisette_product_name;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["price"] = $sub_valisette_product_price;
                    $valisettes[$valisette_ref]["sub"][$sub_valisette_id]["products"][$sub_valisette_product_ref]["img"] = (new FileIOProductProvider())->get_images($sub_valisette_product_ref)[0];
                }
            }

            $stmt->close();
        }

        $response = new JsonResponse(JsonResponseStatusType::OK, null);

        $response->add_payload(new JsonResponsePayload("valisettes", $valisettes));
        $response->transmit();
    } else {
        $query = 'select *
          from products_valisettes v
          ';

        $sql = SQLUtil::open("medelice_products");
        $valisettes = array();
        if ($stmt = $sql->prepare($query)) {
            $stmt->bind_result($valisette_ref,
                $valisette_region,
                $valisette_title);

            $stmt->execute();

            while ($row = $stmt->fetch()) {
                $valisettes[$valisette_ref] = array();
                $valisettes[$valisette_ref]["region"] = $valisette_region;
                $valisettes[$valisette_ref]["title"] = $valisette_title;
                $valisettes[$valisette_ref]["img"] = Valisette::get_preview_img_from_region($valisette_region);
            }

            $stmt->close();
        }

        $response = new JsonResponse(JsonResponseStatusType::OK, null);

        $response->add_payload(new JsonResponsePayload("valisettes", $valisettes));
        $response->transmit();
    }
}
catch (Exception $e){
    exception_to_json_payload($e);
}