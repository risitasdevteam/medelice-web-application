<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/08/2018
 * Time: 17:22
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/products.php');

try{
    get_thrown();

    $provider = new DBProductProvider();
    $gaps = $provider->get_stock_gaps(8);

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("gaps", $gaps)))->transmit();
}
catch(Exception $e)
{
    exception_to_json_payload($e);
}