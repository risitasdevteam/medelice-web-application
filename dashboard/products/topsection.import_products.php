<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 25/08/2018
 * Time: 23:13
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<div class="import-products-container">
    <div class="import-products-box">
        <div class="bold import-products-title">Importer des produits depuis un CSV/XLS</div>
        <input type="file" name="csv_file">
    </div>
</div>
