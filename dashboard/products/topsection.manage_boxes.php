<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 01/08/2018
 * Time: 14:30
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<script src="/dashboard/js/Products.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Valisettes</span>
</div>
<div class="valisettes-info-container">
    <div class="valisettes-info-vlist-container">
        <div class="valisettes-info-list-title roboto bold">Valisettes</div>
        <div class="valisettes-info-vlist">
        </div>
        <script>
            function clearValisetteListSelect()
            {
                $('.valisettes-info-vlist-item').removeClass('valisettes-info-selected');
            }
        </script>
    </div>
    <div style="height: 100%; float: left; margin-left: 1%; display: flex; align-items: center; color: #bfbfbf; font-size: 3.5vw;">
        <i class="fa fa-caret-right"></i>
    </div>
    <div class="valisettes-info-right-container">
        <div style="width: 100%; ">
            <div class="fields-line">
                <div class="field-container" style="width: 65%;">
                    <label class="field-descriptor" for="vname">Nom de la valisette :</label><br>
                    <input class="field-input" id="vname" type="text" style="width: 100%;" value="Valisette Bretagne">
                </div>
                <div class="field-container last-container">
                    <label class="field-descriptor" for="reg-select">Région :</label><br>
                    <select id="region-select" class="custom-select custom-select-responsive" style="margin-left: 0.5vw; width: 13vw">
                    </select>
                </div>
            </div>
            <div class="field-line-separator"></div>
            <div class="fields-line">
                <div class="field-container">
                    <label class="field-descriptor" for="sub-select">Sous-valisette :</label><br>
                    <select id="sub-select" class="custom-select custom-select-responsive" style="margin-left: 0.5vw; width: 12vw">
                        <option value="S">Petite</option>
                        <option value="M">Moyenne</option>
                        <option value="L">Grande</option>
                    </select>
                    <button class="field-button button" id="edit_subv_btn" onclick="showEditSubvsForm();"><i class="fa fa-pencil"></i></button>
                    <button class="field-button button" id="new_subv_btn" onclick="showNewSubvsForm();"><i class="fa fa-plus"></i></button>
                    <button class="field-button button" id="del_subv_btn" onclick="alert('Voulez-vous vraiment supprimer la sous-valisette SM de la valisette V_BRE ?');"><i class="fa fa-close"></i></button>
                </div>
                <div class="field-container">
                    <label class="field-descriptor" for="sub_price">Prix (€) :</label><br>
                    <input class="field-input" type="number" style="width: 7vw" id="sub_price" step="0.01" value="0.00" disabled>
                </div>
            </div>
            <div style="width: 100%; font-size: 1.0vw" class="fields-line roboto bold">Articles :</div>
        </div>
        <div class="valisettes-info-content-display">
        </div>
        <div class="valisettes-info-content-bottom-container">
            <button type="button" class="valisettes-info-content-button green-btn button quicksand bold">Enregistrer la valisette</button>
            <button type="button" class="valisettes-info-content-button red-btn button quicksand bold">Supprimer la valisette</button>
        </div>
        <?php include(__DIR__ . '/add_subv.inc.php'); ?>
        <script>
            var currentValisette = "V_BRE"; // à mettre ailleurs et être changé lors du choix de la valisette dans le panneau latéral gauche
            var currentSubvalisette = "SM"; // à mettre à jour lors du changement de sous-valisette (select)

            $('#edit_subv_btn').click(function () {
                showEditSubvsForm(currentValisette, currentSubvalisette);
            });

            $('#new_subv_btn').click(function () {
                showNewSubvForm(currentValisette);
            });
        </script>
    </div>

    <script src="/dashboard/js/region-select.js"></script>
    <script>
        var valisettes_associated_json = {};

        function left_bar_click_handler(){
            $('.valisettes-info-vlist-item').on("click", function(){
                clearValisetteListSelect();
                var $valisette_region = $(this).attr("id");
                var $valisette_ref = $(this).data("ref");
                var $valisette_title = $(this).find(".valisettes-info-vlist-name");
                var $right_group_valisette_title_input = $('#vname');
                var $sub_valisette_select = $('#sub-select');

                currentValisette = $valisette_ref;

                $('#' + $valisette_region ).addClass('valisettes-info-selected');
                $right_group_valisette_title_input.val($valisette_title [0].textContent);
                $('#region-select option[value="' + $valisette_region + '"]').prop('selected', true);

                if (Object.keys(valisettes_associated_json).length > 0 && $valisette_ref in valisettes_associated_json){
                    var sub_array = valisettes_associated_json[$valisette_ref]["sub"];
                    $('#sub-select option').remove();

                    $.each(sub_array, function(index, item){
                        var sub_valisette_id = index;
                        var sub_valisette_title = item["title"];

                        $sub_valisette_select.append($('<option>', {
                            value: sub_valisette_id,
                            text : sub_valisette_title
                        }));
                    });

                    $sub_valisette_select.trigger("change");
                }
            });
        }

        function sub_valisette_select_change_handler(){
            $sub_valisette_select = $('#sub-select');
            $sub_valisette_select.on("change", function(){
                var sub_valisette_id = $(this).val();
                //var $sub_valisette_price_input = $('#sub_price');
                var $sub_valisette_products_list = $('.valisettes-info-content-display');

                $sub_valisette_products_list.find(".no-products-found").remove();

                if (Object.keys(valisettes_associated_json).length > 0 && currentValisette in valisettes_associated_json){
                    var sub_array = valisettes_associated_json[currentValisette]["sub"];
                    var price = sub_array[sub_valisette_id]["price"];
                    var products = sub_array[sub_valisette_id]["products"];
                    var title = sub_array[sub_valisette_id]["title"];
                    var color = sub_array[sub_valisette_id]["color"];
                    var valisette_title = valisettes_associated_json[currentValisette]["title"];

                    //$sub_valisette_price_input.val(price);
                    $('.valisettes-info-content-product').remove();
                    $("#new_subv_id").val($('#sub-select').val());
                    $("#new_subv_color").val('#' + color);
                    $("#new_subv_price").val(price);
                    $("#new_subv_name").val(valisette_title);

                    var iterator = 0;
                    $.each(products, function(index, item){
                        var product_ref = index;
                        var product_name = item["name"];
                        var product_price = item["price"];
                        var product_img_url = item["img"];

                        $sub_valisette_products_list.append('<div class="valisettes-info-content-product" style="float: ' + (iterator % 2 == 0 ? 'left' : 'right') + ';">'+
                            '<div class="valisettes-info-content-product-image fill-image-container">'+
                            '<img class="fill-image" src="' + product_img_url + '">'+
                            '</div>'+
                            '<div class="valisettes-info-content-product-text quicksand">'+
                            '<span class="bold">' + product_name + '</span><br>'+
                        '<span class="valisettes-info-content-product-ref">' + product_ref + '</span>'+
                            '<span class="valisettes-info-content-product-price montserrat bold">' + product_price +'€</span>'+
                        '</div>'+
                        '</div>');

                        iterator++;
                    });
                }
                else{
                    $('.valisettes-info-content-product').remove();
                    $sub_valisette_products_list.append('<h3 class="no-products-found">Aucun produit trouvé</h3>');
                }
            });
        }

        function get_valisettes_alone(){
            var $left_bar_valisettes_list = $('.valisettes-info-vlist');
            ValisettesUtil.retrieve_alone(function(response){
                if (Object.keys(response.payload.valisettes).length > 0){
                    var iterator = 0;
                    $.each(response.payload.valisettes, function(index, item){
                        var valisette_ref = index;
                        var region = item["region"];
                        var title = item["title"];
                        var img = item["img"];

                        $left_bar_valisettes_list.append('<div class="valisettes-info-vlist-item" id="' + region /* $valisette = temporaire */ + '" data-ref="' + valisette_ref + '">'+
                            '<input type="hidden" value="' + region + '" name="region">'+
                            '<div class="valisettes-info-vlist-image fill-image-container">'+
                            '<img class="fill-image" src="' + img + '">'+
                            '</div>'+
                            '<div class="valisettes-info-vlist-name quicksand bold">'+
                            title+
                            '</div>'+
                            '</div>');

                        iterator++;
                        if (iterator == (Object.keys(response.payload.valisettes).length - 1)){
                            left_bar_click_handler();
                        }
                    });
                }
            }, function(response){

            });
        }

        function get_valisettes_associated_with_sub(){
            ValisettesUtil.retrieve_associated(function(response){
                if (Object.keys(response.payload.valisettes).length > 0){
                    valisettes_associated_json = response.payload.valisettes;
                    console.log(JSON.stringify(valisettes_associated_json));
                }
            }, function(response){

            });
        }

        $(document).ready(function() {
            get_valisettes_alone();

            get_valisettes_associated_with_sub();
            sub_valisette_select_change_handler();
        });
    </script>
</div>