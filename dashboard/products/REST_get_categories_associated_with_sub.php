<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 01/08/2018
 * Time: 16:35
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../includes/products.php');

try{
    get_thrown();

    $mode = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'mode', FILTER_SANITIZE_STRING);

    $sql = SQLUtil::open("medelice_products");

    $categories_array = array();
    switch ($mode){
        case 'alone':
            $query = "select * from products_categories ";
            if ($stmt = $sql->prepare($query)){
                $stmt->bind_result($category_id, $category_name);
                $stmt->execute();

                while ($row = $stmt->fetch()){
                    $categories_array[$category_id] = $category_name;
                }
            }
            break;

        default:
            $query = "select c.id, sc.subc_id, c.name, sc.subc_name from products_categories c join products_sub_categories sc where c.id = sc.c_id";
            if ($stmt = $sql->prepare($query)){
                $stmt->bind_result($category_id, $sub_category_id, $category_name, $sub_category_name);
                $stmt->execute();

                while ($row = $stmt->fetch()){
                    $subcategory = new SubCategory();
                    $subcategory->setId($sub_category_id);
                    $subcategory->setName($sub_category_name);

                    $subcategory->setParentCategoryId($category_id);

                    if (!array_key_exists($category_id, $categories_array)){
                        $categories_array[$category_id] = array();
                        $categories_array[$category_id]["name"] = $category_name;

                    }
                    array_push($categories_array[$category_id], $subcategory->toArray());
                }
                $stmt->close();
            }

            break;
    }

    //var_dump($categories_array);
    SQLUtil::close($sql);
    $response = new JsonResponse(JsonResponseStatusType::OK);
    $response->add_payload(new JsonResponsePayload("categories", $categories_array));

    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}