<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 23/08/2018
 * Time: 13:49
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<script src="/dashboard/js/Products.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Stocks</span>
</div>
<div style="display: none;">
    <span>Trier par</span>
    <select class="custom-select custom-select-responsive" id="sort-change">
        <option>Nom</option>
        <option>Type</option>
        <option>Catégorie</option>
        <option>Niveau d'alerte</option>
    </select>
</div>
<div style="width: 95%; margin: 3em 2.5%; display: flex; justify-content: space-between; flex-wrap: wrap" class="stock-gaps-container">
    <?php

    ?>
</div>
<script>
    function retrieve(){
        var $container = $('.stock-gaps-container');

        ProductsUtil.retrieve_stock_gaps(function(response){
            if (Object.keys(response.payload.gaps).length > 0){
                $.each(response.payload.gaps, function(index, item){
                   var ref = item["ref"];
                   var stock = item["stock"];

                    $container.append('<div style="width: 18em; height: 8em; background-color: white; border-right: 0.1em solid ' + (stock > 15 ? 'limegreen' : (stock < 5 ? 'red' : 'orange')) + '; margin: 1.5em 0.2em; display: inline-block;">'+
                    '<div class="fill-image-container left" style="width: 7em; margin: 0.45em; height: 7em; border: 0.05em dotted #d6d6d6;">'+
                        '<img class="fill-image" src="/images/produits/BRETA17.jpg">'+
                        '</div>'+
                        '<div class="right" style="height: 7em; text-align: right; width: 8.5em; margin: 0.5em 1em 0.5em 0.5em; position: relative;">'+
                        '<span style="color: #bababa; font-size: 1.2em;">' + ref + '</span>'+
                        '<span style="position: absolute; bottom: 0; color: #bababa; font-size: 2.5em; right: 0;">' + stock + '</span>'+
                        '</div>'+
                        '</div>');
                });
            }
        }, function(){

        });
    }

    function sort_by_name(a, b) {
        var sa = a.innerHTML.toLowerCase(), sb = b.innerHTML.toLowerCase();
        return sb<sa ? -1 : sb>sa ? 1 : 0;
    }

    function sort_handler(){
        var select = $('#sort-change');
        select.on("change", function(){

        });
    }

    $(document).ready(function(){
       retrieve();
    });
</script>