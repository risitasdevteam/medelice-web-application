<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 20/08/2018
 * Time: 23:37
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');

try{
    post_thrown();

    $brand_id = filter_input(INPUT_POST, 'brand_id', FILTER_SANITIZE_NUMBER_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));
    if (!$brand_id || !isset($brand_id)){
        throw new MedeliceException("");
    }

    $sql = SQLUtil::open("medelice_products");
    $prepared_query = 'delete from products_brands where id = ?';
    if ($stmt = $sql->prepare($prepared_query)){
        $stmt->bind_param("s", $brand_id);
        $stmt->execute();
        $stmt->close();
    }

    SQLUtil::close($sql);

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Marque supprimée avec succès !"));
    $response->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}