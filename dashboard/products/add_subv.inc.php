<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 02/08/2018
 * Time: 18:41
 */

require_once(__DIR__ . "/../../utility/PageDirectAccessGuardThrow.php");
?>


<div class="edit-subv-form-container overlay-container" style="align-items: center; justify-content: center;">
    <div class="edit-subv-form">
        <input type="hidden" id="mode">
        <div class="new-brand-form-title">
            <span class="roboto bold left" id="edit-subv-form-title">Nouvelle sous-valisette</span>
            <i class="fa fa-close right button" onclick="hideEditSubvForm()"></i>
        </div>
        <div class="fields-line">
            <div class="field-container">
                <label class="field-descriptor" for="new_subv_name">Nom</label><br>
                <input class="field-input" id="new_subv_name" type="text" style="/*width: 28.5vw*/ width: 37.5vw" placeholder="ex: Petite, Moyenne...">
            </div>
            <!--div class="field-container last-container">
                <label class="field-descriptor" for="new_subv_price">Prix (€) :</label><br>
                <input class="field-input" id="new_subv_price" type="number" step="0.01" style="width: 7vw">
            </div-->
        </div>
        <div class="fields-line">
            <!--div class="field-container">
                <label class="field-descriptor" for="subv_parent">Valisette mère</label><br>
                <select class="custom-select custom-select-responsive" id="subv_parent" style="width: 15vw; margin-left: 0.5vw;">
                    <option value="V_ARA">Valisette Auvergne-Rhône-Alpes</option>
                    <option value="V_BRE">Valisette Bretagne</option>
                    <option value="V_CVL">Valisette Centre-Val de Loire</option>
                </select>
            </div-->
            <div class="field-container">
                <label class="field-descriptor" for="new_subv_id">Sous-référence</label><br>
                <input class="field-input" id="new_subv_id" type="text" style="width: 8vw" placeholder="ex: S, M, L">
            </div>
            <div class="field-container last-container">
                <label class="field-descriptor" for="new_subv_color">Couleur d'affichage :</label><br>
                <input class="field-colorpicker" id="new_subv_color" type="color">
            </div>
        </div>
        <div class="fields-line">
            <span class="field-descriptor left">Produits</span>
            <button class="edit-subv-form-add-product-btn field-button button roboto" onclick="showAddSubvProductForm()">
                <i class="fa fa-plus" style="margin-right: 0.5vw"></i>Ajouter un produit
            </button>
        </div>
        <div class="valisettes-info-content-display" id="new_subv_products_container">
            <?php

            $products = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 123, 14, 654, 98775, 561321, 5484, 6532, 31189, 516, 489, 4321, 489 ,894, 564 ,456, 7319, 98311);
            $i = 0;

            foreach($products as $product)
            {
                echo '
                        <div class="valisettes-info-content-product" style="float: '.($i % 2 == 0 ? 'left' : 'right').'; width: 49%;">
                            <div class="valisettes-info-content-product-image fill-image-container">
                                <img class="fill-image" src="/images/produits/BRETA33_1.jpg">
                            </div>
                            <div class="valisettes-info-content-product-text quicksand">
                                <span class="bold">Fricassé de ta mère</span><br>
                                <span class="valisettes-info-content-remove"><i class="fa fa-close"></i></span>
                                <span class="valisettes-info-content-product-ref">BRETA33</span>
                                <span class="valisettes-info-content-product-price montserrat bold">14.95€</span>
                            </div>
                        </div>
                    ';
                $i++;
            }

            ?>
        </div>
        <div style="width: 100%;">
            <button class="edit-subv-form-confirm-button button green-btn quicksand bold">Valider</button>
        </div>
        <div class="add-subv-product-form-container overlay-container">
            <div class="add-subv-product-form">
                <div class="add-subv-product-form-title bold">
                    <span class="left">Chercher un produit</span>
                    <i class="fa fa-close right button" onclick="hideAddSubvProductForm()"></i>
                </div>
                <div class="add-subv-product-form-field-container">
                    <input type="text" class="field-input full-field" id="add_product_searchbar" placeholder="Référence, nom, catégorie...">
                    <div class="add-subv-product-form-searchresults">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function showSubvsForm()
    {
        $('.edit-subv-form-container').css(
            {
                "display":"flex"
            }
        );
    }

    function showEditSubvsForm(parent_v, subv_id)
    {
        showSubvsForm();
        var form = $('.edit-subv-form');
        form.find('#mode').attr("value", "edit");
        form.find('#subv_parent').val(parent_v);
        form.find('#new_subv_id').attr("value", subv_id);
        $('#edit-subv-form-title').html("Éditer une sous-valisette");
    }

    function showNewSubvForm(parent_v)
    {
        showSubvsForm();
        var form = $('.edit-subv-form');
        form.find('#mode').attr("value", "new");
        form.find('#subv_parent').val(parent_v);
        form.find('#new_subv_id').attr("value", "");
        $('#edit-subv-form-title').html("Nouvelle sous-valisette");
    }

    function hideEditSubvForm()
    {
        $('.edit-subv-form-container').css(
            {
                "display":"none"
            }
        );
    }

    function showAddSubvProductForm()
    {
        $('.add-subv-product-form-container').css(
            {
                "display":"block"
            }
        );
    }

    function hideAddSubvProductForm()
    {
        $('.add-subv-product-form-container').css(
            {
                "display":"none"
            }
        );
    }

    function sub_valisette_add_product_button_handler(){
        var $button = $('[name="search-add-button"]');

        $button.on("click", function(){
            var ref = $button.parent().find(".valisettes-info-content-product-ref").text();
            var sub_valisette_id = $('#new_subv_id').val();
            var valisette_id = $('.valisettes-info-vlist-item.valisettes-info-selected').data("ref");

            addSubvProduct(ref, sub_valisette_id, valisette_id);
        });
    }

    function addSubvProduct(ref, sub_valisette_id, valisette_id)
    {
        var provider = new RESTResponseProvider();
        provider.url = "/dashboard/router.php";
        provider.classic_callback = true;

        provider.data = {
            page: 'sub_valisettes_product_editor',
            mode: 'add',
            ref: ref,
            sub_valisette_id: sub_valisette_id,
            valisette_id: valisette_id
        };

        provider.http_post(function(response){
            $('#sub-select').trigger("change");
        }, function(response){

        });

        hideAddSubvProductForm();
    }

    function searchbar_handler(){
        var query = $("#add_product_searchbar").val();

        var provider = new RESTResponseProvider();
        provider.url = "/search.php";
        provider.classic_callback = false;
        provider.data = {query: query};

        provider.http_post(function(response){
            var products = response.payload.products;
            var products_length = products.length;
            var alone = products_length < 2;

            var $results_container = $('.add-subv-product-form-searchresults');
            $('.add-subv-product-form-searchresults-entry').remove();
            $('.add-subv-product-form-searchresults-no-entry').remove();

            if (products_length > 0) {
                $.each(products, function(index, product){
                    var ref = product.ref;
                    var name = product.name;
                    var price = product.price;
                    var img = product.img;

                    $results_container.append(
                        '<div class="add-subv-product-form-searchresults-entry">'+
                            '<div class="fill-image-container left add-subv-product-form-searchresults-entry-imgcontainer">'+
                                '<img src="' + img + '" class="fill-image">'+
                            '</div>'+
                            '<div class="add-subv-product-form-searchresults-entry-text right">'+
                            '<div class="bold">' + name + '</div>'+
                            '<div class="valisettes-info-content-product-ref">' + ref + '</div>'+
                            '<div class="add-subv-product-form-searchresults-entry-price montserrat bold">' + price + '€</div>'+
                            '<button name="search-add-button" class="add-subv-product-form-searchresults-entry-button button green-btn quicksand bold">Ajouter à la valisette</button>'+
                            '</div>'+
                        '</div>');


                    if(index != products_length && !alone)
                        $results_container.append("<hr style='margin: 0.8vw 0'>");
                });

                sub_valisette_add_product_button_handler();
            }else{
                $results_container.append("<span class='add-subv-product-form-searchresults-no-entry' style='font-size: 1vw; font-weight: bold'>Aucun produit trouvé.</span>");
            }
        });
    }

    $('#add_product_searchbar').keyup(
        function () {
            setTimeout(function () {
                $('.add-subv-product-form-searchresults').css(
                    {
                        "display":"block"
                    }
                );
                searchbar_handler();
                sub_valisette_add_product_button_handler();
            }, 500);
        }
    );

    $(document).ready(function(){
       //sub_valisette_add_product_button_handler();
    });
</script>
