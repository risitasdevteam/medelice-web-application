<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 21:06
 */
require_once(__DIR__ . "/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__ . '/../includes/products.php');

?>

<script src="/dashboard/js/Products.js"></script>
<script src="/dashboard/js/Regions.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Nouvelle valisette</span>
</div>
<form class="valisette-data-form" style="width: 35%; margin: 12vw 32.5%; padding: 1vw; background-color: white;">
    <div class="fields-line no-top-margin">
        <div class="field-container" style="width: 100%;">
            <label class="field-descriptor" for="box-name">Nom</label><br>
            <input type="text" class="field-input" id="box-name" name="name" style="width: 98.5%; margin-left: 1.5%;">
        </div>
    </div>
    <div class="fields-line">
        <div class="field-container">
            <label class="field-descriptor" for="box-ref">Référence</label><br>
            <input type="text" class="field-input" id="box-ref" name="ref" style="width: 7.5vw;">
        </div>
        <div class="field-container last-container">
            <label class="field-descriptor" for="region-select">Région</label><br>
            <select class="custom-select custom-select-responsive" id="region-select" name="region" style="width: 14vw; margin-left: 0.5vw"></select>
        </div>
    </div>
    <button type="submit" class="button orange-btn quicksand bold" style="height: 2.5vw; width: 100%; border-radius: 0.2vw; font-size: 1vw; border: none; margin: 1.5vw 0 0;">Ajouter cette valisette</button>
</form>
<script>
    function add(){
        $('.valisette-data-form').submit(function(e){
            e.preventDefault();

            var ref = $('[name="ref"]').val();
            var region = $('[name="region"]').val();
            var name = $('[name="name"]').val();

            ValisettesUtil.add(ref, name, region, function(response){
                info_notification("Valisette ajoutée avec succès, vous pouvez désormais ajouter des sous valisettes !");
            }, function(response){
                error_notification(response.payload.error);
            });
        });
    }

    $(document).ready(function(){
        add();
    });
</script>
<script src="/dashboard/js/region-select.js"></script>
