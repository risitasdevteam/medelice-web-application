<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/08/2018
 * Time: 16:57
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
require_once(__DIR__.'/../../produits/ProductsCategories.class.php');

try{
    post_thrown();

    $mode = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'mode', FILTER_SANITIZE_STRING, true);
    $type = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'type', FILTER_SANITIZE_STRING, true);
    $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_NUMBER_INT, array('flags' => FILTER_FLAG_ALLOW_OCTAL));

    $switch_type = function($category_function, $subcategory_function) use ($type) {
        switch ($type){
            case 'category':
                call_user_func($category_function);
                break;

            case 'subcategory':
                call_user_func($subcategory_function);
                break;

            default:
                throw new MedeliceException("Bad method");
                break;
        }
    };

    switch ($mode){
        case 'edit':
            $name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'name', FILTER_SANITIZE_STRING, true);

            $switch_type(function() use ($id, $name){
                ProductsCategories::update_name($id, $name);
            }, function() use ($id, $name){
                ProductsCategories::update_sub_name($id, $name);
            });
            break;

        case 'remove':
            $switch_type(function() use ($id){
                $category = new ProductsCategories($id);
                ProductsCategories::remove($category);
            }, function() use ($id){
                ProductsCategories::remove_sub($id);
            });
            break;

        case 'add':
            $name = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'name', FILTER_SANITIZE_STRING, true);

            $switch_type(function() use ($name){
                ProductsCategories::add($name);
            }, function() use ($name, $id){
                ProductsCategories::add_sub($name, $id);
            });
            break;

        default:
            throw new MedeliceException("Bad method");
            break;
    }

    (new JsonResponse(JsonResponseStatusType::OK))->transmit();
}
catch(Exception $e){
   exception_to_json_payload($e);
}