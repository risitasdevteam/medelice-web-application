<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 21/08/2018
 * Time: 19:34
 *
 * REST Script Router
 */
require_once(__DIR__."/../utility/PageDirectAccessGuard.php");
include(__DIR__.'/includes/data_handler.php');

try{
    switch ($_SERVER['REQUEST_METHOD']){
        case 'GET':
            $page = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, "page", FILTER_SANITIZE_STRING, true);
            break;
        case 'POST':
            $page = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "page", FILTER_SANITIZE_STRING, true);
            break;

        default:
            throw new MedeliceException("Wrong method");
            break;
    }


    switch ($page){
        case "sub_valisettes_product_editor":
            include_once(__DIR__ . '/products/REST_sub_valisettes_product_editor.php');
            break;

        case "orders_getter":
            include_once(__DIR__.'/orders/REST_orders_getter.php');
            break;

        case "order_status_edit":
            include_once(__DIR__.'/orders/REST_order_edit_status.php');
            break;

        case "user_edit":
            include_once(__DIR__.'/users/REST_edit_user.php');
            break;

        case "user_remove":
            include_once(__DIR__.'/users/REST_delete_user.php');
            break;

        case "user_password_reset":
            include_once(__DIR__.'/users/REST_reset_password.php');
            break;

        case "retrieve_stock_gaps":
            include_once(__DIR__.'/products/REST_get_risked_stock.php');
            break;

        case "user_retrieve_login_attempts":
            include_once(__DIR__.'/users/REST_get_login_attempts.php');
            break;

        case "login":
            include_once(__DIR__."/REST_login.php");
            break;

        case "toggle-maintenance":
            include(__DIR__."/config/REST_maintenance.php");
            break;

        case "retrieve-generic-pages":
            include(__DIR__."/config/REST_generic_pages.php");
            break;

        case "retrieve-valisettes":
            include(__DIR__."/products/REST_get_valisettes.php");
            break;

        case "category-editor":
            include(__DIR__."/products/REST_edit_categories.php");
            break;

        case "retrieve-categories":
            include(__DIR__."/products/REST_get_categories_associated_with_sub.php");
            break;

        case "retrieve-regions":
            include(__DIR__."/products/REST_get_regions.php");
            break;

        case "retrieve-products-by-region":
            include(__DIR__."/products/REST_get_products_by_region.php");
            break;

        case "retrieve-brands":
            include(__DIR__."/products/REST_get_brands.php");
            break;

        case "product-editor":
            include(__DIR__."/products/REST_product_editor.php");
            break;

        case "add-brand":
            include(__DIR__."/products/REST_add_brand.php");
            break;

        case "remove-brand":
            include(__DIR__."/products/REST_remove_brand.php");
            break;

        case "add-valisette":
            include(__DIR__ . "/products/REST_add_valisette.php");
            break;

        case "retrieve-users":
            include(__DIR__."/users/REST_get_users.php");
            break;

        default:
            throw new MedeliceException("Wrong method");
            break;
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}