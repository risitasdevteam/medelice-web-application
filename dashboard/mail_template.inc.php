<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 18/08/2018
 * Time: 16:23
 */

require_once(__DIR__.'/../utility.inc.php');

require_once(__DIR__ . '/../utility/Mail.php');

$mail = new Mail();


$body = '
    <div style="width: 100%; font: 14px Arial, sans-serif;">
        Jules, cliquez sur le lien ci-dessous pour réinitialiser votre mot de passe.<br>
        <br>
        <a href="http://med/index.php?page=LIENGENERÉ">http://med/LIENGENERÉ</a><br>
        <br>
        <br>
        <br>
        Cet email vous a été envoyé suite à votre demande de réinitialisation de mot de passe. Si vous n\'êtes pas à l\'origine de cette action, ignorez tout simplement ce mail, ou contactez notre service technique si vous en ressentez le besoin à l\'adresse suivante :<br>
        <a href="http://med/index.php?page=contact">http://med/index.php?page=contact</a><br>
        <br>
        <br>
        Identifiant client : 04d8f7xf6<br>
        <br>
        <br>
    </div>
    <div style="color: #9b9b9b; padding-top: 15px; border-top: 1px solid #eaeaea; font: 14px Arial, sans-serif;">
        Ce message vous a été envoyé d\'une adresse qui ne peut recevoir d\'emails. Merci de ne pas y répondre.
    </div>
';



$mail->setBody($body);
$mail->setSubject("Compte validé avec succès");
$mail->setTo("romain.galle@outlook.fr");
$mail->setMailConfiguration(new DevteamMailConfiguration());

$sender = new MailSender();
$sender->send($mail, array('/{content}/'), array($mail->getBody()));

?>