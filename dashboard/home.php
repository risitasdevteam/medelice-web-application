<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/07/2018
 * Time: 12:36
 *
 * Description : Main dashboard page, things to display :
 *                  - Statistics (center, data retrieved by REST api)
 *                      - Income
 *                      - Orders
 *                      - Connected users within 24h
 *                  - Dock / Tools shelve
 *                      - Products manager
 *                          - Product by unity
 *                              - Add
 *                              - Edit
 *                              - Remove
 *                          - Valisette
 *                              - Add Edit Remove
 *                      - Orders manager
 *                              - AER
 *                      - Customer manager
 *                              - AER
 *                      - Communication / Mails sender
 *                      - Website configuration
 *                          - Maintenance mode
 *                          - ...
 *
 *
 */

require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
include_once(__DIR__."/includes/admin_min.php");
require_once(__DIR__.'/Modal.php');

$modal_provider = new DBModalProvider();
?>

<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Accueil</span>
</div>
<div class="dashboard-topcards-container">
    <div class="dashboard-topcard blue-card quicksand bold">
        Ventes<br>
        <span class="dashboard-topcard-value"><?php echo $modal_provider->get_current_month_sales() ?></span>
        <span class="dashboard-topcard-bottomtext">ce mois-ci</span>
    </div>
    <div class="dashboard-topcard green-card quicksand bold">
        Recettes<br>
        <span class="dashboard-topcard-value"><?php echo $modal_provider->get_current_month_balance() ?> €</span>
        <span class="dashboard-topcard-bottomtext">ce mois-ci</span>
    </div>
    <div class="dashboard-topcard orange-card quicksand bold">
        Utilisateurs<br>
        <span class="dashboard-topcard-value"><?php echo User::get_customers_count() ?></span>
        <span class="dashboard-topcard-bottomtext">au total</span>
    </div>
    <div class="dashboard-topcard red-card quicksand bold">
        Utilisation CPU<br>
        <span class="dashboard-topcard-value"><?php echo $modal_provider->get_last_five_minutes_cpu_overload()."%" ?></span>
        <span class="dashboard-topcard-bottomtext">ces 5 dernières minutes</span>
    </div>
</div>
<div class="home-upper-charts-section">
    <div class="multi-display-chart-section">
        <div class="mdc-header">
            <div class="mdc-title">
                <span class="quicksand bold">Statistiques globales</span><br>
                <span class="mdc-subtitle">Ventes</span>
            </div>

            <div class="btn-toolbar mt-2" id="mdc-btngroup" role="toolbar" aria-label="Graph toolbar">
                <div class="btn-group mr-2" role="group" aria-label="Select data">
                    <button type="button" class="btn btn-secondary mdc-btn" id="sells-btn" onclick="switchChart('datatype', dataTypes.SELLINGS)"><i class="fa fa-shopping-cart"></i></button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="income-btn" onclick="switchChart('datatype', dataTypes.INCOME)"><i class="fa fa-euro"></i></button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="users-btn" onclick="switchChart('datatype', dataTypes.USERS)"><i class="fa fa-user"></i></button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="visits-btn" onclick="switchChart('datatype', dataTypes.VISITS)"><i class="fa fa-globe"></i></button>
                </div>
                <div class="btn-group mr-2" role="group" aria-label="Select time">
                    <button type="button" class="btn btn-secondary mdc-btn" id="today" onclick="switchChart('timeset', epochs.DAY)">Aujourd'hui</button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="thisweek" onclick="switchChart('timeset', epochs.WEEK)">Cette semaine</button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="thismonth" onclick="switchChart('timeset', epochs.MONTH)">Ce mois-ci</button>
                    <button type="button" class="btn btn-secondary mdc-btn" id="thisyear" onclick="switchChart('timeset', epochs.YEAR)">Cette année</button>
                </div>
                <div  class="btn-group" role="group" aria-label="Refresh">
                    <button type="button" class="btn btn-secondary mdc-btn" onclick="refreshChart()"><i class="fa fa-refresh"></i></button>
                </div>
            </div>

        </div>
        <canvas id="multi-display-chart"></canvas>
        <script>
            var context = document.getElementById("multi-display-chart").getContext("2d");
            var dataTypes = Object.freeze({ SELLINGS:'sells', INCOME:'income', USERS:'users', VISITS:'visits' });
            var epochs = Object.freeze({ DAY:'today', WEEK:'thisweek', MONTH:'thismonth', YEAR:'thisyear' });
            var activeDataset = dataTypes.SELLINGS;
            var activeTimeset = epochs.YEAR;
            var chart;

            function refreshChart()
            {
                chart.destroy();
                buildChart();
            }

            function buildChart()
            {
                chart = new Chart(context, { // default chart
                    type: 'line',
                    data: {
                        labels: getXAxisFromTimeset(activeTimeset),
                        datasets: [{
                            label: getTitleFromDatatype(activeDataset),
                            backgroundColor: getColorsFromDatatype(activeDataset)[1],
                            borderColor: getColorsFromDatatype(activeDataset)[0],
                            lineTension: 0,
                            pointHitRadius:20,
                            response: getDatasetFromTimeAndDataParameters(activeTimeset, activeDataset)
                        }]
                    },
                    options:{
                        legend:[{
                            display:false
                        }],
                        tooltip:[{
                            mode:'nearest',
                            intersect:false
                        }]
                    }
                });
            }

            function refreshButtons()
            {
                $('.mdc-btn').each(
                    function () {
                        $( this ).attr("aria-pressed", 'false');
                        $( this ).removeClass('active');
                    }
                );

                $('#'+ activeDataset + '-btn').button('toggle');
                $('#'+ activeTimeset).button('toggle');
            }

            function getDatasetFromTimeAndDataParameters(timeset, datatype)
            {

                // TEMPORARY, used for debug
                switch(datatype)
                {
                    case dataTypes.SELLINGS:
                        return [0, 2, 7, 13, 17, 31, 47, 97, 59, 73, 71, 179];

                    case dataTypes.INCOME:
                        return [2000, 12100, 27500, 13240, 17010, 3101, 42117, 9337, 12529, 73015, 71010, 17219];

                    case dataTypes.USERS:
                        return [10, 21, 47, 130, 17, 80, 47, 27, 89, 73, 71, 120];

                    case dataTypes.VISITS:
                        return [1000, 2154, 5787, 1320, 5147, 3321, 4457, 9567, 5129, 7643, 7641, 1739];
                }
            }

            function getColorsFromDatatype(datatype)
            {
                switch(datatype)
                {
                    case dataTypes.SELLINGS:
                        return ['#00b9e6', 'rgba(0, 192, 239, 0.4)'];

                    case dataTypes.INCOME:
                        return ['#009d54', 'rgba(0, 166, 90, 0.4)'];

                    case dataTypes.USERS:
                        return ['#ea9512', 'rgba(243, 156, 18, 0.4)'];

                    case dataTypes.VISITS:
                        return ['#d44a38', 'rgba(221, 75, 57, 0.4)'];
                }
            }

            function getTitleFromDatatype(datatype)
            {
                switch(datatype)
                {
                    case dataTypes.SELLINGS:
                        return "Ventes";

                    case dataTypes.INCOME:
                        return "Recettes";

                    case dataTypes.USERS:
                        return "Nouveaux utilisateurs";

                    case dataTypes.VISITS:
                        return "Visites";
                }
            }

            function getXAxisFromTimeset(timeset)
            {
                switch(timeset)
                {
                    case epochs.DAY:
                        return ['00h', '01h', '02h', '03h', '04h', '05h', '06h', '07h', '08h', '09h', '10h', '11h', '12h', '13h', '14h', '15h', '16h', '17h', '18h', '19h', '20h', '21h', '22h', '23h'];

                    case epochs.WEEK:
                        return ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

                    case epochs.MONTH:
                        return [<?php
                            $year = date("y", time());
                            $month = date("m", time());

                            $days = cal_days_in_month(CAL_GREGORIAN, $month, $year);

                            for($i = 0; $i < $days; $i++)
                            {
                                if($i != 0) echo ',';
                                echo "'" . strval($i+1) . "/" . $month . "'";
                            }

                            ?>];

                    case epochs.YEAR:
                        return ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
                }
            }

            function switchChart(source, data)
            {
                if(source === "datatype")
                {
                    activeDataset = response;
                }
                else
                {
                    activeTimeset = response;
                }

                $('.mdc-subtitle').html(getTitleFromDatatype(activeDataset));

                refreshButtons();
                refreshChart();
            }

            buildChart(); // called for initial chart
            refreshButtons();

        </script>
    </div>
    <div class="trends-section">
        <div class="trends-title"><span class="quicksand bold">Tendances</span><span class="trends-subtitle">ce mois-ci</span></div>
        <div style="text-align: justify; margin-top: 2vw">
            <span style="margin-left: 2.5vw">Évolutions</span> constatées par rapport au dernier mois (la suite du mois est estimée à partir des résultats observés ce mois-ci).
        </div>
        <table class="trends-content" style="width: 95%; margin: 1.5vw 2.5%">
            <tr style="height: 3vw">
                <th style="width: 10%"></th>
                <th style="width: 40%; padding-left: 5%">Ce mois-ci</th>
                <th style="width: 50%; text-align: right">Évolution<br><span style="color: #888888; font-size: 0.8vw; font-weight: normal">par rapport au mois dernier</span></th>
            </tr>
            <tr style="height: 0.5vw"></tr>
            <tr class="trends-data-line">
                <td class="trends-icon-column"><i class="fa fa-user fa-2x"></i></td>
                <td class="trends-value-column">+12900</td>
                <td class="trends-tendency-column"><span class="trends-green"><i class="fa fa-caret-up" style="margin-right: 0.3vw"></i>7.0%</span></td>
            </tr>

            <tr class="trends-data-line">
                <td class="trends-icon-column"><i class="fa fa-shopping-cart fa-2x"></i></td>
                <td class="trends-value-column">15900</td>
                <td class="trends-tendency-column"><span class="trends-green"><i class="fa fa-caret-up" style="margin-right: 0.3vw"></i>2.0%</span></td>
            </tr>

            <tr class="trends-data-line">
                <td class="trends-icon-column"><i class="fa fa-euro fa-2x"></i></td>
                <td class="trends-value-column">170,4k €</td>
                <td class="trends-tendency-column"><span class="trends-red"><i class="fa fa-caret-down" style="margin-right: 0.3vw"></i>2.3%</span></td>
            </tr>

            <tr class="trends-data-line">
                <td class="trends-icon-column"><i class="fa fa-globe fa-2x"></i></td>
                <td class="trends-value-column">20k</td>
                <td class="trends-tendency-column"><span class="trends-green"><i class="fa fa-caret-up" style="margin-right: 0.3vw"></i>1.2%</span></td>
            </tr>

            <tr class="trends-data-line">
                <td class="trends-icon-column"><i class="fa fa-microchip fa-2x"></i></td>
                <td class="trends-value-column">48%</td>
                <td class="trends-tendency-column"><span class="trends-red"><i class="fa fa-caret-down" style="margin-right: 0.3vw"></i>7.0%</span></td>
            </tr>
        </table>
    </div>
</div>
<div class="home-second-graph-line" style="width: 95%; margin: 1vw 2.5%; display: flex; ">
    <div class="home-last-orders" style="width: 45%; float: left; background-color: #FFFFFF; flex: 1; margin-right: 2.5%; padding: 1vw">
        <div class="quicksand bold" style="font-size: 1vw; margin-bottom: 0.8vw">Dernières commandes</div>
    </div>
    <div class="home-visits-spread" style="width: 50%; float: right; padding: 1vw; /*height: 100%;*/ background-color: #FFFFFF">
        <div class="quicksand bold" style="font-size: 1vw; margin-bottom: 0.8vw">Répartition des visites (dernières 24h)</div>
        <div id="world-map" style="height: 20vw; width: 97%; margin: 0 1.5%"></div>
        <script>
            var visitsData = {

                <?php
                    $dataset = ["FR" => 156, "BE" => 12, "IT" => 33];
                    $i = 0;

                    foreach($dataset as $country => $visits)
                    {
                        echo '"'. $country .'":' . $visits . ($i == (count($dataset) - 1) ? '' : ',');
                        $i++;
                    }
                ?>
            };

            $('#world-map').vectorMap(
                {
                    map:'world_mill',
                    series:{
                        regions:[{
                            values:visitsData,
                            scale:['#FFFFFF', '#3a84b1'],
                            normalizeFunction:'polynomial'
                        }]
                    },
                    onRegionTipShow: function(e, el, code)
                    {
                        el.html(el.html() + ' - Visites : ' + visitsData[code]);
                    }
                }
            );
        </script>
    </div>
</div>
