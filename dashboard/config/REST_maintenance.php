<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 13/08/2018
 * Time: 00:03
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../includes/admin_min.php');
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__.'/../../utility/Option.php');

try{
    post_thrown();

    $option_provider = new DBOptionProvider();

    $maintenance_option = $option_provider->get('maintenance');
    $maintenance_option->setValue(!boolval($maintenance_option->getValue()));
    $option_provider->update_value($maintenance_option);

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Maintenance ". ($maintenance_option->getValue() ? "activée" : "desactivée"))))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}