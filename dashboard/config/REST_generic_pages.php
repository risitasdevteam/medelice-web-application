<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/08/2018
 * Time: 22:39
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__."/../includes/admin_min.php");
include(__DIR__.'/../includes/data_handler.php');
include(__DIR__ . '/../../page/GenericPage.php');

function get_generic_pages(){
    $db_pages_provider = new DBGenericPageProvider();
    $payload = new JsonResponsePayload("pages", $db_pages_provider->get_all());

    $response = new JsonResponse(JsonResponseStatusType::OK, $payload);
    $response->transmit();
}

function update_generic_page(string $old_id, string $id, string $title, string $content){
    $page = new GenericPage();
    $page->setId($id);
    $page->setTitle($title);
    $page->setContent($content);
    $page->setCreator("x"); //TODO
    $page->setCreationdate(time());

    $db_pages_provider = new DBGenericPageProvider();
    $db_pages_provider->update($old_id, $page);

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "La page ".$page->getId()." a été mise à jour !"));
    $response->transmit();
}

function add_generic_page(string $id, string $title){
    $page = new GenericPage();
    $page->setId($id);
    $page->setTitle($title);
    //$page->setContent($content);
    $page->setCreator((new User(UserUtil::get_session_data()['mail']))->id); //TODO
    $page->setCreationdate(time());

    $db_pages_provider = new DBGenericPageProvider();
    $db_pages_provider->insert($page);
    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "La page ".$page->getId()." a été ajoutée avec succès !"));
    $response->transmit();
}

try{
    switch ($_SERVER['REQUEST_METHOD']){
        case 'GET':
                get_generic_pages();
            break;

        case 'POST':
                $mode = RegexUtil::filter_in_var_with_regex(INPUT_POST, "mode", "/^(update|new)$/");
                switch ($mode){
                    case "update":
                        $old_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "old_id", "/^[a-z0-9-_]{1,50}$/");
                        $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "id", "/^[a-z0-9-_]{1,50}$/");
                        $content = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "content", FILTER_SANITIZE_SPECIAL_CHARS, true);
                        $title = RegexUtil::filter_in_var_with_regex(INPUT_POST, "title", "/^[a-zA-Z0-9éÉèÈàÀçÇ() !]{1,100}$/");

                        update_generic_page($old_id, $id, $title, $content);
                        break;

                    case "new":
                        $id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "id", "/^[a-z0-9-_]{1,50}$/");
                        //$content = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "content", FILTER_SANITIZE_SPECIAL_CHARS, true);
                        $title = RegexUtil::filter_in_var_with_regex(INPUT_POST, "title", "/^[a-zA-Z0-9éÉèÈàÀçÇ() !]{1,100}$/");

                        add_generic_page($id, $title);
                        break;
                }
            break;

        default:
            throw new BadRequestMethodException("Must use get / post requests methods");
            break;
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}