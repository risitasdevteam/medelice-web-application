(function ($) {
    'use strict';

    $.extend(true, $.trumbowyg, {
        langs: {
            // jshint camelcase:false
            en: {
                fontFamily: 'Font'
            },
            fr: {
                fontFamily: 'Police'
            },
            nl: {
                fontFamily: 'Lettertype'
            },
            tr: {
                fontFamily: 'Yazı Tipi'
            }
        }
    });
    // jshint camelcase:true

    var defaultOptions = {
        fontList: [
            {name: 'Quicksand', family: 'Quicksand, sans-serif'},
            {name: 'Roboto', family: 'Roboto, sans-serif'},
            {name: 'Lobster', family: '\'Lobster\', sans-serif'},
            {name: 'Montserrat', family: 'Montserrat, sans-serif'},
            {name: 'Abril', family: '\'Abril\', sans-serif'},
            {name: 'Poiret One', family: '\'Poiret One\', sans-serif'},
            {name: 'Bromello', family: '\'bromello\', sans-serif'}
        ]
    };


    // Add dropdown with web safe fonts
    $.extend(true, $.trumbowyg, {
        plugins: {
            fontfamily: {
                init: function (trumbowyg) {
                    trumbowyg.o.plugins.fontfamily = trumbowyg.o.plugins.fontfamily || defaultOptions;
                    trumbowyg.addBtnDef('fontfamily', {
                        dropdown: buildDropdown(trumbowyg),
                        hasIcon: false,
                        text: trumbowyg.lang.fontFamily
                    });
                }
            }
        }
    });

    function buildDropdown(trumbowyg) {
        var dropdown = [];

        $.each(trumbowyg.o.plugins.fontfamily.fontList, function (index, font) {
            trumbowyg.addBtnDef('fontfamily_' + index, {
                title: '<span style="font-family: ' + font.family + ';">' + font.name + '</span>',
                hasIcon: false,
                fn: function () {
                    trumbowyg.execCmd('fontName', font.family, true);
                }
            });
            dropdown.push('fontfamily_' + index);
        });

        return dropdown;
    }
})(jQuery);
