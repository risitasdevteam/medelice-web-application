<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 06/08/2018
 * Time: 17:41
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../../page/GenericPage.php');

?>

<script src="/dashboard/js/Util.js"></script>
<script src="/dashboard/js/Config.js"></script>
<div class="page-title quicksand bold">
    Dashboard <span class="page-subtitle">/ Pages génériques</span>
</div>
<div class="genericpages-container">
    <div class="genericpages-choice-container">
        <div class="genericpages-choice-title quicksand bold">Pages existantes</div>
        <div class="genericpages-choice-list top-border">
        </div>
        <button class="genericpages-choice-add-btn button quicksand bold" onclick="showNewGenPageForm()">
            <i class="fa fa-plus" style="margin-right: 0.6vw"></i>Nouvelle page
        </button>
    </div>
    <div class="genericpages-edition-container">
        <div class="genericpages-edition-section">
            <div class="genericpages-editor-title quicksand bold">
                <span class="left">Éditer la page "cgu"</span>
                <button class="btn btn-secondary mdc-btn right" style="height: 2vw; font-size: 1vw;" onclick="save_current_page()"><i class="fa fa-save"></i></button>
            </div>
            <div class="fields-line">
                <div class="field-container">
                    <label class="field-descriptor" for="page_id">Identifiant</label><br>
                    <input class="field-input" id="page_id" type="text" value="cgu">
                </div>
                <div class="field-container last-container">
                    <label class="field-descriptor" for="page_name">Nom</label><br>
                    <input class="field-input" id="page_name" type="text" value="Conditions Générales d'Utilisation" style="width: 32.5vw">
                </div>
            </div>
            <div class="genericpages-editor" style="height: 0;"></div>
            <script>
                $('.genericpages-editor').trumbowyg({
                    btns: [
                        ['viewHTML'],
                        ['undo', 'redo'],
                        ['formatting'],
                        ['bold', 'italic', 'strikethrough', 'underline'],
                        ['fontfamily', 'fontsize', 'lineheight'],
                        ['foreColor', 'backColor'],
                        ['superscript', 'subscript'],
                        ['link'],
                        ['insertImage', 'upload', 'base64'],
                        ['justifyLeft', 'justifyCenter', 'justifyRight', 'justifyFull'],
                        ['unorderedList', 'orderedList'],
                        ['table'],
                        ['horizontalRule'],
                        ['removeformat'],
                        ['fullscreen']
                    ],
                    semantic: false,
                    imageWidthModalEdit: true,
                    lang: 'fr',
                    autogrow: true
                });

                $('.trumbowyg-box').css(
                    {
                        "margin":"0.95vw auto 0"
                    }
                );
            </script>
        </div>
    </div>
    <div class="newgeneric-form-container">
        <div class="newgeneric-form">
            <div class="newgeneric-title-container">
                <span class="newgeneric-title quicksand bold">Nouvelle page générique</span>
                <i class="fa fa-close newgeneric-close-btn" onclick="hideNewGenPageForm()"></i>
            </div>
            <div class="fields-line">
                <div class="field-container">
                    <label class="field-descriptor" for="newgeneric_id">Identifiant :</label><br>
                    <input type="text" class="field-input" id="newgeneric_id" style="width: 10vw;">
                </div>
                <div class="field-container last-container">
                    <label class="field-descriptor" for="newgeneric_name">Nom de la page :</label><br>
                    <input type="text" class="field-input" id="newgeneric_name" style="width: 20vw;">
                </div>
            </div>
            <button class="newgeneric-form-add-btn orange-btn button quicksand bold" onclick="confirmNewGenericPage()">
                Ajouter cette page
            </button>
        </div>
    </div>
    <script>
        function retrieve_pages(){
            ConfigUtil.get_generic_pages(function(response){
                    if (Object.keys(response.payload.pages).length > 0) {
                        $('.genericpages-choice-list-element').remove();
                        var $pages_list = $(".genericpages-choice-list.top-border");

                        $.each(response.payload.pages, function(index, item){
                            var id = item["id"];
                            var title = item["title"];
                            var content = item["content"];

                            $pages_list.append('<div class="genericpages-choice-list-element" data-content="' + content + '" data-title="' + title + '" data-id="' + id + '">'+
                                id + ' - ' + title +
                                '</div>');
                        });

                        page_list_click_handler();

                    }
            },
                function(){

            });
        }

        function page_list_click_handler(){
            var $list_elements = $('.genericpages-choice-list-element');
            $list_elements.click(function(){
                var id = $(this).data("id");
                var title = $(this).data("title");
                var content = HTMLUtil.decodeEntities($(this).data("content"));

                var $page_id_input = $('#page_id');
                var $page_title_input = $('#page_name');
                var $page_container_title_id = $('#page-container-title-id');

                $page_id_input.val(id);
                $page_title_input.val(title);
                $page_container_title_id.text(id);
                $('.genericpages-editor').trumbowyg('html', content);

                $('.genericpages-choice-list-element[selected]').attr("selected", false);
                $(this).attr("selected", true);
            });

        }

        function save_current_page(){
            var page_id = $('#page_id').val();
            var page_title = $('#page_name').val();
            var page_content =  $('.genericpages-editor').trumbowyg('html');

            var provider = new RESTResponseProvider();
            provider.url = "/dashboard/config/REST_generic_pages.php";
            provider.classic_callback = true;
            provider.data = {
                "mode":"update",
                "old_id": $('.genericpages-choice-list-element[selected]').data("id"),
                "id":page_id,
                "title":page_title,
                "content":page_content
            };

            provider.http_post();
        }

        function add_page(){
            var page_id = $('#newgeneric_id').val();
            var page_title = $('#newgeneric_name').val();

            var provider = new RESTResponseProvider();
            provider.url = "/dashboard/config/REST_generic_pages.php";
            provider.classic_callback = true;
            provider.data = {
                "mode":"new",
                "id":page_id,
                "title":page_title
            };

            provider.http_post(function(){
                retrieve_pages();
            });
        }

        function showNewGenPageForm()
        {
            $('.newgeneric-form-container').css(
                {
                    "display":"block"
                }
            );
        }

        function hideNewGenPageForm()
        {
            $('.newgeneric-form-container').css(
                {
                    "display":"none"
                }
            );
        }

        function confirmNewGenericPage()
        {
            // todo Jul
            add_page();
            hideNewGenPageForm();
        }

        $(document).ready(function(){
            retrieve_pages();
        });
    </script>
</div>
