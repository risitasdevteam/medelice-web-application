<?php
/**
 * Created by PhpStorm.
 * User: romai
 * Date: 08/08/2018
 * Time: 13:20
 */

require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../../utility/Option.php');

$option_provider = new DBOptionProvider();
$maintenance = boolval($option_provider->get('maintenance')->getValue());

?>

<script src="/dashboard/js/Config.js"></script>

<div style="display: flex; align-items: center; justify-content: center; width: 100%; height: 100%;">
    <div class="roboto" style="width: 40%; padding: 1vw 1.5vw; font-size: 1.2vw; background-color: white">
        <div class="bold" style="width: 100%; font-size: 1.6vw; text-align: justify;">
            <i class="fa fa-warning" style="margin-right: 0.6vw"></i>
            <?php
                echo ($maintenance ? 'Le site est en mode maintenance.' : 'Attention, vous allez activer le mode maintenance du site.');
            ?>
        </div>
        <div style="width: 93%; text-align: justify; margin-top: 1vw; margin-left: 5%;">
            <?php
                echo ($maintenance ? 'Le site redeviendra accessible pour tous les utilisateurs.' : 'Le site deviendra alors inaccessible pour tous les utilisateurs non-admin. Votre accès est assuré tant que vous restez connecté, ou que votre adresse IP ne change pas.');
            ?>
        </div>
        <button class="button quicksand bold <?php echo ($maintenance ? 'green-btn' : 'red-btn'); ?>" style="height: 2.5vw; width: 100%; border: none; font-size: 1vw; margin-top: 1.5vw" onclick="ConfigUtil.toggle_maintenance(true)">
            <?php
                echo ($maintenance ? 'Désactiver le mode maintenance' : 'Activer le mode maintenance');
            ?>
        </button>
    </div>
</div>
