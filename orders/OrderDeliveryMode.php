<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/07/2018
 * Time: 11:37
 */

abstract class OrderDeliveryMode{
    const normal = "normal";
    const express = "express";
}