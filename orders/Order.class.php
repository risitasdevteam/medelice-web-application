<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/10/2017
 * Time: 17:59
 */
require_once(__DIR__.'/../initializer.inc.php');

require_once(__DIR__.'/../exceptions/data/DataException.php');
require_once(__DIR__.'/../exceptions/data/DataBadException.php');

require_once(__DIR__.'/../exceptions/DatabaseException.php');
require_once(__DIR__.'/../exceptions/DatabaseConnectionException.php');
require_once(__DIR__.'/../exceptions/DatabaseInvalidQueryException.php');

require_once(__DIR__.'/OrderStatus.enum.php');
require_once(__DIR__.'/OrderProduct.php');

require_once(__DIR__.'/../utility/SQLUtil.php');
require_once(__DIR__.'/../utility/IDBTableStructure.php');
require_once(__DIR__.'/../customer/shipping/OrderCustomerAddresses.php');

class DBOrderStructure implements IDBTableStructure{

    static function getDatabase()
    {
        return "medelice_users";
    }

    static function getColumns()
    {
        return [
            "id",
            "user_id",
            "products",
            "account_time",
            "status_last_update",
            "status",
            "customer_addresses",
            "delivery_mode",
            "tracking",
            "total_bill"
        ];
    }

    static function getTable()
    {
        return "users_orders";
    }
}

interface OrderProvider{
    public function insert(OrderNew $order);
    public function update(string $order_id, OrderNew $new_order);
    public function remove(string $order_id);

    public function get_on_unique_id(string $unique_id);
    public function retrieve_orders_for_specific_customer(string $customer_unique_id);
    public function retrieve_orders_on_status(int $status, bool $as_array = true);
    public function retrieve_unprepared_orders(bool $as_array = true);
    public function retrieve_processing_orders(bool $as_array = true);
    public function retrieve_done_orders(bool $as_array = true);
}

class DBOrderProvider implements OrderProvider{

    public function insert(OrderNew $order)
    {
        if (empty($order->getId()))
            $order->setId(hash("crc32", openssl_random_pseudo_bytes(3)));
        else {
            if (preg_match('/[a-f0-9]{8}/', $order->getId()) != 1) {
                throw new Exception('Unique id must respects CRC32 laws.');
            }
        }

        SQLUtil::insert(DBOrderStructure::getTable(), $order->to_array());
        return true;
    }

    public function get_on_unique_id(string $unique_id){

        $column_id = DBOrderStructure::getColumns()[0];
        $raw_order = SQLUtil::select(DBOrderStructure::getTable(), ['*'], [$column_id => $unique_id]);
        return count($raw_order) > 0 ? OrderNew::from_array($raw_order[0]) : null;
    }

    public function update(string $order_id, OrderNew $new_order)
    {
        $column_id = DBOrderStructure::getColumns()[0];

        $old_order = $this->get_on_unique_id($order_id);
        $difference = OrderNew::array_difference($old_order, $new_order);

        SQLUtil::update(DBOrderStructure::getTable(), $difference, [$column_id => $order_id]);
        return true;
    }

    public function remove(string $order_id){
        $column_id = DBOrderStructure::getColumns()[0];

        SQLUtil::delete(DBOrderStructure::getTable(), [$column_id => $order_id]);
        return true;
    }

    public function retrieve_orders_for_specific_customer(string $customer_unique_id)
    {
        $column_user_id = DBOrderStructure::getColumns()[1];
        $retrieved_data = SQLUtil::select(DBOrderStructure::getTable(), ['*'], [$column_user_id => $customer_unique_id]);
        $retrieved_data_count = count($retrieved_data);
        $orders = array();
        if ($retrieved_data_count > 0) {
            for ($i = 0; $i < $retrieved_data_count; $i++) {
                $pointer = $retrieved_data[$i];
                array_push($orders, OrderNew::from_array($pointer));
            }
        }

        return $orders;
    }

    public function retrieve_orders_on_status(int $status, bool $as_array = true)
    {
        $column_status = DBOrderStructure::getColumns()[5];
        $orders = array();

        $sql = SQLUtil::open("medelice_users");
        $query = 'select * from '.DBOrderStructure::getTable().' where '.$column_status . ' = ?';

        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("d", $status);
            $stmt->bind_result($id, $user_id, $products, $account_time, $status_last_update, $status, $customer_addresses, $delivery_mode, $tracking, $total_bill);
            $stmt->execute();

            if ($stmt->error !== ''){
                throw new MedeliceException("Error, can't retrieve undone orders");
            }

            while ($stmt->fetch()) {
                array_push($orders, $as_array ?
                    array_combine(DBOrderStructure::getColumns(), array($id, $user_id, $products, $account_time, $status_last_update, $status, OrderCustomerAddresses::decode_from_json($customer_addresses), $delivery_mode, $tracking, $total_bill))
                    :
                    OrderNew::from_array(array_combine(DBOrderStructure::getColumns(), array($id, $user_id, $products, $account_time, $status_last_update, $status, OrderCustomerAddresses::decode_from_json($customer_addresses), $delivery_mode, $tracking, $total_bill))));
            }

            $stmt->close();
        }

        SQLUtil::close($sql);
        return $orders;
    }

    public function retrieve_unprepared_orders(bool $as_array = true)
    {
        return $this->retrieve_orders_on_status(OrderStatus::RECEIVED, $as_array);
    }

    public function retrieve_processing_orders(bool $as_array = true)
    {
       return $this->retrieve_orders_on_status(OrderStatus::PROCESSING, $as_array);
    }

    public function retrieve_done_orders(bool $as_array = true)
    {
        $column_status = DBOrderStructure::getColumns()[5];
        $sent = OrderStatus::SENT;
        $delivered = OrderStatus::DELIVERED;
        $orders = array();

        $sql = SQLUtil::open("medelice_users");
        $query = 'select * from '.DBOrderStructure::getTable().' where '.$column_status . ' = ? or '.$column_status.' = ?';

        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("dd", $sent, $delivered);
            $stmt->bind_result($id, $user_id, $products, $account_time, $status_last_update, $status, $customer_addresses, $delivery_mode, $tracking, $total_bill);
            $stmt->execute();

            if ($stmt->error !== ''){
                throw new MedeliceException("Error, can't retrieve undone orders");
            }

            while ($stmt->fetch()) {
                array_push($orders, $as_array ?
                    array_combine(DBOrderStructure::getColumns(), array($id, $user_id, $products, $account_time, $status_last_update, $status, OrderCustomerAddresses::decode_from_json($customer_addresses), $delivery_mode, $tracking, $total_bill))
                :
                    OrderNew::from_array(array_combine(DBOrderStructure::getColumns(), array($id, $user_id, $products, $account_time, $status_last_update, $status, OrderCustomerAddresses::decode_from_json($customer_addresses), $delivery_mode, $tracking, $total_bill))));
            }

            $stmt->close();
        }

        SQLUtil::close($sql);
        return $orders;
    }
}

class OrderNew{
    private $id;
    private $user_id;
    private $products;
    private $account_time;
    private $status_last_update;
    private $status;
    private $customer_addresses;
    private $delivery_mode;
    private $tracking;
    private $total_bill;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return mixed
     */
    public function getTotalBill()
    {
        return $this->total_bill;
    }

    /**
     * @param mixed $total_bill
     */
    public function setTotalBill($total_bill)
    {
        $this->total_bill = $total_bill;
    }

    /**
     * @return mixed
     */
    public function getAccountTime()
    {
        return $this->account_time;
    }

    /**
     * @param mixed $account_time
     */
    public function setAccountTime($account_time)
    {
        $this->account_time = $account_time;
    }

    /**
     * @return mixed
     */
    public function getStatusLastUpdate()
    {
        return $this->status_last_update;
    }

    /**
     * @param mixed $status_last_update
     */
    public function setStatusLastUpdate($status_last_update)
    {
        $this->status_last_update = $status_last_update;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getCustomerAddresses()
    {
        return $this->customer_addresses;
    }

    /**
     * @param mixed $customer_addresses
     */
    public function setCustomerAddresses($customer_addresses)
    {
        $this->customer_addresses = $customer_addresses;
    }

    /**
     * @return mixed
     */
    public function getDeliveryMode()
    {
        return $this->delivery_mode;
    }

    /**
     * @param mixed $delivery_mode
     */
    public function setDeliveryMode($delivery_mode)
    {
        $this->delivery_mode = $delivery_mode;
    }

    /**
     * @return mixed
     */
    public function getTracking()
    {
        return $this->tracking;
    }

    /**
     * @param mixed $tracking
     */
    public function setTracking($tracking)
    {
        $this->tracking = $tracking;
    }

    public function to_array(){
        return array_combine(DBOrderStructure::getColumns(), [
            $this->getId(),
            $this->getUserId(),
            $this->getProducts(),
            $this->getAccountTime(),
            $this->getStatusLastUpdate(),
            $this->getStatus(),
            $this->getCustomerAddresses(),
            $this->getDeliveryMode(),
            $this->getTracking(),
            $this->getTotalBill()
        ]);
    }

    public static function from_array(array $array){
        $order = new OrderNew();

        $mapped_numerically_columns_array = DBOrderStructure::getColumns();
        $get_column = function($index) use ($array, $mapped_numerically_columns_array){
            return $array[$mapped_numerically_columns_array[$index]];
        };

        $order->setId($get_column(0));
        $order->setUserId($get_column(1));
        $order->setProducts($get_column(2));
        $order->setAccountTime($get_column(3));
        $order->setStatusLastUpdate($get_column(4));
        $order->setStatus($get_column(5));
        $order->setCustomerAddresses($get_column(6));
        $order->setDeliveryMode($get_column(7));
        $order->setTracking($get_column(8));
        $order->setTotalBill($get_column(9));

        return $order;
    }

    public static function array_difference(OrderNew $order1, OrderNew $order2){
        $order1_array = $order1->to_array();
        $order2_array = $order2->to_array();

        $sql_update_sets_matrix = array();
        foreach($order1_array as $key => $value){
            $order2_array_value = $order2_array[$key];

            if ($value != $order2_array_value){
                $sql_update_sets_matrix[$key] = $order2_array_value;
            }
        }

        return $sql_update_sets_matrix;
    }
}

class Order
{
    public $id;
    public $user_id;
    public $products;
    public $total_bill;
    public $account_time;
    public $status_last_update;
    public $status;
    public $customer_addresses;
    public $delivery_mode;
    public $tracking;

    public $exists = false;

    /**
     * Allowed variables name to initialize the constructor.
     * Names and order, are the same between here and the columns in the sql table.
     * @var array
     */
    protected static $_allowed_variables_names = array('id', 'user_id', 'products', 'status_last_update', 'status', 'customer_addresses', 'delivery_mode', 'tracking', 'account_time', 'total_bill');

    public function __construct($array){
        foreach ($array as $key=>$value){
            if (in_array($key, self::$_allowed_variables_names)){
                switch($key){
                    case self::$_allowed_variables_names[0]:
                        $this->id = $value;
                        break;

                    case self::$_allowed_variables_names[1]:
                        $this->user_id = $value;
                        break;

                    case self::$_allowed_variables_names[2]:
                        $this->products = $value;
                        break;

                    case self::$_allowed_variables_names[3]:
                        $this->status_last_update = $value;
                        break;

                    case self::$_allowed_variables_names[4]:
                        $this->status = $value;
                        break;

                    case self::$_allowed_variables_names[5]:
                        $this->customer_addresses = $value;
                        break;

                    case self::$_allowed_variables_names[6]:
                        $this->delivery_mode = $value;
                        break;

                    case self::$_allowed_variables_names[7]:
                        $this->tracking = $value;
                        break;

                    case self::$_allowed_variables_names[8]:
                        $this->account_time = $value;
                        break;

                    case self::$_allowed_variables_names[9]:
                        $this->total_bill = $value;
                        break;
                }
            }else{
                throw new Exception('Argument "'.$key.'" isn\'t allowed.');
            }
        }
    }

    /**
     * @param bool|false $rebind
     * @return bool Si vraie, les valeurs de l'objet seront mises à jours avec les valeurs récupérées ici.
     * @throws DataBadException
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public function exists($rebind = false){
        if (isset($this->user_id)){
            $returned_data = SQLUtil::select(ORDERS_TABLE, ['*'], ['id' => $this->id, 'user_id' => $this->user_id], 'limit 1');
        }else{
            $returned_data = SQLUtil::select(ORDERS_TABLE, ['*'], ['id' => $this->id], 'limit 1');
        }

        if (count($returned_data) > 0){
            if ($rebind){
                $pointer = $returned_data[0];

                //$this->id = $pointer[self::$_allowed_variables_names[0]];
                $this->user_id = $pointer[self::$_allowed_variables_names[1]];
                $this->products = $pointer[self::$_allowed_variables_names[2]];
                $this->status_last_update = $pointer[self::$_allowed_variables_names[3]];
                $this->status = $pointer[self::$_allowed_variables_names[4]];
                $this->customer_addresses = $pointer[self::$_allowed_variables_names[5]];
                $this->delivery_mode = $pointer[self::$_allowed_variables_names[6]];
                $this->tracking = $pointer[self::$_allowed_variables_names[7]];
                $this->account_time = $pointer[self::$_allowed_variables_names[8]];

                $this->total_bill = $pointer[self::$_allowed_variables_names[9]];
            }
            $this->exists = true;
        }

        return $this->exists;
    }

    public static function generateUniqueOrderId(){
        return hash("crc32", openssl_random_pseudo_bytes(3));
    }

    /**
     * Ajouter une commande grâce à l'objet Order @see Order.
     * @param $order
     * @return bool
     * @throws DataBadException
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function put($order){
        if (empty($order) || !$order instanceof Order){
            throw new DataBadException('');
        }

        if (empty($order->id))
            $order->id = self::generateUniqueOrderId();
        else
            if (preg_match('/[a-f0-9]{8}/', $order->id) != 1){
                throw new Exception('Unique id must respects CRC32 laws.');
            }


        SQLUtil::insert(ORDERS_TABLE,
            ['id' => $order->id,
                'user_id' => $order->user_id,
                'products' => $order->products,
                'status_last_update' => $order->status_last_update,
                'status' => $order->status,
                'customer_addresses' => $order->customer_addresses,
                'delivery_mode' => $order->delivery_mode,
                'account_time' => $order->account_time,
                'total_bill' => $order->total_bill
            ]);
    }

    /**
     * Update specific data by comparing fields
     * @param $new_order
     * @throws DataBadException
     * @throws DatabaseInvalidQueryException
     * @throws MedeliceException
     */
    public function update($new_order){
        if (empty($new_order) || !$new_order instanceof Order){
            throw new DataBadException('');
        }

        $current_order_id = $this->id;

        if (empty($current_order_id)){
            throw new MedeliceException('Unique id must respects CRC32 laws.');
        }

        if (preg_match(RegexUtil::crc32(), $current_order_id) != 1){
            throw new MedeliceException('Unique id must respects CRC32 laws.');
        }

        $order1_matrix = array();
        $order2_matrix = array();

        $order1_matrix['id'] = $this->id;
        $order1_matrix['user_id'] = $this->user_id;
        $order1_matrix['products'] = $this->products;
        $order1_matrix['account_time'] = $this->account_time;
        $order1_matrix['status_last_update'] = $this->status_last_update;
        $order1_matrix['status'] = $this->status;
        $order1_matrix['customer_addresses'] = $this->customer_addresses;
        $order1_matrix['delivery_mode'] = $this->delivery_mode;
        $order1_matrix['tracking'] = $this->tracking;
        $order1_matrix['total_bill'] = $this->total_bill;

        $order2_matrix['id'] = $new_order->id;
        $order2_matrix['user_id'] = $new_order->user_id;
        $order2_matrix['products'] = $new_order->products;
        $order2_matrix['account_time'] = $this->account_time;
        $order2_matrix['status_last_update'] = $new_order->status_last_update;
        $order2_matrix['status'] = $new_order->status;
        $order2_matrix['customer_addresses'] = $new_order->customer_addresses;
        $order2_matrix['delivery_mode'] = $new_order->delivery_mode;
        $order2_matrix['tracking'] = $new_order->tracking;
        $order2_matrix['total_bill'] = $new_order->total_bill;

        $sql_update_sets_matrix = [];

        foreach($order1_matrix as $key => $value){
            $order2_matrix_value = $order2_matrix[$key];

            if ($value != $order2_matrix_value){
                $sql_update_sets_matrix[$key] = $order2_matrix_value;
            }
        }

        var_dump($sql_update_sets_matrix);
        SQLUtil::update(ORDERS_TABLE, $sql_update_sets_matrix, ['id' => $new_order->id]);
    }

    /**
     * Récupérer toutes les commandes d'un client en fonction de son numéro d'identification unique.
     * @param $customer_unique_id string Numéro d'identification unique du client.
     * @return array Un tableau d'objets Order
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function retrieveOrders($customer_unique_id){
        if (empty($customer_unique_id)){
            throw new DataBadException('');
        }

        $orders = array();
        $retrieved_data = SQLUtil::select(ORDERS_TABLE, ['*'], ['user_id' => $customer_unique_id]);
        $retrieved_data_count = count($retrieved_data);
        if ($retrieved_data_count > 0){
            for ($i = 0; $i < $retrieved_data_count; $i++) {
                $pointer = $retrieved_data[$i];

                $order = new Order(array(
                    "id" => $pointer[self::$_allowed_variables_names[0]],
                    "user_id" => $pointer[self::$_allowed_variables_names[1]],
                    "products" => $pointer[self::$_allowed_variables_names[2]],
                    "status_last_update" => $pointer[self::$_allowed_variables_names[3]],
                    "status" => $pointer[self::$_allowed_variables_names[4]],
                    "customer_addresses" => $pointer[self::$_allowed_variables_names[5]],
                    "delivery_mode" => $pointer[self::$_allowed_variables_names[6]],
                    "tracking" => $pointer[self::$_allowed_variables_names[7]],
                    "account_time" => $pointer[self::$_allowed_variables_names[8]],
                    "total_bill" => $pointer[self::$_allowed_variables_names[9]]
                ));

                array_push($orders, $order);
            }
        }

        return $orders;
    }

    /**
     * Récupération des commandes non traitées.
     * @return array
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function retrieveUndoneOrders(){
        $undone = OrderStatus::RECEIVED; //we've received the order but it was not packed and sent
        $orders = array();

        $retrieved_data = SQLUtil::select(ORDERS_TABLE,
            [self::$_allowed_variables_names[0],
                self::$_allowed_variables_names[1],
                self::$_allowed_variables_names[2],
                self::$_allowed_variables_names[3],
                self::$_allowed_variables_names[5],
                self::$_allowed_variables_names[6],
                self::$_allowed_variables_names[7],
                self::$_allowed_variables_names[8],
                self::$_allowed_variables_names[9]
            ],
            [self::$_allowed_variables_names[4] => $undone]);
        $retrieved_data_count = count($retrieved_data);

        if ($retrieved_data_count > 0){
            for ($i = 0; $i < $retrieved_data_count; $i++){
                $pointer = $retrieved_data[$i];

                $data = function($index) use($pointer) {
                    return $pointer[self::$_allowed_variables_names[$index]];
                };

                $order = new Order(array(
                    "id" => $data(0),
                    "user_id" => $data(1),
                    "products" => $data(2),
                    "status_last_update" => $data(3),
                    "status" => $undone,
                    "customer_addresses" => $data(5),
                    "delivery_mode" => $data(6),
                    "tracking" => $data(7),
                    "account_time" => $data(8),
                    "total_bill" => $data(9)
                ));

                array_push($orders, $order);
            }
        }
        return $orders;
    }
}