<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/10/2017
 * Time: 17:49
 */
abstract class OrderStatus
{
    const PENDING_3D_SECURE = -1;
    const RECEIVED = 0;
    const PROCESSING = 1;
    const SENT = 2;
    const DELIVERED = 3;
    const ERROR = 4; //any kind of error
    const CANCELED = 5;

    public static function exists($code){
        for ($i = -1; $i <= 5; $i++){
            if ($code == $i)
                return true;
        }
        return false;
    }

    public static function readableForCode($code){
        switch ($code){
            case self::PENDING_3D_SECURE:
                return "En attente de la validation par le 3D Secure";

            case self::RECEIVED:
                return L::account_order_received;

            case self::PROCESSING:
                return L::account_order_processing;

            case self::SENT:
                return L::account_order_sent;

            case self::DELIVERED:
                return L::account_order_delivered;

            case self::ERROR:
                return L::account_order_error;

            case self::CANCELED:
                return "Commande annulée";

            default:
                return null;
        }
    }
}