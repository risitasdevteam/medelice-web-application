<svg xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 14.61 14.61">
    <defs>
        <style>
            .a
            {
                fill:black
            }

            .b
            {
                fill: #<?php echo ($current_page == "produits" || $current_page == "voir" || $current_page == "valisette") ? $colors[0] : "a0782f" ?>;
                stroke: #fff;
                stroke-width: 1px;
            }

        </style>
    </defs>
    <title>Logo Icon</title>
    <path class="b" d="M6.3,12.61A6.3,6.3,0,1,0,0,6.3a6.3,6.3,0,0,0,6.3,6.3"/>
    <path class="a" d="M10.41,3.29a.31.31,0,0,1,.06.22.16.16,0,0,1-.1.12H2.24a.15.15,0,0,1-.1-.12.25.25,0,0,1,0-.21A5.36,5.36,0,0,1,6.3,1.21a5.34,5.34,0,0,1,4.07,2l0,0"/>
</svg>