<?php

    echo '
    <div id="sidebar" class="'.(($current_page == "produits" || $current_page == "voir") ? ($colors[0] == Regions::getDefaultColor() ? "black-bg" : "colored-bg") : "black-bg").' colored-font-opposite">
        <div class="sidebar-content">';

        if($current_page == "produits" || $current_page == "voir")
        {
            require_once (__DIR__ . '/produits/ProductsCategories.class.php');
            $categories = ProductsCategories::get_all_categories_as_array();
            ksort($categories);
            echo '<div class="sidebar-menu-container">';
            foreach ($categories as $category_id => $category_name){
                echo '
                    <a class="button-link menu-category-item quicksand medium '.($current_category == $category_id ? ' menu-category-item-active' : '').'" href="/index.php?page=produits&reg='.$current_region_id.'&cat='.$category_id.'">'.$categories[$category_id].'</a>
                ';
            }
            echo '</div>';
            unset($name);
        }
        echo '</div>';

        echo '
            <script>
                $(\'.menu-category-item\').css(
                    {
                        "color":"'.$colors[1].'"
                    }
                );
            </script>
        ';
    echo '</div>';
?>