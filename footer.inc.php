<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 28/08/2017
 * Time: 18:06
 */

?>

<div class="footer">
    <div class="footer-menus">
        <ul class="footer-menu">
            <?php echo L::footer_medelice ?>
            <div class="footer-separator"></div>
            <li style="margin-bottom: 2px"><a href="/index.php?page=generic&id=about"><?php echo L::footer_about ?></a></li>
            <li style="margin-bottom: 2px"><a href="/index.php?page=generic&id=travaillez_chez_nous"><?php echo L::footer_work ?></a></li>
            <li style="margin-bottom: 2px"><a href="/index.php?page=generic&id=equipe"><?php echo L::footer_team ?></a></li>
            <li style="margin-bottom: 2px"><a href="#"><?php echo L::footer_news ?></a></li>
            <li style="margin-bottom: 2px"><a href="/index.php?page=generic&id=boutique"><?php echo L::footer_resell_points ?></a></li>
        </ul>
        <ul class="footer-menu">
            <?php echo L::footer_help ?>
            <div class="footer-separator"></div>
            <li class="footer-link"><a href="/index.php?page=contact"><?php echo L::footer_contactus ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=faq"><?php echo L::footer_faq ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=guarantees"><?php echo L::footer_guarantees ?></a></li>
        </ul>
        <ul class="footer-menu">
            <?php echo L::footer_orders ?>
            <div class="footer-separator"></div>
            <li class="footer-link"><a href="/index.php?page=mes-commandes"><?php echo L::footer_order_monitoring ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=retour_commande"><?php echo L::footer_returns ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=paiments"><?php echo L::footer_payments ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=discounts"><?php echo L::footer_discounts ?></a></li>
        </ul>
        <ul class="footer-menu">
            <?php echo L::footer_legal ?>
            <div class="footer-separator"></div>
            <li class="footer-link"><a href="/index.php?page=generic&id=confidentialite"><?php echo L::footer_confidentiality ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=cgv"><?php echo L::footer_gcu ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=cookies"><?php echo L::footer_cookies ?></a></li>
            <li class="footer-link"><a href="/index.php?page=generic&id=mentions_legales"><?php echo L::footer_legal_notice ?></a></li>
        </ul>
    </div>

    <div class="footer-madein">
        <p><span style=" color: mediumblue">Med</span><span style="color: gray">'in</span> <span style="color: firebrick">FRANCE</span></p>
    </div>
</div>