<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 01/11/2017
 * Time: 13:55
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/produits/Regions.class.php');

$redirect = isset($_GET["mode"]) ? $_GET['mode'] : 'produits';

if($redirect != "produits" && $redirect != "valisette")
    $redirect = "produits";

?>

<div id="topsection">

    <?php
        if($redirect == "produits") echo '<div class="regionselect-title roboto">Choisissez une région</div>';
        else echo '<div class="regionselect-title bromello colored-font" style="letter-spacing: 0.1vw; font-size: 2.5vw">Nos valisettes</div>'
    ?>
    <div class="regionselect-content">
        <?php

        foreach(Regions::getRegions() as $region)
        {
            $color = Regions::getColorsFromRegionId($region[0])[0];

            echo '
                
                <a class="regionselect-box" id="select'.$region[0]. '" href="/index.php?page='.$redirect. ($redirect == 'valisette' ? '&ref=V_' : '&reg=') .$region[0].'">
                    <div class="regionselect-subbox" id="sub' .$region[0].'" style="background-color: '.$color.';">
                        <div class="regionselect-regname quicksand bold" style="">'.$region[1].'</div>
                    </div>
                </a>
                <script>
                    $("#select'.$region[0].'").css(
                        {
                            "background-image":"url(/images/regions/carte_sidebar/'.$region[0].'.jpg)",
                            "background-size":"105% auto",
                            "background-position":"center"
                        }
                    );
                    
                    $("#select'.$region[0].'").hover(function(e) {
                        $("#sub'.$region[0].'").css(
                            {
                                "max-height":"0"
                            }    
                        );
                        
                        $("#select'.$region[0].'").css(
                            {
                                "background-size":"115% auto"
                            }
                        );
                    }, function(e) {
                        $("#sub'.$region[0].'").css(
                            {
                                "max-height":"100%"
                            }    
                        );
                        
                        $("#select'.$region[0].'").css(
                            {
                                "background-size":"105% auto"
                            }
                        );
                    });
                    
                </script>
            ';
        }

        ?>
    </div>
</div>