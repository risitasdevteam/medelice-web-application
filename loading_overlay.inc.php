<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2018
 * Time: 15:49
 */

?>


<div class="loading-screen" style="position: fixed; top: 0; left: 0; right: 0; bottom: 0; background-color: rgba(0, 0, 0, 0.4); display: none; align-items: center; justify-content: center; z-index: 9999;">
    <div class="spinner">
        <div class="rect1"></div>
        <div class="rect2"></div>
        <div class="rect3"></div>
        <div class="rect4"></div>
        <div class="rect5"></div>
    </div>
</div>
<script>
    function showLoadingScreen()
    {
        $('.loading-screen').css(
            {
                "display":"flex"
            }
        );
    }

    function hideLoadingScreen()
    {
        $('.loading-screen').css(
            {
                "display":"none"
            }
        );
    }
</script>
