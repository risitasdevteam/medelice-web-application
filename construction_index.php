<html>
    <head>
        <meta charset="UTF-8">
        <title>Medelice - Mets gastronomiques de bon goût</title>

        <link rel="stylesheet" href="/css/temp_style.css">
        <link rel="stylesheet" href="/css/new_fonts.css">
        <link href="https://fonts.googleapis.com/css?family=Lobster|Montserrat|Roboto" rel="stylesheet">
    </head>
    <body>
        <div class="bandeau">
            <div class="data-container">
                <img id="logo" src="/images/logo.png"/>
                <div class="msg-container">
                    <span style="font-size: 400%; padding-top: 10%">We're still working on it...</span><br>
                    <form method="post">
                        <span style="font-family: 'Montserrat', sans-serif, 'Arial'; font-size: 120%">Let us contact you when the site is ready :</span><br>
                        <input class="input-field" type="text" name="email" placeholder="mail@host.com"/>
                        <button class="submit-button" type="submit">Warn me</button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/09/2017
 * Time: 01:54
 */

