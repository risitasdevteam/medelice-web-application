<?php
require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");

    switch ($current_page) // partie gauche de la topsection
    {
        /*case 'panier':
            break;*/

        case 'passer-commande':
            include('customer/sidebar.buy.inc.php');
            break;

        case 'produits':
            include('sidebar.products.inc.php');
            break;

        case 'voir':
            include('sidebar.products.inc.php');
            break;

        case 'panier':
            include('customer/sidebar.buy.inc.php');
            break;

        default:
            include('sidebar.default.inc.php');
    }

    switch($current_page) // partie droite de la topsection
    {
        case 'inscription':
            include('customer/topsection.inscription.inc.php');
            break;

        case 'inscription-ok':
            include('customer/topsection.inscription_succes.inc.php');
            break;

        /*case 'panier':
            break;*/

        case 'home': // à décider
            include('topsection.home.inc.php');
            break;

        case 'chooseregion':
            include('topsection.region_select.inc.php');
            break;

        case 'voir':
            include('produits/view/topsection.voir.inc.php');
            break;

        case 'panier':
            include(__DIR__.'/customer/purchase/panier/topsection.panier.inc.php');
            break;

        case 'mes-commandes':
            include('customer/account/topsection.last_orders.inc.php');
            break;

        case 'commande':
            include('customer/account/topsection.show_order.inc.php');
            break;

        case "mes-adresses":
            include('customer/account/topsection.my_addresses.inc.php');
            break;

        case 'passer-commande':
            include('customer/purchase/passer-commande/passer-commande.inc.php');
            break;

        case 'valisette':
            include('produits/view/topsection.valisette.inc.php');
            break;

        case 'produits':
            include ('topsection.default.inc.php');
            break;

        case 'generic':
            include ('topsection.genericpage.inc.php');
            break;

        case '404':
            include('topsection.404.inc.php');
            break;

        case 'mdp-oublie':
            include('customer/account/forgotten_password/topsection.mot-de-passe-oublie.inc.php');
            break;

        case 'nouveau-mdp':
            include('customer/account/forgotten_password/topsection.nouveau-mdp.inc.php');
            break;

        case 'moi':
            include('customer/account/topsection.moi.inc.php');
            break;

        case 'mes-parametres':
            include('customer/account/my_settings/my_settings.get.php');
            break;

        case 'mes-coupons':
            include('customer/account/topsection.coupons.inc.php');
            break;

        case 'contact':
            include('page/topsection.contact.inc.php');
            break;

        case 'erreur':
            include('topsection.erreur.inc.php');
            break;

        case 'verification':
            include('customer/topsection.inscription_confirmed.inc.php');
            break;

        case 'thankyou':
            include('customer/purchase/passer-commande/topsection.order_thankyou.inc.php');
            break;

        default: // produits, default
            include ('topsection.home.inc.php');
    }
?>