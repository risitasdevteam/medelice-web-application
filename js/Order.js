/**
 * Created by jsanglier on 26/08/2018.
 */

class OrderUtil{
    static checkout(token, token_hash, billing_address_signature, delivery_address_signature, delivery_mode, stripe_source, three_d_secure, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: "passer-commande",
            passer_commande_token: token,
            passer_commande_token_hash: token_hash,
            billing_address_signature: billing_address_signature,
            delivery_address_signature: delivery_address_signature,
            delivery_mode: delivery_mode,
            stripe_source: stripe_source,
            three_d_secure: three_d_secure
        };

        provider.classic_callback = true;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback(self, response);
        });
    }

    static three_d_secure_final_checkout(customer_id, order_id, stripe_source_token, stripe_source_secret_token, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: "poll-3dsecure",
            customer_id: customer_id,
            order_id: order_id,
            stripe_source_token: stripe_source_token,
            stripe_source_token_secret: stripe_source_secret_token
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static cancelled(order_id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: "commande-annulee",
            order_id: order_id
        };

        provider.classic_callback = false;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }
}