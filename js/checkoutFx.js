function toAddressesFormBackward()
{
    $('#addresses-container').css(
        {
            'display':'block',
            'width':'30%'
        }
    );

    $('#bluecard-container').css(
        {
            'display':'none'
        }
    );

    setTimeout(function () {
        $('#addresses-container').css(
            {
                'opacity':'1'
            }
        );
    }, 550);

    disableSidebarOrderLink("sidebar-payment");

    //update_url_parameters({"sub": "delivery"});
}

function toPaymentFormForward()
{
    $('#addresses-container').css(
        {
            'opacity':'0'
        }
    );

    $('#bluecard-container').css(
        {
            'display':'block'
        }
    );

    setTimeout(function () {
        $('#addresses-container').css(
            {
                'width':'0%',
            }
        );

        setTimeout(function () {
            $('#addresses-container').css(
                {
                    'display':'none'
                }
            );
        }, 520);
    }, 550);

    enableSidebarOrderLink("sidebar-payment");

    //update_url_parameters({"sub": "payment"});
}

function toPaymentFormBackward()
{
    $('#bluecard-container').css(
        {
            'display':'block',
            'width':'30%'
        }
    );

    $('#payment-summary').css(
        {
            'display':'none'
        }
    );

    setTimeout(function () {
        $('#bluecard-container').css(
            {
                'opacity':'1'
            }
        );
    }, 550);

    disableSidebarOrderLink("sidebar-confirm");

    //update_url_parameters({"sub": "delivery"});
}

function toConfirmFormForward()
{
    $('#bluecard-container').css(
        {
            'opacity':'0'
        }
    );

    $('#payment-summary').css(
        {
            'display':'block'
        }
    );

    setTimeout(function () {
        $('#bluecard-container').css(
            {
                'width':'0%'
            }
        );

        setTimeout(function () {
            $('#bluecard-container').css(
                {
                    'display':'none'
                }
            );
        }, 520);
    }, 550);

    enableSidebarOrderLink("sidebar-confirm");

    //update_url_parameters({"sub": "confirm"});
}