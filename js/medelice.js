/**
 * Created by jsanglier on 06/08/2017.
 */

var NotificationType = {
    INFO: "info",
    SUCCESS: "success",
    WARN: "warning",
    ERROR: "error"
};

function info_notification(msg){
    var notification = new Notification();
    notification.type = "info";
    notification.message = msg;

    var factory = new NotificationFactory();
    factory.show(notification);
}

function error_notification(msg){
    var notification = new Notification();
    notification.type = "alert";
    notification.message = msg;

    var factory = new NotificationFactory();
    factory.show(notification);
}

function notification(type, msg, delay = 5000, alertIcon = 'fa fa-exclamation-circle', confirmIcon = 'fa fa-check-circle'){
    /*var notyf = new Notyf({
        delay:delay,
        alertIcon:alertIcon,
        confirmIcon:confirmIcon
    });*/

    switch (type){
        case "info":
            notyf.confirm(msg);
            break;
        default:
            notyf.alert(msg);
    }
}

var scroll_keys = {37: 1, 38: 1, 39: 1, 40: 1};

function preventDefault(e)
{
    e = e || window.event;
    if (e.preventDefault)
        e.preventDefault();
    e.returnValue = false;
}

function preventDefaultForScrollKeys(e)
{
    if (scroll_keys[e.keyCode]) {
        preventDefault(e);
        return false;
    }
}

function disableScroll()
{
    if (window.addEventListener) // older FF
        window.addEventListener('DOMMouseScroll', preventDefault, false);
    window.onwheel = preventDefault; // modern standard
    window.onmousewheel = document.onmousewheel = preventDefault; // older browsers, IE
    window.ontouchmove  = preventDefault; // mobile
    document.onkeydown  = preventDefaultForScrollKeys;
}

function enableScroll()
{
    if (window.removeEventListener)
        window.removeEventListener('DOMMouseScroll', preventDefault, false);
    window.onmousewheel = document.onmousewheel = null;
    window.onwheel = null;
    window.ontouchmove = null;
    document.onkeydown = null;
}

function getCartCount(){
    var selector = $("#cart_count");
    return selector[0].innerHTML = parseInt(selector[0].innerHTML);
}

function setCartCount(value){
    var selector = $("#cart_count");
    selector[0].innerHTML = (parseInt(selector[0].innerHTML) + value);
}

function resetCartCountTo(value){
    var selector = $("#cart_count");

    selector[0].innerHTML =  parseInt(value);
}



function post(url, serialized_data, ok_status_callback, error_status_callback, standard_error_handler){
    return $.post(url, serialized_data, function(response){
        response = JSON.parse(response);
        switch (response.status){
            case 'ok':
                ok_status_callback.call(this, response);
                break;

            case 'error':
                error_status_callback(this, response);
                if (standard_error_handler == true)
                    notification("error", response.payload.error);

                break;
        }
    });
}

function serializeForm(jquery_selector){
    var array = jquery_selector.serializeArray();
    var twisted_array = {};
    for (var i = 0; i < array.length; i++){
        twisted_array[array[i].name] = array[i].value;
    }
    return twisted_array;
}

function get_bill(callback) {
    $.get('/customer/purchase/panier/REST_bill.php').done(function(response) {
        response = JSON.parse(response);
        switch (response.status) {
            case 'ok':
                var payload = response.payload;
                var products = payload.products;
                var price = payload.price;
                var mass = payload.mass;
                var shipping = payload.taxes;
                var discount = payload.discount;
                var total = payload.total;

                var returned_results = {
                    'products': products,
                    'price': price,
                    'mass': mass,
                    'taxes': shipping,
                    'discount': discount,
                    'total': total
                };

                callback.call(this, returned_results);
                break;

            case 'error':
                return null;
        }
    });
}

function get_url_parameters(){
    var queryParameters = {}, queryString = location.search.substring(1),
        re = /([^&=]+)=([^&]*)/g, m;

    while (m = re.exec(queryString)) {
        queryParameters[decodeURIComponent(m[1])] = decodeURIComponent(m[2]);
    }

    return queryParameters;
}

function update_url_parameters(array){
    var queryParameters = get_url_parameters();
    $.forEach(array, function(index, item){
       queryParameters[index] = item;
    });

    location.search = $.param(queryParameters); // Causes page to reload
}

//http://www.josscrowcroft.com/2011/code/format-unformat-money-currency-javascript/
// Extend the default Number object with a formatMoney() method:
// usage: someVar.formatMoney(decimalPlaces, symbol, thousandsSeparator, decimalSeparator)
// defaults: (2, "$", ",", ".")
Number.prototype.formatMoney = function(places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "") + symbol;
};