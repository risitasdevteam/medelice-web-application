/**
 * Created by jsanglier on 09/08/2018.
 */

class NotificationFactory{

    constructor(){
    }

    show(notification){
        if (!notification instanceof Notification)
            throw new TypeError("notification must be an instance of Notification");

        notyf.delay = notification.delay;
        switch (notification.type){
            case "info":
                notyf.confirm(notification.message);
                break;

            default:
                notyf.alert(notification.message);
        }
    }

    static init(){
        return notyf = new Notyf();
    }
}