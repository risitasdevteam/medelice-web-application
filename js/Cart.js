
class CartUtil{
    static insert(ref, quantity, callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;
        provider.data = {
            page: 'ajout-panier',
            ref: ref,
            quant: quantity
        };

        provider.http_post(function(){
            var $cart_count = $("#cart_count");
            var current_cart_quantity = parseInt($cart_count.text());
            var new_cart_quantity = parseInt(quantity) + current_cart_quantity;

            $cart_count.text(new_cart_quantity);

            if (typeof callback !== "undefined"){
                callback.call();
            }
        });
    }

    static set(ref, quantity, callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;
        provider.data = {
            page: 'ajout-panier',
            ref: ref,
            quant: quantity,
            type: 'set'
        };

        provider.http_post(function(){
            var $cart_count = $("#cart_count");
            var current_cart_quantity = parseInt($cart_count.text());
            var new_cart_quantity = parseInt(quantity) + current_cart_quantity;

            $cart_count.text(new_cart_quantity);

            if (typeof callback !== "undefined"){
                callback.call();
            }
        });
    }

    static delete(ref, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = false;
        provider.data = {
            page: "suppression-panier",
            ref: ref
        };

        provider.http_post(function(response){
            var $cart_count = $("#cart_count");
            var current_cart_quantity = parseInt($cart_count.text());
            var new_cart_quantity = current_cart_quantity - parseInt(response.payload.quantity);

            $cart_count.text(new_cart_quantity);

            if (typeof callback !== "undefined") {
                callback.call(self, response);
            }
        }, function(response){
            if (typeof callback !== "undefined") {
                error_callback.call(self, response);
            }
        });
    }
}