/**
 * Created by jsanglier on 09/08/2018.
 */
class Notification{
    constructor(){
    }

    get type(){
        return this.style_type;
    }

    set type(type){
        this.style_type = type;
    }

    get message(){
        return this.msg;
    }

    set message(message){
        this.msg = message;
    }

    get delay(){
        return this.show_delay;
    }

    set delay(delay){
        this.show_delay = delay;
    }
}