/**
 * Created by jsanglier on 23/07/2018.
 */

function storageAvailable(type) {
    try {
        var storage = window[type],
            x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return false;
    }
}

function store(key, item){
    localStorage.setItem(key, item);
}

function get_stored_data(key){
    return localStorage.getItem(key);
}

/**
 * SQL Anti-redundunce
 * array(regions & boxes) encoded in b64
 * si non sauvegardé alors
    * sauvegarde
   sinon
    comparaison hashs entre script et local
        si différents
            sauvegarde à nouveau
 */
function compare_valisettes_and_regions_hash(){
    var regions_hash = get_stored_data("regions");
    var valisettes_hash = get_stored_data("valisettes");
}

function save_valisettes(valisettes_json){

}