
function showLoginForm()
{
    var login_form = $('#login-form');
    var acc_btn = $('#myaccount-button');

    acc_btn.css(
        {
            'background-color':'#ffffff',
            'color':'#000000'
        }
    );

    login_form.css(
        {
            'display':'block'
        }
    );

    setTimeout(function()
        {
            login_form.css(
                {
                    'opacity':'1'
                }
            )
        },1
    );
}

function hideLoginForm()
{
    var login_form = $('#login-form');
    var acc_btn = $('#myaccount-button');

    acc_btn.css(
        {
            'background-color':'inherit',
            'color':'white'
        }
    );

    login_form.css(
        {
            'opacity':'0'
        }
    );

    var overlay = document.getElementById('warnLoginForm-overlay');
    if(overlay !== null)
    {
        document.body.removeChild(overlay);
    }


    setTimeout(function()
        {
            login_form.css(
                {
                    'display':'none'
                }
            )
        },210
    );
}

function warnLoginForm(redirect = "")
{
    showLoginForm();

    var div = document.createElement("div");
    div.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
    div.style.position = "fixed";
    div.style.top = 0;
    div.style.left = 0;
    div.style.right = 0;
    div.style.bottom = 0;
    div.style.zIndex = 666;
    div.id = "warnLoginForm-overlay";

    var body = document.body;
    body.appendChild(div);

    if (redirect !== ''){
        var $login_form = $('#login-form');
        $('<input type="hidden" name="redirect" value="' + redirect + '"/>').appendTo($login_form);
    }

    blink(3);
}

function warnLoginFormBeforeBuy(token, token_hash){
    warnLoginForm();

    var $login_form = $('#login-form');

    $('<input type="hidden" name="passer_commande_token" value="' + token + '"/>').appendTo($login_form);
    $('<input type="hidden" name="passer_commande_token_hash" value="' + token_hash + '"/>').appendTo($login_form);
    $('<input type="hidden" name="redirect" value="passer-commande"/>').appendTo($login_form);
}

function blink(blinks)
{
    var fields = $('.login-form-field');
    var delay = 300;

    fields.css(
        {
            "background-color":"#ff5d54"
        }
    );

    setTimeout(function() {
        fields.css(
            {
                "background-color":"#ffffff"
            }
        );

        setTimeout(
            function()
            {
                if(blinks > 1)
                    blink(blinks - 1);
            }, delay
        );

    },delay);
}

function toggleLoginForm()
{
    if($('#login-form').css('display') === 'none')
    {
        showLoginForm();
    }
    else
    {
        hideLoginForm();
    }
}
