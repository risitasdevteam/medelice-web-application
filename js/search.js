/**
 * Created by jsanglier on 04/11/2017.
 */

$(document).ready(function () {
    var timeout = null;
    var search_box_selector = $('#search_box');

    search_box_selector.keyup(function () {
        if (timeout != null) {
            clearTimeout(timeout);
        }
        timeout = setTimeout(function () {
            timeout = null;

            var results_container_selector = $('#search-results');
            results_container_selector.empty();

            var data = {
                'query': search_box_selector.val()
            };

            $.post("search.php", data,
                function(response) {
                    response = JSON.parse(response);
                    switch (response.status){
                        case "ok":
                            if (Object.keys(response.payload).length > 0) {
                                var products = response.payload.products;
                                var products_length = products.length;
                                var alone = products_length < 2;

                                if (products_length > 0) {
                                    $.each(products, function(index, product){
                                        var ref = product.ref;
                                        var name = product.name;
                                        var brand = product.brand;
                                        var price = product.price;
                                        var img = product.img;

                                        results_container_selector.append(
                                            "<a class='search-result' href='index.php?page=voir&ref=" + ref + "'>"
                                            + "<div class='search-result-preview-product'>"
                                            + "   <img style='width: 100%; height: auto' src='" + img + "'/>"
                                            + "</div>"
                                            + "<span class='search-result-name'>" + name + " (" + brand + ")</span>"
                                            + "<span class='search-result-price quicksand'>" + price + "€</span>" // TODO attention à la devise
                                            + "</a>"
                                        );


                                        if(index != products_length && !alone)
                                            results_container_selector.append("<hr style='margin: 0.8vw 0'>");
                                    });
                                }else{
                                    results_container_selector.append("<span style='font-size: 1vw; font-weight: bold'>Aucun produit trouvé.</span>");
                                }
                            }
                            break;

                            case 'error':
                                notification(NotificationType.ERROR, response.payload.error);
                                break;
                    }
            });


            if(search_box_selector.val())
            {
                results_container_selector.css(
                    {
                        "display": "block",
                        "opacity": "1"
                    }
                );
            }
            else
            {
                results_container_selector.css("opacity", "0");
                setTimeout(function () {
                    results_container_selector.css("display", "none");
                }, 1500);
            }

        }, 440);
    });
});