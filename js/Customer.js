/**
 * Created by jsanglier on 09/08/2018.
 */

class CustomerUtil{
    static is_connected(callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php?page=verfication-connexion";
        provider.classic_callback = false;

        provider.http_get(function(response){
            callback.call(self, response.payload.connected);
        });
    }

    static is_connected_for_checkout(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php?page=verfication-connexion&check_time=true";
        provider.classic_callback = false;

        provider.http_get(function(response){
           callback.call(self, response);
        }, function(){
            error_callback.call(self, response);
        });
    }

    static refresh_topbar(connected){
        var $login_status = $("#login-status");
        var $login_status_parent = $login_status.parent();
        var $shopping_cart_section = $('#shopping-cart-section');

        if (connected){
            $login_status.text('MON COMPTE');
            $login_status_parent.attr("href", '/index.php?page=moi');

            $('<a class="topbar-element topbar-border-left button-link right" href="/index.php?page=deconnexion">DECONNEXION</a>').insertAfter($shopping_cart_section);

            hideLoginForm();
        }
        else{
            $login_status.text('CONNEXION');
            $login_status_parent.attr("href", '#');
            $login_status.on("click", function(e){
                e.preventDefault();

                toggleLoginForm();
            });
        }
    }

    static confirm_password(unique_id, verification_key, password, confirm_password, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
          page: 'confirmation-mdp-oublie',
          uid: unique_id,
          vkey: verification_key,
          passwd: password,
          passwd_confirm: confirm_password
        };
        provider.classic_callback = true;

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
           error_callback.call(self, response);
        });
    }

    static send_forgotten_password(email, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: 'envoi-lien-mdp-oublie',
            email: email
        };
        provider.classic_callback = true;

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static login(data, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;

        provider.data = data;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static register(mail, password, password_confirm, firstname, surname, birthday, phone, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: 'inscription',
            email: mail,
            password1: password,
            password2: password_confirm,
            firstname: firstname,
            surname: surname,
            birthday: birthday,
            phone: phone
        };
        provider.classic_callback = true;

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static exists_by_mail(mail, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: 'existence-compte',
            email: mail
        };
        provider.classic_callback = false;

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        });
    }

    static verify(verification_key, id, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
          page: "verification",
            vkey: verification_key,
            uid: id
        };

        provider.classic_callback = true;
        provider.http_post(function(response){
                callback.call(self, response);
        }, function(response){
                error_callback.call(self, response);
        });
    }

    static send_contact(subject, content, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.data = {
            page: "contact-client",
            subject: subject,
            content: content
        };

        provider.classic_callback = true;
        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            error_callback.call(self, response);
        })
    }

    static get_addresses(callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php?page=recuperation-adresses";
        provider.classic_callback = false;
        provider.http_get(function(response){
            callback.call(self, response);
        }, function(){
            error_callback.call(self, response);
        })
    }

    static edit_address(emitter_name, address1, address2, city, zip, state, country, phone, signature, weight, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;
        provider.data = {
            'page': 'edition-adresse',
            'emitter_name': emitter_name,
            'address1': address1,
            'address2': address2,
            'city': city,
            'zip': zip,
            'state': state,
            'country': country,
            'phone': phone,
            'weight': weight,
            'signature': signature
        };

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            if (typeof error_callback !== "undefined")
                error_callback.call(self, response);
        });
    }

    static add_address(emitter_name, address1, address2, city, zip, state, country, phone, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;
        provider.data = {
            'page': 'ajout-adresse',
            'emitter_name': emitter_name,
            'address1': address1,
            'address2': address2,
            'city': city,
            'zip': zip,
            'state': state,
            'country': country,
            'phone': phone
        };

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            if (typeof error_callback !== "undefined")
                error_callback.call(self, response);
        });
    }

    static remove_address(signature, callback, error_callback){
        var provider = new RESTResponseProvider();
        provider.url = "/router.php";
        provider.classic_callback = true;
        provider.data = {
            'page': 'suppression-adresse',
            'signature': signature
        };

        provider.http_post(function(response){
            callback.call(self, response);
        }, function(response){
            if (typeof error_callback !== "undefined")
                error_callback.call(self, response);
        });
    }
}