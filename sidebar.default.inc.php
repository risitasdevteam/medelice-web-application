<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 01/11/2017
 * Time: 16:06
 */

?>

    <div id="sidebar" class="black-bg colored-font-opposite">
        <div class="sidebar-content" style="width: 70%; margin: 0 auto">
            <span class="bromello" style="color: white; font-size: 3vw">Découvrez</span><br>
            <br>
            <br>
            <a class="sidebar-link poiret" href="/index.php?page=chooseregion&mode=valisette">
                <div class="sidebar-link-dot">
                    <?php include(__DIR__ . '/images/logo_icon_inverted_color.svg.php'); ?>
                </div>
                <div class="sidebar-link-text" >NOS VALISETTES GASTRONOMIQUES RAFFINÉES</div>
            </a><br>
            <br>
            <a class="sidebar-link poiret" href="/index.php?page=chooseregion&mode=produits">
                <div class="sidebar-link-dot">
                    <?php include(__DIR__ . '/images/logo_icon_inverted_color.svg.php'); ?>
                </div>
                <div class="sidebar-link-text" >NOS PRODUITS AU DÉTAIL</div>
            </a><br>
            <div style="height: 10vw"></div>
        </div>
        <?php

        $no_france_pages = array("404", "generic", "moi", "mes-parametres", "mes-commandes", "commande", "mes-adresses", "erreur", "inscription", "inscription-ok", "mdp-oublie", "chooseregion", "mes-coupons", "contact", "nouveau-mdp", "verification", "thankyou");

        if(!in_array($current_page, $no_france_pages))
        {
            echo '<div class="sidebar-country-container">';
                include(__DIR__ . '/images/france.svg.php');
            echo '</div>';
        }
        
        ?>
    </div>
