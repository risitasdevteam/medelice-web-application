<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/08/2017
 * Time: 22:08
 */

require_once(__DIR__.'/vendor/autoload.php');
require_once(__DIR__.'/utility/JsonResponse.class.php');
require_once(__DIR__.'/utility/RegexUtil.php');
require_once(__DIR__.'/utility.inc.php');
use Elasticsearch\ClientBuilder;

/*
 * Ordre :
 *      - recherche des régions dans le tableau des terms
 *      - recherche ensuite des produits
 *      - récupération des ids de boxes contenu dans les produits
 *      - recherche des boxes associées
 *      - classement : boxes (>$), produits
 */
try{
    post_thrown();
    $query = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "query", FILTER_SANITIZE_STRING, true);

    $client = ClientBuilder::create()->build();
    $params = [
        'index' => 'products',
        'type' => 'product',
        'body' => [
            'query' => [
                'multi_match' => [
                    "query" => $query,
                    "fields" => ["name", "ref"]
                ]
            ]
        ]
    ];

    $search_response = $client->search($params);
    $json_response = new JsonResponse(JsonResponseStatusType::OK, null);
    $products = array();

    if ($search_response["hits"]["total"] > 0){
        $hits =  $search_response["hits"]["hits"];

        foreach ($hits as $hit){
            $source_aka_raw_product = $hit["_source"];
            array_push($products, $source_aka_raw_product);
        }
    }

    $json_response->add_payload(new JsonResponsePayload('products', $products));
    $json_response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}