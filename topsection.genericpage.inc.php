<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 24/11/2017
 * Time: 20:57
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/page/GenericPage.php');

?>

<div id="topsection">
    <div style="width: 90%; margin: 5vw 5%">
        <?php
            $id = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
            if ($id == false){
                redirect_to_error_page(404);
            }
            else
            {
                $page_provider = new DBGenericPageProvider();
                $page = $page_provider->get($id);
                if ($page === null){
                    redirect_to_error_page(404);
                }else{
                    echo html_entity_decode(htmlspecialchars_decode($page->getContent()));
                }
            }
        ?>
    </div>
</div>
