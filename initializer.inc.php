<?php
/**
 * Fichier d'initialisation des supers-globales.
 */

require_once(__DIR__.'/lang/i18n.class.php');
require_once(__DIR__.'/utility.inc.php');
require_once(__DIR__.'/stripe/init.php');
require_once(__DIR__.'/utility/SQLUtil.php');

if (!defined('DEBUG')){
    define('DEBUG', strcmp(gethostname(), "medelice") !== 0);
}

if(!defined('DEV')){
    $cousUID = $_SERVER['HTTP_USER_AGENT'].$_SERVER['REMOTE_ADDR'];
    define('DEV', md5($cousUID));
}

//mails pour les erreurs techniques
if (!defined('DEV_MAILS')){
    define('DEV_MAILS', array('jsgr@protonmail.ch', 'romain.galle@outlook.fr'));
}

if(!defined('BOT_MAIL')){
    define('BOT_MAIL', 'med@medelice.com');
}

if (DEBUG){
    error_reporting(E_ALL | E_STRICT);
    ini_set('display_errors', 'On');
    ini_set('log_errors', 'Off');
}else{
    error_reporting(0);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    //ini_set('error_log', '/Medelice.com/error_log');
}

if(!defined('DB_ADDRESS') || !defined('DB_USER') || !defined('DB_PASSWORD') || !defined('DB'))
{
    if (DEBUG) {
        define('DB_ADDRESS', "localhost");
        define('DB_USER', "root");
        define('DB_PASSWORD', "root");
        define('DB', "medelice");
    }
    else{
        define('DB_ADDRESS', "localhost");
        define('DB_USER', "dev");
        define('DB_PASSWORD', "Skj37dbzet6!Ep3h");
        define('DB', "medelicepebd");
    }
}

if (!defined('USERS_SESSIONS_TABLE')){
    define('USERS_SESSIONS_TABLE', 'users_sessions');
}

if(!defined('GATEWAY_TABLE'))
{
    define('GATEWAY_TABLE', "users_gateway");
}

if(!defined('USERS_TABLE'))
{
    define('USERS_TABLE', "users");
}

if(!defined('PRODUCTS_TABLE'))
{
    define('PRODUCTS_TABLE', "products");
}

if(!defined('OPTIONS_TABLE'))
{
    define('OPTIONS_TABLE', "config_website_options");
}

if(!defined('PRODUCTS_CATEGORIES_TABLE'))
{
    define('PRODUCTS_CATEGORIES_TABLE', "products_categories");
}

if (!defined('LOGIN_ATTEMPTS_TABLE')){
    define('LOGIN_ATTEMPTS_TABLE', "users_login_attempts");
}

if (!defined('PRODUCTS_BOXES_TABLE')){
    define('PRODUCTS_BOXES_TABLE', "products_boxes");
}

if (!defined('PRODUCTS_REGIONS_TABLE')){
    define('PRODUCTS_REGIONS_TABLE', "products_regions");
}

if (!defined('LOGIN_ATTEMPTS')){
    define('LOGIN_ATTEMPTS', true);
}

if (!defined('SEARCH_MAX_TERMS')){
    define('SEARCH_MAX_TERMS', 10);
}

if (!defined('SITE_ROOT')){
    define('SITE_ROOT', dirname(__FILE__));
}

if (!defined('PRODUCTS_SUBC_TABLE')){
    define('PRODUCTS_SUBC_TABLE', 'products_sub_categories');
}

if (!defined('ORDERS_TABLE')){
    define('ORDERS_TABLE', 'users_orders');
}

if (!defined('COUPONS_TABLE')){
    define('COUPONS_TABLE', 'users_coupons');
}

if (!defined('USERS_CARTS')){
    define('USERS_CARTS', 'users_carts');
}

if (!defined('PAGES_TABLE')){
    define('PAGES_TABLE', 'config_pages');
}

if (!defined('SALT')){
    define('SALT', 'gXZD6BzXj2HuK7NJ4xGxyeZwrmRRrWbpHac3');
}

if (!defined('STRIPE_PVTKEY')){
    if (DEBUG){
        define('STRIPE_PVTKEY', 'sk_test_RCN9bUjMLIuZHL18KilhBUaJ');
    }else{
        define('STRIVE_PVTKEY', '');
    }
}

setlocale(LC_TIME, 'fr_FR');
setlocale(LC_MONETARY, 'fr_FR'); //TODO set locale money to EUR, must change for non FRENCH users ?

$i18n = new i18n(SITE_ROOT. '/lang/translations/lang_{LANGUAGE}.ini', SITE_ROOT. '/lang/cache', 'fr');
$i18n->init();

require_once(__DIR__ . '/customer/misc/UserSession.class.php');
require_once(__DIR__.'/customer/security/UserToken.class.php');

if (session_status() == PHP_SESSION_ACTIVE) {
    //variable par défaut
    if (!isset($_SESSION['loggedIn'])) {
        $_SESSION['loggedIn'] = false;
        //$_SESSION['remember'] = false;
    }

    //dépendance
    require_once(__DIR__ . '/customer/User.class.php');
    //user qui vient de se loguer avec le remember

    //TODO faille critique ici
    if (isset($_SESSION['remember']) && $_SESSION['remember'] == true) {
        if (!isset($_SESSION['remember_cookie_set']) || $_SESSION['remember_cookie_set'] != true){}
            //User::setPersistentAuth(true); //ajout cookie + variable dans la db
    }

    //user non logué, on teste le login par cookie
    if (!$_SESSION['loggedIn']) {
        User::loginByPersistentAuth();
    }
}

UserToken::clear_died_session_tokens();

if(isset($_GET['page']))
    $current_page = $_GET['page'];
else
    $current_page = 'home';