<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 14/08/2018
 * Time: 17:10
 */

require_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
?>


<div id="topsection">
    <div class="quicksand bold" style="font-size: 1.6vw; margin: 1.5vw 5%;">Nous contacter</div>
    <div style="width: 90%; background-color: #EEEEEE; padding: 1vw 2.5%; margin: 1vw 2.5%; text-align: justify;">
        Si vous avez un problème, vous pouvez nous contacter. Cependant, notre équipe ayant déjà beaucoup de travail et afin d'éviter les question récurrentes, assurez-vous avant toute chose d'avoir consulté notre <a class="link" href="/index.php?page=generic&id=faq">FAQ</a>.
    </div>
    <form name="contact-form" style="width: 80%; margin: 1vw 10%;">
        <div class="fields-line">
            <div class="field-container" style="width: 100%;">
                <label class="field-descriptor" for="contact_topic" style="margin-bottom: 0.5vw">Sujet</label><br><br>
                <input type="text" class="field-input full-field" id="contact_topic">
            </div>
        </div>
        <div class="fields-line">
            <div class="field-container" style="width: 100%;">
                <label class="field-descriptor" for="contact_msg">Message</label><br><br>
                <textarea class="field-text full-field" id="contact_content" style="height: 15vw; outline: none; border-radius: 0.2vw; border: 0.052vw solid #ced4da; padding: 0.2vw 0.3vw;"></textarea>
            </div>
        </div>
        <button class="contact-form-send quicksand bold button">Envoyer</button>
    </form>
</div>

<script>
    function handler(){
        var $form = $("[name='contact-form']").on("submit", function(e){
            e.preventDefault();

            CustomerUtil.is_connected(function(connected){
                if (!connected){
                    warnLoginForm("contact");
                }
                else{
                    var subject = $('#contact_topic').val();
                    var content = $('#contact_content').val();

                    showLoadingScreen();

                    CustomerUtil.send_contact(subject, content, function(){
                        hideLoadingScreen();

                        setTimeout(function(){
                            location.href = "/index.php?page=home";
                        }, 3500);

                    }, function(){
                        hideLoadingScreen();
                    });
                }
            });
        });
    }

    $(document).ready(function(){
        handler();
    });
</script>
