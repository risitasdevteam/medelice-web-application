<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/08/2018
 * Time: 22:56
 */

abstract class DBGenericPageStructure{
    const columns = [
        "id",
        "title",
        "content",
        "creator",
        "creation_date"
    ];

    const table = "config_pages";
}

interface GenericPageProvider{
    public function get(string $unique_id);
    public function get_all();
    public function insert(GenericPage $generic_page);
    public function update(string $old_id, GenericPage $new_generic_page);
    public function remove(string $unique_id);
}

class DBGenericPageProvider implements GenericPageProvider{
    public function get(string $unique_id)
    {
        $columns = DBGenericPageStructure::columns;
        $raw_generic_page = SQLUtil::select(DBGenericPageStructure::table, $columns, [$columns[0] => $unique_id]);
        if (count($raw_generic_page) > 0){
            $pointer = $raw_generic_page[0];

            return GenericPage::from_array($pointer);
        }

        return null;
    }

    public function get_all(){
        $pages = array();

        $columns = DBGenericPageStructure::columns;
        $raw_generic_page = SQLUtil::select(DBGenericPageStructure::table, $columns, []);
        $rows_count = count($raw_generic_page);
        if ($rows_count > 0){
            for ($i = 0; $i < $rows_count; $i++) {

                $pointer = function($column_index) use ($i, $columns, $raw_generic_page){
                    return $raw_generic_page[$i][$columns[$column_index]];
                };

                $page_as_array = [$columns[0] => $pointer(0),
                    $columns[1] => $pointer(1),
                    $columns[2] => htmlspecialchars($pointer(2)),
                    $columns[3] => $pointer(2),
                    $columns[4] => $pointer(3)];

                array_push($pages, $page_as_array);
            }
        }

        return $pages;
    }

    public function insert(GenericPage $generic_page)
    {
        SQLUtil::insert(DBGenericPageStructure::table, $generic_page->to_array());
        return true;
    }

    public function update(string $old_id, GenericPage $new_generic_page)
    {
        $old_page = $this->get($old_id);
        if ($old_page == null){
            throw new MedeliceException("Config ".$old_id." doesn't exists");
        }

        SQLUtil::update(DBGenericPageStructure::table,
            GenericPage::array_difference($old_page, $new_generic_page),
            [
                array_values(DBGenericPageStructure::columns)[0] => $old_id
            ]);

        return true;
    }

    public function remove(string $unique_id){
        SQLUtil::delete(DBProductsStructure::table,
            [
                array_values(DBGenericPageStructure::columns)[0] => $unique_id
            ]);

        return true;
    }
}

class GenericPage
{
    private $id;
    private $title;
    private $content;
    private $creator;
    private $creation_date;

    public function __construct(){
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param mixed $creator
     */
    public function setCreator($creator)
    {
        $this->creator = $creator;
    }

    /**
     * @return mixed
     */
    public function getCreationdate()
    {
        return $this->creation_date;
    }

    /**
     * @param mixed $creation_date
     */
    public function setCreationdate($creation_date)
    {
        $this->creation_date = $creation_date;
    }

    public static function from_array(array $page_array){
        $page = new GenericPage();

        $mapped_numerically_columns_array = array_values(DBGenericPageStructure::columns);
        $get_column = function($index) use ($page_array, $mapped_numerically_columns_array){
            return $page_array[$mapped_numerically_columns_array[$index]];
        };

        $page->setId($get_column(0));
        $page->setTitle($get_column(1));
        $page->setContent(htmlspecialchars($get_column(2)));
        $page->setCreator($get_column(3));
        $page->setCreationdate($get_column(4));

        return $page;
    }

    public function to_array(){
        return array_combine(DBGenericPageStructure::columns,
            [$this->getId(),
             $this->getTitle(),
             $this->getContent(),
             $this->getCreator(),
             $this->getCreationdate()]);
    }

    public static function array_difference(GenericPage $page1, GenericPage $page2){
        $page1_matrix = get_object_vars($page1);
        $page2_matrix = get_object_vars($page2);

        $sql_update_sets_matrix = [];

        foreach($page1_matrix as $key => $value){
            $page2_matrix_value = $page2_matrix[$key];

            if ($value != $page2_matrix_value){
                $sql_update_sets_matrix[$key] = $page2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }
}