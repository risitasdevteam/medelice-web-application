<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2017
 * Time: 16:39
 */

require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/produits/Regions.class.php');
require_once(__DIR__ . "/produits/Valisette.php");
require_once(__DIR__ . '/produits/ProductsCategories.class.php');
require_once(__DIR__.'/customer/misc/UserUtil.php');

if(!isset($_SESSION['firstvisitoftheday']))
{
    $_SESSION['firstvisitoftheday'] = false;
    echo '
        <div id="overhaul">
            <div style="position: fixed; width: 100%">
                <img style="width: auto; height: 120vh; margin-left: auto; margin-right: auto; margin-top: -10vh; display: block" src="/images/welcome_slider_bg.svg"/>
            </div>
            <div style="position: fixed; width: 100%; height: 100%">
                <div style="width: 100%; background-color: #000000; height: 20vh; margin: 40vh 0"></div>
            </div>
            <div style="position: fixed; width: 100%; height: 100%">
                <img style="width: auto; height: 40vh; margin: 30vh auto; z-index: 1338; display: block" src="/images/logo.svg"/>
            </div>
        </div>
        <script>
            window.onload = function()
            {
                disableScroll();
                setTimeout(function () {
                    $(\'#overhaul\').css({"transform":"translate(0, -120%)"});
                    enableScroll();
                    
                    setTimeout(function() {
                        $(\'#overhaul\').css({"display":"none"});
                    }, 1500);
                }, 1500);
            }
        </script>
    ';
}

if($current_page == "produits" || $current_page == "valisette")
{
    // print region name
    $current_region_id = isset($_GET['reg']) ? $_GET['reg'] : Regions::getDefaultRegionId();
    if(!Regions::exists($current_region_id)) $current_region_id = "ARA";
    $current_category = isset($_GET['cat']) ? $_GET['cat'] : 0;
    if(!ProductsCategories::doesExists($current_category)) $current_category = 0;
}

$product_provider = new DBProductProvider();

if($current_page == "voir" || $current_page == "valisette")
{
    require_once(__DIR__ . "/produits/Product.php");

    $ref = isset($_GET['ref']);
    $current_region_id = "ARA"; // ça, il faut le laisser comme ça ! (valeur par défaut)
    $current_category = 0; // idem

    if ($ref != false && $ref != null)
    {
        $ref = RegexUtil::filter_in_var_with_regex(INPUT_GET, "ref", RegexUtil::intersect_product_and_box_ref(), false);
        $_current_product = ($current_page == "voir" ? $product_provider->get($ref) : new Valisette($ref));
        if($_current_product !== null || $_current_product->exists)
        {
            $current_region_id = $_current_product->getRegion();
            if ($current_page == "produits")
                $current_category = $_current_product->getCategory();
        }else{
            //  redirect_to_error_page(404);
        }
    }
}

if($current_page == "produits" || $current_page == "voir" || $current_page == "valisette")
    $colors = Regions::getColorsFromRegionId($current_region_id);
else
    $colors = array(0 => Regions::getDefaultColor(), 1 => "000000");

$_display_boxes = Valisette::get_filled_valisettes();
//var_dump($_display_boxes);
$_display_regions = $product_provider->get_regions_where_products_exists();

require_once(__DIR__.'/utility.inc.php');
require_once(__DIR__.'/customer/security/UserToken.class.php');
require_once(__DIR__.'/customer/User.class.php');

// TODO persistent auth => avertissement de login quand bouton passer ma commande (dans panier)

$login_token = UserToken::generate_token_and_hash('login', 360);
//var_dump($login_token);
?>

<header>
    <div class="topbar" style="">
        <div class="topbar-element topbar-border-right">
            <img class="" style="height: 60%; margin-right: 0.3vw" src="/images/flags/<?php echo $i18n->getAppliedLang() ?>.png"/>
            <span class="header-language-name"><?php echo strtoupper($i18n->getAppliedLang()); ?></span>
        </div>
        <div class="topbar-element">
            <?php echo L::description ?>
        </div>

        <?php

            if($maintenance)
            {
                echo '
                    <div class="topbar-element" style="color: red">
                        <i class="fa fa-warning" style="margin-right: 0.35vw"></i> MAINTENANCE EN COURS
                    </div>
                ';
            }

        ?>

        <a class="button-link topbar-element right" id="shopping-cart-section" href="/panier">
            <span style="color: white">
                <?php
                    require_once(__DIR__ . '/customer/purchase/CartUtil.php');

                    if (isset($_SESSION['panier'])){
                        echo '<span id="cart_count">'.CartUtil::total_items().'</span>';
                    }
                    else{
                        echo '<span id="cart_count">0</span>';
                    }
                ?>
                <!-- Afficher juste icone panier + quantité, et 'mon panier' quand survolé à la souris -->

                <i class="fa fa-shopping-cart" id="shopping-cart-icon" aria-hidden="true"></i>
                <span id="header-cart-text"></span>
            </span>
        </a>

        <?php

            if(isset($_SESSION['mail']))
            {
                echo '
                    <a class="topbar-element topbar-border-left button-link right" href="/index.php?page=deconnexion">
                        DECONNEXION
                    </a>
                ';
            }

        ?>

        <a class="topbar-element topbar-border-left button-link right" href="<?php if(isset($_SESSION['mail'])) { echo '/index.php?page=moi'; } else { echo '#'; } ?>" onclick="<?php if(!isset($_SESSION['mail'])) { echo 'toggleLoginForm()'; } else { echo ''; }  ?>">
            <?php
            if (isset($_SESSION['mail']))
            {
                echo '<span class="button-link-text" id="login-status">' . L::my_account . '</span>';
            }
            else
            {
                echo '<span class="button-link-text" id="login-status">CONNEXION</span>';
            }
            ?>
        </a>

        <?php
            if(UserUtil::is_a_data_manager()) // TODO fetch admin perm
            {
                echo '
                    <a class="topbar-element topbar-border-left button-link right" href="/dashboard/index.php?page=home">
                        ADMINISTRATION
                    </a>
                ';
            }
        ?>

        <div class="topbar-element right">
            <div class="search-bar">
                <input type="search" placeholder="<?php echo L::search ?>" id="search_box" autocomplete="off">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div>
            <div id="search-results">

            </div>
        </div>

        <script src="/js/Customer.js"></script>
        <form id="login-form">
            <?php UserToken::insert_token_and_hash_in_current_page('login', $login_token['token'], $login_token['hash']) ?>
            <input type="hidden" name="page" value="connexion">
            <div class="login-form-field-container">
                <span class="login-form-field-title">Email</span><br>
                <input class="login-form-field" type="email" name="mail" placeholder="exemple@hebergeur.fr">
            </div>
            <div class="login-form-field-container">
                <span class="login-form-field-title">Mot de passe</span><br>
                <input class="login-form-field" type="password" name="password" placeholder="●●●●●●●●●●●●●●">
            </div>
            <div class="login-form-field-container" style="margin-left: 0.8vw">
                <input type="checkbox" id="login-remember-me"><label id="login-remember-me-label" for="login-remember-me">Se souvenir de moi</label><br>
            </div>

            <button type="submit" class="login-button quicksand bold">
                Connexion
            </button><br>
            <hr class="login-form-separator">
            <div class="login-bottom-register">
                Vous n'avez pas de compte ? <a class="link" href="/index.php?page=inscription">Inscrivez-<br>vous</a>
            </div>
            <a class="link" href="/index.php?page=mdp-oublie">Mot de passe oublié ?</a>
        </form>
    </div>

    <script>
        $('#shopping-cart-section').hover(
            function () {
                $('#header-cart-text').html('<?php echo L::my_cart ?>');
                $('#header-cart-text').css(
                    {
                        'margin-right':'0.1vw'
                    }
                );
            }, function () {
                $('#header-cart-text').html('');
                $('#header-cart-text').css(
                    {
                        'margin-right':'0'
                    }
                );
            }
        )
    </script>
</header>

<div class="navigation-bar">
    <?php include(__DIR__ . '/images/logo.svg.php'); ?>
    <div class="navigation-bar-menu quicksand bold">
        <div class="navbar-li quicksand bold" id="navbar-boxes">
            <?php echo L::navbar_boxes?>
            <div class="navdisplay" id="boxes-navdisplay">
                <?php
                    $i = 0;
                    foreach($_display_boxes as $valisette)
                    {
                        if (!$valisette instanceof Valisette){
                        }

                        $region = $valisette->getRegion();
                        $title = $valisette->getTitle();
                        $temp_colors = Regions::getColorsFromRegionId($region);
                        echo '
                            <a href="/valisettes/'.$valisette->getRef().'" class="navdisplay-valisette-object" id="navdisplay-valisette-'.$valisette->getRef().'" style="float: '.($i % 2 == 0 ? 'left' : 'right').'">
                                <img style="height: 80%; margin: auto 0" src="/images/valisettes/'.$region.'.png"><br>
                                <span id="navdisplay-valisette-'.$region.'-text" style="padding: 0 2vw 0 0.2vw; font-size: 1vw">Valisette '.$title.'</span>
                            </a>
                            <script>
                                $("#navdisplay-valisette-'.$valisette->getRef().'").hover(
                                    function(e)
                                    {
                                        $("#navdisplay-valisette-'.$valisette->getRef().'").css(
                                            {
                                                "background-color":"#'.$temp_colors[0].'"    
                                            }    
                                        );
                                        
                                        $("#navdisplay-valisette-'.$region.'-text").css(
                                            {
                                                "color":"#'.$temp_colors[1].'"
                                            }
                                        );
                                    },
                                    function(e)
                                    {
                                        $("#navdisplay-valisette-'.$valisette->getRef().'").css(
                                            {
                                                "background-color":"#ffffff"    
                                            }
                                        );
                                        
                                        $("#navdisplay-valisette-'.$region.'-text").css(
                                            {
                                                "color":"#000000"
                                            }
                                        );
                                    }
                                );
                            </script>
                        ';
                        $i++;
                    }
                ?>
            </div>
        </div>

        <div class="navbar-li quicksand bold" id="navbar-products">
            <?php echo L::navbar_products?>
            <div class="navdisplay" id="products-navdisplay">
                <?php
                    foreach($_display_regions as $region)
                    {
                        $region_name = $region["name"];
                        $region_id = $region["id"];
                        $region_color = $region["hex"];
                        $region_neg_color = $region["neg_hex"];

                        echo '
                            <a href="/produits/'.$region_id.'/0" class="navdisplay-object" id="regdisplay-'.$region_id.'" style="background-color: #'.$region_color.'; color: #'.$region_neg_color.'">
                                <div class="navdisplay-object-overlay" id="regdisplay-'.$region_id.'-overlay"></div>
                                <div class="navdisplay-object-text" id="regdisplay-'.$region_id.'-name">'.$region_name.'</div>
                            </a>
                            <script>
                                var containerSelector = $("#regdisplay-'.$region_id.'");
                                
                                containerSelector.css(
                                    {
                                        "background-image":"url(/images/regions/carte_sidebar/'.$region_id.'.jpg)",
                                        "background-size":"100% auto",
                                        "background-position":"center"
                                    }
                                );
                                
                                containerSelector.hover(
                                    function(e) 
                                    {
                                        $("#regdisplay-'.$region_id.'-name").css(
                                            {
                                                "opacity":"1"
                                            }
                                        );
                                        
                                        $("#regdisplay-'.$region_id.'-overlay").css(
                                            {
                                                "opacity":"0.5"
                                            }
                                        );
                                        
                                       $("#regdisplay-'.$region_id.'").css(
                                            {
                                                "background-image":"url(/images/regions/carte_sidebar/'.$region_id.'.jpg), linear-gradient(#ffaaee, #d15f3c)",
                                                "background-size":"120% auto"
                                            }
                                        );
                                    },
                                    function(e) 
                                    {
                                        $("#regdisplay-'.$region_id.'-name").css(
                                            {
                                                "opacity":"0"
                                            }
                                        );
                                        
                                        $("#regdisplay-'.$region_id.'-overlay").css(
                                            {
                                                "opacity":"0"
                                            }
                                        );
                                        
                                        $("#regdisplay-'.$region_id.'").css(
                                            {
                                                "background-image":"url(/images/regions/carte_sidebar/'.$region_id.'.jpg)",
                                                "background-size":"100% auto"
                                            }
                                        );
                                    }
                                );
                            </script>
                        ';
                    }
                ?>
            </div>
        </div>

        <a class="navbar-li quicksand bold" href="/index.php?page=generic&id=about">
            <?php echo L::navbar_about?>
        </a>
        <a class="navbar-li quicksand bold" href="/index.php?page=contact">
            <?php echo L::navbar_contact?>
        </a>
    </div>
</div>

<script>
    $('#navbar-boxes').hover(
        function(e)
        {
            $('#boxes-navdisplay').css(
                {
                    "display":"inline-block",
                    "opacity":"0"
                }
            );
            setTimeout(function () {
                $('#boxes-navdisplay').css(
                    {
                        "opacity":"1"
                    }
                );
            }, 0);
        },
        function(e)
        {
            $('#boxes-navdisplay').css(
                {
                    "opacity":"0"
                }
            );

            setTimeout(function () {
                $('#boxes-navdisplay').css(
                    {
                        "display":"none"
                    }
                );
            }, 200);
        }
    );

    $('#navbar-products').hover(
        function(e)
        {
            $('#products-navdisplay').css(
                {
                    "display":"inline-block",
                    "opacity":"0"
                }
            );
            setTimeout(function () {
                $('#products-navdisplay').css(
                    {
                        "opacity":"1"
                    }
                );
            }, 0);
        },
        function(e)
        {
            $('#products-navdisplay').css(
                {
                    "opacity":"0"
                }
            );

            setTimeout(function () {
                $('#products-navdisplay').css(
                    {
                        "display":"none"
                    }
                );
            }, 200);
        }
    );
</script>

<div id="alert-container">
</div>
