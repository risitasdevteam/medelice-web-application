<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/08/2018
 * Time: 13:42
 */

require_once(__DIR__.'/../customer/User.class.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserMustbeConnectedException.php');

if (!UserUtil::isLogged()){
    if (!UserUtil::is_a_data_manager())
        throw new UserMustbeConnectedException("Vous devez être connecté.");
}