<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 17/07/2018
 * Time: 19:44
 */

require_once(__DIR__.'/../customer/User.class.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserMustbeConnectedException.php');

if (!User::isLogged()){
    throw new UserMustbeConnectedException("Vous devez être connecté.");
}