<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 11/07/2018
 * Time: 16:40
 */

require_once(__DIR__.'/../customer/User.class.php');

if (!User::isLogged()){
    redirect("/index.php?page=erreur&code=403");
    exit;
}