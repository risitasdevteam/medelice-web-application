<?php

/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 09/11/2017
 * Time: 21:03
 */
require_once(__DIR__."/utility/PageDirectAccessGuardThrow.php");
?>

<div id="topsection">
    <div id="topsection-texttop">
        <div class="poiret bold" style="float: right; margin-right: 2%">
            <div style="font-size: 1.9vw; text-align: justify; text-align-last: justify;">DE L'ÉPICERIE FINE RÉGIONALE</div>
            <div style="font-size: 1.4vw; text-align: justify; text-align-last: justify;">en valisettes prêtes à offrir ou au détail</div>
        </div>
        <div class="bromello colored-font" style="float: right; font-size: 3vw; margin-right: 2%">
            le meilleur
        </div>
    </div>
    <div id="topsection-imgzone" style="margin-top: 1vw">
        <img style="width: 100%; height: auto" src="/images/accueil_banniere.jpg"/>
        <div class="home-headtext poiret">
            en route pour un <span class="bromello" style="text-transform: none; color: #8c7748; font-size: 2.35vw">Tour de France</span> unique et un <span class="bromello" style="text-transform: none; color: #8c7748; font-size: 2.35vw">voyage gustatif exclusif...</span>
        </div>
        <div class="medelice-home-text quicksand">
            Médélice a sélectionné les meilleurs spécialités culinaires de chacune de nos régions. Toutes sont élaborées de façon artisanale et bien souvent reprennent des recettes ancestrales.<br>
            <br>
            Véritable alternative contemporaine aux paniers et colis gourmands traditionnels, Médélice propose une gamme complète de jolies valisettes colorées prêtes à offrir alliant
            à la fois goût et raffinement.<br><br>
            <div class="medelice-home-text-bottom bromello">
                Offrir ou vous offrir le meilleur...
            </div>
        </div>
        <div class="medelice-home-bar">
            <span class="medelice-home-bar-text poiret">que l'occasion soit</span>
        </div>
        <div id="medelice-home-occasions-container" class="quicksand">
            <div id="medelice-home-occasions-perso">
                <span class="medelice-home-occasions-cat-title bromello">Personnelle</span><br>
                <div style="width: 100%; height: 0.5vw"></div>
                <span class="medelice-home-occasions-cat-text">
                    Anniversaires, Noël, Saint Valentin, fête des pères, fête des mères...<br>
                    Envie de retrouver le goût de vos vacances, de votre enfance ?<br>
                    Découvrir une région que vous ne connaissez pas encore ?
                </span>
            </div>
            <div id="medelice-home-occasions-pro">
                <span class="medelice-home-occasions-cat-title bromello">Professionnelle</span><br>
                <div style="width: 100%; height: 0.5vw"></div>
                <span class="medelice-home-occasions-cat-text">
                    Cadeaux de fin d'année offerts aux salariés par le comité d'entreprise ou par l'employeur, réunion d'affaires, journée de cohésion, départ à la retraite, médaille du travail,
                    Sainte Catherine, Saint Nicolas
                </span>
            </div>
            <div id="medelice-home-occasions-soc">
                <span class="medelice-home-occasions-cat-title bromello">Sociale</span><br>
                <div style="width: 100%; height: 0.5vw"></div>
                <span class="medelice-home-occasions-cat-text">
                    Cadeaux de mairie offerts aux seniors<br>
                    <br>
                    Cadeaux CCAS, CAS, COS...
                </span>
            </div>
        </div>
        <div id="medelice-home-occasions-bottom" class="quicksand">
            Quelle que soit la personne à qui vous offrirez ce cadeau gastronomique, soyez certains de lui faire vraiment plaisir !<br>
            De plus vous pouvez lui laisser un message personnalisé à l'intérieur.
        </div>
        <?php
            include(__DIR__. '/medelice-bottom.inc.php');
        ?>
        <div style="height: 2vw"></div>
    </div>
    <img src="/images/accueil_gps_icon.png" style="position: absolute; top: 45vw; left: -11vw; width: 2vw; z-index: 10;">
    <img src="/images/accueil_chemin.png" style="position: absolute; top: 8vw; left: -9vw; width: 62vw; z-index: 10;">
    <img src="/images/accueil_medelice_airlines.png" style="position: absolute; top: 7vw; left: 53vw; width: 10vw;">
    <img src="/images/accueil_valisette.png" style="position: absolute; top: 0; left: -3.5vw; width: 48vw; z-index: 11;">
</div>


