<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2018
 * Time: 23:05
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>

<div class="addresses-modform-container" data-type="mod">
    <div class="addresses-modform">
        <div class="addresses-modform-title roboto">Modifier une adresse</div>
        <div class="addresses-modform-fields">
            <input type="hidden" name="signature" id="mod_form_signature">
            <input type="hidden" name="weight" id="mod_form_weight">
            <input class="addresses-modform-field large-field payment-name-field" name="name" placeholder="Nom complet">
            <input class="addresses-modform-field large-field payment-address-field" name="address" placeholder="Rue, voie, boîte postale, nom de société">
            <br>
            <input class="addresses-modform-field large-field payment-address-field" name="address2" placeholder="Bâtiment, étage, lieu-dit, indication au livreur...">
            <br>
            <div class="addresses-modform-multifield-container">
                <input class="addresses-modform-field city-field left payment-city-field" name="city" placeholder="Ville">
                <select class="addresses-modform-field country-field right" name="country" style="outline: none">
                    <option value="FR">France</option>
                </select>
                <input class="addresses-modform-field zip-field right payment-zip-field" name="zip" placeholder="Code postal">
            </div>
            <br>
            <div class="addresses-modform-multifield-container">
                <input class="addresses-modform-field phone-field left" name="phone" placeholder="Téléphone">
                <input class="addresses-modform-field state-field right" name="state" placeholder="État / Région / Canton (si approprié)">
            </div>
            <br>
        </div>
        <div class="addresses-modform-buttons">
            <button class="addresses-modform-button amfb-colored right quicksand bold" type="button" onclick="confirmModForm()">Valider</button>
            <button class="addresses-modform-button amfb-gray left quicksand bold" type="button" onclick="closeModForm()">Annuler</button>
        </div>
    </div>
    <script>
        function zip_code_key_up_guess_city(){
            /*var timeout = null;
            $('.zip-field').keyup(function() {
                if (timeout != null) {
                    clearTimeout(timeout);
                }
                timeout = setTimeout(function () {
                    timeout = null;

                    var zip = $(this).val();
                    var url = "https://api.zippopotam.us/fr/" + zip;

                    $.get(url,function (response) {
                        console.log(response);
                        if (response.length > 2) {
                            suggestions = [];
                            for (ii in result['places']) {
                                suggestions.push(result['places'][ii]['place name']);
                            }

                            $(".city-field").autocomplete({
                                source: suggestions,
                                delay: 50,
                                disabled: false,
                                minLength: 0
                            });

                            if (suggestions.length > 0) {
                                $('#city').placeholder = suggestions[0];
                            }
                        }
                    });
                }, 500);
            });*/
        }

        function add_new_address_button(){
            var modform_container_selector =  $(".addresses-modform-container");

            modform_container_selector.css(
                {
                    "display":"block"
                }
            );

            $('.addresses-modform-title').html("Ajouter une adresse");

            modform_container_selector.attr("data-type", "add");
        }

        function removeAddress(address_hash){
            var div_selector = $('#addr' + address_hash + '_content');
            if (div_selector == null){
                return "";
            }

            CustomerUtil.remove_address(address_hash, function(response){
                showLoadingScreen();
                setTimeout(function(){
                    location.reload();
                }, 1500);
            });
        }

        function showModForm(address_hash)
        {
            $(".addresses-modform-container").css(
                {
                    "display":"block"
                }
            );

            $(".addresses-modform-container").attr("data-type", "mod");

            var div_selector = $('#addr_' + address_hash + '_content');
            if (div_selector == null){
                return "";
            }

            $('.addresses-modform-title').html("Modifier une adresse");

            var emitter_name = div_selector.find("#addr_emitter_name")[0].textContent;
            var address1 = div_selector.find("#addr_address1")[0].textContent;
            var address2 = div_selector.find("#addr_address2")[0].textContent;
            var phone = div_selector.find("#addr_phone")[0].textContent;
            var city = div_selector.find("#addr_city")[0].textContent;
            var state = div_selector.find("#addr_state")[0].textContent;
            var country = div_selector.find("#addr_country")[0].textContent;
            var zip = div_selector.find("#addr_zip")[0].textContent;
            var weight = div_selector.find("#addr_weight")[0].value;

            if (address2.localeCompare("Aucune indication particulière") == 0){
                address2 = "";
            }

            zip = zip.replace(/\s+/g, '').substring(1, (zip.length - 2));
            country = country.replace(/\s+/g, '');


            $('.payment-name-field[name="name"]').val(emitter_name);
            $('.payment-address-field[name="address"]').val(address1);
            $('.payment-address-field[name="address2"]').val(address2);
            $('.payment-city-field[name="city"]').val(city);
            $('.payment-zip-field[name="zip"]').val(zip);
            $('.addresses-modform-field[name="state"]').val(state);
            $('.addresses-modform-field[name="country"]').val(country);
            $('.addresses-modform-field[name="phone"]').val(phone);

            $('#mod_form_signature')[0].value = address_hash;
            $('#mod_form_weight')[0].value = weight;
        }

        function closeModForm()
        {
            $(".addresses-modform-container").css(
                {
                    "display":"none"
                }
            );
        }

        function confirmModForm()
        {
            var emitter_name = $('.payment-name-field[name="name"]').val();
            var address1 = $('.payment-address-field[name="address"]').val();
            var address2 = $('.payment-address-field[name="address2"]').val();
            var city = $('.payment-city-field[name="city"]').val();
            var zip = $('.payment-zip-field[name="zip"]').val();
            var state = $('.addresses-modform-field[name="state"]').val();
            var country = $('.addresses-modform-field[name="country"]').val();
            var phone = $('.addresses-modform-field[name="phone"]').val();

            var modform_container_selector =  $(".addresses-modform-container");

            if (modform_container_selector.attr("data-type") == "add"){
                CustomerUtil.add_address(emitter_name, address1, address2, city, zip, state, country, phone, function(response){
                    showLoadingScreen();
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    closeModForm();
                });
            }
            else {
                var signature = $('#mod_form_signature')[0].value;
                var weight = $('#mod_form_weight')[0].value;

                CustomerUtil.edit_address(emitter_name, address1, address2, city, zip, state, country, phone, signature, weight, function(response){
                    showLoadingScreen();
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                    closeModForm();
                });
            }
        }

        zip_code_key_up_guess_city();
    </script>
</div>
