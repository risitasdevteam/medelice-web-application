<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 17/07/2018
 * Time: 19:35
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../misc/UserUtil.php');
require_once(__DIR__.'/../../utility/RegexUtil.php');
require_once(__DIR__.'/../shipping/Address.php');
require_once(__DIR__.'/../shipping/AddressManager.php');

try{
    post_thrown();
    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');

    $sanitize = function($var_name, $throw = true, $filter = FILTER_SANITIZE_STRING){
        return RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, $var_name, $filter, $throw);
    };

    $signature = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'signature', RegexUtil::sha1());
    $emitter_name = $sanitize('emitter_name');
    $address1 = $sanitize('address1');
    $address2 = $sanitize('address2', false);
    $city = $sanitize('city');
    $state = $sanitize('state');
    $country = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'country', RegexUtil::iso3166_country());
    $zip = $sanitize('zip', true, FILTER_SANITIZE_NUMBER_INT);
    $phone = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'phone', RegexUtil::international_phone());
    $weight = filter_input(INPUT_POST, 'weight', FILTER_SANITIZE_NUMBER_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));

    $address_obj = new Address($emitter_name, $phone, $address1, $address2, $zip, $city, $state, $country, $weight);
    $user_id = (new User(UserUtil::get_session_data()['mail']))->id;

    if (!AddressManager::update_address_from_signature($signature, $address_obj, $user_id)){
        throw new MedeliceException("Une erreur s'est glissée dans le processus, veuillez verifier les informations et réésayer.");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Adresse modifiée avec succès !")))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}