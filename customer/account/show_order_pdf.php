<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/07/2018
 * Time: 19:42
 */
//include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../../vendor/autoload.php');
require_once(__DIR__.'/../../utility/RegexUtil.php');
require_once(__DIR__.'/../../orders/Order.class.php');
require_once(__DIR__.'/../../customer/User.class.php');
require_once(__DIR__ . '/../../produits/Product.php');
require_once(__DIR__.'/../misc/UserUtil.php');
require_once(__DIR__ . '/../../exceptions/utilisateur/UserMustbeConnectedException.php');

use Konekt\PdfInvoice\InvoicePrinter;

try{
    $order_id = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'ref', FILTER_SANITIZE_STRING, true);

    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');

    $order_provider = new DBOrderProvider();
    $order = $order_provider->get_on_unique_id($order_id);

    if ($order === null){
        throw new MedeliceException("Cette commande n'existe pas.");
    }

    if (!UserUtil::is_a_data_manager() && $order->getUserId() !== (new User($_SESSION['mail']))->id){
        throw new MedeliceException("Accès interdit");
    }

    $invoice = new InvoicePrinter();

    $invoice->setLogo(__DIR__."/../../images/logo.jpg");
    $invoice->setColor("#a0782f");
    $invoice->setType("Facture");
    $invoice->setReference($order_id);
    $invoice->setDate(date('d/m/Y',$order->getAccountTime()));
    $invoice->hide_tofrom();

    $decoded_array = json_decode($order->getProducts());
    $count = count($decoded_array);

    $total_tva = 0;
    for ($i = 0; $i < $count; $i++){
        $tva = 0;

        $product_ref = $decoded_array[$i]->_product_ref;
        $product_quantity = $decoded_array[$i]->_product_quantity;

        $product_unity_price = 0;
        $product_own_tva = 0;

        if (CartUtil::is_a_valisette($product_ref)) {
            $product = new Valisette($product_ref);

            $sub_valisette = $product->get_specific_sub_valisette_from_id(explode(":", $product_ref)[1]);
            if (!($sub_valisette instanceof SubValisette)) {
                break;
            }

            $description = $sub_valisette->title;
            $price = $sub_valisette->price;

            foreach ($sub_valisette->products as $ref){
                $product = ((new DBProductProvider())->get($ref) ?? new Product());
                $sub_valisette_product_price = $product->getPrice();

                switch ($product->getVatType()){
                    case 0:
                        $tva += 0.2*$sub_valisette_product_price;
                        $product_own_tva += 0.2*$sub_valisette_product_price;
                        $product_unity_price += $sub_valisette_product_price;
                        break;

                    case 1:
                        $tva += 0.055*$sub_valisette_product_price;
                        $product_own_tva += 0.055*$sub_valisette_product_price;
                        $product_unity_price += $sub_valisette_product_price;
                        break;
                }
            }

            $product_unity_price -= $tva;

        }else{
            $product = ((new DBProductProvider())->get($product_ref) ?? new Product());
            $description = $product->getName();
            $price = $product->getPrice();

            switch ($product->getVatType()){
                case 0:
                    $tva += 0.2*$price;
                    $product_own_tva = 0.2*$price;
                    $product_unity_price = $price - $product_own_tva;
                    break;

                case 1:
                    $tva += 0.055*$price;
                    $product_own_tva = 0.055*$price;
                    $product_unity_price = $price - $product_own_tva;
                    break;
            }
        }

        $total_tva += $product_own_tva * $product_quantity;
        $invoice->addItem($product_ref, $description, $product_quantity, $product_own_tva * $product_quantity, $product_unity_price, false, $price * $product_quantity);
    }

    $invoice->addTotal("TVA", $total_tva);
    $invoice->addTotal("Total", $order->getTotalBill());
    $invoice->render($order_id.'.pdf','I');
}
catch(UserMustbeConnectedException $e) {
    redirect("/index.php?page=moi");
}
catch (Exception $e){
    redirect_to_error_page(404);
}