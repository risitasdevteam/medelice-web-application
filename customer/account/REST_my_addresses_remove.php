<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 19/07/2018
 * Time: 17:22
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../misc/UserUtil.php');
require_once(__DIR__.'/../../utility/RegexUtil.php');
require_once(__DIR__.'/../shipping/Address.php');
require_once(__DIR__.'/../shipping/AddressManager.php');

try{
    post_thrown();

    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');

    $signature = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'signature', RegexUtil::sha1());
    $user_id = (new User(UserUtil::get_session_data()['mail']))->id;

    if (!AddressManager::remove_address_from_signature($signature, $user_id)){
        throw new MedeliceException("Une erreur s'est glissée dans le processus, veuillez verifier les informations et réésayer.");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Adresse supprimée avec succès !")))->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}