<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 03/08/2017
 * Time: 20:14
 * "Hub de redirection"
 *  Vos commandes
 *  Paramètres de connexion / sécurité
 *  Adresses
 *  Coupons
 */

include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__ . '/../User.class.php');
require_once(__DIR__ . '/../misc/UserType.enum.php');
require_once(__DIR__.'/../misc/UserUtil.php');

try {
    include(__DIR__ . '/../../includes/need_to_be_logged_throw.php');

    $user = new User($_SESSION['mail']);
    if (!isset($user)) {
        throw new MedeliceException("");
    } //TODO
}
catch(UserMustbeConnectedException $e){
    echo '<script>warnLoginForm("moi")</script>';
}
catch(Exception $e){

}
?>

<div id="topsection">
    <div class="myaccount-container">
        <div class="myaccount-topmsg">
            <?php echo (isset($user->firstname) ? "Bonjour ".$user->firstname. " , nous sommes heureux de vous retrouver ! " : 'Veuillez vous connecter, afin d\'accéder à cette page !')?>
        </div>

        <div class="myaccount-whatwillyoudo">
            Que souhaitez-vous faire ?
        </div>

        <a href="/index.php?page=mes-commandes" about="_blank" class="myaccount-box left">
            <i class="fa fa-truck fa-2x myaccount-box-icon"></i><span class="middle-align">Mes commandes</span>
        </a>

        <a href="/index.php?page=mes-parametres" about="_blank" class="myaccount-box right" style="padding: 4.32vw 0;">
            <i class="fa fa-lock fa-2x myaccount-box-icon"></i><span class="middle-align">Mes paramètres de connexion<br> et de sécurité</span>
        </a>

        <div style="float: left; width: 100%; height: 5vw"></div>

        <a href="/index.php?page=mes-adresses" about="_blank" class="myaccount-box left">
            <i class="fa fa-home fa-2x myaccount-box-icon"></i><span class="middle-align">Mes adresses</span>
        </a>
        <a href="/index.php?page=mes-coupons" about="_blank" class="myaccount-box right myaccount-box-disabled">
            <i class="fa fa-tag fa-2x myaccount-box-icon"></i><span class="middle-align">Mes coupons</span>
        </a>

        <div style="float: left; width: 100%; height: 2vw"></div>
    </div>
</div>