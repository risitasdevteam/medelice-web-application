/**
 * Created by jsanglier on 19/03/2018.
 */

$('#update-settings-info, #update-settings-security').submit(function(e){
    e.preventDefault();
    var url = '/customer/account/my_settings/my_settings.post.php';

    $.post(url, serializeForm($(this)), function(response){
        response = JSON.parse(response);
        switch (response.status){
            case 'ok':
                if (typeof response.payload.redirect != undefined)
                    location.href = "/index.php?page=" + response.payload.redirect;

                notification('info', response.payload.msg);
                break;
            case 'error':
                notification('warning', response.payload.error);
                break;
        }
    });
});