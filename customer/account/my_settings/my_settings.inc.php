<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/11/2017
 * Time: 22:42
 */

require_once(__DIR__.'/../../misc/UserUtil.php');
require_once(__DIR__.'/../../misc/UserGender.enum.php');

if (!UserUtil::isLogged()){
    http_response_code(403);
    redirect('/index.php?page=connexion');
    exit;
}

switch ($_SERVER['REQUEST_METHOD']){
    case 'GET':
        //include(__DIR__.'/my_settings.get.php');
        break;
    case 'POST':
        include(__DIR__.'/my_settings.post.php');
        break;
    default:
        http_response_code(400);
        exit;
}