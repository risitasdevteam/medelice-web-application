<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/11/2017
 * Time: 22:23
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../../security/UserToken.class.php');
require_once(__DIR__.'/../../../utility/RegexUtil.php');

try {
    post_thrown();

    if (!User::isLogged()){
        throw new MedeliceException('error');
    }

    $token_prefix = 'mes-parametres';
    $attempted_args = array($token_prefix . '_token', $token_prefix . '_token_hash', 'firstname', 'lastname', 'dob', 'phone', 'email', 'password', 'confirmation_password', 'update_type');

    /*foreach ($attempted_args as $post) {
        if (empty($_GET[$post]) || !in_array($post, $_GET)) {
            throw new DataMissingException('sss');
        }
    }*/

    /*if (!UserToken::can_access(600, '/index.php', 'page=mes-parametres', $token_prefix)){
        throw new UserInvalidTokenException('ddd');
    }*/


    $update_type = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, $attempted_args[9], FILTER_SANITIZE_STRING, true);
    if (!is_string($update_type)){
        throw new MedeliceException('Invalid update object type');
    }

    //sec = security & inf = information
    if ($update_type !== 'sec' && $update_type !== 'inf'){
        throw new MedeliceException('Invalid update type');
    }

    $security_update = $update_type === 'sec';

    $new_profile = new User($_SESSION['mail']);
    $profile = clone $new_profile;
    $reconnection_needed = false;

    if (!$security_update)
    {
        $firstname = RegexUtil::filter_var_with_regex($_POST[$attempted_args[2]], RegexUtil::name());
        $lastname = RegexUtil::filter_var_with_regex($_POST[$attempted_args[3]], RegexUtil::name());

        //$birth = RegexUtil::filter_var_with_regex($_POST[$attempted_args[4]], RegexUtil::birth());
        $phone = RegexUtil::filter_var_with_regex($_POST[$attempted_args[5]], RegexUtil::international_phone());

        $new_profile->firstname = $firstname;
        $new_profile->surname = $lastname;

        //$dt = DateTime::createFromFormat("d/m/Y", "30/06/2000");
        //$new_profile->_birthday = $dt->getTimestamp();

        $new_profile->phone = $phone;
    }
    else
    {
        $mail = RegexUtil::filter_var_with_regex($_POST[$attempted_args[6]], RegexUtil::email());
        $password = RegexUtil::filter_var_with_regex($_POST[$attempted_args[7]], RegexUtil::password(), false);
        $confirmation_password = RegexUtil::filter_var_with_regex($_POST[$attempted_args[8]], RegexUtil::password(), false);

        if (strcmp($new_profile->mail, $mail) != 0) {
            $new_profile->mail = $mail;
        }

        if (strcmp($password, $confirmation_password) != 0){
            throw new MedeliceException('Les deux mots de passe ne sont pas égaux.');
        }

        if (strcmp($new_profile->password, $password) != 0) {
            $new_profile->setPassword($password); //
        }

        $reconnection_needed = true;
    }

    $success = $profile->edit_profile($new_profile);

    if ($success)
    {
        $json_response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Compte mis à jour !"));
        if ($reconnection_needed)
            $json_response->add_payload(new JsonResponsePayload("redirect", "home"));

        $json_response->transmit();

        if ($reconnection_needed){
            User::disconnect();
        }
    }
    else
    {
        throw new MedeliceException('Something bad occurred, but please, don\'t panic');
    }
}
catch (Exception $e){
    exception_to_json_payload($e);
}