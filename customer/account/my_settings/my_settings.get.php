<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/11/2017
 * Time: 22:21
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../../misc/UserUtil.php');
require_once(__DIR__.'/../../misc/UserGender.enum.php');
require_once(__DIR__.'/../../security/UserToken.class.php');
require_once(__DIR__.'/../../../utility/RegexUtil.php');

include(__DIR__.'/../../../includes/need_to_be_logged.php');

//my security settings email + password

$token_prefix = 'mes-parametres';
$signature = UserToken::generate_token_and_hash($token_prefix, 360);
$token = $signature['token'];
$hash = $signature['hash'];

$user = new User($_SESSION['mail']);

$email = $user->mail;
$firstname = $user->firstname;
$surname = $user->surname;
$birth = date('d/m/Y', $user->birthday);
$phone = $user->phone;
?>

<script src="/js/jquery.maskedinput.min.js"></script>
<div id="topsection">

    <div class="account-settings-container">
        <form id="update-settings-info" class="acc-settings-left">
            <?php UserToken::insert_token_and_hash_in_current_page($token_prefix, $token, $hash); ?>
            <input type="hidden" name="update_type" value="inf">
            <div class="acc-settings-cat-name">Mon compte :</div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Prénom</div>
                <input class="acc-settings-field" id="customer_firstname" type="text" name="firstname" pattern="<?php RegexUtil::output2HTML(RegexUtil::name()); ?>" value="<?php echo $firstname; ?>"/>
            </div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Nom de famille</div>
                <input class="acc-settings-field" id="customer_lastname" type="text" name="lastname" pattern="<?php RegexUtil::output2HTML(RegexUtil::name()); ?>" value="<?php echo $surname; ?>"/>
            </div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Téléphone</div>
                <input class="acc-settings-field" id="customer_phone" type="text" name="phone" pattern="<?php RegexUtil::output2HTML(RegexUtil::international_phone()) ?>" value="<?php echo $phone ?>"/>
            </div>
            <button type="submit" class="acc-settings-update-button">
                Mettre à jour
            </button>
        </form>
        <form id="update-settings-security" class="acc-settings-right">
            <?php UserToken::insert_token_and_hash_in_current_page($token_prefix, $token, $hash); ?>
            <input type="hidden" name="update_type" value="sec">
            <div class="acc-settings-cat-name">Paramètres de sécurité :</div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Email</div>
                <input class="acc-settings-field" id="customer_mail" type="email" name="email" value="<?php echo $email ?>"/>
            </div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Mot de passe</div>
                <input class="acc-settings-field" id="customer_password" type="password" name="password" pattern="<?php RegexUtil::output2HTML(RegexUtil::password()); ?>" placeholder="Mot de passe"/>
            </div>
            <div class="acc-settings-field-container">
                <div class="acc-settings-field-name quicksand">Vérification</div>
                <input class="acc-settings-field" id="customer_password_check" type="password" name="confirmation_password" pattern="<?php RegexUtil::output2HTML(RegexUtil::password()) ?>" placeholder="Confirmer le mot de passe"/>
            </div>
            <button type="submit" class="acc-settings-update-button">
                Mettre à jour
            </button>
        </form>
    </div>
    <a class="button-link acc-settings-back-button quicksand bold" href="/index.php?page=moi">
        <span class="button-link-text">< Précédent</span>
    </a>
    <script src="/customer/account/my_settings/my_settings.js"></script>
</div>
