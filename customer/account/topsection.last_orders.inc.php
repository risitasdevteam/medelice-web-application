<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/10/2017
 * Time: 17:51
 * Permet au client de voir ses dernières commandes ainsi que le statut sous forme d'un tableau.
 */

include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__ . '/../User.class.php');
require_once(__DIR__ . '/../../orders/Order.class.php');

$orders = array();
try {
    include(__DIR__ . '/../../includes/need_to_be_logged_throw.php');

    $customer = new User($_SESSION['mail']);
    $orders = Order::retrieveOrders($customer->id);

    function sort_by_date($a, $b)
    {
        if (!$a instanceof Order || !$b instanceof Order) {
            throw new MedeliceException("");
        }

        return intval($b->account_time) - intval($a->account_time);
    }

    usort($orders, "sort_by_date");
}
catch(UserMustbeConnectedException $e){
    echo '<script>warnLoginForm("mes-commandes")</script>';
}
catch(Exception $e){

}
?>


<div id="topsection">

    <div class="bromello colored-font" style="font-size: 3vw; margin: 2vw 0 0 3vw; font-style: italic">Vos commandes</div>
    <?php
    if (count($orders) == 0){
        echo '<div class="myaddresses-noaddresses bold">
                    <div class="myaddresses-noaddresses-text">Vous n\'avez encore rien commandé ! Rendez-vous sur <a class="link" href="/index.php?page=chooseregion">la boutique</a></div>
                </div>';
    }else{
        echo '<table class="my-orders-table" cellspacing="0">
          <tr class="my-orders-tablerow my-orders-tablerow-nohover" style="border-bottom: 0.15vw #575757 solid">
            <th style="width: 10%; text-align: left; padding: 0 1%">N°</th>
            <th style="width: 15%; text-align: left; padding: 0 1%">Date</th>
            <th style="width: 55%; text-align: left; padding: 0 1%">Statut</th>
            <th style="width: 20%; text-align: left; padding: 0 1%">Détails</th>
          </tr>
    ';
        foreach ($orders as $key => $order){
            if ($order instanceof Order) {
                echo '<tr class="my-orders-tablerow">
                <td>' . $order->id . '</td>
                <td>' . date("d/m/y", $order->status_last_update) . '</td>
                <td>' . OrderStatus::readableForCode($order->status) . '</td>
                <td><a style="color: blue; text-decoration: underline" href="/index.php?page=commande&ref='.$order->id.'">Voir ma commande</a></td>
            </tr>';
            }
        }
        echo '</table>';
    }
    ?>
</div>
