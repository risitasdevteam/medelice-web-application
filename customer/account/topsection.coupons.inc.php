<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 13/08/2018
 * Time: 18:06
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
?>


<div id="topsection">
    <div class="bromello colored-font" style="font-size: 3vw; margin: 2vw 0 0 3vw; font-style: italic">Vos coupons</div>
    <table class="my-orders-table" cellspacing="0">
        <tr class="my-orders-tablerow my-orders-tablerow-nohover">
            <th style="width: 10%; text-align: left; padding: 0 1%;">Nom</th>
            <th style="width: 65%; text-align: left; padding: 0 1%;">Avantage</th>
            <th style="width: 25%; text-align: left; padding: 0 1%;">Expiration</th>
        </tr>
        <?php

            $coupons = array(0, 1, 2, 3, 4, 5, 6);

            foreach ($coupons as $coupon)
            {
                echo '
                    <tr class="my-orders-tablerow">
                        <td style="text-align: left;">ANNIV2018</td>
                        <td style="text-align: left;">-5% sur tout le site</td>
                        <td style="text-align: left;">31/09/2018 12:00:00</td>
                    </tr>
                ';
            }
        ?>
    </table>
</div>