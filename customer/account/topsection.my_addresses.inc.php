<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 29/06/2018
 * Time: 11:22
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../misc/UserUtil.php');

$not_logged = false;
try{
    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');
}
catch (UserMustbeConnectedException $e){
    $not_logged = true;
    echo '<script>warnLoginForm("mes-adresses")</script>';
    //exit;
}
?>

<div id="topsection">
    <div class="myaddresses-header-section">
        <a class="myaddresses-back-button button-link quicksand bold" type="button" href="/index.php?page=moi"><span class="button-link-text">< Retour vers Mon compte</span></a>
        <i class="myaddresses-title bromello colored-font">Vos adresses</i>
    </div>
    <div class="myaddresses-separator"></div>
    <button id="new-address-btn" class="bold" onclick="add_new_address_button();"><i class="fa fa-plus  myaddresses-address-details-modrem-icon"></i>Ajouter une nouvelle adresse</button>
    <div class="myaddresses-container">
        <?php
            require_once (__DIR__ . '/../../customer/shipping/Address.php');
            require_once(__DIR__.'/../../customer/shipping/AddressManager.php');


            $addresses = $not_logged ? array() : AddressManager::get_addresses_from_user_id((new User($_SESSION['mail']))->id);

            if (count($addresses) > 0) {

                AddressManager::range_addresses($addresses);
                foreach ($addresses as $address) {
                    if (!$address instanceof Address) {
                        throw new MedeliceException("address !instanceof Address obj");
                    }

                    $emitter_name = $address->emitter_name;
                    $phone = $address->phone;
                    $address_line_1 = $address->address1;
                    $address_line_2 = $address->address2;
                    $address_city = $address->city;
                    $address_zip = $address->zip;
                    $address_state = $address->state;
                    $address_country = $address->country;
                    $address_hash = $address->signature;
                    //var_dump($address_hash);

                    echo '
                    <div class="myaddresses-address-header" onclick="
                        
                        var select = $(\'#addr_' . $address_hash . '_content\');
                        var arrow = $(\'#arrow_'.$address_hash.'\');
                        
                        function toggleSub_addr' . $address_hash . '()
                        {
                            var max_height = parseFloat(select.css(\'max-height\').replace(\'px\', \'\'));
                            if (max_height > 0)
                            {
                                select.css(
                                    {
                                        \'max-height\':\'0\'
                                    }
                                );
                                
                                arrow.removeClass(\'fa-angle-up\');
                                arrow.addClass(\'fa-angle-down\');
                                arrow.css({\'height\':\'auto\'});
                            }
                            else
                            {
                                select.css(
                                    {
                                        \'max-height\': \'14vw\'
                                    }
                                );
                                
                                arrow.removeClass(\'fa-angle-down\');
                                arrow.addClass(\'fa-angle-up\');
                                arrow.css({\'height\':\'80%\'});
                            }
                        }

                        toggleSub_addr' . $address_hash . '();

                    ">
                        <div class="myaddresses-address-header-text roboto">
                            ' . $address_line_1 . ', ' . $address_zip . ' ' . $address_city . '
                        </div>
                        <div class="myaddresses-address-header-arrow">
                            <i class="fa fa-angle-down fa-4x" id="arrow_'.$address_hash.'"></i>
                        </div>
                    </div>
                    <div class="myaddresses-address-details-container" id="addr_' . $address_hash . '_content">
                        <input type="hidden" id="addr_weight" value="'.$address->weight.'">
                        <div class="myaddresses-address-details">
                            <div class="myaddresses-address-details-section" style="margin-bottom: 1vw">
                                <span class="myaddresses-address-details-bigtext roboto left" id="addr_emitter_name">' . $emitter_name . '</span>
                                <span class="myaddresses-address-details-bigtext roboto right" id="addr_phone">' . $phone . '</span>
                            </div>
                            <div class="myaddresses-address-details-section">
                                <span class="myaddresses-address-details-bigtext roboto left" id="addr_address1">' . $address_line_1 . '</span>
                                <div class="myaddresses-address-details-bigtext roboto right">
                                    <span id="addr_city">' . $address_city . '</span><span id="addr_zip"> (' . $address_zip . ')</span>
                                </div>
                            </div>
                            <div class="myaddresses-address-details-section">
                                <span class="myaddresses-address-details-smalltext roboto left" id="addr_address2">' . (strlen($address_line_2) > 0 ? $address_line_2 : 'Aucune indication particulière') . '</span>
                                <span class="myaddresses-address-details-smalltext roboto right" id="addr_state">' . $address_state . '</span>
                            </div>
                            <div class="myaddresses-address-details-country roboto" id="addr_country">
                                '.$address_country.'
                            </div>

                            <div class="myaddresses-address-details-modrem-container">
                                <button class="myaddresses-address-details-modrem-button quicksand bold" type="button" onclick="
                                    showModForm(\''.$address_hash.'\');">
                                    <i class="fa fa-pencil myaddresses-address-details-modrem-icon"></i>
                                    Modifier
                                </button>
                                <button class="myaddresses-address-details-modrem-button quicksand bold" type="button" onclick="removeAddress(\''.$address_hash.'\');">
                                    <i class="fa fa-times myaddresses-address-details-modrem-icon"></i>
                                    Supprimer
                                </button>
                            </div>
                        </div>
                    </div>
                
                ';
                }
            }else{
                echo '<div class="myaddresses-noaddresses bold">
                    <div class="myaddresses-noaddresses-text">Aucune adresse n\'est enregistrée pour le moment !</div>
                </div>';
            }

            include (__DIR__ . '/my_addresses.modform.inc.php');

        ?>

    </div>
</div>
