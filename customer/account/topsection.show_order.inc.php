<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 20/05/2018
 * Time: 12:11
 */

include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

include(__DIR__ . '/../../includes/orders.php');
include(__DIR__ . '/../../includes/data_handler.php');
require_once(__DIR__ . '/../../produits/Product.php');
require_once(__DIR__.'/../../produits/Valisette.php');
require_once(__DIR__.'/../../produits/SubValisette.php');

//>50 valisettes, le service clientèle vous contactera pour la livraison **MENTION IMPORTANTE pour cause de rupture**
$express_delivery_delay = 2; //j
$standard_delivery_delay = 7;

$delivery_progress = 0;
$delivery_progress_interval = array(
    OrderStatus::RECEIVED => 5,
    OrderStatus::PROCESSING => 45,
    OrderStatus::SENT => 75,
    OrderStatus::DELIVERED => 100,
    OrderStatus::ERROR => 0,
    OrderStatus::PENDING_3D_SECURE => 0
);

$order_id = null;
$order = null;

try {
    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');

    $order_id = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'ref', FILTER_SANITIZE_STRING, true);

    $order = new Order(
        [
            "id" => $order_id,
            "user_id" => (new User($_SESSION['mail']))->id
        ]);

    if (!$order->exists(true)) {
        throw new MedeliceException("Cette commande n'existe pas.");
    }

   $delivery_progress = $delivery_progress_interval[$order->status];
}
catch(UserMustbeConnectedException $e){
    $order_id = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'ref', FILTER_SANITIZE_STRING, false);

    echo '<script>warnLoginForm("'.(isset($order_id) ? 'commande&ref='.$order_id : 'moi').'")</script>';
    exit;
}
catch(Exception $e){

}

$account_time = $order->account_time;
$delivery_mode = $order->delivery_mode;

$current_week_day = date("N", $account_time);

$now = $account_time;
$one_week = strtotime("+1 weeks", $account_time);

if ($current_week_day > 5){
    $now += 86400*(8 - $current_week_day);
    $one_week += 86400*(8 - $current_week_day);
}

$decoded_address = OrderCustomerAddresses::decode_from_json($order->customer_addresses);

?>


<div id="topsection">
    <div class="order-container">
        <div class="quicksand bold" style="font-size: 2vw">Votre commande n°<?php echo $order_id ?></div>
        <div class="order-status-container quicksand bold">
            <div class="order-status-progress-bar">
                <div class="order-progress-bar">
                    <span style="width: <?php echo ($delivery_progress < 31 ? 0 : ($delivery_progress < 65 ? 33 : ($delivery_progress < 98 ? 66 : 100))); ?>%"></span>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-1 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::RECEIVED] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-check"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande prise en compte
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-2 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::PROCESSING] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-archive"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande en préparation
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-3 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::SENT] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-truck"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande expédiée
                    </div>
                </div>
                <div class="order-progress-bar-step order-progress-bar-step-4 <?php echo $delivery_progress >= $delivery_progress_interval[OrderStatus::DELIVERED] ? 'order-progress-bar-step-enabled' : 'order-progress-bar-step-disabled'; ?>">
                    <div class="order-progress-bar-step-icon">
                        <i class="fa fa-flag"></i>
                    </div>
                    <div class="order-progress-bar-step-desc">
                        Commande reçue
                    </div>
                </div>
                <script>
                    $(document).ready(function () {
                        setTimeout(function() {
                            $(".order-progress-bar > span").css(
                                {
                                    "width":"<?php echo $delivery_progress ?>%"
                                }
                            );
                        }, 200);
                    });
                </script>
            </div>
            <div class="order-status-description">
                <?php
                    $msg = "";
                    $msg_indication = "";
                    $msg_indication_color = "";

                    switch ($order->status){
                        case OrderStatus::RECEIVED:
                            $msg = "Votre commande a été réçue, elle sera traitée très vite.";
                            $msg_indication = "Date de livraison estimée : ".strftime("%d %B", $now). " et le ". strftime("%d %B", $one_week)." (Livraison ".$delivery_mode.')';
                            $msg_indication_color = "#009700";
                            break;

                        case OrderStatus::PROCESSING:
                            $msg = "Votre commande est en cours de préparation.";
                            $msg_indication = "Date de livraison estimée : ".strftime("%d %B", $now). " et le ". strftime("%d %B", $one_week)." (Livraison ".$delivery_mode.')';
                            $msg_indication_color = "#009700";
                            break;

                        case OrderStatus::SENT:
                            $msg = "Votre commande a été envoyée, elle est prise en charge par nos pigeons voyageurs.";
                            $msg_indication = "N° de tracking : ".$order->tracking.". Date de livraison estimée : ".strftime("%d %B", $now). " et le ". strftime("%d %B", $one_week)." (Livraison ".$delivery_mode.')';
                            $msg_indication_color = "#009700";
                            break;

                        case OrderStatus::DELIVERED:
                            $msg = "Votre commande a été livrée, régalez-vous !";
                            $msg_indication = "Commande livrée le : ".date("d/m/Y", $order->status_last_update);
                            $msg_indication_color = "#009700";
                            break;

                        case OrderStatus::ERROR:
                            $msg = "Une erreur s'est dissimulée dans votre commande, nos épicuriens font le nécessaire afin de débloquer la situation.";
                            $msg_indication = "<span style='text-decoration: line-through'>Date de livraison estimée : N/A (Livraison ".$delivery_mode.')</span>';
                            $msg_indication_color = "#db0f0f";
                            break;
                    }

                    echo $msg." ";
                    echo '<span style="color: '.$msg_indication_color.'">'.$msg_indication.'</span>';
                ?>
            </div>
        </div>
        <div class="order-addresses-container quicksand">
            <div class="order-address-container order-delivery-address-container">
                <div class="order-address-title bold">Adresse de livraison</div>
                <div style="margin-left: 0.2vw">
                    <?php
                        $delivery_address = $decoded_address->delivery;
                        if (!$delivery_address instanceof Address){

                        }

                        $full_address = $delivery_address->emitter_name.' ('.$delivery_address->phone.')'.
                            br().
                            $delivery_address->address1.
                            br().
                            ($delivery_address->address2 != '' ? $delivery_address->address2.br() : '').
                            $delivery_address->zip.', '.$delivery_address->city.
                            br().
                            $delivery_address->state.
                            br().
                            $delivery_address->country;

                        echo $full_address;
                    ?>
                </div>
            </div>
            <div class="order-address-container order-billing-address-container">
                <div class="order-address-title bold">Adresse de facturation</div>
                <div style="margin-left: 0.2vw">
                    <?php
                    $billing_address = $decoded_address->billing;

                    if (!$billing_address instanceof Address){

                    }

                    $full_address = $billing_address->emitter_name.' ('.$delivery_address->phone.')'.
                        br().
                        $billing_address->address1.
                        br().
                        ($billing_address->address2 != '' ? $billing_address->address2.br() : '').
                        $billing_address->zip.', '.$billing_address->city.
                        br().
                        $billing_address->state.
                        br().
                        $billing_address->country;

                    echo $full_address;
                    ?>
                </div>
            </div>
        </div>
        <div class="order-products-container quicksand">
            <div class="order-products-title bold">Contenu de votre commande</div>
            <div class="order-products-subcontainer">
                <div class="order-products-header">
                    <?php
                        $products = CartUtil::decode_cart_from_json($order->products);
                        $products_objects = array();
                        $valisettes_within_objects = array();

                        $total_mass = 0;

                        $add_product_mass = function($product_reference, $come_from_valisette = false) use (&$total_mass, &$products_objects){
                            $product = (new DBProductProvider)->get($product_reference);
                            if ($product === null){
                                throw new MedeliceException("The specified product doesn't exists");
                            }

                            if (!$come_from_valisette) {
                                array_push($products_objects, $product);
                            }

                            $total_mass += $product->getTotalMass();
                        };

                        if ($products == null || count($products) < 1){
                            echo "Panier vide, don't panic !";
                            return;
                        }

                        foreach ($products as $product_reference => $product_quantity)
                        {
                            if (CartUtil::is_a_valisette($product_reference))
                            {
                                $valisette_mother = new Valisette($product_reference);
                                $sub_valisette = $valisette_mother->get_specific_sub_valisette_from_id(explode(Valisette::SUB_VALISETTE_DELIMITER, $product_reference)[1]);
                                if (!$sub_valisette instanceof SubValisette){
                                    throw new MedeliceException("");
                                }

                                $products_within = $sub_valisette->products;

                                foreach ($products_within as $product_reference_within){
                                    $add_product_mass($product_reference_within, true);
                                }

                                $valisettes_within_objects[$product_reference] = $valisette_mother;
                                $total_mass += CartUtil::valisette_default_mass();
                            }
                            else
                            {
                                $add_product_mass($product_reference);
                            }
                        }

                    echo 'Masse totale : '.($total_mass * 10**(-3)).'kg';
                    ?>

                    <span style="float: right">Prix total : <?php echo $order->total_bill ?>€</span>
                </div>
                <div class="order-products-container">

                    <?php
                        $iterator = 0;
                        $print_product_individually = function($product, $product_ref = "") use (&$products_objects, &$products, &$iterator){
                            $brand = "";
                            $url = 'index.php?page=';

                            if ($product instanceof Valisette){
                                $sub_valisette = $product->get_specific_sub_valisette_from_id(explode(Valisette::SUB_VALISETTE_DELIMITER, $product_ref)[1]);
                                if (!$sub_valisette instanceof SubValisette){
                                    throw new MedeliceException("");
                                }

                                $ref = $product_ref;
                                $quantity = $products[$ref];

                                $preview_image_path = $product->image();
                                $name = $sub_valisette->title;
                                $price = ($sub_valisette->price * $quantity);

                                $url .= 'valisette&ref='.$ref;
                            }
                            else if ($product instanceof Product){
                                $images = (new FileIOProductProvider())->get_images($product->getRef());

                                $ref = $product->ref;
                                $quantity = $products[$product->ref];
                                $preview_image_path = $images[0];
                                $name = $product->name;
                                $brand = $product->brand;
                                $price = ($product->price * $quantity);
                                $url .= 'voir&ref='.$ref;
                            }
                            else{
                                throw new MedeliceException("");
                            }

                            echo '
                                <div class="order-product-display" style="float: '.($iterator % 2 == 0 ? "left" : "right").';">
                                    <a class="button-link order-product-picture" href="'.$url.'">
                                        <img class="fill-image" src="' . $preview_image_path . '"/>
                                    </a>
                                    <div class="order-product-infos">
                                        <span class="order-product-title">'.$name.'</span><br>
                                        <span class="order-product-brand">'.$brand.'</span><br>
                                        <div style="height: 0.3vw; width: 100%"></div>
                                        <span class="order-product-qty">Qté : '.$quantity.'</span>
                                        <span class="order-product-totalprice">'.($price * $quantity).'€</span>
                                    </div>
                                </div>
                            ';

                            $iterator++;
                        };

                    foreach ($products_objects as $product){
                        $print_product_individually($product);
                    }

                    foreach ($valisettes_within_objects as $product_reference => $sub_valisette){
                        $print_product_individually($sub_valisette, $product_reference);
                    }
                    ?>
                </div>
            </div>
        </div>

        <button style="height: 2vw; width: 100%; background-color: #8c7748; border: none; color: white; cursor: pointer;" onclick="location.href = '/index.php?page=ma-commande&ref=<?php echo $order_id ?>'">Voir ma facture en PDF</button>
    </div>
</div>