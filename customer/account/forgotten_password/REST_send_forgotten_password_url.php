<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 07/01/2018
 * Time: 00:56
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../../../includes/data_handler.php');
require_once(__DIR__.'/../../User.class.php');

try {
    post_thrown();
    $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL, true);

    $provider = new DBCustomerProvider();
    $customer = $provider->get_on_email($mail);
    if ($customer === null){
        throw new MedeliceException("Ce compte n'existe pas.");
    }

    if (!$provider->generate_forgotten_password_url($customer)){
        throw new MedeliceException("Aucun lien n'a pu être généré.");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Un lien de rénitialisation vous a été envoyé !")))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}