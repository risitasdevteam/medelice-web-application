<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/08/2017
 * Time: 11:49
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../../User.class.php');
require_once(__DIR__ . '/../../security/UserToken.class.php');

$token_prefix = 'confirmation_forgotten_password';
$token_death_time = 600; //10mn

try {
    $uid = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'uid', RegexUtil::uid());
    $verification_key = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'vkey', RegexUtil::sha256());
    $password = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'passwd', RegexUtil::password());
    $password_confirm = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'passwd_confirm', RegexUtil::password());

    $gateway_provider = new DBGatewayProvider();

    $by_an_admin = false;
    if (!$gateway_provider->has_asked_password_reset($verification_key, $uid, $by_an_admin)){
        throw new MedeliceException(L::form_account_not_asked_for_lost_password);
    }

    if (!$by_an_admin) {
        if (!UserToken::can_access('/index.php', 'page=pre-confirmation-mot-de-passe-oublie', $token_prefix)) {
            throw new UserInvalidTokenException('');
        }
    }
    
    $customer_provider = new DBCustomerProvider();
    if (!$customer_provider->confirm_new_password_after_forgoten($uid, $verification_key, $password, $password_confirm)){
        throw new MedeliceException("Une erreur est survenue");
    }

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Mot de passe mis à jour avec succès !")))->transmit();
}
catch (Exception $e){
    exception_to_json_payload($e);
}