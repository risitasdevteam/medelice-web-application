<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/08/2017
 * Time: 22:38
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../../User.class.php');

?>

<div id="topsection">
    <div class="forgotten-pswd-title quicksand bold">Mot de passe oublié</div>
    <form class="forgotten-pswd-form quicksand">
        <div class="forgotten-pswd-text">Veuillez entrer votre adresse mail ci-dessous :</div><br>
        <input type="text" class="field-input" name="email" placeholder="exemple@hebergeur.fr"><br>
        <button class="button quicksand bold" type="submit">
            Envoyer un email de réinitialisation
        </button>
    </form>
</div>
<script>
    function handler(){
        var $form = $('.forgotten-pswd-form');
        $form.on("submit", function(e){
            e.preventDefault();

            var email = $('[name="email"]').val();

            showLoadingScreen();

            CustomerUtil.send_forgotten_password(email, function(response){
                hideLoadingScreen();

                setTimeout(function(){
                    location.href = "/index.php";
                }, 3000);
            }, function(response){
                hideLoadingScreen();
            });
        });
    }

    $(document).ready(function(){
        handler();
    });
</script>