<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 17/08/2018
 * Time: 15:21
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
?>

<div id="topsection">
    <form class="reset-password-form">
        <div class="reset-password-form-title quicksand bold">Choisissez un nouveau mot de passe</div>
        <div class="reset-password-form-fields-container">
            <input type="password" class="field-input full-field register-password1" style="margin-bottom: 0.5vw;" placeholder="Mot de passe" name="passwd" id="pwd1">
            <div class="register-password-tooltip" style="top: -25%; left: -17vw;">
                <div class="password-tooltip-entry" id="chars-condition">
                    <i class="fa fa-close red left"></i>
                    <div class="password-tooltip-entry-text right">Au moins 8 caractères</div>
                </div><br>
                <div class="password-tooltip-entry" id="uplo-case-condition">
                    <i class="fa fa-close red left"></i>
                    <div class="password-tooltip-entry-text right">Majuscules et minuscules</div>
                </div><br>
                <div class="password-tooltip-entry" id="alphanumerics-condition">
                    <i class="fa fa-close red left"></i>
                    <div class="password-tooltip-entry-text right">Chiffres</div>
                </div><br>
                <div class="password-tooltip-entry" id="specials-condition">
                    <i class="fa fa-close red left"></i>
                    <div class="password-tooltip-entry-text right">Caractères spéciaux (&~@!:;,(){}\-\|_%€§)</div>
                </div><br>
            </div>
            <input type="password" class="field-input full-field" placeholder="Confirmez le mot de passe" name="passwd_confirm">
        </div>
        <button type="submit" class="reset-password-form-button button quicksand bold">
            Changer de mot de passe
        </button>
    </form>
</div>

<script>
    function handler(){
        var $form = $('.reset-password-form');
        $form.on("submit", function(e){
            e.preventDefault();

            var url = new URL(location.href);
            var unique_id = url.searchParams.get("uid");
            var verification_key = url.searchParams.get("vkey");
            var password = $('[name="passwd"]').val();
            var password_confirm = $('[name="passwd_confirm"]').val();

            showLoadingScreen();

            CustomerUtil.confirm_password(unique_id, verification_key, password, password_confirm, function(response){
                hideLoadingScreen();

                setTimeout(function(){
                    location.href = "/index.php";
                }, 3000);
            }, function(response){
                hideLoadingScreen();
            });
        });
    }

    function password_change_handler(){
        var $password_handler = $('#pwd1');
        $password_handler.on("keyup", function(){
            var password = $(this).val();

            password.match(/^.{8,64}$/) ? validateCondition("chars-condition") :  unvalidateCondition("chars-condition");
            password.match(/(?=.*[a-z])(?=.*[A-Z])/) ? validateCondition("uplo-case-condition") : unvalidateCondition("uplo-case-condition");
            password.match(/(?=.*[0-9])/) ? validateCondition("alphanumerics-condition") : unvalidateCondition("alphanumerics-condition");
            password.match(/(?=.*[&~@!:;(){}\-\|_])/) ? validateCondition("specials-condition") : unvalidateCondition("specials-condition");
        });
    }

    $(".register-password1").focusin(function() {
        $(".register-password-tooltip").css(
            {
                "visibility":"visible",
                "opacity":"1"
            }
        );
    });

    $(".register-password1").focusout(function() {
        $(".register-password-tooltip").css(
            {
                "visibility":"hidden",
                "opacity":"0"
            }
        );
    });

    function validateCondition(condition)
    {
        var select = $("#" + condition).find("i");
        select.removeClass("red fa-close");
        select.addClass("green fa-check");
    }

    function unvalidateCondition(condition)
    {
        var select = $("#" + condition).find("i");
        select.removeClass("green fa-check");
        select.addClass("red fa-close");
    }

    $(document).ready(function(){
        handler();
        password_change_handler();
    });
</script>
