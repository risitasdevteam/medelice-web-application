<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 25/08/2018
 * Time: 19:23
 */

include_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");

try{
    $name = RegexUtil::filter_in_var_with_regex(INPUT_GET, 'firstname', RegexUtil::name());
    $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'mail', FILTER_SANITIZE_EMAIL, true);
}
catch(Exception $e){
    redirect("/index.php?page=home");
    exit;
}

?>

<div id="topsection" class="register-confirm-container">
    <div class="register-confirm-box">
        Félicitations <?php echo $name; ?>, vous venez de créer votre compte Médélice !<br>
        <br>
        Pour finaliser votre inscription, rendez-vous sur <?php echo $mail; ?> et ouvrez le lien dans le mail de confirmation qui vous a été envoyé.
    </div>
</div>