<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 31/10/2017
 * Time: 13:34
 */

include_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/security/UserToken.class.php');
$register_token = UserToken::generate_token_and_hash('register', 600);

?>

<div id="topsection" style="display: inline-block">
    <div class="medelice-miss-404-container">
        <img src="/images/medelice_404.png" class="medelice-miss-404">
    </div>
    <div class="register-container">
        <div class="register-title quicksand bold"><?php echo L::form_register; ?></div>
        <form class="register-form">
            <?php UserToken::insert_token_and_hash_in_current_page('register', $register_token['token'], $register_token['hash']) ?>
            <div class="register-field-container-half">
                <label class="register-field-descriptor" for="firstname">Prénom</label>
                <input class="register-field-half" type="text" id="firstname" pattern="<?php RegexUtil::output2HTML(RegexUtil::name()) ?>" name="firstname">
            </div>
            <div class="register-field-container-half">
                <label class="register-field-descriptor" for="surname">Nom</label>
                <input class="register-field-half" type="text" id="surname" pattern="<?php RegexUtil::output2HTML(RegexUtil::name()) ?>" name="surname">
            </div>

            <div class="register-field-container-full">
                <label class="register-field-descriptor" for="email">Adresse mail</label>
                <input class="register-field-full" type="email" id="email" name="email" placeholder="exemple@hebergeur.fr">
            </div>

            <div class="register-field-container-full register-password-container">
                <label class="register-field-descriptor" for="pwd1">Mot de passe</label>
                <input class="register-field-full register-password1" type="password" id="pwd1" name="password1" pattern="<?php RegexUtil::output2HTML(RegexUtil::password()) ?>" placeholder="●●●●●●●●●●●●●●">
                <div class="register-password-tooltip">
                    <div class="password-tooltip-entry" id="chars-condition">
                        <i class="fa fa-close red left"></i>
                        <div class="password-tooltip-entry-text right">Au moins 8 caractères</div>
                    </div><br>
                    <div class="password-tooltip-entry" id="uplo-case-condition">
                        <i class="fa fa-close red left"></i>
                        <div class="password-tooltip-entry-text right">Majuscules et minuscules</div>
                    </div><br>
                    <div class="password-tooltip-entry" id="alphanumerics-condition">
                        <i class="fa fa-close red left"></i>
                        <div class="password-tooltip-entry-text right">Chiffres</div>
                    </div><br>
                    <div class="password-tooltip-entry" id="specials-condition">
                        <i class="fa fa-close red left"></i>
                        <div class="password-tooltip-entry-text right">Caractères spéciaux (&~@!:;,(){}\-\|_%€§)</div>
                    </div><br>
                </div>
                <script>
                    function password_change_handler(){
                        var $password_handler = $('#pwd1');
                        $password_handler.on("keyup", function(){
                            var password = $(this).val();

                            password.match(/^.{8,64}$/) ? validateCondition("chars-condition") :  unvalidateCondition("chars-condition");
                            password.match(/(?=.*[a-z])(?=.*[A-Z])/) ? validateCondition("uplo-case-condition") : unvalidateCondition("uplo-case-condition");
                            password.match(/(?=.*[0-9])/) ? validateCondition("alphanumerics-condition") : unvalidateCondition("alphanumerics-condition");
                            password.match(/(?=.*[&~@!:;(){}\-\|_])/) ? validateCondition("specials-condition") : unvalidateCondition("specials-condition");
                        });
                    }

                    $(".register-password1").focusin(function() {
                        $(".register-password-tooltip").css(
                            {
                                "visibility":"visible",
                                "opacity":"1"
                            }
                        );
                    });

                    $(".register-password1").focusout(function() {
                        $(".register-password-tooltip").css(
                            {
                                "visibility":"hidden",
                                "opacity":"0"
                            }
                        );
                    });

                    function validateCondition(condition)
                    {
                        var select = $("#" + condition).find("i");
                        select.removeClass("red fa-close");
                        select.addClass("green fa-check");
                    }

                    function unvalidateCondition(condition)
                    {
                        var select = $("#" + condition).find("i");
                        select.removeClass("green fa-check");
                        select.addClass("red fa-close");
                    }

                    password_change_handler();
                </script>
            </div>

            <div class="register-field-container-full">
                <label class="register-field-descriptor" for="pwd2">Confirmez le mot de passe</label>
                <input class="register-field-full" type="password" id="pwd2" name="password2" pattern="<?php RegexUtil::output2HTML(RegexUtil::password()) ?>" placeholder="●●●●●●●●●●●●●●">
            </div>

            <div class="register-field-container-half">
                <label class="register-field-descriptor" for="register-dob">Naissance</label>
                <input class="register-field-half" type="date" id="register-dob" name="dob">
            </div>

            <div class="register-field-container-half">
                <label class="register-field-descriptor" for="phone">Téléphone</label>
                <input class="register-field-half" type="text" id="phone" name="phone" pattern="<?php RegexUtil::output2HTML(RegexUtil::international_phone()) ?>" placeholder="+33 123456789">
            </div>

            <div class="register-field-container-full" style="text-align: center; margin-top: 2vw">
                <input type="checkbox" id="cgu" style="cursor: pointer"><label style="margin-left: 0.8vw; cursor: pointer" for="cgu">J'ai lu et j'accepte les <a class="link" href="/index.php?page=generic&id=cgu">Conditions Générales d'Utilisation</a></label>
            </div>

            <div class="register-field-container-full" style="text-align: center; margin-top: 1vw">
                <button class="register-button quicksand bold" id="register_btn" style="">Je m'inscris !</button>
            </div>
        </form>
    </div>
</div>

<script>
    function handler(){
        $('.register-form').submit(function(event){
            event.preventDefault();

            var cgu_checked = $('#cgu').is(':checked');
            if (cgu_checked) {

                var mail = $('[name="email"]').val();
                var password1 = $('[name="password1"]').val();
                var password2 = $('[name="password2"]').val();
                var firstname = $('[name="firstname"]').val();
                var surname = $('[name="surname"]').val();
                var birthday = $('[name="dob"').val();
                var phone = $('[name="phone"]').val();

                showLoadingScreen();

                CustomerUtil.register(mail, password1, password2, firstname, surname, birthday, phone, function () {
                    $('#register_btn').attr("disabled", true);

                    hideLoadingScreen();
                    setTimeout(function(){
                        location.href = "/index.php?page=inscription-ok&firstname="+firstname+"&mail="+mail;
                    }, 3500);
                }, function () {
                    hideLoadingScreen();
                });
            }else{
                error_notification("Vous devez accepter les conditions générales d'utilisation !");
            }
        });
    }

    function exists_handler(){
        var timeout = null;
        $('#email').keyup(function(){

            if (timeout != null) {
                clearTimeout(timeout);
            }

            timeout = setTimeout(function ()
                {
                    timeout = null;
                    var mail = $('#email').val();

                    CustomerUtil.exists_by_mail(mail, function(response){
                        if (response.payload.exist){
                            $('#email').css('color', 'rgba(159, 12, 14, 0.77)');
                            notification(NotificationType.ERROR, "L'adresse mail saisie est déjà utilisée.");
                        }else{
                            $('#email').css('color', 'black');
                        }
                    }, function(response){
                    });
                },
                440);
        });
    }

    $(document).ready(function(){
        handler();
        exists_handler();
    });
</script>
