<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/09/2017
 * Time: 15:21
 */
//require_once(__DIR__.'/../../initializer.inc.php');
//require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../../exceptions/utilisateur/UserException.php');
require_once(__DIR__.'/../../exceptions/utilisateur/UserInvalidTokenException.php');

//Protection contre le CRSF
class UserToken
{
    /**
     * Retourne un tableau avec deux colonnes, "token" : token, "hash" : hash
     * @param $token_prefix string Préfixe du token
     * @return array
     */
    public static function generate_token_and_hash($token_prefix, $token_lifespan){
        $token = bin2hex(openssl_random_pseudo_bytes(32));

        $_SESSION[$token_prefix.'_token'] = $token;
        $_SESSION[$token_prefix.'_token_ls'] = (time() + $token_lifespan);

        $token_hash = md5($token . SALT);
        return array("token" => $token, "hash" => $token_hash);
    }

    public static function insert_token_and_hash_in_current_page($token_prefix, $token, $token_hash){
        echo '
            <input type="hidden" name="'.$token_prefix . '_token" value="'.$token.'">
            <input type="hidden" name="'.$token_prefix . '_token_hash" value="'.$token_hash .'">
        ';
    }

    /**
     * Return as an array filtered sent hashes
     * @param $request_type (int) INPUT_GET, INPUT_POST, INPUT_COOKIE, ...
     * @param $token_prefix (str)
     * @return array$
     */
    public static function filter_sent_integrity_keys($request_type, $token_prefix){
        $integrity_key_token = filter_input($request_type, ($token_prefix . '_token'), FILTER_SANITIZE_STRING);
        $integrity_key_token_hash = filter_input($request_type, ($token_prefix . '_token_hash'), FILTER_SANITIZE_STRING);

        return array('token' => $integrity_key_token, 'hash' => $integrity_key_token_hash);
    }

    /**
     * Donne le droit ou non d'accéder à une page selon une verification du token et de différents paramètres.
     * @param $token_death_time int Temps de vie du token
     * @param $referer string Page précedente
     * @param $referer_arg string Arguments liés à la page précedente
     * @param $token_prefix string le Préfixe du token
     * @return bool
     */
    public static function isAllowed2Do($token_death_time, $referer, $referer_arg, $token_prefix){
        //existence au sein des sessions du token et de son expiration
        if (isset($_SESSION[$token_prefix.'_token']) && isset($_SESSION[$token_prefix.'_token_iat']))
        {
            //existence dans la requête POST du token et de son hash de vérification
            if (isset($_POST[$token_prefix.'_token']) && isset($_POST[$token_prefix.'_token_hash']))
            {
                //si le token est identique entre la session et celui envoyé
                if ($_SESSION[$token_prefix.'_token'] === $_POST[$token_prefix.'_token'])
                {
                    //si le hash du token est identique entre le calcul local et la valeur envoyée
                    if (($_POST[$token_prefix.'_token_hash']) == (md5($_SESSION[$token_prefix.'_token'] . SALT)))
                    {
                        //si le temps de vie est supérieur au temps actuel - le temps de vie
                        if ($_SESSION[$token_prefix . '_token_iat'] >= (time() - $token_death_time))
                        {
                            //si la page de référence est identique à la page entrée & si les arguments de la page de référence sont identiques à ceux entrés
                            if (parse_url($_SERVER['HTTP_REFERER'])['path'] == $referer && parse_url($_SERVER['HTTP_REFERER'])['query'] == $referer_arg)
                            {
                                return true;
                            }
                        }
                    }
                }
            }
        }

        return false;
    }

    public static function can_access($referrer, $referrer_arg, $token_prefix, $use_post_check = true){
        //existence au sein des sessions du token et de son expiration
        //var_dump($_SESSION);
        if (DEV){
            return true;
        }

        if (!isset($_SESSION[$token_prefix.'_token']) || !isset($_SESSION[$token_prefix.'_token_ls'])){
            return false;
        }

        $session_token =  $_SESSION[$token_prefix.'_token'];
        $session_token_lifespan = $_SESSION[$token_prefix.'_token_ls'];
        //var_dump($session_token);
        //var_dump($session_token_lifespan);
        if (!isset($session_token) || !isset($session_token_lifespan)){
            return false;
        }

        $integrity_keys = self::filter_sent_integrity_keys(($use_post_check ? INPUT_POST : INPUT_GET), $token_prefix);
        $integrity_key_token = $integrity_keys['token'];
        $integrity_key_token_hash = $integrity_keys['hash'];
        //var_dump($integrity_keys);
        //existence dans la requête POST du token et de son hash de vérification
        if (!isset($integrity_key_token) && !isset($integrity_key_token_hash)) {
            return false;
        }
        //echo 'integrity ok';
        //si le token est identique entre la session et celui envoyé
        if (strcmp($session_token, $integrity_key_token) != 0) {
            return false;
        }
        //echo 'token cmp ok';
        //si le hash du token est identique entre le calcul local et la valeur envoyée
        if (strcmp($integrity_key_token_hash, md5($session_token . SALT)) != 0) {
            return false;
        }
        //echo 'token hash cmp nok';
        //si le temps de vie est supérieur au temps actuel - le temps de vie
        if ($session_token_lifespan < time()) {
            return false;
        }
        //echo 'token lifespan ok';
        //
        //si la page de référence est identique à la page entrée & si les arguments de la page de référence sont identiques à ceux entrés (spoofable, donc ne pas tout miser dessus...)
        /*if (isset($_SERVER['HTTP_REFERER'])){
            $reference_referrer = $_SERVER['HTTP_REFERER'];
            //var_dump($reference_referrer);
            //var_dump($reference_referrer['path']);
            //var_dump(parse_url($reference_referrer)['query']);
            var_dump($reference_referrer['path']);
            var_dump($referrer);
            if (strcmp(parse_url($reference_referrer)['path'], $referrer) != 0 || strcmp(parse_url($reference_referrer)['query'], $referrer_arg) != 0) {
                return false;
            }
        }*TODO*/

        return true;
    }

    public static function clear_died_session_tokens(){
        foreach ($_SESSION as $key => $value){
            if (strpos($key, '_token_ls') !== false){
                if ($value  < time()) {
                    unset($_SESSION[$key]);
                    unset($_SESSION[substr($key, 0, (strlen($key) - 4))]);
                }
            }
        }
    }
}
