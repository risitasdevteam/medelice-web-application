<?php

class IntegrityToken{

  private $_payload;
  
  public function __construct($payload){
    $this->_payload = $payload;
  }

  public function generate(){
      return JWT::encode($this->_payload);
  }
}
