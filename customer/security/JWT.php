<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/10/2017
 * Time: 12:46
 * Some ideas are from https://github.com/firebase/php-jwt/blob/master/src/JWT.php
 */
class JWT
{
    protected static $algorithm = 'sha512';
    protected static $salt = "57E50AB7FFABA9D97C37D70F7855EAAE58247169661891F5BDD9D67121747B6883D5C8A46E99F6ABAD49DC6D214D7CF070E289CD604808B7B7087B96BA649451";

    public static function decode($jwt){
        $timestamp = time();

        $token = explode('.', $jwt);
        if (count($token) != 3){
            throw new Exception('Segments are missing.');
        }

        list($header_b64, $payload_b64, $signature_b64) = $token;
        if (null == ($header = json_decode(self::urlsafeB64Decode($header_b64)))){
            throw new Exception('Invalid header encoding.');
        }

        if (null == ($payload = json_decode(self::urlsafeB64Decode($payload_b64)))){
            throw new Exception('Invalid payload encoding');
        }

        if (false == ($signature = self::urlsafeB64Decode($signature_b64))){
            throw new Exception('Invalid signature encoding.');
        }

        if (empty($header->alg)){
            throw new Exception('Cryptographic algorithm is missing.');
        }

        if (!self::verify("$header_b64.$payload_b64", $signature)){

            throw new Exception('Signature may be corrupted.');
        }

        if (isset($payload->nbf) && $payload->nbf > $timestamp){
            throw new Exception('This token will not be handle before ' . date(DateTime::ISO8601, $payload->nbf));
        }

        if (isset($payload->exp) && $timestamp >= $payload->exp) {
            throw new Exception('Expired token');
        }

        return $payload;
    }

    public static function verify($msg, $signature){
        $hash = hash_hmac(self::$algorithm, $msg, self::$salt);
        return hash_equals($signature, $hash);
    }

    public static function encode($payload){
        $segments = array();
        $segments[] = self::urlsafeB64Encode(json_encode(['alg' => self::$algorithm, 'typ' => 'JWT']));
        $segments[] = self::urlsafeB64Encode(json_encode($payload));
        $segments[] = self::urlsafeB64Encode(self::sign(implode('.', $segments)));
        return implode('.', $segments);
    }

    protected static function sign($msg){
        return hash_hmac(self::$algorithm, $msg, self::$salt);
    }

    /**
     * Decode a string with URL-safe Base64.
     *
     * @param string $input A Base64 encoded string
     *
     * @return string A decoded string
     */
    private static function urlsafeB64Decode($input)
    {
        $remainder = strlen($input) % 4;
        if ($remainder) {
            $padlen = 4 - $remainder;
            $input .= str_repeat('=', $padlen);
        }
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * Encode a string with URL-safe Base64.
     *
     * @param string $input The string you want encoded
     *
     * @return string The base64 encode of what you passed in
     */
    private static function urlsafeB64Encode($input)
    {
        return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
    }
}