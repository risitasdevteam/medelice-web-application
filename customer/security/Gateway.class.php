<?php
require_once(__DIR__ . '/../User.class.php');
require_once(__DIR__.'/../../utility/IDBTableStructure.php');

class DBGatewayStructure implements IDBTableStructure
{
    static function getDatabase()
    {
        return "medelice_users";
    }

    static function getColumns()
    {
        return array("verification_id", "verification_type", "user_id", "verification_key", "verification_stamp");
    }

    static function getTable()
    {
        return "users_gateway";
    }
}

interface GatewayProvider{
    public function insert_entry(int $gateway_type, string $user_id, int $time_addition);
    public function check_entries(int $gateway_type, string $user_id, array $selected_columns);
    public function check_entries_for_specific_columns(array $where_columns, array $selected_columns);
    public function remove_entry(array $where);
}

class DBGatewayProvider implements GatewayProvider{

    public function insert_entry(int $gateway_type, string $user_id, int $time_addition)
    {
        $verification_key =  hash("sha256", openssl_random_pseudo_bytes(10));
        $issued_until = (time() + $time_addition);
        $columns = DBGatewayStructure::getColumns();

        SQLUtil::insert(GATEWAY_TABLE, array_combine(array_slice($columns, 1, 4), array($gateway_type, $user_id, $verification_key, $issued_until)));

        return $verification_key;
    }

    public function check_entries_for_specific_columns(array $where_columns, array $selected_columns)
    {
        $entries = array();
        $raw_gateway = SQLUtil::select(DBGatewayStructure::getTable(), $selected_columns, $where_columns);
        $count = count($raw_gateway);
        if ($count > 0){
            for ($i = 0; $i < $count; $i++){
                $pointer = $raw_gateway[$i];
                array_push($entries, GatewayNew::from_array($pointer, false));
            }
        }

        return $entries;
    }

    public function check_entries(int $gateway_type, string $user_id, array $selected_columns)
    {
        $entries = array();
        $columns = DBGatewayStructure::getColumns();
        $raw_gateway = SQLUtil::select(DBGatewayStructure::getTable(), $selected_columns, [$columns[2] => $user_id, $columns[1] => $gateway_type]);
        $count = count($raw_gateway);
        if ($count > 0){
            for ($i = 0; $i < $count; $i++){
                $pointer = $raw_gateway[$i];
                array_push($entries, GatewayNew::from_array($pointer, false));
            }
        }

        return $entries;
    }

    public function remove_entry(array $where)
    {
        SQLUtil::delete(DBGatewayStructure::getTable(), $where);
        return true;
    }

    public function has_asked_password_reset(string $verification_key, string $unique_id, bool &$by_an_admin){
        $sql = SQLUtil::open(DBGatewayStructure::getDatabase());

        $columns = DBGatewayStructure::getColumns();
        $veritifcation_stamp_column = $columns[4];
        $verification_key_column = $columns[3];
        $user_id_column = $columns[2];
        $type_column = $columns[1];

        $forgotten_type = GatewayType::forgotten_password_generated_by_admin;
        $forgotten_type2 = GatewayType::forgotten_password;

        $query = "select ".$veritifcation_stamp_column.",".$type_column." from ".DBGatewayStructure::getTable()." where ".$verification_key_column." = ? and ".$user_id_column." = ? and ".$type_column." = ? or ".$verification_key_column." = ? and ".$user_id_column." = ? and ".$type_column." = ? limit 1";
        $found = false;
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("ssdssd", $verification_key, $unique_id, $forgotten_type, $verification_key, $unique_id, $forgotten_type2);
            $stmt->bind_result($stamp, $type);
            $stmt->execute();

            while ($stmt->fetch()){

                if ($stamp < time()){
                    SQLUtil::delete(DBGatewayStructure::getTable(), [$verification_key_column => $verification_key]);
                    throw new MedeliceException(L::gateway_link_expired);
                }

                if ($type == GatewayType::forgotten_password_generated_by_admin){
                    $by_an_admin = true;
                }

                $found = true;
            }

            $stmt->close();
        }

        return $found;
    }

    function remove_entry_after_password_forgotten(string $verification_key){
        $columns = DBGatewayStructure::getColumns();
        $verification_key_column = $columns[3];

        SQLUtil::delete(DBGatewayStructure::getTable(), [$verification_key_column => $verification_key]);
        return true;
    }
}

class GatewayNew{
    private $verification_id;
    private $verification_type;
    private $user_id;
    private $verification_key;
    private $verification_stamp;

    /**
     * @return mixed
     */
    public function getVerificationId()
    {
        return $this->verification_id;
    }

    /**
     * @param mixed $verification_id
     */
    public function setVerificationId($verification_id)
    {
        $this->verification_id = $verification_id;
    }

    /**
     * @return mixed
     */
    public function getVerificationType()
    {
        return $this->verification_type;
    }

    /**
     * @param mixed $verification_type
     */
    public function setVerificationType($verification_type)
    {
        $this->verification_type = $verification_type;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getVerificationKey()
    {
        return $this->verification_key;
    }

    /**
     * @param mixed $verification_key
     */
    public function setVerificationKey($verification_key)
    {
        $this->verification_key = $verification_key;
    }

    /**
     * @return mixed
     */
    public function getVerificationStamp()
    {
        return $this->verification_stamp;
    }

    /**
     * @param mixed $verification_stamp
     */
    public function setVerificationStamp($verification_stamp)
    {
        $this->verification_stamp = $verification_stamp;
    }

    public function to_array(){
        return array_combine(DBGatewayStructure::getColumns(), array(
            $this->getVerificationId(),
            $this->getVerificationType(),
            $this->getUserId(),
            $this->getVerificationKey(),
            $this->getVerificationStamp()));
    }

    public static function from_array(array $array, bool $intersection_check = true){
        $gateway = new GatewayNew();

        $columns = DBGatewayStructure::getColumns();
        if ($intersection_check) {
            if (count(array_intersect_key($array, array_flip($columns))) != count($columns)) {
                throw new DataBadException("false arrays intersection");
            }
        }

        $get_column = function($index) use ($array, $columns){
            return $array[$columns[$index]] ?? "";
        };

        $gateway->setVerificationId($get_column(0));
        $gateway->setVerificationType($get_column(1));
        $gateway->setUserId($get_column(2));
        $gateway->setVerificationKey($get_column(3));
        $gateway->setVerificationStamp($get_column(4));

        return $gateway;
    }
}
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 29/08/2017
 * Time: 16:10
 */
class Gateway
{
    /**
     * Return an hashed string from 10 openSSL 'pseudo' random bytes.
     * Used for verification key, as explicitly named.
     * @return string
     */
    public static function generate_gateway_table_verification_key(){
        return hash("sha256", openssl_random_pseudo_bytes(10));
    }

    /**
     * Adding gateway entry, specify the type and the concerned user with his id.
     * @param $gateway_type int|GatewayType
     * @param $user_id
     * @return string
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function add_entry($gateway_type, $user_id){
        if (!is_integer($gateway_type)){
            throw new Exception(L::data_bad);
        }

        if (strlen($user_id) != 13){ //user id length
            throw new Exception(L::data_bad);
        }

        $verification_key = self::generate_gateway_table_verification_key();
        $now = (time() + GATEWAY_TIME2VERIF);

        SQLUtil::insert(GATEWAY_TABLE, ['verification_type' => $gateway_type, 'user_id' => $user_id, 'verification_key' => $verification_key, 'verification_stamp' => $now]);

        return $verification_key;
    }
}
