<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2018
 * Time: 12:27
 */
include_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/misc/UserVerificationIssue.php');

try {

    $issue = filter_input(INPUT_GET, "issue", FILTER_SANITIZE_NUMBER_INT, array("flags" => FILTER_FLAG_ALLOW_OCTAL));
    switch ($issue) {
        case UserVerificationIssue::ERROR:
            $message = 'Une erreur est survenue, impossible de compléter le processus d\'activation du compte.<br>
         <br>
         Veuillez, rééssayer.';
            break;

        case UserVerificationIssue::SUCCESS:
            $uid = RegexUtil::filter_in_var_with_regex(INPUT_GET, "uid", RegexUtil::uid());

            $provider = new DBCustomerProvider();
            $customer = $provider->get($uid);

            $message = 'Félicitations ' . $customer->getFirstname(). ', votre compte Médélice est maintenant activé !<br>
        <br>
        Vous pouvez maintenant vous connecter sur le site avec l\'adresse ' . $customer->getMail() . ' et passer votre première commande !';
            break;

        case UserVerificationIssue::TIME_ERROR:
            $message = 'Le lien n\'est plus valide, veuillez vous réinscrire.<br>';
            break;
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
    //redirect_to_error_page(500);
}
?>

<div id="topsection" class="register-confirm-container">
    <div class="register-confirm-box">
        <?php
            echo $message ?? "ERREUR";
        ?>
    </div>
</div>