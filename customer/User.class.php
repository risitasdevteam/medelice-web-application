<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/08/2017
 * Time: 16:47
 */

require_once(__DIR__ . '/../initializer.inc.php');
require_once(__DIR__.'/../utility.inc.php');
require_once(__DIR__.'/../utility/JsonResponse.class.php');

require_once(__DIR__ . '/misc/GatewayType.enum.php');
require_once(__DIR__ . '/misc/UserType.enum.php');
require_once(__DIR__ . '/misc/UserLoginAttemptsIssues.enum.php');
require_once(__DIR__ . '/security/Gateway.class.php');
require_once(__DIR__ . '/misc/UserGender.enum.php');
require_once(__DIR__ . '/misc/UserServiceType.enum.useless.php');
require_once(__DIR__.'/purchase/CartUtil.php');
/**
 * EXCEPTIONS DEPT
 */
require_once(__DIR__ . '/../exceptions/utilisateur/UserException.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserAccountExistenceException.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserAccountSuspendedException.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserAccountIsntBlockedException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserFakeMailException.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserLoginEmptyMailException.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserLoginEmptyPasswordException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserRegisterAccountAlreadyExistException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserRegisterPasswordNEqualsException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserRejectedPasswordRegexException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserRegisterMissingMandatoryData.php');
require_once(__DIR__ . '/../exceptions/utilisateur/UserInputLengthReachedMaxException.php');
require_once(__DIR__.'/../exceptions/utilisateur/UserAccountVerificationLinkExpiredException.php');

require_once(__DIR__.'/../exceptions/DatabaseException.php');
require_once(__DIR__.'/../exceptions/DatabaseConnectionException.php');
require_once(__DIR__.'/../exceptions/DatabaseInvalidDataException.php');
require_once(__DIR__.'/../exceptions/DatabaseInvalidQueryException.php');

require_once(__DIR__.'/../exceptions/data/DataException.php');
require_once(__DIR__.'/../exceptions/data/DataBadException.php');
require_once(__DIR__.'/../exceptions/data/DataMissingException.php');

require_once(__DIR__.'/../utility/IDBTableStructure.php');
require_once(__DIR__.'/../utility/Environment.php');
require_once(__DIR__ . '/../utility/Mail.php');
require_once(__DIR__.'/security/Gateway.class.php');

const REMEMBER_ME_COOKIE_TIME = (52*7*24*60^2); //1an
const GATEWAY_TIME2VERIF = 900; //15min
const COOKIE_PREFIX = 'usr';
const DEFAULT_MEMBER_USER = UserType::user;
const MAX_ATTEMPTS_BEFORE_BLOCKED = 10;

const CHECKOUT_INTEGRITY_PATH_SESSION_NAME = 'checkout-integrity';

class User
{
    public $id;
    public $user_type;
    public $firstname = '';
    public $surname = '';
    public $mail = '';
    public $password;
    public $phone;
    public $ip;
    public $since;
    public $last_connection;
    public $birthday;

    public $exists = false;

    protected static $_db_rows = array('id', 'user_type', 'firstname', 'surname', 'mail', 'password', 'phone', 'ip', 'since', 'last_connection', 'birthday');

    public function __construct($mail){
        if ($mail == null){

        }
        else {
            $this->mail = $mail;

            $input_raw = SQLUtil::select(USERS_TABLE, ['*'], [self::$_db_rows[4] => $mail]);

            if (count($input_raw) > 0) {
                $input = $input_raw[0];

                $input_to_var = function($db_row) use ($input){
                    return $input[self::$_db_rows[$db_row]];
                };

                $this->id = $input_to_var(0);
                $this->user_type = $input_to_var(1);
                $this->firstname = $input_to_var(2);
                $this->surname = $input_to_var(3);
                $this->password = $input_to_var(5);
                $this->phone = $input_to_var(6);
                $this->ip = $input_to_var(7);
                $this->since = $input_to_var(8);
                $this->last_connection = $input_to_var(9);
                $this->birthday = $input_to_var(10);

                $this->exists = true;
            }
        }
    }

    public function setPassword($password){
        $this->password = $password = self::generate_most_optimal_password_hash($password);
    }

    /**
     * Se connecter.
     * @param $mail
     * @param $password
     * @param $remember
     * @return JsonResponse|string
     * @throws DataBadException
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     * @throws UserAccountExistenceException
     * @throws UserAccountSuspendedException
     * @throws UserInputLengthReachedMaxException
     * @throws UserLoginEmptyMailException
     * @throws UserLoginEmptyPasswordException
     */
    public static function login($mail, $password, $remember){
        $exist = self::user_exist($mail, true); //return id if exists
        if (!$exist){
            throw new UserAccountExistenceException(sprintf(L::form_account_not_exist, $mail));
        }

        if (LOGIN_ATTEMPTS && self::login_attempts_has_blocked_user($exist)){
            throw new UserAccountSuspendedException('Compte temporairement suspendu, veuillez regarder vos emails.');
        }

        if (!is_bool($remember)){
            throw new MedeliceException("remember must be a boolean");
        }

        $raw_user = SQLUtil::select(USERS_TABLE, ['id', 'user_type', 'firstname', 'surname', 'password'], ['mail' => $mail]);
        if (count($raw_user) < 1){
            if (LOGIN_ATTEMPTS) {
                self::add_login_attempts($exist, UsersLoginAttempsIssues::DISALLOWED);
            }
            throw new UserAccountExistenceException(sprintf(L::form_account_not_exist_or_invalid_password, $mail));
        }

        $columns = function($column) use ($raw_user){
          return $raw_user[0][$column];
        };

        if ($columns('user_type') > UserType::user){
            throw new MedeliceException("Connexion impossible. Veuillez verifier que votre compte soit activé.", 403);
        }

        $hashed_password = $columns('password');//TODO à ajouter dans la requête sql
        if (!password_verify($password, $hashed_password)){
            if (LOGIN_ATTEMPTS) {
                self::add_login_attempts($exist, UsersLoginAttempsIssues::DISALLOWED);
            }
            throw new UserAccountExistenceException(sprintf(L::form_account_not_exist_or_invalid_password, $mail));
        }

        SQLUtil::update(USERS_TABLE, ['last_connection' => time(), 'ip' => $_SERVER['SERVER_ADDR']], ['id' => $columns('id')]);

        $_SESSION['loggedIn'] = true;
        $_SESSION['mail'] = $mail;
        $_SESSION['firstname'] = $columns('firstname');
        $_SESSION['surname'] = $columns('surname');
        $_SESSION['remember'] = $remember;
        $_SESSION['rank'] = $columns('user_type');
        $_SESSION['loggedAt'] = time();

        if (LOGIN_ATTEMPTS) {
            self::add_login_attempts($columns('id'), UsersLoginAttempsIssues::SUCCEEDED);
        }

        self::retrieve_and_set_to_session_saved_cart($columns('id'));
        //TODO Next build : implementation of JWT
        //$JWT_payload = array('loggedIn' => 'true', 'iat' => time(), 'exp' => (time() + 3600));

        return true;
    }

    /**
     * Disconnect user.
     * @throws DatabaseInvalidQueryException
     */
    public static function disconnect(){
        if (!isset($_SESSION['mail'])){
            echo "You're not connected"; //TODO
        }

        if (self::isLoggedByPersistentAuth()){
            $user = new User($_SESSION['mail']);
            self::revokePersistentAuth($user->id);
        }

        session_unset();
        $_SESSION = array();
        session_destroy();

        $_SESSION['loggedIn'] = false;
        $_SESSION['firstvisitoftheday'] = false;
    }


    /**
     * Vérifier si le compte d'un customer n'est pas bloqué à cause d'un trop grand nombre d'échecs de connexions.
     * @param $sql mysql L'instance sql ouverte.
     * @param $user_id string L'identifiant unique de l'customer
     * @return bool Vrai si l'customer est bloqué.
     * @throws DataBadException
     * @throws DatabaseInvalidQueryException
     */
    public static function login_attempts_has_blocked_user($user_id){
        if (empty($user_id)){
            throw new DataBadException(L::data_bad);
        }

        $verification_type = GatewayType::blocked_by_login_attempts;

        $returned_data = SQLUtil::select(GATEWAY_TABLE, ['*'], ['user_id' => $user_id, 'verification_type' => $verification_type]);
        return count($returned_data) > 0;
    }


    /**
     * Unblock a customer after too much login attempts.
     * This transaction must be ordered by an email verification operation or by an administrator.
     * @param $verification_key string Gateway unique id
     * @param $user_id string Customer unique id
     * @throws DataBadException
     * @throws DatabaseInvalidQueryException
     * @throws UserAccountIsntBlockedException
     */
    public static function unblock_user_after_too_much_login_attempts($verification_key, $user_id){
        if (empty($verification_key)){
            throw new DataBadException(L::data_bad);
        }
        if (empty($user_id)){
            throw new DataBadException(L::data_bad);
        }

        if (!self::login_attempts_has_blocked_user($user_id)){
            throw new UserAccountIsntBlockedException('Utilisateur non bloqué.');
        }

        $verification_type = GatewayType::blocked_by_login_attempts;
        SQLUtil::delete(GATEWAY_TABLE, ['verification_key' => $verification_key, 'user_id' => $user_id, 'verification_type' => $verification_type]);
    }

    /**
     * Ajouter une issue positive ou négative lors d'une connexion.
     * @param $sql mysqli Instance sql ouverte.
     * @param $user_id  string Identifiant unique de l'customer.
     * @param $issue integer Issue de la connexion, @see UserLoginAttemptsIssues
     * @throws DataBadException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function add_login_attempts($user_id, $issue){
        if (empty($user_id)){
            throw new DataBadException(L::data_bad);
        }
        if (!is_integer($issue) && ($issue != UsersLoginAttempsIssues::SUCCEEDED) && ($issue != UsersLoginAttempsIssues::DISALLOWED)){
            throw new DataBadException(L::data_bad);
        }

        $now = time(); //temps UNIX
        $back_in_time = $now - (60**60); //temps - 1h

        $returned_data = SQLUtil::select(LOGIN_ATTEMPTS_TABLE, ['*'], ['id' => $user_id, 'attempts_time:>' => $back_in_time, 'issue' => UsersLoginAttempsIssues::DISALLOWED]);
        if ($issue == UsersLoginAttempsIssues::DISALLOWED){
            if (count($returned_data) == (MAX_ATTEMPTS_BEFORE_BLOCKED - 1)){
                $user = SQLUtil::select(USERS_TABLE, ['mail'], ['id' => $user_id]); //récupération de l'email depuis
                if (count($user) == 0){
                    throw new Exception('Utilisateur non trouve'); //ne doit pas se produire
                }
                $mail = $user[0]['mail'];
                $verification_key = Gateway::add_entry(GatewayType::blocked_by_login_attempts, $user_id);

                $msg = 'Bonjour %s,
                Votre compte a subit un très grand nombre de connexions qui ne semblent pas être valides.
                Était-ce vous ? Par mesure de securité nous avons bloqué votre compte temporairement.
                Pour le débloquer vous devez cliquer sur le lien ci-dessous :
                '.$url = $_SERVER['SERVER_NAME'].'/index.php?page=gateway&type='.GatewayType::blocked_by_login_attempts.'&vkey='.$verification_key.'&uid='.$user_id;

                custom_mail($mail, L::mail_title_blocked_account, $msg);
            }
        }else{
            SQLUtil::delete(LOGIN_ATTEMPTS_TABLE, ['id' => $user_id, 'attempts_time:>' => $back_in_time, 'issue' => UsersLoginAttempsIssues::DISALLOWED]);
        }

        //ajout dernière tentative
        SQLUtil::insert(LOGIN_ATTEMPTS_TABLE, ['id' => $user_id, 'attempts_time' => time(), 'issue' => $issue, 'ip' => $_SERVER['REMOTE_ADDR']]);
    }

    public function to_array(){
        return get_object_vars($this);
    }

    public static function from_array($array){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        if (count(array_intersect_key($array, array_flip(self::columns()))) != count(self::columns())) {
            throw new DataBadException("false arrays intersection");
        }

        //fuzzy way to cast
        $user = new User(null);
        $user->id = $array["id"];
        $user->user_type = $array["user_type"];
        $user->mail = $array["mail"];
        $user->firstname = $array["firstname"];
        $user->surname = $array["surname"];
        $user->password = $array["password"];
        $user->phone = $array["phone"];
        $user->ip = $array["ip"];
        $user->since = $array["since"];
        $user->last_connection = $array["last_connection"];
        $user->birthday = $array["birthday"];


        return $user;
    }

    public static function get_user_from($from, $data){
        if (empty($data) || !is_string($from) || !is_string($data)){
            return false;
        }

        $returned_data = SQLUtil::select(USERS_TABLE, ['*'], [$from => $data]);
        if (count($returned_data) < 1) {
            return null;
        }

        $user_array = $returned_data[0];
        return self::from_array($user_array);
    }

    /**
     * Check if user exists by giving mail.
     * @param $mail
     * @param bool|false $return_id Return user id if he exist.
     * @return bool|string
     * @throws DatabaseInvalidQueryException
     */
    public static function user_exist($mail, $return_id = false){
        if (empty($mail)){
            return false;
        }

        $returned_data = null;
        if (!$return_id){
            $returned_data = SQLUtil::select(USERS_TABLE, ['mail'], ['mail' => $mail]);
        }else{
            $returned_data = SQLUtil::select(USERS_TABLE, ['mail', 'id'], ['mail' => $mail]);
        }

        return (count($returned_data) == 0 ? false : ($return_id ? $returned_data[0]['id'] : true));
    }


    public static function columns()
    {
        return array('id',
            'user_type',
            'firstname',
            'surname',
            'mail',
            'password',
            'addresses',
            'phone',
            'ip',
            'since',
            'last_connection',
            'birthday',
            'genre');
    }

    public static function array_difference($user1, $user2){
        if (!$user1 instanceof User || !$user2 instanceof User){
            throw new DataBadException("user 1 or 2 must be User objects");
        }

        $user1_matrix = $user1->to_array();
        $user2_matrix = $user2->to_array();

        $sql_update_sets_matrix = [];

        foreach($user1_matrix as $key => $value){
            $user2_matrix_value = $user2_matrix[$key];

            if ($value != $user2_matrix_value){
                $sql_update_sets_matrix[$key] = $user2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }

    public function edit_profile($modified_profile){
        if (!$modified_profile instanceof User)
            return false;

        $sets = self::array_difference($this, $modified_profile);
        //var_dump($sets);

        SQLUtil::update(USERS_TABLE, $sets, [self::$_db_rows[0] => $modified_profile->id]);

        return true;
    }

    /**
     * Save customer cart as a json.
     * @param $unique_id string
     * @param $cart
     * @return bool
     * @throws DataBadException
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function saveCart($unique_id, $cart){
        $returned_data = SQLUtil::select(USERS_CARTS, ['unique_id'], ['unique_id' => $unique_id]);
        $already_saved = count($returned_data) > 0;

        $time = time();
        if (!$already_saved)
            SQLUtil::insert(USERS_CARTS, ['unique_id' => $unique_id, 'cart' => $cart, 'saved_at' => $time]);
        else
            SQLUtil::update(USERS_CARTS, ['cart' => $cart, 'saved_at' => $time], ['unique_id' => $unique_id]);

        return true;
    }

    /**
     * Get saved customer cart as an array.
     * @param $unique_id string
     * @return array
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws MedeliceException
     */
    public static function getSavedCart($unique_id){
        $retrieved_data_array = array();
        $sql = SQLUtil::open("medelice_users");

        $returned_data = SQLUtil::select(USERS_CARTS, ['cart', 'saved_at'], ['unique_id' => $unique_id]);
        if (count($returned_data) > 0){
            $retrieved_data_array['unique_id'] = $unique_id;
            $retrieved_data_array['cart'] = $returned_data[0]['cart'];
            $retrieved_data_array['saved_at'] = $returned_data[0]['saved_at'];
        }else{
            //throw new MedeliceException('No cart for the given uID');
        }

        return $retrieved_data_array;
    }

    /**
     *
     * @param $unique_id
     */
    public static function retrieve_and_set_to_session_saved_cart($unique_id){
        //get back saved cart
        $saved_cart = self::getSavedCart($unique_id);
        if (count($saved_cart) > 0) {
            $saved_products_json = json_decode($saved_cart['cart']);
            foreach ($saved_products_json as $product) {
                $ref = $product->_product_ref;
                $quant = $product->_product_quantity;

                CartUtil::set($ref, $quant, true);
            }
        }
    }

    /**
     * Destroy saved cart to prevent useless mass data collecting...
     * @param $unique_id
     * @return bool
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function destroySavedCart($unique_id){
        SQLUtil::delete(USERS_CARTS, ['unique_id' => $unique_id]);
        return true;
    }

    /**
     * Check if user is logged.
     * @return bool
     */
    public static function isLogged(){
        return (isset($_SESSION['loggedIn']) && $_SESSION['loggedIn']);
    }

    /**
     * Check if user is logged and logged by persistent auth system.
     * @return bool
     */
    public static function isLoggedByPersistentAuth(){
        return self::isLogged() && isset($_SESSION['loggedByPAuth']) && $_SESSION['loggedByPAuth'];
    }

    /**
     * Check if user possesses a persistent auth cookie.
     * @return bool|mixed
     */
    public static function hasPersistentAuth(){
        if (!isset($_COOKIE['medpauth'])){
            return false;
        }
        return $_COOKIE['medpauth'];
    }

    /**
     * Check if user possesses a valid auth cookie according to his information.
     * Return unique id if user has a valid valid persistent auth data otherwise false.
     * @return bool|mixed
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function hasValidPersistentAuth(){
        $cookie = self::hasPersistentAuth();
        if ($cookie == false){
            return false;
        }

        if (strlen($cookie) != 80){
            return false;
        }

        /*$in_random_hash = substr($cookie, 0, 64);
        $in_browser_hash = substr($cookie, 64, 8);
        $in_ip_hash = substr($cookie, 72, 8);*/

        $db_check = self::hasValidPersistentAuthInDB($cookie);
        return ($db_check != null ? $db_check : false);
    }

    /**
     * Check from database if user has a valid persistent auth.
     * Return unique id if valid persistent auth, otherwise null.
     * @param $unique_hash
     * @return string If == null : invalid persistent auth, != null : valid (unique_id)
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    private static function hasValidPersistentAuthInDB($unique_hash){
        $lifetime = time();
        $unique_id = null;
        $sql = SQLUtil::open("medelice_users");
        $query = 'select unique_id from users_persistent_auth where unique_hash = ? and lifetime >= ?';
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param('sd', $unique_hash, $lifetime);
            $stmt->bind_result($id);
            $stmt->execute();

            while ($stmt->fetch()){
                $unique_id = $id;
            }

            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }
        return $unique_id;
    }

    /**
     * Insert in database and set cookie for persistent auth.
     * @param bool|false $set_cookie
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     * @throws Exception
     */
    public static function setPersistentAuth($set_cookie = false){
        if (!self::isLogged()){
            throw new Exception('Can\'t set persistent auth without being connected');
        }

        $profile = new User($_SESSION['mail']);
        $id = $profile->id;

        //Generate hash
        $random_hash = hash("sha256", openssl_random_pseudo_bytes(10));
        $browser_hash = hash("crc32", $_SERVER['HTTP_USER_AGENT']);
        $ip_hash = hash("crc32", $_SERVER['REMOTE_ADDR']);
        $hash = $random_hash.$browser_hash.$ip_hash;

        $lifetime = (time() + (86400*14)); //2w
        if ($set_cookie){ //if set, must be in the initializer
           // if (!isset($_SESSION['remember_cookie_set']) || $_SESSION['remember_cookie_set'] != true)
            setcookie('medpauth', $hash, $lifetime, '/', (DEBUG ? '.medelice.dev' : '.medelice.com'), true, true);

            $_SESSION['remember_cookie_set'] = false;
        }

        SQLUtil::insert('users_persistent_auth', ['unique_id' => $id, 'unique_hash' => $hash, 'lifetime' => $lifetime]);
    }

    /**
     * Login remotely by persistent cookie.
     * @return bool True if successfully logged.
     * @throws DatabaseConnectionException
     * @throws UserAccountExistenceException
     */
    public static function loginByPersistentAuth()
    {
        $unique_id = self::hasValidPersistentAuth();
        if (is_bool($unique_id) && !$unique_id) {
            return false;
        }

        $profile = self::get_user_from('id', $unique_id);
        if ($profile == null) {
            throw new UserAccountExistenceException(printf(L::form_account_not_exist_id, $unique_id));
        }

        $_SESSION['loggedIn'] = true;
        $_SESSION['loggedByPAuth'] = true;
        $_SESSION['mail'] = $profile->mail;
        $_SESSION['firstname'] = $profile->firstname;
        $_SESSION['surname'] = $profile->surname;
        $_SESSION['loggedByPAuth'] = true;
        //$_SESSION['persistent'] = true;
        //$_SESSION['remember'] = true;

        self::retrieve_and_set_to_session_saved_cart($unique_id);

        return true;
    }

    /**
     * Revoke a persistent auth session from DB. No cookie security here, only DB.
     * @param $unique_id
     * @return bool If true, persistent auth's revoked.
     * @throws DatabaseConnectionException
     * @throws DatabaseInvalidQueryException
     */
    public static function revokePersistentAuth($unique_id){
        /*if ($login_check && !self::isLogged()){
            throw new Exception('Can\'t revoke persistent auth without being connected');
        }*/

        /*if ($cookie_check && !self::hasPersistentAuth()){
            throw new Exception('Can\'t revoke persistent auth without possesses a valid cookie');
        }*/

        /*$profile = new User($_SESSION['mail']);
        $id = $profile->id();*/

        $hash = null;
        $sql = SQLUtil::open("medelice_users");
        $select = 'select unique_hash from users_persistent_auth where unique_id = ?';
        if ($stmt = $sql->prepare($select)){
            $stmt->bind_param('s', $unique_id);
            $stmt->bind_result($out_hash);
            $stmt->execute();

            while ($stmt->fetch()){
                $hash = $out_hash;
            }
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        if ($hash == null)
            return false;

        $delete = 'delete from users_persistent_auth where unique_hash = ?';
        if ($stmt = $sql->prepare($delete)){
            $stmt->bind_param('s', $hash);
            $stmt->execute();
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        return true;
    }

    /**
     * Check if user's logged by persistent auth and also logged with a valid cookie, if yes
     * we refresh the unique_id for each request, if not we disconnect him.
     */
    public static function watchdogPersistentAuthCookieIntegrity(){
        if (!self::isLoggedByPersistentAuth()){
            return;
        }

        if (self::hasValidPersistentAuth()){
            self::setPersistentAuth(true); //refresh the id
            return;
        }

        self::disconnect();
    }

    public static function generate_most_optimal_password_hash($password){
        $timeTarget = 0.05;
        $cost = 8; //Default cost
        do{
            $cost++;
            $start = microtime(true);
            $hashed_password = password_hash($password, PASSWORD_BCRYPT, ["cost" => $cost]);
            $end = microtime(true);
        }while (($end - $start) < $timeTarget);

        return $hashed_password;
    }

    public static function hasBegunCheckout(){
        return isset($_SESSION[CHECKOUT_INTEGRITY_PATH_SESSION_NAME]);
    }

    public static function incrementCheckoutStep(){
        $step = $_SESSION[CHECKOUT_INTEGRITY_PATH_SESSION_NAME];
        $_SESSION[CHECKOUT_INTEGRITY_PATH_SESSION_NAME] = ((!isset($step) ? 0 : $step) + 1);
        return true;
    }

    public static function hasCompletedCheckout(){
        $step = $_SESSION[CHECKOUT_INTEGRITY_PATH_SESSION_NAME]; //array
        return isset($step) &&  $step == 4;
    }

    public static function deleteCheckoutProcessFromSession(){
        unset($_SESSION[CHECKOUT_INTEGRITY_PATH_SESSION_NAME]);
    }

    public static function get_customers($limit){
        if (!is_int($limit)){
            throw new MedeliceException("Limit must be an integer var");
        }

        $customers = [];
        $raw_customers = SQLUtil::select(USERS_TABLE, ['*'], [], 'limit ' . strval($limit));

        $raw_customers_count = count($raw_customers);
        for ($i = 0; $i < $raw_customers_count; $i++){
            $raw_customer = $raw_customers[$i];

            if (!isset($raw_customer)) {
                throw new MedeliceException("Null raw customer pointer");
            }

            $customer = new User(null);
            $customer->id = $raw_customer['id'];
            $customer->mail = $raw_customer['mail'];
            $customer->surname = $raw_customer['surname'];
            $customer->firstname = $raw_customer['firstname'];
            $customer->password = $raw_customer['password'];
            $customer->_addresses = $raw_customer['addresses'];
            $customer->phone = $raw_customer['phone'];
            $customer->ip = $raw_customer['ip'];
            $customer->since = $raw_customer['since'];
            $customer->birthday = $raw_customer['birthday'];
            $customer->last_connection = $raw_customer['last_connection'];
            $customer->_genre = $raw_customer['genre'];
            $customer->user_type = $raw_customer['user_type'];
            $customer->exists = true;

            $customers[$i] = (object)(array)$customer;
        }
        //var_dump($customers);
        return $customers;
    }

    public static function get_customers_count(){
        $raw_customers = SQLUtil::select(USERS_TABLE, ['id as total'], []);
        return count($raw_customers);
    }
}

class DBCustomerStructure implements IDBTableStructure{
    static function getDatabase()
    {
        return "medelice_users";
    }

    static function getColumns()
    {
        return  array('id', 'user_type', 'firstname', 'surname', 'mail', 'password', 'phone', 'ip', 'since', 'last_connection', 'birthday');
    }

    static function getTable()
    {
        return "users";
    }

}

class DBCustomerLoginAttempts implements IDBTableStructure{
    static function getDatabase()
    {
        return "medelice_users";
    }

    static function getColumns()
    {
        return array("id", "time", "issue");
    }

    static function getTable()
    {
        return "users_login_attempts";
    }
}


interface CustomerProvider{
    function get_in_interval(int $lower, int $upper, array $columns = ['*']);
    function get_total();
    function get_specific_columns_from_id();
    function get(string $unique_id);
    function get_on_email(string $email);
    function generate_forgotten_password_url(Customer $customer, bool $by_admin = false);
    function confirm_new_password_after_forgoten(string $user_id, string $verification_key, string $password, string $password_confirm);
    function edit(Customer $customer);
    function delete(string $unique_id);
    function send_billing_mail(OrderNew $order, OrderCustomerAddresses $addresses, string $mail);
    function send_validation_mail(string $verification_key, string $firstname, string $unique_id, string $mail);
    function send_password_forgotten_mail(string $verification_key, Customer $customer);
    function send_password_forgotten_mail_confirmation(Customer $customer);
    /**
     * @param string $mail
     * @param string $password
     * @param string $firstname
     * @param string $surname
     * @param string $ip
     * @param string $phone
     * @param int $birthday
     * @return mixed
     */
    function register(string $mail, string $password, string $firstname, string $surname, string $ip, string $phone, int $birthday);
    function get_login_attempts(string $unique_id);
    function get_on_rank(int $type, array $retrieved_columns = array("*"));
}

class DBCustomerProvider implements CustomerProvider{

    function get_in_interval(int $lower, int $count, array $columns = ['*'])
    {
        $customers = array();
        $raw_customers = SQLUtil::select(USERS_TABLE, $columns, [], 'limit ' . $lower.', '.$count);
        $raw_customers_count = count($raw_customers);
        if ($raw_customers_count > 0){
            for ($i = 0; $i < $raw_customers_count; $i++){
                $customer_pointer = $raw_customers[$i];

                array_push($customers, $customer_pointer);
            }
        }

        return $customers;
    }

    function get_total(){
        $sql = SQLUtil::open("medelice_users");
        $query = "select count(*) as total from ".DBCustomerStructure::getTable();

        $count_total = 0;
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_result($total);
            $stmt->execute();

            while ($stmt->fetch()){
                $count_total = $total;
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $count_total;
    }

    function get_specific_columns_from_id()
    {
        // TODO: Implement get_specific_columns_from_id() method.
    }


    function get(string $unique_id)
    {
        $customer = new Customer();
        $sql = SQLUtil::open(DBCustomerStructure::getDatabase());
        $column_id = DBCustomerStructure::getColumns()[0];
        $query = "select * from ".DBCustomerStructure::getTable()." where ".$column_id. " = ?";
        $found = false;
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("s", $unique_id);
            $stmt->bind_result($id, $user_type, $firstname, $surname, $mail, $password, $phone, $ip, $since, $last_connection, $birthday);
            $stmt->execute();

            while ($stmt->fetch()){
                $customer->setId($id);
                $customer->setUserType($user_type);
                $customer->setFirstname($firstname);
                $customer->setSurname($surname);
                $customer->setMail($mail);
                $customer->setPassword($password);
                $customer->setPhone($phone);
                $customer->setIp($ip);
                $customer->setSince($since);
                $customer->setLastConnection($last_connection);
                $customer->setBirthday($birthday);

                $found = true;
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $found ? $customer : null;
    }

    function get_on_rank(int $type, array $retrieved_columns = array("*")){
        $customers = array();

        $column_rank = DBCustomerStructure::getColumns()[1];
        $raw_customers = SQLUtil::select(DBCustomerStructure::getTable(), $retrieved_columns, [$column_rank => $type]);
        $count = count($raw_customers);
        for ($i = 0; $i < $count; $i++){
            $customer_pointer = $raw_customers[$i];
            array_push($customers, Customer::from_array($customer_pointer, false));
        }

        return $customers;
    }

    function get_on_email(string $email)
    {
        $customer = new Customer();
        $sql = SQLUtil::open(DBCustomerStructure::getDatabase());
        $column_id = DBCustomerStructure::getColumns()[4];
        $query = "select * from ".DBCustomerStructure::getTable()." where ".$column_id. " = ?";
        $found = false;
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("s", $email);
            $stmt->bind_result($id, $user_type, $firstname, $surname, $mail, $password, $phone, $ip, $since, $last_connection, $birthday);
            $stmt->execute();

            while ($stmt->fetch()){
                $customer->setId($id);
                $customer->setUserType($user_type);
                $customer->setFirstname($firstname);
                $customer->setSurname($surname);
                $customer->setMail($mail);
                $customer->setPassword($password);
                $customer->setPhone($phone);
                $customer->setIp($ip);
                $customer->setSince($since);
                $customer->setLastConnection($last_connection);
                $customer->setBirthday($birthday);

                $found = true;
            }

            $stmt->close();
        }

        SQLUtil::close($sql);

        return $found ? $customer : null;
    }

    function edit(Customer $customer)
    {
        $column_id = DBCustomerStructure::getColumns()[0];
        $old = $this->get($customer->getId());
        $difference = Customer::array_difference($old, $customer);
        if (count($difference) == 0){
            throw new MedeliceException("Aucun changement n'a été détecté.");
        }
        SQLUtil::update(DBCustomerStructure::getTable(), $difference, [$column_id => $customer->getId()]);

        return true;
    }

    function delete(string $unique_id){
        $columns = DBCustomerStructure::getColumns();
        $column_id = $columns[0];
        $column_user_type = $columns[1].':!=';

        SQLUtil::delete(DBCustomerStructure::getTable(), [$column_id => $unique_id, $column_user_type => UserType::administrator]);

        return true;
    }

    function generate_forgotten_password_url(Customer $customer, bool $by_admin = false)
    {
        $gateway_type = GatewayType::forgotten_password;

        $columns = DBGatewayStructure::getColumns();
        $provider = new DBGatewayProvider();
        $entries = $provider->check_entries($gateway_type, $customer->getId(), [$columns[3]]);
        if (count($entries) > 0) {
            foreach ($entries as $entry) {
                if (!$entry instanceof GatewayNew) {
                    throw new MedeliceException("");
                }

                $provider->remove_entry([$columns[2] => $customer->getId(), $columns[1] => $gateway_type, $columns[3] => $entry->getVerificationKey()]);
            }
        }

        $verification_key = $provider->insert_entry(($by_admin ? GatewayType::forgotten_password_generated_by_admin : $gateway_type), $customer->getId(), 3600);

        $this->send_password_forgotten_mail($verification_key, $customer);

        return true;
    }

    function send_password_forgotten_mail(string $verification_key, Customer $customer){
        $mail = new Mail();
        $sender = new MailSender();

        $mail->setTo($customer->getMail());
        $mail->setMailConfiguration(new RobotMailConfiguration());
        $mail->setSubject("Mot de passe oublié");

        $patterns = array('/{content}/',
            '/{server_name}/',
            '/{customer_firstname}/',
            '/{vkey}/',
            '/{customer_id}/');

        $replacements = array(file_get_contents(__DIR__.'/../utility/mail_templates/asked_password_reset.html'),
            Environment::get_server_name(),
            $customer->getFirstname(),
            $verification_key,
            $customer->getId());

        $sender->send($mail, $patterns, $replacements);
    }

    function confirm_new_password_after_forgoten(string $user_id, string $verification_key, string $password, string $password_confirm){
        $gateway_provider = new DBGatewayProvider();

        if (strcmp($password, $password_confirm) !== 0){
            throw new MedeliceException("Mots de passes non identiques");
        }

        if (!$gateway_provider->remove_entry_after_password_forgotten($verification_key)){
            throw new MedeliceException("");
        }

        $customer = $this->get($user_id);
        $customer->setPassword($password, true);
        if (!$this->edit($customer)){
            throw new MedeliceException("Erreur lors de l'édition");
        }

        $this->send_password_forgotten_mail_confirmation($customer);

        return true;
    }

    function send_password_forgotten_mail_confirmation(Customer $customer)
    {
        $mail = new Mail();
        $sender = new MailSender();

        $mail->setTo($customer->getMail());
        $mail->setMailConfiguration(new RobotMailConfiguration());
        $mail->setSubject("Mot de passe oublié");

        $patterns = array('/{content}/',
            '/{server_name}/',
            '/{customer_firstname}/',
            '/{customer_id}/');

        $replacements = array(file_get_contents(__DIR__.'/../utility/mail_templates/confirm_password_reset.html'),
            Environment::get_server_name(),
            $customer->getFirstname(),
            $customer->getId());

        $sender->send($mail, $patterns, $replacements);
    }

    /**
     * @param string $mail
     * @param string $password
     * @param string $firstname
     * @param string $surname
     * @param string $ip
     * @param string $phone
     * @param int $birthday
     * @return bool
     * @throws DatabaseConnectionException
     * @throws MedeliceException
     * @throws UserFakeMailException
     * @throws UserRegisterAccountAlreadyExistException
     */
    public function register(string $mail, string $password, string $firstname, string $surname, string $ip, string $phone, int $birthday){
        if (is_a_fake_mail($mail)){
            throw new UserFakeMailException(L::form_fake_mail);
        }

        $sql = SQLUtil::open(DBCustomerStructure::getDatabase());
        $query = "insert into ".DBCustomerStructure::getTable()." values(?,?,?,?,?,?,?,?,?,?,?)";

        $unique_id = uniqid();
        $user_type = UserType::user_not_verified;
        $hashed_password = User::generate_most_optimal_password_hash($password);
        $timestamp = time();

        $duplicate_key_error = 1062;

        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("sdssssssddd", $unique_id, $user_type, $firstname, $surname, $mail, $hashed_password, $phone, $ip, $timestamp, $timestamp, $birthday);
            $stmt->execute();

            if ($stmt->error !== "") {
                if ($stmt->errno === $duplicate_key_error) {
                    $stmt->close();
                    throw new UserRegisterAccountAlreadyExistException(sprintf(L::form_account_exist, $mail));
                }else{
                    $stmt->close();
                    throw new MedeliceException("Erreur");
                }
            }
            $stmt->close();
        }

        SQLUtil::close($sql);

        $gateway_provider = new DBGatewayProvider();
        $verification_key = $gateway_provider->insert_entry(GatewayType::verify, $unique_id, 3600);

        $this->send_validation_mail($verification_key, $firstname, $unique_id, $mail);

        return true;
    }

    function send_billing_mail(OrderNew $order, OrderCustomerAddresses $addresses, string $mail)
    {
        $mail_sender = new MailSender();
        $billing_mail = new Mail();
        $product_provider = new DBProductProvider();
        $product_io_provider = new FileIOProductProvider();

        $billing_mail->setMailConfiguration(new RobotMailConfiguration());
        $billing_mail->setSubject("Confirmation de votre commande n°".$order->getId());
        $billing_mail->setTo($mail);

        $bill = CartUtil::bill();
        $content = CartUtil::contents();
        $compiled_content = "";

        $content_patterns = array('/{product_img}/', '/{product_name}/', '/{product_price_by_quantity}/');

        $content_template = file_get_contents(__DIR__.'/../utility/mail_templates/billing_product.html');
        foreach ($content as $product_ref => $quantity){
            if (CartUtil::is_a_valisette($product_ref)){
                $valisette = new Valisette($product_ref);

                $valisette_id = explode(Valisette::SUB_VALISETTE_DELIMITER, $product_ref)[1];
                $sub_valisette = $valisette->get_specific_sub_valisette_from_id($valisette_id);
                if (!$sub_valisette instanceof SubValisette){
                    throw new MedeliceException("");
                }

                $price = 0;
                foreach ($sub_valisette->products as $sub_ref){
                    $product = $product_provider->get($sub_ref);
                    if (!$product instanceof Product){
                        throw new MedeliceException("");
                    }

                    $price += $product->getPrice();
                }

                $content_replacements = array("https://".Environment::get_server_name().'/'.$valisette->image(), $price * $quantity);
                $compiled_content .= preg_replace($content_patterns, $content_replacements, $content_template);

            }else{
                $product = $product_provider->get($product_ref);

                $content_replacements = array("https://".Environment::get_server_name().'/'.$product_io_provider->get_images($product_ref)[0], $product->getName(), $product->getPrice() * $quantity);
                $compiled_content .= preg_replace($content_patterns, $content_replacements, $content_template);
            }
        }

        $billing_address = $addresses->billing;
        if (!$billing_address instanceof Address){
            throw new MedeliceException("");
        }

        $patterns = array('/{content}/',
            '/{server_name}/',
            '/{order_id}/',
            '/{order_delivery_date}/',
            '/{order_delivery_mode}/',
            '/{order_delivery_emitter_name}/',
            '/{order_delivery_address1}/',
            '/{order_delivery_address2}/',
            '/{order_delivery_city}/',
            '/{order_delivery_zip}/',
            '/{order_delivery_state}/',
            '/{order_delivery_country}/',
            '/{order_content}/',
            '/{order_total_products}/',
            '/{order_shipping_duty}/',
            '/{order_total_bill}/');

        $mail_sender->send($billing_mail, $patterns,
            array(html_entity_decode(file_get_contents(__DIR__."/../utility/mail_templates/billing.html")),
                Environment::get_server_name(),
                $order->getId(),
                "Vendredi 14 juillet, 20/10/2018",
                $order->getDeliveryMode(),
                $billing_address->emitter_name,
                $billing_address->address1,
                count($billing_address->address2) > 0 ? $billing_address->address2 : 'Aucun indication',
                $billing_address->city,
                $billing_address->zip,
                $billing_address->state,
                $billing_address->country,
                $compiled_content,
                $bill["price"],
                $bill["taxes"],
                $order->getTotalBill()));
    }

    function send_validation_mail(string $verification_key, string $firstname, string $unique_id, string $mail)
    {
        $mail_sender = new MailSender();
        $mail_obj = new Mail();

        $mail_obj->setMailConfiguration(new RobotMailConfiguration());
        $mail_obj->setSubject("Validation de compte");
        $mail_obj->setTo($mail);

        $patterns = array('/{content}/',
            '/{server_name}/',
            '/{customer_firstname}/',
            '/{customer_id}/',
            '/{vkey}/');

        $mail_sender->send($mail_obj, $patterns,
            array(html_entity_decode(file_get_contents(__DIR__."/../utility/mail_templates/confirm_account.html")),
                Environment::get_server_name(),
                $firstname,
                $unique_id,
                $verification_key));
    }

    function get_login_attempts(string $unique_id)
    {
        $login_attempts = array();
        $sql = SQLUtil::open(DBCustomerLoginAttempts::getDatabase());
        $issue = UsersLoginAttempsIssues::DISALLOWED;
        $query = "select attempts_time, ip from ".DBCustomerLoginAttempts::getTable()." where id = ? and issue = ?";
        if ($stmt = $sql->prepare($query)){
            $stmt->bind_param("sd", $unique_id, $issue);
            $stmt->bind_result($time,  $ip);
            $stmt->execute();

            if ($stmt->error !== ''){
                $stmt->close();
                SQLUtil::close($sql);

                throw new MedeliceException("Can't get login attempts");
            }

            while ($stmt->fetch()){
                array_push($login_attempts, array("time" => $time, "ip" => $ip));
            }

            $stmt->close();
        }

        SQLUtil::close($sql);
        return $login_attempts;
    }

    function verify(string $verification_key, string $user_id){
        $gateway_provider = new DBGatewayProvider();
        $gateway_columns = DBGatewayStructure::getColumns();

        $verification_key_column = $gateway_columns[3];
        $user_id_column = $gateway_columns[2];
        $type_column = $gateway_columns[1];
        $stamp_column = $gateway_columns[4];

        $entries = $gateway_provider->check_entries_for_specific_columns([$type_column => GatewayType::verify, $verification_key_column => $verification_key, $user_id_column => $user_id], [$stamp_column]);
        if (count($entries) === 0){
            throw new MedeliceException("Impossible de verifier l'utilisateur");
        }

        if ($entries[0] instanceof GatewayNew && $entries[0]->getVerificationStamp() < time()){
            $gateway_provider->remove_entry([$verification_key_column => $verification_key]);
            $this->delete($user_id);
            throw new UserAccountVerificationLinkExpiredException("Le lien n'est plus valide");
        }

        $customer_provider = new DBCustomerProvider();
        $customer = $customer_provider->get($user_id);
        $customer->setUserType(UserType::user);
        $customer_provider->edit($customer);

        $gateway_provider->remove_entry([$verification_key_column => $verification_key]);

        $this->send_final_validation_mail($customer->getFirstname(), $user_id, $customer->getMail());

        return true;
    }

    function send_final_validation_mail(string $firstname, string $unique_id, string $mail){
        $mail_sender = new MailSender();
        $mail_obj = new Mail();

        $mail_obj->setMailConfiguration(new RobotMailConfiguration());
        $mail_obj->setSubject("Finalisation de la création du compte");
        $mail_obj->setTo($mail);

        $patterns = array('/{content}/',
            '/{server_name}/',
            '/{customer_firstname}/',
            '/{customer_id}/');

        $mail_sender->send($mail_obj, $patterns,
            array(html_entity_decode(file_get_contents(__DIR__."/../utility/mail_templates/account_confirmed.html")),
                Environment::get_server_name(),
                $firstname,
                $unique_id));
    }
}

//Future "User" object replacement
class Customer{
    private $id;
    private $user_type;
    private $firstname = '';
    private $surname = '';
    private $mail = '';
    private $password;
    private $phone;
    private $ip;
    private $since;
    private $last_connection;
    private $birthday;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserType()
    {
        return $this->user_type;
    }

    /**
     * @param mixed $user_type
     */
    public function setUserType($user_type)
    {
        $this->user_type = $user_type;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @param bool $hash
     */
    public function setPassword($password, bool $hash = false)
    {
        $this->password = $hash ? User::generate_most_optimal_password_hash($password) : $password;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    }

    /**
     * @return mixed
     */
    public function getSince()
    {
        return $this->since;
    }

    /**
     * @param mixed $since
     */
    public function setSince($since)
    {
        $this->since = $since;
    }

    /**
     * @return mixed
     */
    public function getLastConnection()
    {
        return $this->last_connection;
    }

    /**
     * @param mixed $last_connection
     */
    public function setLastConnection($last_connection)
    {
        $this->last_connection = $last_connection;
    }

    /**
     * @return mixed
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param mixed $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    public static function from_array(array $customers_array, bool $intersection_check = true){
        $customer = new Customer();

        if ($intersection_check) {
            if (count(array_intersect_key($customers_array, array_flip(DBCustomerStructure::getColumns()))) != count(DBCustomerStructure::getColumns())) {
                throw new DataBadException("false arrays intersection");
            }
        }

        $mapped_numerically_columns_array = array_values(DBCustomerStructure::getColumns());
        $get_column = function($index) use ($customers_array, $mapped_numerically_columns_array){
            return $customers_array[$mapped_numerically_columns_array[$index]] ?? "";
        };

        $customer->setId($get_column(0));
        $customer->setUserType($get_column(1));
        $customer->setFirstname($get_column(2));
        $customer->setSurname($get_column(3));
        $customer->setMail($get_column(4));
        $customer->setPassword($get_column(5));
        $customer->setPhone($get_column(6));
        $customer->setIp($get_column(7));
        $customer->setSince($get_column(8));
        $customer->setLastConnection($get_column(9));
        $customer->setBirthday($get_column(10));

        return $customer;
    }

    public function to_array(){
        return array_combine(DBCustomerStructure::getColumns(), array($this->getId(),
            $this->getUserType(),
            $this->getFirstname(),
            $this->getSurname(),
            $this->getMail(),
            $this->getPassword(),
            $this->getPhone(),
            $this->getIp(),
            $this->getSince(),
            $this->getLastConnection(),
            $this->getBirthday()));
    }

    public static function array_difference(Customer $customer1, Customer $customer2){
        $customer1_matrix = $customer1->to_array();
        $customer2_matrix = $customer2->to_array();

        $sql_update_sets_matrix = [];

        foreach($customer1_matrix as $key => $value){
            $customer2_matrix_value = $customer2_matrix[$key];

            if ($value != $customer2_matrix_value){
                $sql_update_sets_matrix[$key] = $customer2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }
}