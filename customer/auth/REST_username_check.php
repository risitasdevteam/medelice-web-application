<?php
/**
 * LR : 16/7/18
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../User.class.php');
include(__DIR__.'/../../includes/data_handler.php');

try{
  post_thrown();

  $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, true);

  $exist = User::user_exist($mail);
  $response = new JsonResponse(JsonResponseStatusType::OK, null);
  $response->add_payload(new JsonResponsePayload("exist", $exist));

  $response->transmit();
}
catch(Exception $e){
    //var_dump($e);
  exception_to_json_payload($e);
}
