<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/10/2017
 * Time: 11:48
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../User.class.php');

try {
    User::disconnect();
    redirect('index.php?page=home');
}
catch(Exception $e){
    exception_to_json_payload($e);
}