$(document).ready(function() {
  $("#login-form").submit(function (event) {
    event.preventDefault();

    CustomerUtil.login(serializeForm($(this)), function(response){
        if (typeof response.payload.redirect !== 'undefined') {
            if (response.payload.redirect !== 'contact'){
                var url = "/index.php?page=" + response.payload.redirect;
                if (response.payload.redirect.localeCompare('passer-commande') == 0) {
                    url += "&passer_commande_token=" + response.payload.passer_commande_token;
                    url += "&passer_commande_token_hash=" + response.payload.passer_commande_token_hash;
                }

                location.href = url;
            }else{
                CustomerUtil.refresh_topbar(true);
            }
        }
        else
            location.href = "/index.php?page=moi";
    }, function(response){

    });

  })});
