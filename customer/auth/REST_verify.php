<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 26/08/2018
 * Time: 12:18
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . "/../User.class.php");
require_once(__DIR__.'/../misc/UserType.enum.php');
include(__DIR__.'/../../includes/data_handler.php');
require_once(__DIR__.'/../misc/UserVerificationIssue.php');

try{
    $type = GatewayType::verify;

    $verification_key = RegexUtil::filter_in_var_with_regex(INPUT_GET, "vkey", RegexUtil::sha256());
    $user_id = RegexUtil::filter_in_var_with_regex(INPUT_GET, "uid", RegexUtil::uid());

    $provider = new DBCustomerProvider();
    if (!$provider->verify($verification_key, $user_id)){
        throw new MedeliceException("Impossible de vérifier l'utilisateur");
    }

    redirect("/index.php?page=verification&issue=".UserVerificationIssue::SUCCESS."&uid=".$user_id);
}
catch (UserAccountVerificationLinkExpiredException $e){
    redirect("/index.php?page=verification&issue=".UserVerificationIssue::TIME_ERROR);
}
catch (Exception $e){
    redirect("/index.php?page=verification&issue=".UserVerificationIssue::ERROR);
}
