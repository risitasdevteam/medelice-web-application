<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 16/08/2018
 * Time: 13:31
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
include(__DIR__.'/../misc/UserUtil.php');

try{
    get_thrown();

    $check_time_for_checkout = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, "check_time", FILTER_VALIDATE_BOOLEAN);

    $connected = UserUtil::isLogged();
    if ($connected){
        if ($check_time_for_checkout && UserUtil::get_logged_at() < (time() - 30*60)){
            $payload = new JsonResponsePayload("connected", false);
        }else{
            $payload = new JsonResponsePayload("connected", true);
        }
    }else{
        $payload = new JsonResponsePayload("connected", false);
    }

    (new JsonResponse(JsonResponseStatusType::OK, $payload))->transmit();
}catch(Exception $e){
    exception_to_json_payload($e);
}