<?php
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../User.class.php');
include(__DIR__.'/../../includes/data_handler.php');

try{
  post_thrown();
    
  $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'mail', FILTER_SANITIZE_EMAIL, true);
  $password = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'password', FILTER_SANITIZE_STRING, true);
  $redirect = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'redirect', FILTER_SANITIZE_STRING);

  //$remember = isset($_POST['remember']); TODO
  $result = User::login($mail, $password, false); //TODO

  if ($result){
    $response = new JsonResponse(JsonResponseStatusType::OK, null);
    if ($redirect != null && $redirect != false)
    {
      $payload = new JsonResponsePayload("redirect", $redirect);
      $response->add_payload($payload);

      if (strcmp($redirect, 'passer-commande') == 0){
        $token = filter_input(INPUT_POST, 'passer_commande_token', FILTER_SANITIZE_STRING);
        $token_hash = filter_input(INPUT_POST, 'passer_commande_token_hash', FILTER_SANITIZE_STRING);

        $token_payload = new JsonResponsePayload("passer_commande_token", $token);
        $token_hash_payload = new JsonResponsePayload("passer_commande_token_hash", $token_hash);

        $response->add_payload($token_payload);
        $response->add_payload($token_hash_payload);
      }
    }
    $response->add_payload(new JsonResponsePayload("msg", "Connecté avec succès !"));
    $response->transmit();
  }else{
      throw new MedeliceException("Une erreur est survenue lors de la connexion");
  }
}
catch(Exception $e){
    exception_to_json_payload($e);
}
