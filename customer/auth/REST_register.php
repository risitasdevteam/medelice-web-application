<?php
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../../utility.inc.php');
require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../../exceptions/utilisateur/UserRegisterMissingMandatoryData.php');

try{
    post_thrown();
    //basic customer args
    $mail = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL, true);
    $password = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'password1', RegexUtil::password());
    $password_confirm = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'password2', RegexUtil::password());

    if (strcmp($password, $password_confirm) != 0){
        throw new MedeliceException("Les mots de passe ne sont pas égaux.");
    }

    $firstname = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'firstname', RegexUtil::name());
    $surname = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'surname', RegexUtil::name());

    $ip = RegexUtil::classic_filter_var_with_error_thrower($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP);

    //regex timestamp unix
    $birthday = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'birthday', RegexUtil::input_birth()); //time to unix nworking?
    $phone = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'phone', RegexUtil::international_phone());

    $redirect = RegexUtil::classic_filter_in_with_error_thrower(INPUT_GET, 'redirect', FILTER_SANITIZE_STRING); //TODO protection pages qui n'appartiennent pas à Medelice

      //CAPTCHA DEPT
      if (!DEBUG) {
          $secret = "6LdYEDAUAAAAALHv13xLDKNpg9qiHsxSO2Pc80UH";
          $response = $_POST['g-recaptcha-response'];

          $api_url = "https://www.google.com/recaptcha/api/siteverify?secret="
              . $secret
              . "&response=" . $response
              . "&remoteip=" . $ip;

          $decode = json_decode(file_get_contents($api_url), true);
          if ($decode['success'] != true) {
              throw new UserCaptchaException('Captcha non résolu');
          }
      }

    $provider = new DBCustomerProvider();
    $registered = $provider->register($mail, $password, $firstname, $surname, $ip, $phone, strtotime($birthday));
    if ($registered)
    {
        $response = new JsonResponse(JsonResponseStatusType::OK, null);
        $response->add_payload(new JsonResponsePayload("msg", "Vous êtes désormais inscrit ! Veuillez valider votre compte."));

        if ($redirect != false){
            $payload = new JsonResponsePayload("redirect", $redirect);
            $response->add_payload($payload);
        }

        $response->transmit();
    }else{
        throw new MedeliceException("Une erreur s'est produite");
    }
}
catch(Exception $e){
    exception_to_json_payload($e);
}
