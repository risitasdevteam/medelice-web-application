<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 09/09/2017
 * Time: 02:18
 */
require_once(__DIR__ . '/../../initializer.inc.php');
require_once(__DIR__ . '/../../exceptions/DatabaseException.php');
require_once(__DIR__ . '/../../exceptions/DatabaseConnectionException.php');
require_once(__DIR__ . '/../../exceptions/DatabaseInvalidQueryException.php');

class UserSession
{
    public $_session_death = 3600; //1h (s)
    public $_session = array();
    private $_db;

    public function __construct(){
    }

    public function open(){
        $ok = true;
        $db = new mysqli(DB_ADDRESS, DB_USER, DB_PASSWORD, "medelice_users");
        if ($db->connect_error){
            $db = false;
        }

        $this->_db = $db;
        $this->gc(); //nettoyage vieilles sessions
        return $ok;
    }

    public function close(){
        if ($this->_db == null || !$this->_db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        return $this->_db->close();
    }

    public function read($id)
    {
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli) { //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $data = null;
        $query = 'select s_data from ' . USERS_SESSIONS_TABLE . ' where s_id = ?';
        if ($stmt = $db->prepare($query)) {
            $stmt->bind_param('s', $id);
            $stmt->execute();

            $stmt->bind_result($data);

            $stmt->fetch();
            $stmt->close();
        } else {
            throw new DatabaseInvalidQueryException('');
        }

        return stripslashes($data);
    }

    public function write($id, $data){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $data_expiration = (time() + $this->_session_death);
        $data = $db->real_escape_string($data); //+id, si id changé depuis cookie injection possible?


        $insert = 'replace into '.USERS_SESSIONS_TABLE.' values(?,?,?)';
        if ($stmt = $db->prepare($insert)){
            $stmt->bind_param('ssd',
                $id,
                $data,
                $data_expiration);

            $stmt->execute();
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }

        //debug_print_backtrace();
        return true;
    }

    public function destroy($id){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $delete = 'delete from '.USERS_SESSIONS_TABLE.' where s_id = ?';
        if ($stmt = $db->prepare($delete)){
            $stmt->bind_param('s', $id);
            $stmt->execute();
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }
        return true;
    }

    public function gc(){
        $db = $this->_db;
        if ($db == null || !$db instanceof mysqli){ //integrity check
            //throw
            throw new DatabaseConnectionException('');
        }

        $delete = 'delete from '.USERS_SESSIONS_TABLE.' where s_death < unix_timestamp(now())';
        if ($stmt = $db->prepare($delete)){
            $stmt->execute();
            $stmt->close();
        }else{
            throw new DatabaseInvalidQueryException('');
        }
        return true;
    }
}

ini_set('session.save_handler', 'user');
$session = new UserSession();

session_set_save_handler(array($session, 'open'),
    array($session, 'close'),
    array($session, 'read'),
    array($session, 'write'),
    array($session, 'destroy'),
    array($session, 'gc'));
//header('Authorization: Bearer x');
session_start();