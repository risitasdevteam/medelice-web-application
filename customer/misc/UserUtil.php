<?php

require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../misc/UserType.enum.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 28/10/2017
 * Time: 21:52
 */
class UserUtil
{
    public static function isLogged(){
        return $_SESSION['loggedIn'] === true;
    }

    public static function get_session_data(){
        $stored_key = ['loggedIn', 'mail', 'firstname', 'surname', 'remember', 'rank', 'loggedAt'];

        $session_data = [];
        foreach ($stored_key as $key){
            $session_data[$key] = $_SESSION[$key];
        }

        return $session_data;
    }

    public static function getUserType(){
        return isset($_SESSION['rank']) ? $_SESSION['rank'] : null;
    }

    public static function is_a_data_manager(){
        $rank = self::getUserType();
        return $rank !== null ? $rank < UserType::user : false;
    }

    public static function highest_privileges($rank){
        return $rank == UserType::administrator;
    }

    public static function get_mail(){
        return $_SESSION["mail"];
    }

    public static function get_logged_at(){
        return $_SESSION['loggedAt'];
    }
}