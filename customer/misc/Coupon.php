<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 10/03/2018
 * Time: 23:52
 */

require_once(__DIR__.'/../../utility/SQLUtil.php');
require_once(__DIR__.'/../../exceptions/MedeliceException.php');

class Coupon
{
    public $code; //(str)
    public $name; //str
    public $discount; //%
    private $birth; //date de début de validité (s)
    private $death; //date de fin de validité (s)
    private $mode; //"all" | "owner" (CouponMode) (str)
    private $capacity_left; //n -> -1

    private $exist = false;

    protected static $_allowed_variables_names = array('id', 'name', 'discount', 'birth', 'death', 'mode', 'capacity_left');

    public function __construct($array){
        foreach ($array as $key=>$value){
            if (in_array($key, self::$_allowed_variables_names)){
                switch($key){
                    case self::$_allowed_variables_names[0]:
                        $this->id = $value;
                        break;

                    case self::$_allowed_variables_names[1]:
                        $this->name = $value;
                        break;

                    case self::$_allowed_variables_names[2]:
                        $this->discount = $value;
                        break;

                    case self::$_allowed_variables_names[3]:
                        $this->birth = $value;
                        break;

                    case self::$_allowed_variables_names[4]:
                        $this->death = $value;
                        break;

                    case self::$_allowed_variables_names[5]:
                        $this->mode = $value;
                        break;

                    case self::$_allowed_variables_names[6]:
                        $this->capacity_left = $value;
                        break;
                }
            }else{
                throw new MedeliceException('Argument "'.$key.'" isn\'t allowed.');
            }
        }
    }

    public function exists($rebind = false){
        $where = array(); //we adjust research as much as there's completed variables, then we gain sql search time even if all the submitted columns are UNIQUE
        foreach (get_object_vars($this) as $variable => $variable_value){
            if ($variable_value != null) {
                $where[$variable] = $variable_value;
            }
        }

        $returned_data = SQLUtil::select(COUPONS_TABLE, ['*'], $where, 'limit 1');
        if (count($returned_data) > 0){
            if ($rebind){
                $pointer = $returned_data[0];

                $this->code = $pointer[self::$_allowed_variables_names[0]];
                $this->name = $pointer[self::$_allowed_variables_names[1]];
                $this->discount = $pointer[self::$_allowed_variables_names[2]];
                $this->birth = $pointer[self::$_allowed_variables_names[3]];
                $this->death = $pointer[self::$_allowed_variables_names[4]];
                $this->mode = $pointer[self::$_allowed_variables_names[5]];
                $this->capacity_left = $pointer[self::$_allowed_variables_names[6]];
            }
            $this->exist = true;
        }

        return $this->exist;
    }

    public static function create($coupon){
        if (!isset($coupon) || (!($coupon instanceof Coupon))){
            throw new MedeliceException('');
        }

        if (!isset($coupon->id) || empty($coupon->id)){
            $coupon->id = hash("crc32", openssl_random_pseudo_bytes(3));
        }else{
            if (preg_match('/[a-f0-9]{8}/', $coupon->code) != 1){
                throw new Exception('Unique id must respects CRC32 laws.');
            }
        }

        //if (!($coupon->code))
        SQLUtil::insert(COUPONS_TABLE,
            [
               'code' => $coupon->code,
                'name' => $coupon->name,
                'discount' => $coupon->discount,
                'birth' => $coupon->birth,
                'death' => $coupon->death,
                'mode' => $coupon->mode,
                'capacity_left' => $coupon->capacity_left
            ]);

        return true;
    }

    public static function decrement_capacity($id){
        $coupon = new Coupon([
            'id' => $id
        ]);

        if (!$coupon->exists(true)){
            return false;
        }

        $capacity_left = $coupon->capacity_left;
        if ($capacity_left < 0){
            return false;
        }

        SQLUtil::update(COUPONS_TABLE, ['capacity_left' => ($capacity_left -1)], ['id' => $id]);
        return true;
    }

    /**
     * @param $name
     */
    public static function apply_mode_own($name){
        if (!isset($name) || empty($name)){
            return false;
        }

        $coupon = new Coupon([
            'name' => $name,
            'mode' => 'own'
        ]);

        if (!$coupon->exists(true)) {
            return false;
        }

        if ($coupon->capacity_left < 1){
            return false;
        }

        return true;
    }

    /**
     *
     */
    public static function apply_mode_all(){

    }

    public static function delete(){

    }
}