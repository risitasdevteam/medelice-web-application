<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 06/08/2017
 * Time: 20:08
 */
abstract class UserType
{
    const moderator = -2;
    const administrator = -1;
    const user = 0;
    const user_not_verified = 1;
    const banned_user = 2;
}
