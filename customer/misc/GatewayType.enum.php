<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/08/2017
 * Time: 10:45
 */
abstract class GatewayType
{
    const verify = 0;
    const forgotten_password = 1;
    const blocked_by_login_attempts = 2;
    const forgotten_password_generated_by_admin = 3;
}
