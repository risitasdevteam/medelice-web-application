<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 29/08/2017
 * Time: 15:09
 */
abstract class UsersLoginAttempsIssues
{
    const SUCCEEDED = 0;
    const DISALLOWED = 1;
}
