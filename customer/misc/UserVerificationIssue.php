<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 26/08/2018
 * Time: 13:05
 */
abstract class UserVerificationIssue
{
    const SUCCESS = 0;
    const TIME_ERROR = 1;
    const ERROR = 2;
}