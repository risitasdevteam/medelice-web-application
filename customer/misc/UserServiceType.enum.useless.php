<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 08/09/2017
 * Time: 21:36
 */
class UserServiceType
{
    const LOGIN = 0;
    const REGISTRATION = 1;
    const USERNAME_CHECKIN = 2;
    const DISCONNECT = 3;
    //const CAPTCHA = 3;
}