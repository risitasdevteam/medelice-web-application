<?php

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 04/09/2017
 * Time: 23:38
 */
class UserGender
{
    const MALE = 0;
    const FEMALE = 1;
    const UNDEFINED = 2;
}