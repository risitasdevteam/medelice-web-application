<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 10/03/2018
 * Time: 23:59
 */

namespace customer\misc;


abstract class CouponMode
{
    const universal = "all";
    const owner = "owner";
}