<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/08/2018
 * Time: 22:52
 */
require_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../../utility/Mail.php');
require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../misc/UserUtil.php');
include_once(__DIR__.'/../../includes/data_handler.php');

try{
    include(__DIR__.'/../../includes/need_to_be_logged_throw.php');

    $customer_mail = UserUtil::get_mail();
    $subject = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "subject", FILTER_SANITIZE_STRING, true);
    $content = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, "content", FILTER_SANITIZE_STRING, true);

    $mail = new Mail();
    $mail_sender = new MailSender();
    $customer_provider = new DBCustomerProvider();
    $mail_configuration = new ContactClientMailConfiguration();

    $customer = $customer_provider->get_on_email($customer_mail);
    if ($customer === null){
        throw new MedeliceException("Customer is null");
    }

    $mail->setMailConfiguration($mail_configuration);
    $mail->setTo($mail_configuration->getUsername());
    $mail->setSubject("Formulaire contact, client n°".$customer->getId().' | '.$subject);
    $mail->setBody($content);

    $mail_sender->send($mail, array(), array(), true);

    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Message envoyé avec succès !")))->transmit();
}catch(Exception $e){
    exception_to_json_payload($e);
}