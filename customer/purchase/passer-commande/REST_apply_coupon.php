<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 18/03/2018
 * Time: 13:38
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../../misc/Coupon.php');
require_once(__DIR__.'/../../../utility/JsonResponse.class.php');
require_once(__DIR__.'/../CartUtil.php');

try
{
    post_thrown();

    $coupon_name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);
    if (!isset($coupon_name) || !$coupon_name){
        throw new MedeliceException("Une erreur a eu lieu lors de la saisie du coupon");
    }

    $result = CartUtil::apply_coupon($coupon_name);

    if ($result) {
        $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Coupon " . $coupon_name . " ajouté avec succès. Mise à jour du panier."));
        $response->transmit();
    }else{
        throw new MedeliceException("Le coupon " . $coupon_name . " n'existe pas.");
    }
}
catch (Exception $e)
{
    exception_to_json_payload($e);
}