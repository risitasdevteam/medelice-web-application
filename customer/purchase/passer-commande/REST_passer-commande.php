<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/10/2017
 * Time: 16:15
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__ . '/../../../orders/Order.class.php');
require_once(__DIR__.'/../../../utility/RegexUtil.php');
require_once(__DIR__.'/../../../exceptions/utilisateur/UserInvalidTokenException.php');
require_once(__DIR__.'/../../../utility.inc.php');
require_once(__DIR__.'/../../security/UserToken.class.php');
require_once(__DIR__.'/../../shipping/OrderCustomerAddresses.php');
require_once(__DIR__.'/../../shipping/Address.php');
require_once(__DIR__.'/../../misc/UserUtil.php');

try {
    include(__DIR__.'/../../../includes/need_to_be_logged_throw.php');

    $token_prefix = 'passer_commande';

    if (!UserToken::can_access('/index.php', 'page=passer-commande', $token_prefix)){
        throw new UserInvalidTokenException('La page demandée a expirée, aucune action n\'a été prise en compte.');
    }

    $customer_provider = new DBCustomerProvider();
    $customer = $customer_provider->get_on_email(UserUtil::get_mail());
    if ($customer === null){
        throw new MedeliceException('Une erreur inconnue a été générée');
    }

    $billing_address_signature = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'billing_address_signature', FILTER_SANITIZE_STRING, true);
    $delivery_address_signature = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'delivery_address_signature', FILTER_SANITIZE_STRING, true);
    $delivery_mode = RegexUtil::filter_in_var_with_regex(INPUT_POST, 'delivery_mode', RegexUtil::delivery_mode());

    $stripe_token = filter_input(INPUT_POST, 'stripe_source', FILTER_SANITIZE_STRING);

    if (strlen($stripe_token) != 28){
        throw new MedeliceException('Stripe token length error');
    }

    if (!isset($_POST["three_d_secure"])){
        throw new MedeliceException('Une erreur avec le système de 3D secure a été détectée.');
    }

    $three_d_secure = toBool($_POST["three_d_secure"]);
    $generated_order_id = Order::generateUniqueOrderId();
    $bill_array = CartUtil::bill();

    if (!isset($bill_array)){
        throw new MedeliceException('Impossible de générer la facture. Veuillez rééssayer.');
    }

    if (!CartUtil::verifyLockHash()){
        throw new MedeliceException('Le contenu du panier a été changé, redirection.'); //redirection
    }

    CartUtil::unlock();

    $total_bill = $bill_array['total'];

    \Stripe\Stripe::setApiKey(STRIPE_PVTKEY);
    $response = new JsonResponse(JsonResponseStatusType::OK, null);

    if ($three_d_secure){
        $return_url = 'https://'. Environment::get_server_name().
            '/3dsecure_loading.php?'.
            'uid='. $customer->getId().
            '&oid='. $generated_order_id;

        $source = \Stripe\Source::create(array(
            "amount" => round($total_bill * 100, 2),
            "currency" => "eur",
            "type" => "three_d_secure",
            "three_d_secure" => array(
                "card" => $stripe_token,
            ),
            "redirect" => array(
                "return_url" => $return_url
            ),
        ), array(
            "idempotency_key" => uniqid(),
        ));

        $source_array = $source->jsonSerialize();

        if (!isset($source_array["redirect"]["url"])){
            throw new MedeliceException("Invalid operation");
        }

        $three_d_secure_url = $source_array["redirect"]["url"];
        $response->add_payload(new JsonResponsePayload("three_d_sec_redirect", $three_d_secure_url));
        $response->add_payload(new JsonResponsePayload("id", $generated_order_id));
        $response->add_payload(new JsonResponsePayload("msg", "Demande d'autorisation envoyée à la banque, redirection en cours..."));

    }else{
        $charge = \Stripe\Charge::create(array(
            "amount" => round($total_bill * 100, 2),
            "currency" => "eur",
            "source" => $stripe_token,
            "description" => "Commande MED-" . $generated_order_id,
            "receipt_email" => $customer->getMail(),
            'statement_descriptor' => 'Médélice'
        ), array(
            "idempotency_key" => uniqid(),
        ));

        $response->add_payload(new JsonResponsePayload("order_id", $generated_order_id));
        $response->add_payload(new JsonResponsePayload("msg", "Commande passée avec succès ! Vous allez recevoir un mail de confirmation d'ici quelques minutes."));
    }

    $order_addresses = new OrderCustomerAddresses();
    $order_addresses->billing = AddressManager::get_address_from_signature($billing_address_signature);
    $order_addresses->delivery = AddressManager::get_address_from_signature($delivery_address_signature);

    $product_provider = new DBProductProvider();
    $product_provider->decrement_stock_for_specific_order(CartUtil::contents());

    $order = new OrderNew();
    $order->setId($generated_order_id);
    $order->setUserId($customer->getId());
    $order->setProducts(CartUtil::encodeCartAsJSON());
    $order->setDeliveryMode($delivery_mode);
    $order->setStatus(($three_d_secure ? OrderStatus::PENDING_3D_SECURE : OrderStatus::RECEIVED));
    $order->setStatusLastUpdate(time());
    $order->setAccountTime(time());
    $order->setTotalBill($total_bill);

    $customer_provider = new DBCustomerProvider();
    $customer_provider->send_billing_mail($order, $order_addresses, $customer->getMail());

    $order->setCustomerAddresses($order_addresses->encode_to_json());

    $order_provider = new DBOrderProvider();
    $order_provider->insert($order);

    CartUtil::destroy();

    $response->transmit();
}
catch (\Stripe\Error\Card $e){
    $body = $e->getJsonBody();
    $err = $body['error'];
    $code = $err['code'];

    exception_to_json_payload(new MedeliceException($code));

    switch ($code){
        case 'invalid_number':
            break;
        case 'invalid_expiry_month':
            break;
        case 'invalid_expiry_year':
            break;
        case 'invalid_cvc':
            break;
        case 'invalid_swipe_data':
            break;
        case 'incorrect_number':
            break;
        case 'expired_card':
            break;
        case 'incorrect_cvc':
            break;
        case 'incorrect_zip':
            break;
        case 'card_declined':
            break;
        case 'missing':
            break;
        case 'processing_error':
            break;
    }
}
catch (\Stripe\Error\RateLimit $e){
    exception_to_json_payload(new MedeliceException($e->getMessage()));
}
catch (\Stripe\Error\InvalidRequest $e){
    exception_to_json_payload(new MedeliceException($e->getMessage()));
}
catch (\Stripe\Error\Authentication $e){
    exception_to_json_payload(new MedeliceException($e->getMessage()));
}
catch (\Stripe\Error\ApiConnection $e){
    exception_to_json_payload(new MedeliceException($e->getMessage()));
}
catch (Exception $e){
    exception_to_json_payload($e);
}