<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 26/04/2018
 * Time: 19:07
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/../../../exceptions/MedeliceException.php');
require_once(__DIR__.'/../../../orders/Order.class.php');
require_once(__DIR__.'/../../../orders/OrderStatus.enum.php');
require_once(__DIR__.'/../../../utility/RegexUtil.php');
require_once(__DIR__.'/../../../utility.inc.php');

try {
    post_thrown();

    $customer_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "customer_id", RegexUtil::uid());
    $order_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "order_id", RegexUtil::order_uid());
    $stripe_source_token = RegexUtil::filter_in_var_with_regex(INPUT_POST, "stripe_source_token", RegexUtil::stripe_source_token());
    $stripe_source_token_secret = RegexUtil::filter_in_var_with_regex(INPUT_POST, "stripe_source_token_secret", RegexUtil::stripe_source_secret_token());

    $order_provider = new DBOrderProvider();
    $order = $order_provider->get_on_unique_id($order_id);
    if ($order === null){
        throw new MedeliceException("Order null");
    }

    $order->setStatus(OrderStatus::RECEIVED);
    $order->setStatusLastUpdate(time());

    \Stripe\Stripe::setApiKey(STRIPE_PVTKEY);
    $source = \Stripe\Source::retrieve($stripe_source_token);

    $charge = \Stripe\Charge::create(array(
        "amount" => $source->__get('amount'),
        "currency" => "eur",
        "source" => $stripe_source_token,
        "description" => "Commande MED-" . $order_id
    ));

    $order_provider->update($order_id, $order);

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", "Commande n°".$order_id. " passée avec succès, redirection en cours..."));
    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}
