<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/09/2017
 * Time: 18:50
 */

require_once(__DIR__ . '/../../../utility.inc.php');
//require_once(__DIR__ . '/../../shipping/CustomerAddresses.class.old.php');
require_once(__DIR__ . '/../../User.class.php');
require_once(__DIR__ . '/../../security/UserToken.class.php');
require_once(__DIR__ . '/../CartUtil.php');

//TODO token via panier.php

switch ($_SERVER['REQUEST_METHOD']){
    case 'GET':
        include(__DIR__ . '/topsection.passer-commande.php');
        break;
    case 'POST':
        include(__DIR__ . '/REST_passer-commande.php');
        break;
    default:
        exit('Matrix error');
        break;
}