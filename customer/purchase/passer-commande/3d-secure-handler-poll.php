<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 31/07/2018
 * Time: 21:58
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

?>

<div class="3d-secure-source" style="width: 100%; height: 100%; display: flex; align-items: center; justify-content: center;">
    <div>
        <img src="/images/loading.gif" id="loading-img" style="margin: 0 50% 2vw; transform: translateX(-50%);">
        <div style="font-size: 1.4vw; font-family: Arial, sans-serif;" id="msg">Veuillez patienter pendant que nous recevons la réponse de votre banque...</div>
    </div>
</div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
<script src="/dashboard/js/RESTResponseProvider.js"></script>
<script src="/js/Order.js"></script>
<script src="/js/medelice.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script>
    var stripe = Stripe('pk_test_fU9boDkbPby6C65LonQgUA1A');

    var MAX_POLL_COUNT = 10;
    var pollCount = 0;
    //TODO
    function pollForSourceStatus() {
        var url = new URL(location.href);
        var order_id = url.searchParams.get("oid");
        var customer_id = url.searchParams.get("uid");
        var stripe_source_token = url.searchParams.get("source");
        var stripe_source_secret_token = url.searchParams.get("client_secret");

        var $loading_img =  $("#loading-img");

        stripe.retrieveSource({id: stripe_source_token, client_secret: stripe_source_secret_token}).then(function(result) {
            var source = result.source;
            var $msg = $('#msg');

            if (source.status === 'chargeable') {
                OrderUtil.three_d_secure_final_checkout(customer_id, order_id, stripe_source_token, stripe_source_secret_token, function(response){
                    $loading_img.css("display", "none");
                    $msg.text("Succès ! Redirection en cours ...");
                    setTimeout(function(){
                        location.href = "/index.php?page=thankyou&order_id=" + order_id;
                    }, 3500);
                }, function(response){
                    $loading_img.css("display", "none");
                });
            } else if (source.status === 'pending' && pollCount < MAX_POLL_COUNT) {
                pollCount += 1;
                setTimeout(pollForSourceStatus, 1000);
            }
            else{
                $loading_img.css("display", "none");
                switch (source.status){
                    case "consumed":
                        $msg.text("Échec, commande déjà passée, redirection en cours ...");
                        redirect_error(order_id);
                        break;

                    case "canceled":
                        $msg.text("Commande annulée, redirection en cours ...");
                        redirect_error(order_id);
                        break;

                    case "failed":
                        $msg.text("Échec, commande annulée, redirection en cours ...");
                        redirect_error(order_id);
                        break;

                    case "error":
                        $msg.text("Échec, commande annulée, redirection en cours ...");
                        redirect_error(order_id);
                        break;

                    case "succeeded":
                        $msg.text("Succès ! Redirection en cours ...");
                        break;
                }
            }
        });
    }

    function redirect_error(order_id){
        OrderUtil.cancelled(order_id, function(){
            setTimeout(function(){
                location.href = "/index.php";
            }, 3500);
        }, function(){
        });
    }

    pollForSourceStatus();
</script>
<div id="alert-container">
</div>