/**
 * Created by jsanglier on 17/03/2018.
 */

function checkout_basic_handler(){
    function patch_numerically_masked_inputs() {
        var default_mask_regex = ["99999", "99 99 99 99 99"];
        var default_mask_options = {placeholder: '', autoclear: false};
        var numeric_masked_inputs = ['del_zip', 'bill_zip', 'del_phone'];

        for (var i = 0; i < numeric_masked_inputs.length; i++) {
            $("[name='" + numeric_masked_inputs[i] + "']").mask((i < 2 ? default_mask_regex[0] : default_mask_regex[1]), default_mask_options);
        }
    }
    patch_numerically_masked_inputs();
}

function checkout_stripe_handler(){
    var stripe = Stripe('pk_test_fU9boDkbPby6C65LonQgUA1A');
    var elements = stripe.elements();
    var card = elements.create('card');
    var form = $('form[id="checkout"]');
    var create_source_result = "";
    var regular_payment = false;

    function stripe_card_handler() {
        card.mount(".stripe-card-data-fields-container");

        card.addEventListener('change', function (event) {
            if (event.error) {
                error_notification(event.error.message);
                $('.stripe-pay-button').attr("disabled", "");
            }
            if (event.complete){
                create_source(card);

                $('.stripe-pay-button').removeAttr("disabled");
            }
        });
    }

    function create_source(card){
        stripe.createSource(card).then(function(result){
            if (result.error) {
                error_notification(event.error.message);
            } else {
                var three_d_secure = result.source.card.three_d_secure;

                if (three_d_secure == "not_supported" || three_d_secure == "optional"){
                    regular_payment = true;
                }

                var card_last4 = result.source.card.last4;
                var card_brand = result.source.card.brand;
                var card_exp_month = result.source.card.exp_month;
                var card_exp_year = result.source.card.exp_year;

                var $card_brand = $('#card_brand');
                var $card_end = $('#card_end');
                var $card_exp = $('#card_exp');

                $card_brand.text(card_brand);
                $card_end.text(card_last4);
                $card_exp.text(card_exp_month + " / " + card_exp_year);

                create_source_result = result.source;
            }
        });
    }

    function stripe_form_submit_handler() {
        form.keyup(function(event){
            if (event.key == "Enter"){
                event.preventDefault();
            }
        });

        form.submit(function(event) {
            event.preventDefault();

            var token = get_url_parameters()["passer_commande_token"];
            var token_hash = get_url_parameters()["passer_commande_token_hash"];
            var billing_address_signature = $('[name="billing_signature"]').val();
            var delivery_address_signature = $('[name="delivery_signature"]').val();
            var delivery_mode = "normal";
            var stripe_source = create_source_result.id;

            showLoadingScreen();

            OrderUtil.checkout(token, token_hash, billing_address_signature, delivery_address_signature, delivery_mode, stripe_source, !regular_payment, function(response){
                if (typeof response.payload.three_d_sec_redirect !== 'undefined') {
                    location.href = response.payload.three_d_sec_redirect;
                }

                var order_id = response.payload.order_id;

                hideLoadingScreen();

                setTimeout(function(){
                    location.href = "/index.php?page=thankyou&order_id=" + order_id;
                }, 3500);

            }, function(response){
                hideLoadingScreen();
            });
        });
    }

    stripe_card_handler();
    stripe_form_submit_handler();
}

function coupon_button_handler(){
    var button_selector = $('[name="checkout_apply_coupon_submit"]');

    button_selector.click(function(e){
        var coupon_name_container_selector = $('[name="sales_coupon"]');

        var coupon_name_container_value = coupon_name_container_selector.val();
        if (coupon_name_container_value.length == 0){
            return false;
        }

        apply_coupon(coupon_name_container_value);
    });
}

function apply_coupon(name){
    var url = "/customer/purchase/passer-commande/REST_apply_coupon.php";
    var data = {'name' : name};

    post(url, response, function(response){
        notification("info", response.payload.msg);
        refresh_cart();

        //disable container, you can only add once a coupon
        var container = $('.coupon_container');

    }, function(response){
    }, true);
}

function get_shipping_taxes(mass){
    var url = "/customer/shipping/shipping_duty.post.php";
    var data = {'mass': mass};

    return $.post(url, response,
        function(response)
        {
            return response.payload.duty;
        },
        function(){ //Error
            return "-1";
        }
    );
}

function refresh_cart(){
    var payment_summary_products_price_selector = $('[id="payment_summary_products_price"]');
    var payment_summary_shipping_price_selector = $('[id="payment_summary_shipping_price"]');
    var payment_summary_total_price_selector = $('[id="payment_summary_total_price"]');
    var payment_summary_coupon_discount_selector = $('[id="payment_summary_coupon_discount"]');

    get_bill(function(bill){
        var products_price = bill['price'];
        var shipping_price = bill['taxes'];
        var total = bill['total'];
        var coupon_discount = bill['discount'];

        payment_summary_products_price_selector.text(products_price.formatMoney(2, '€', '', ','));
        payment_summary_shipping_price_selector.text(shipping_price.formatMoney(2, '€', '', ','));
        payment_summary_total_price_selector.text(total.formatMoney(2, '€', '', ','));
        payment_summary_coupon_discount_selector.text("-" + coupon_discount.formatMoney(2, '€', '', ','));
    });
}

refresh_cart();
checkout_basic_handler();
checkout_stripe_handler();
coupon_button_handler();