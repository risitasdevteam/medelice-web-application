<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 27/08/2018
 * Time: 01:36
 */
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

include(__DIR__."/../../../includes/data_handler.php");
include(__DIR__."/../../../includes/orders.php");

try{
    $order_id = RegexUtil::filter_in_var_with_regex(INPUT_POST, "order_id", RegexUtil::order_uid());

    $order_provider = new DBOrderProvider();
    $order_provider->remove($order_id);

    (new JsonResponse(JsonResponseStatusType::OK, null))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}