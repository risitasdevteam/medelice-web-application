<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 22/07/2018
 * Time: 11:38
 */

?>

<div class="payment-container payment-bluecard-container" id="bluecard-container" style="display: none;">
    <div class="payment-stripe-container">
        <div class="payment-section-title quicksand"><i class="fa fa-credit-card payment-section-title-icon"></i>Paiement</div>
        <div class="stripe-section-container">
            <div class="stripe-container">
                <div class="stripe-pay-title quicksand bold">Payer par carte bancaire</div>
                <div class="stripe-card-data-fields-container"></div>
                <button class="stripe-pay-button" type="button" onclick="toConfirmFormForward()" disabled>
                    Continuer
                </button>
            </div>
        </div>
    </div>
    <button class="payment-next-button" type="button" style="margin-left: 29.7vw; background-color: #b7b7b7" onclick="toAddressesFormBackward()">< PRÉCÉDENT</button>
</div>
