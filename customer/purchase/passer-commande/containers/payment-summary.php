<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 22/07/2018
 * Time: 11:40
 */
require_once(__DIR__ . '/../../../../produits/Product.php');
require_once(__DIR__.'/../../../../produits/Valisette.php');
?>

<div class="payment-container payment-confirm-container" id="payment-summary" style="display: none;">
    <div class="payment-section-title quicksand"><i class="fa fa-bars payment-section-title-icon"></i>Résumé de votre commande</div>
    <div class="payment-summary-container">
        <div class="payment-summary-informations-section">
            <div class="payment-summary-addresses-container">
                <div class="payment-summary-informations-container payment-summary-billing">
                    <div class="payment-summary-informations-title">Facturation</div><br>
                    <p id="confirmation_billing_emitter_name"></p>
                    <p id="confirmation_billing_address"></p>
                    <p id="confirmation_billing_address2"></p>
                    <p id="confirmation_billing_zip_n_city"></p>
                    <p id="confirmation_billing_country"></p>
                </div>
                <div class="payment-summary-informations-container payment-summary-delivery" style="">
                    <div class="payment-summary-informations-title">Livraison</div><br>
                    <p id="confirmation_delivery_emitter_name"></p>
                    <p id="confirmation_delivery_address"></p>
                    <p id="confirmation_delivery_address2"></p>
                    <p id="confirmation_delivery_zip_n_city"></p>
                    <p id="confirmation_delivery_country"></p>
                </div>
            </div>
            <div class="payment-summary-delivery-mode">
                <input type="radio" id="delivery_express" name="delivery_mode" value="express" class="payment-summary-delivery-mode-radio-selector" style="margin-bottom: 0.5vw;" disabled>
                <label for="delivery_express" style="text-decoration: line-through;">Livraison express <span class="payment-summary-delivery-mode-radio-selector-description">- vous serez livré.e sous 72h</span></label> Indisponible actuellement<br>
                <input type="radio" id="delivery_normal" name="delivery_mode" value="normal" class="payment-summary-delivery-mode-radio-selector" checked>
                <label for="delivery_normal">Livraison normale <span class="payment-summary-delivery-mode-radio-selector-description">- vous serez livré.e sous 7 jours</span></label><br>
                <br>
                <script type="application/javascript">
                    $('[name="delivery_mode"').click(function(){
                        var mode = $(this).val();
                        var delivery_date = new Date();

                        delivery_date.setDate(delivery_date.getDate() + (mode.localeCompare("express") ? 3 : 7));
                    });
                </script>
                Votre commande sera livrée entre le <?php
                    $current_week_day = date("N");

                    $now = strtotime("now");
                    $one_week = strtotime("+1 weeks");

                    if ($current_week_day > 5){
                        $now += 86400*(8 - $current_week_day);
                        $one_week += 86400*(8 - $current_week_day);
                    }

                    echo strftime("%d %B", $now). " et le ". strftime("%d %B", $one_week)
                ?>
            </div>
            <div class="payment-summary-cardinfo">
                <div style="margin-bottom: 0.4vw">Mode de paiement :</div>
                <span id="card_brand"></span> finissant par <span id="card_end"></span> et expirant le <span id="card_exp"></span>
            </div>
        </div>
        <div class="payment-summary-pay-section">
            <button class="payment-summary-confirm-and-pay-button quicksand bold" type="submit">CONFIRMER ET PASSER MA COMMANDE</button><br>
            <div class="payment-summary-price-container">
                <p class="payment-summary-price-title quicksand bold" style="">Récapitulatif de commande :</p>
                <div class="payment-summary-price-details">
                    <p class="payment-summary-price-details-entry">Articles<span class="payment-summary-price-details-entry-price" id="payment_summary_products_price"></span></p>
                    <p class="payment-summary-price-details-entry">Livraison<span class="payment-summary-price-details-entry-price" id="payment_summary_shipping_price"></span></p>
                    <p class="payment-summary-price-details-entry">Total<span class="payment-summary-price-details-entry-price" id="payment_summary_total_price"></span></p>
                    <p class="payment-summary-price-details-entry">Bon de réduction<span class="payment-summary-price-details-entry-price" id="payment_summary_coupon_discount"></span></p>
                    <hr class="payment-summary-price-details-separator">
                    <p class="payment-summary-price-details-final-price">Montant total :<span class="payment-summary-price-details-entry-price" id="payment_summary_total_price"></span></p>
                </div>
                <hr class="payment-summary-price-container-coupon-separator">
                <div class="coupon_container">
                    <p class="payment-summary-coupon-title">Coupon de réduction :</p>
                    <input type="text" class="payment-summary-enter-coupon quicksand" name="sales_coupon"/>
                    <button class="payment-summary-add-coupon-button quicksand" type="button" name="checkout_apply_coupon_submit"><i style="font-size: initial" class="fa fa-plus"></i></button>
                </div>
                <hr class="payment-summary-price-container-show-articles-separator">

                <script>
                    function showArticles()
                    {
                        $('.payment-summary-myarticles-section').css(
                            {
                                'display':'block'
                            }
                        );
                    }

                    function hideArticles()
                    {
                        $('.payment-summary-myarticles-section').css(
                            {
                                'display':'none'
                            }
                        );
                    }
                </script>

                <button type="button" class="payment-summary-show-articles-button quicksand bold" onclick="showArticles()">MES ARTICLES</button>

                <div class="payment-summary-myarticles-section">
                    <div class="payment-summary-myarticles">
                        <i class="payment-sumart-close fa fa-times fa-2x" onclick="hideArticles()"></i>
                        <div style="height: 0.0000001%"></div>
                        <div class="payment-sumart-topgradient"></div>
                        <div class="payment-summary-myarticles-display-container">

                            <?php

                            require_once(__DIR__ . '/../../CartUtil.php');
                            $bill = CartUtil::bill();

                            if($bill != null)
                            {
                                $products = $bill["products"];
                                //var_dump($products);
                                $products_count = count($products);
                                for($i = 0; $i < $products_count; $i++)
                                {
                                    $ref = $products[$i]['ref']; //product reference
                                    $quantity = $products[$i]['quantity']; //product quantity
                                    $isbox = CartUtil::is_a_valisette($ref);

                                    $product = $isbox ? new Valisette($ref) : (new DBProductProvider($ref))->get($ref);
                                    if ($product === null){
                                        throw new MedeliceException("The specified product doesn't exists");
                                    }

                                    if($product instanceof Product)
                                    {
                                        echo '
                                                        <div class="payment-summary-myarticles-element">
                                                            <div class="paysum-art-picture">
                                                                <img class="fill-image" src="'.(new FileIOProductProvider())->get_images($product->getRef())[0].'">
                                                            </div>
                                                            <div class="paysum-art-info quicksand">
                                                                <span class="paysum-art-info-name quicksand bold">'.$product->getName().'</span><br>
                                                                <i>'.$product->getBrand().'</i><br>
                                                                <div style="height: 0.5vw"></div>
                                                                <span>'.$product->getPrice().'€, '.$product->getTotalMass().$product->getQuantityUnity().' / unité</span><br>
                                                                <div style="height: 1vw"></div>
                                                                <span style="font-size: 1.2vw">Quantité : '.$quantity.'</span><br>
                                                                <span class="paysum-art-price colored-font roboto">Total produit : '.($quantity * $product->getPrice()).'€</span>
                                                            </div>
                                                        </div><br>
                                                    ';
                                    }
                                    else if($product instanceof Valisette) {
                                        require_once(__DIR__ . '/../../../../produits/Regions.class.php');
                                        $sub_box = $product->get_specific_sub_valisette_from_id(explode(Valisette::SUB_VALISETTE_DELIMITER, $ref)[1]);
                                        if ($sub_box instanceof SubValisette) {
                                            //var_dump($product);
                                            echo '
                                                        <div class="payment-summary-myarticles-element">
                                                            <div class="paysum-art-picture">
                                                                <img class="fill-image" src="' . $product->image(). '">
                                                            </div>
                                                            <div class="paysum-art-info quicksand">
                                                                <span class="paysum-art-info-name quicksand bold">La valisette ' . Regions::getRegionNameFromId($product->region) . '</span><br>
                                                                <i>' . $sub_box->title . '</i><br>
                                                                <div style="height: 0.5vw"></div>
                                                                <span>' . $sub_box->price . '€ ' . ' / unité</span><br>
                                                                <div style="height: 1vw"></div>
                                                                <span style="font-size: 1.2vw">Quantité : ' . $quantity . '</span><br>
                                                                <span class="paysum-art-price colored-font abril">Total : ' . ($quantity * $sub_box->price) . '€</span>
                                                            </div>
                                                        </div><br>
                                                    ';
                                        }
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="payment-sumart-bottomgradient"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <button class="payment-next-button" style="margin-left: 16.383vw; margin-top: 0.6vw; background-color: #b7b7b7" type="button" onclick="toPaymentFormBackward()">< PRÉCÉDENT</button>
</div>
