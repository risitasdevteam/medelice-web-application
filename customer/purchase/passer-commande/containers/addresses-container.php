<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 22/07/2018
 * Time: 11:37
 */

require_once (__DIR__ . '/../../../shipping/Address.php');
require_once(__DIR__.'/../../../shipping/AddressManager.php');

include(__DIR__.'/../../../../includes/need_to_be_logged_throw.php');

$addresses = AddressManager::get_addresses_from_user_id((new User($_SESSION['mail']))->id) ?? array();
AddressManager::range_addresses($addresses);

$has_at_least_one_address = count($addresses) > 0;
?>

<div class="payment-container" id="addresses-container">
    <div class="payment-section-title quicksand"><i class="fa fa-truck payment-section-title-icon"></i>Livraison et facturation</div>
    <div class="payment-addresses-container">
        <div class="payment-billing-address-container">
            <div class="payment-section-subtitle quicksand bold">Informations de facturation</div>
            <div class="payment-address-container quicksand">
                <?php
                $address = '<div class="payment-address-top">
                        <span class="left"><b id="{form_name}_emitter_name">{emitter_name}</b><span id="{form_name}_phone">, ({phone})</span></span>
                        <button type="button" class="payment-address-switch-button button right quicksand bold" onclick="showAddSwitchForm(\'{form_name}\');"><i class="fa fa-exchange" style="margin-right: 0.5vw"></i>Changer d\'adresse</button>
                    </div>
                    <div class="payment-address-bottom">
                        <div class="bold" id="{form_name}_addr1_city_zip_country">{address1}, {city}, ({zip}), {country}</div>
                        <div class="payment-address-bottom-sub" id="{form_name}_addr2">{address2}</div>
                    </div>
                    <input type="hidden" name="{form_name}_signature">';

                $patterns = array('/{emitter_name}/', '/{phone}/', '/{address1}/', '/{address2}/', '/{city}/', '/{zip}/', '/{country}/', '/{form_name}/');

                $get_replacements = function($form_name) use ($has_at_least_one_address, $addresses){
                    $first_address = $addresses[0];
                    if (!$first_address instanceof Address){
                        throw new MedeliceException("");
                    }

                    $replacements = array($first_address->emitter_name,
                        $first_address->phone,
                        $first_address->address1,
                        strlen($first_address->address2) > 0 ? $first_address->address2 : "Aucune indication particulière",
                        $first_address->city,
                        $first_address->zip,
                        $first_address->country,
                        $form_name);

                    return $replacements;
                };

                if ($has_at_least_one_address)
                    echo preg_replace($patterns, $get_replacements("billing"), $address);
                else
                    echo "Veuillez ajouter une adresse.";
                ?>
            </div>
        </div>
        <div class="payment-delivery-address-container">
            <div class="payment-section-subtitle quicksand bold">Informations de livraison</div>
            <div class="payment-address-container quicksand">
                <?php
                if ($has_at_least_one_address)
                    echo preg_replace($patterns, $get_replacements("delivery"), $address);
                else
                    echo "Veuillez ajouter une adresse.";
                ?>
            </div>
            <input type="hidden" name="delivery_signature">
        </div>
        <div class="payment-address-switch-form-container overlay-container">
            <div class="payment-address-switch-form">
                <div class="payment-address-switch-form-title quicksand bold">
                    <span class="left">Choisir une autre adresse</span>
                    <i class="fa fa-close right button" onclick="hideAddSwitchForm()"></i>
                </div>
                <input type="hidden" id="addr_switch_mode">
                <button class="payment-add-address-button button quicksand bold" onclick="add_new_address_button();">
                    <i class="fa fa-plus" style="margin-right: 0.5vw"></i>Ajouter une adresse
                </button>
                <div class="payment-address-switch-form-list">
                    <?php
                        if (!$has_at_least_one_address > 0)
                        {
                            echo '
                            <div class="myaddresses-noaddresses bold">
                                <div class="myaddresses-noaddresses-text">Aucune adresse n\'est enregistrée pour le moment !</div>
                            </div>
                            ';
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php
            include (__DIR__ . '/../../../account/my_addresses.modform.inc.php');
        ?>
        <script>
            function showAddSwitchForm(addr_type)
            {
                $('.payment-address-switch-form-container').css(
                    {
                        "display":"flex"
                    }
                );

                $('#addr_switch_mode').val(addr_type);
            }

            function hideAddSwitchForm()
            {
                $('.payment-address-switch-form-container').css(
                    {
                        "display":"none"
                    }
                );
            }

            function switchPaymentAddress(addr_hash)
            {
                var addr_type = $('#addr_switch_mode').val();

                var $phone = $('#' + addr_type + "_phone");
                var $emitter_name = $('#' + addr_type + "_emitter_name");
                var $main_line = $('#' + addr_type + '_addr1_city_zip_country');
                var $addr2 = $('#' + addr_type + '_addr2');
                var $signature = $('[name="' + addr_type + '_signature"]');

                var $switched_address_container = $('#addr_' + addr_hash + "_content");
                var switch_address_emitter_name = $switched_address_container.find('[name="addr_emitter_name"]');
                var switch_address_phone = $switched_address_container.find('[name="addr_phone"]');
                var switch_address_address1 = $switched_address_container.find('[name="addr_address1"]');
                var switch_address_address2 = $switched_address_container.find('[name="addr_address2"]');
                var switch_address_zip = $switched_address_container.find('[name="addr_zip"]');
                var switch_address_city = $switched_address_container.find('[name="addr_city"]');
                var switch_address_state = $switched_address_container.find('[name="addr_state"]');
                var switch_address_country = $switched_address_container.find('[name="addr_country"]');

                var confirmation_emitter_name = $('#confirmation_' + addr_type + '_emitter_name');
                var confirmation_address = $('#confirmation_' + addr_type + '_address');
                var confirmation_address2 = $('#confirmation_'+ addr_type + '_address2');
                var confirmation_zip_and_city = $('#confirmation_'+ addr_type +'_zip_n_city');
                var confirmation_country = $('#confirmation_' + addr_type + '_country');

                $phone.text(", (" + switch_address_phone.text() + ")");
                $emitter_name.text(switch_address_emitter_name.text());
                $main_line.text(switch_address_address1.text() + ", " + switch_address_city.text() + ", " + switch_address_zip.text() + ", " + switch_address_country.text());
                $addr2.text(switch_address_address2.text());
                $signature.val(addr_hash);

                confirmation_emitter_name.text(switch_address_emitter_name.text());
                confirmation_address.text(switch_address_address1.text());
                confirmation_address2.text(switch_address_address2.text());
                confirmation_zip_and_city.text(switch_address_city.text() + ", " + switch_address_zip.text());
                confirmation_country.text(switch_address_country.text());

                hideAddSwitchForm();
            }

            function retrieve_addresses(callback){
                var $list = $('.payment-address-switch-form-list');

                CustomerUtil.get_addresses(function(response){
                    if (Object.keys(response.payload.addresses).length > 0){

                        $.each(response.payload.addresses, function(index, item){

                            var signature = item["signature"];
                            var emitter_name = item["emitter_name"];
                            var phone = item["phone"];
                            var address1 = item["address1"];
                            var address2 = item["address2"];
                            var zip = item["zip"];
                            var city = item["city"];
                            var state = item["state"];
                            var country = item["country"];
                            var weight = item["weight"];

                                $list.append('<div class="myaddresses-address-header" style="width: 95%; margin-left: 2.5%;" onclick="'+

                            'var select = $(\'#addr_'  + signature + '_content\');'+
                            'var arrow = $(\'#arrow_'+ signature + '\');'+


                            'function toggleSub_addr' + signature + '()'+
                            '{'+
                                'var max_height = parseFloat(select.css(\'max-height\').replace(\'px\', \'\'));'+
                                'if (max_height > 0)'+
                                '{'+
                                    'select.css('+
                                        '{'+
                                                    '\'max-height\':\'0\''+
                                '}'+
                            ');'+

                                'arrow.removeClass(\'fa-angle-up\');'+
                                'arrow.addClass(\'fa-angle-down\');'+
                                'arrow.css({\'height\':\'auto\'});'+
                            '}'+
                            'else'+
                            '{'+
                                'select.css('+
                                    '{'+
                                                    '\'max-height\': \'14vw\''+
                            '}'+
                            ');'+

                            'arrow.removeClass(\'fa-angle-down\');'+
                            'arrow.addClass(\'fa-angle-up\');'+
                            'arrow.css({\'height\':\'80%\'});'+
                        '}'+
                    '}'+

                    'toggleSub_addr' + signature + '();'+

                    '">'+
                    '<div class="myaddresses-address-header-text roboto">'+
                         address1  + ', ' + zip + ' ' + city +
                    '</div>'+
                    '<div class="myaddresses-address-header-arrow">'+
                        '<i class="fa fa-angle-down fa-4x" id="arrow_' + signature + '"></i>'+
                        '</div>'+
                        '</div>'+
                        '<div class="myaddresses-address-details-container" style="width: 94%; margin-left: 3%;" id="addr_' + signature + '_content">'+
                        '<input type="hidden" id="addr_weight" value="'+ weight +'">' +
                        '<div class="myaddresses-address-details">'+
                        '<div class="myaddresses-address-details-section" style="margin-bottom: 1vw">'+
                        '<span class="myaddresses-address-details-bigtext roboto left" name="addr_emitter_name" id="addr_emitter_name">' + emitter_name + '</span>'+
                        '<span class="myaddresses-address-details-bigtext roboto right" name="addr_phone" id="addr_phone">' + phone + '</span>'+
                        '</div>'+
                        '<div class="myaddresses-address-details-section">'+
                        '<span class="myaddresses-address-details-bigtext roboto left" name="addr_address1" id="addr_address1">' + address1 + '</span>'+
                        '<div class="myaddresses-address-details-bigtext roboto right">'+
                        '<span name="addr_city" id="addr_city">' + city + '</span><span name="addr_zip" id="addr_zip"> (' + zip + ')</span>'+
                    '</div>'+
                    '</div>'+
                    '<div class="myaddresses-address-details-section">'+
                        '<span class="myaddresses-address-details-smalltext roboto left" name="addr_address2" id="addr_address2">' + (address2.length > 0 ? address2 : 'Aucune indication particulière') + '</span>'+
                    '<span class="myaddresses-address-details-smalltext roboto right" name="addr_state" id="addr_state">' + state + '</span>'+
                        '</div>'+
                        '<div class="myaddresses-address-details-country roboto" name="addr_country" id="addr_country">'+
                         country +
                        '</div>'+

                        '<div class="myaddresses-address-details-modrem-container">'+
                        '<button class="myaddresses-address-details-modrem-button quicksand bold" type="button" onclick="removeAddress(\'' + signature + '\');">' +
                        '<i class="fa fa-times myaddresses-address-details-modrem-icon"></i>' +
                        'Supprimer' +
                        '</button>'+
                        '<button class="myaddresses-address-details-modrem-button quicksand bold" onclick="showModForm(\'' + signature + '\')">'+
                        '<i class="fa fa-pencil myaddresses-address-details-modrem-icon"></i>'+
                        'Modifier'+
                        '</button>'+
                        '<button class="myaddresses-address-details-modrem-button quicksand bold" type="button" onclick="switchPaymentAddress(\'' + signature + '\');">'+
                        '<i class="fa fa-check myaddresses-address-details-modrem-icon"></i>'+
                        'Sélectionner'+
                        '</button>'+
                        '</div>'+
                        '</div>'+
                        '</div>');
                        });

                        if (typeof callback !== "undefined")
                            callback.call(self);
                    }
                }, function(response){

                });
            }

            <?php
                if ($has_at_least_one_address){
                    echo '$(document).ready(function(){
                        retrieve_addresses(function(){
                             $(\'#addr_switch_mode\').val("billing");
                             switchPaymentAddress(\''.$addresses[0]->signature.'\');
                            $(\'#addr_switch_mode\').val("delivery");
                             switchPaymentAddress(\''.$addresses[0]->signature.'\');
                        });
                    });';
                }else{
                    echo '$(document).ready(function(){
                        window.setInterval(function(){
                         $(".myaddresses-address-header").remove();
                         $(".myaddresses-noaddresses").remove();
                         retrieve_addresses();
                        }, 5000);
                    });';
                }
            ?>
        </script>
    </div>
    <button class="payment-next-button" type="button" onclick="toPaymentFormForward()" <?php echo !$has_at_least_one_address ? 'disabled' : '' ?>>CONTINUER ></button>
</div>