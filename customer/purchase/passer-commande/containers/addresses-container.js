/**
 * Created by jsanglier on 23/07/2018.
 */

function address_fields(){
    return ["emitter_name", "phone", "address1", "address2", "city", "zip", "state", "country"];
}

function choose_address(element, is_billing){
    var prefix = (is_billing ? "billing" : "delivery");
    address_fields().forEach(function(index, item) {
        $('[id="' + prefix + '_' + item + '"]').val(element.find("#mod_form_" + item).val());
    });
}

function get_address(is_billing){
    address = [];
    var prefix = (is_billing ? "billing" : "delivery");
    address_fields().forEach(function(index, item) {
        address.push($('[id="' + prefix + '_' + item + '"]').val());
    });
    return address;
}

function get_billing_address(){
    return get_address(true);
}

function get_delivery_address(){
    return get_address(false);
}