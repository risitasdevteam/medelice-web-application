<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/10/2017
 * Time: 16:08
 */


include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../../security/UserToken.class.php');

$token_prefix = 'passer-commande';
$bill = CartUtil::bill();
$price = $bill['price'];

$integrity_keys = UserToken::generate_token_and_hash($token_prefix, 60*10);

if (CartUtil::isempty()) {
    redirect('/index.php');
    exit;
}

include(__DIR__.'/../../../includes/need_to_be_logged.php');

CartUtil::lock();
?>

<script src="/js/Order.js"></script>
<script src="https://js.stripe.com/v3/"></script>
<script src="/js/checkoutFx.js"></script>
<div id="topsection" style="overflow: hidden">
    <form class="payment-form" id="checkout" method="post">
        <?php
            //UserTok¨en::insert_token_and_hash_in_current_page($token_prefix, $integrity_keys['token'], $integrity_keys['hash']);
            include(__DIR__ . '/containers/addresses-container.php');
            include(__DIR__ . '/containers/stripe-payment.php');
            include(__DIR__ . '/containers/payment-summary.php');
        ?>
    </form>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/customer/purchase/passer-commande/checkout.js"></script>

    <script type="application/javascript">
        disableSidebarOrderLink("sidebar-payment");
        disableSidebarOrderLink("sidebar-confirm");
    <?php
    // just for when you have &sub=subpage, to go directly to the right place
        if(isset($_GET['sub']))
        {
            $sub = $_GET['sub'];
            switch ($sub)
            {
                case 'delivery':
                    break;

                case 'payment':
                    echo '                         
                            toPaymentFormForward();
                            enableSidebarOrderLink("sidebar-payment");
                         ';
                    break;

                case 'confirm':
                    echo '
                            toPaymentFormForward();
                            toConfirmFormForward();
                            enableSidebarOrderLink("sidebar-payment");
                            enableSidebarOrderLink("sidebar-confirm");
                         ';
                    break;

                default:
                    break;
            }
        }
    ?>
    </script>
</div>