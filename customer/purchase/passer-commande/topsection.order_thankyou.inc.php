<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 26/08/2018
 * Time: 23:51
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once (__DIR__ . '/../../misc/UserUtil.php');

if(!isset($_GET['order_id']) || !UserUtil::isLogged())
{
    redirect("/index.php?page=home");
}

$ref = $_GET['order_id'];

?>

<div id="topsection" class="thankyou-container">
    <div class="thankyou-box">
        <b><?php echo $_SESSION['firstname']; ?>, merci d'avoir commandé chez Médélice !</b><br>
        <br>
        Un email de confirmation de commande récapitulant le contenu de celle-ci et le mode de livraison sélectionné a été envoyé à <?php echo $_SESSION['mail']; ?>.<br>
        <br>
        Pour consulter les détails sur votre commande, rendez-vous <a href="/index.php?page=commande&ref=<?php echo $ref; ?>" class="link">ici</a>.<br>
        <br>
        Référence commande : <?php echo $ref; ?>
    </div>
</div>


