<?php
include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__ . '/../../../utility/JsonResponse.class.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 30/10/2017
 * Time: 15:34
 */

try {
    $gift_message = filter_input(INPUT_POST, 'gift_message', FILTER_SANITIZE_STRING);
    if ($gift_message == false) {
        throw new DataBadException('');
    }

    $_SESSION['panier-gift_message'] = $gift_message;

    $response = new JsonResponse(JsonResponseStatusType::OK, null);
    $payload = new JsonResponsePayload("ok", 'Message personnalisé ajouté.');
    $response->add_payload($payload);

    $response->transmit();
}
catch (DataBadException $e){
    $response = new JsonResponse(JsonResponseStatusType::ERROR, new JsonResponsePayload("error", $e->getMessage()));
    $response = $response->encode();
    echo $response;
}