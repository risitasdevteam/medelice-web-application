<div class="cart-bottom-container quicksand bold">
    <div class="cart-bottom-link">
        <i class="cart-bottom-icon fa fa-truck" style="font-size: 2vw" aria-hidden="true"></i><br>
        <span>livraison à la carte</span>
    </div>
    <div class="cart-bottom-link">
        <i class="cart-bottom-icon fa fa-lock" style="font-size: 2vw" aria-hidden="true"></i><br>
        <span>paiement sécurisé</span><br>
        <span class="cart-bottom-subtext medium">cartes bancaires</span>
    </div>
    <a class="cart-bottom-link cart-bottom-link-clickable" href="/index.php?page=generic&id=boutique"> <?php /* TODO onclick -> ouvrir mappy et hop là */ ?>
        <i class="cart-bottom-icon fa fa-map-marker" style="font-size: 2vw" aria-hidden="true"></i><br>
        <span>notre boutique</span>
    </a>
    <div class="cart-bottom-link" style="border: none">
        <i class="cart-bottom-icon fa fa-comments" style="font-size: 2vw" aria-hidden="true"></i><br>
        <span>service client</span><br>
        <span class="cart-bottom-subtext medium">du lundi au vendredi au</span><br>
        <span>0 805 46 32 32</span><br>
        <span class="cart-bottom-subsubtext medium">(appel gratuit depuis un poste fixe)</span>
    </div>
</div>

<script>
    $("#page-container").css(
        {
            "margin-top": "0"
        }
    );
</script>