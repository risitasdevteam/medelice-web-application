/**
 * Created by jsanglier on 30/10/2017.
 */

$(document).ready(function(){
    $('.cart-additional-data').submit(function(e){
        e.preventDefault();

        var data = {
            'gift_message': $('input[name=gift_message]').val()
        };

        $.ajax({
            type: 'POST',
            url: '/customer/purchase/REST_gift.php',
            response: response,
            dataType: 'json',
            encode: true
        })

        .done(function(data){
                console.log(response);
            switch (response.status){
                case 'ok':
                    notification(NotificationType.INFO, response.payload.ok);
                    break;

                case 'error':
                    notification("warning", response.payload.error);
                    break;
            }
        });
    })
});
