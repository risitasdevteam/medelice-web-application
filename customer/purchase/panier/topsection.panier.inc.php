<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 15/11/2017
 * Time: 09:54
 */

include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__ . '/../../../produits/Valisette.php');
require_once(__DIR__ . '/../../../produits/Regions.class.php');

$token_prefix = 'passer_commande';
$integrity_keys = UserToken::generate_token_and_hash($token_prefix, 60*5);

?>


<script src="/js/Cart.js"></script>
<div id="topsection">
    <div id="topsection-texttop">
        <div class="bromello colored-font" style="float: right; font-size: 3vw; margin-right: 8%">
            <?php echo L::checkout_mycart_header; ?>
        </div>
        <div id="pattern-header" style="background-image: url('/images/accueil.png'); float: left; background-position-y: 26%;"></div>
    </div>
    <div id="topsection-imgzone">
        <div style="min-height: 35vw; width: 100%">
        <?php
            require_once (__DIR__ . '/../CartUtil.php');
            $bill = CartUtil::bill();
            if (CartUtil::isLocked()){
                CartUtil::unlock();
            }

            if($bill == null)
            {
                echo '<div style="margin-top: 16vw; font-size: 3vw; font-weight: bold; width: 100%; vertical-align: middle; text-align: center; color: #2a2a2a"><i>Panier vide</i></div>';
            }
            else
            {
                echo '
                    <table class="cart-table quicksand">
                        <tr class="quicksand" style="text-transform: uppercase; font-size: 1vw">
                            <th style="width: 16%">'.L::checkout_cart_product.'</th>
                            <th style="width: 34%; text-align: left">'.L::checkout_cart_desc.'</th>
                            <th style="width: 11%">'.L::checkout_cart_unique_price.'</th>
                            <th style="width: 14%">'.L::checkout_cart_quantity.'</th>
                            <th style="width: 15%">'.L::checkout_cart_total_price.'</th>
                            <th style="width: 10%">'.L::checkout_cart_remove.'</th>
                        </tr>
                ';

                $products = $bill['products']; //order products
                $mass = $bill['mass']; //order total mass
                $shipping_taxes = ShippingUtil::estimate_shipping_price($mass); //duty according to the mass
                $fullcart_price = ($bill["total"]); //total price : products + shipping duty
                $count = count($products);
                for ($i = 0; $i < $count; $i++) {
                    $reference = $products[$i]['ref']; //product reference
                    $unity_price = $products[$i]['unity_price']; //product unity price
                    $quantity = $products[$i]['quantity']; //product quantity

                    $product_isbox = CartUtil::is_a_valisette($reference); //check if it's a product or a valisette

                    $product = null;
                    $product_img = null;
                    $product_name = null;
                    $product_link = null; // link to see the individual product

                    if ($product_isbox) //fuck u
                    {
                        $truncated_ref = explode(Valisette::SUB_VALISETTE_DELIMITER, $reference);
                        $non_var_ref = $truncated_ref[0];
                        $var_ref = $truncated_ref[1];

                        $box = new Valisette($non_var_ref); // declare a box group instance
                        $product = $box->get_specific_sub_valisette_from_id($var_ref); // assign $product to sub-box
                        if (!$product instanceof SubValisette){

                        }

                        $product_img = $box->image();
                        $product_name = $product->title;
                        $product_link = "/index.php?page=valisette&ref=".$non_var_ref;
                    }
                    else
                    {
                        $product = (new DBProductProvider())->get($reference);
                        if ($product === null){
                            throw new MedeliceException("The specified product doesn't exists");
                        }

                        $product_img = (new FileIOProductProvider())->get_images($product->getRef())[0];
                        $product_name = $product->getName();
                        $product_link = "/index.php?page=voir&ref=".$reference;
                    }


                    if ($product == null)
                    {
                        continue; //technical error
                    }

                    $price_formatter = new NumberFormatter($i18n->getAppliedLang(), NumberFormatter::CURRENCY );
                    $price = $price_formatter->formatCurrency($unity_price, "EUR");
                    $total_price = $price_formatter->formatCurrency($unity_price * $quantity, "EUR");

                    echo '
                        <tr style="height: 11.6vw">
                            <td class="cart-table-row">
                                <a href="'.$product_link.'" class="fill-image-container" style="height: 100%; width: 100%">
                                    <img class="fill-image" style="cursor: pointer" src="'.$product_img.'">
                                </a>
                            </td>
                            <td class="cart-table-row">
                                <a href="'.$product_link.'" class="cart-table-product-name bold">'.$product_name.'</a>'.
                                (!$product_isbox ? '<br><span class="cart-table-product-desc">'.$product->getBrand().'</span>' : '') .'
                            </td>
                            <td class="cart-table-row">
                                <div class="cart-table-product-price bold montserrat">'.$price.'</div>
                            </td>
                            <td class="cart-table-row">
                                <form class="cart-table-quantity-container">
                                    <button class="cart-table-quantity-btn quicksand" name="remove" type="submit"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                    <input class="cart-table-quantity-field quicksand bold" name="quantity" min="0" id="cart-quant" value="'.$quantity.'" pattern="^[0-9]{1,}$"> <!-- TODO définir un maximum ? + marquer quantité actuelle + si changé, update la variable de session -->
                                    <button class="cart-table-quantity-btn quicksand" name="add" type="submit"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                    <input type="hidden" name="ref" id="product-ref" value="'.$reference.'">
                                </form>
                            </td>
                            <td class="cart-table-row price">
                                <div class="cart-table-product-total-price bold montserrat">'.$total_price.'</div>
                            </td>
                            <td class="cart-table-row">
                                <form class="cart-table-remove-container">
                                    <button class="cart-table-remove-btn" id="cart-remove" type="submit"><i class="fa fa-times" style="color: #FFFFFF" aria-hidden="true"></i></button>
                                </form>
                            </td>
                        </tr>
                    ';
                }

                $price_formatter = new NumberFormatter($i18n->getAppliedLang(), NumberFormatter::CURRENCY);
                $f_price = $price_formatter->formatCurrency($bill['price'], "EUR"); // formatted
                $f_taxes = $price_formatter->formatCurrency($shipping_taxes, "EUR");

                echo '
                    </table>
                    <div class="cart-price-bill-container">
                        <div class="cart-price-bill-title-container montserrat">
                            <span class="cart-price-bill-prices-text bold" id="cart-products-price">'.$f_price.'</span><br>
                            <div class="cart-price-bill-container-separator" ></div>
                            <span class="cart-price-bill-prices-text bold" id="cart-duty-price">'.$f_taxes.'</span>
                        </div>
                        <div class="cart-price-bill-prices-container montserrat">
                            <span class="cart-price-bill-title-text">TOTAL ARTICLES</span><br>
                            <div class="cart-price-bill-container-separator" style="width: 100%; float: right"></div>
                            <span class="cart-price-bill-title-text">FRAIS DE PORT</span>
                        </div>
                    </div>
                    <div class="cart-price-total-container">
                        <div class="cart-price-total-price bold" id="cart-total-price">'.$price_formatter->formatCurrency($fullcart_price, "EUR").'</div>
                        <div class="cart-price-total-title bold">TOTAL</div>
                    </div>
                    <div class="cart-buy-container">
                        <div class="cart-continue-shopping-container">
                            <a class="button-link cart-continue-shopping-button quicksand medium" href="/index.php?page=home">
                                <span class="button-link-text">continuer mes achats</span>
                            </a>
                        </div>
                        <form class="cart-buy-form">
                            <div class="cart-buy-form-cgu">
                                <input type="checkbox" id="accept-cgu" name="accept-cgu" class="cgu-checkbox">
                                <label for="accept-cgu" class="cart-buy-form-cgu-text quicksand medium">J\'accepte les <a style="text-decoration: underline; color: #a0782f" target="_blank" href="/index.php?page=generic&id=cgv">conditions générales de vente</a></label>
                            </div><br>
                            <button type="submit" id="submit-cart" class="cart-buy-form-button quicksand medium"><i style="padding-right: 3%" class="fa fa-check" aria-hidden="true"></i>valider mon panier</button><br>
                            <div class="cart-buy-form-secure quicksand bold"><i class="fa fa-lock cart-buy-form-secure-icon" aria-hidden="true"></i>Paiement 100% sécurisé</div>
                        </form>
                    </div>
                ';
            }

        ?>
        </div>
    </div>
</div>

<script src="/js/jquery.maskedinput.min.js"></script>
<script>
    $(document).ready(function(){ //TODO crappy crappy
        $(".cart-buy-form").submit(function(e){
            e.preventDefault();

            if (!($("#accept-cgu").is(':checked'))){
                error_notification("Vous devez accepter les CGV !");
                return false;
            }

            CustomerUtil.is_connected_for_checkout(function(response){
                if (!response.payload.connected){
                    warnLoginFormBeforeBuy('<?php echo $integrity_keys["token"] ?>', '<?php echo $integrity_keys["hash"] ?>');
                }else{
                    location.href = "/index.php?page=passer-commande&token=<?php echo $integrity_keys["token"] ?>&hash=<?php echo $integrity_keys["hash"] ?>";
                }
            });
        });

        var buttons = ["remove", "add"];
        var submitted_button = "";
        var plus_or_minus = false;

        //buttons checker
        $(':input[type="submit"]').click(function(){
            var id = $(this).attr("name");

            if (id === buttons[0] || id === buttons[1]) {
                submitted_button = id;
                plus_or_minus = true;
            }
        });

        //when a product remove cross is clicked
        $('.cart-table-remove-container').submit(function(e) {
            e.preventDefault();

            var product_structure = $($(this).parent().parent());
            var product_ref = $($($(this).parent().parent()).find('.cart-table-row > .cart-table-quantity-container')).find('#product-ref');

            product_structure.css(
                {
                    "margin-left":"100%"
                }
            );

            setTimeout(
                function () {
                    product_structure.css(
                        {
                            "display":"none"
                        }
                    )
                },250
            );

            setTimeout(function () {
                var post_data = {
                    'ref' : product_ref.val()
                };

                var ref = product_ref.val();
                CartUtil.delete(ref, function(response){
                    product_structure.remove();
                    updateCart();
                    location.href = "/index.php?page=panier";
                }, function(response){

                });
            },600);
        });

        //when a quantifer (+/-) is clicked
        $('.cart-table-quantity-container').submit(function(e){
            e.preventDefault();

            if (plus_or_minus) {
                var formSelector = $(this);
                var quantitySelector = $(this).children("#cart-quant");
                var quantity = quantitySelector.val();
                var reference = $(this).children(':input[name="ref"]').val();


                if (submitted_button == buttons[0] && quantity < 2){
                    $('.cart-table-remove-container').trigger("submit");
                    return false;
                }

                CartUtil.insert(reference, (submitted_button == buttons[0] ? -1 : 1), function(){
                    var bill = getBill(function(response){
                        if (response == null){
                            return false;
                        }
                        var products_from_bill = response['products'];
                        updateProductFromBill(formSelector, quantitySelector, products_from_bill, reference);
                        updateCart();
                    });
                });
            }
        });

        //why not making unique id? because i can
        var timeout = null;
        var cart_quant_selector = $('[id=cart-quant]');
        //cart_quant_selector.mask("99999",  {placeholder: '', autoclear: false});
        cart_quant_selector.keyup(function (e) {
            if (timeout != null) {
                clearTimeout(timeout);
            }
            timeout = setTimeout(function () {
                timeout = null;

                var quantitySelector = $(e.target);
                var formSelector = $(quantitySelector.parent());
                var content = quantitySelector.val();
                var pattern = /^[0-9]{1,}$/;

                if (!pattern.test(content)){
                    return false;
                }

                var filtered_quantity = parseInt(content);
                var product_reference = $(quantitySelector.parent()).find('#product-ref').val();
                updateQuantityForXRef(formSelector, quantitySelector, product_reference, filtered_quantity);
            }, 500)});

        function updateQuantityForXRef(formSelector, quantitySelector, reference, quantity){
            CartUtil.set(reference, quantity, function(response){
                var bill = getBill(function(response){
                    if (response == null){
                        return false;
                    }

                    var products_from_bill = response['products'];
                    updateProductFromBill(formSelector, quantitySelector, products_from_bill, reference);
                    updateCart();
                });
            });
        }

        function editTotalPrice(price){
            $('#cart-total-price').text(formatWithCurrency(price));
        }

        function editProductsPrice(price){
            $('#cart-products-price').text(formatWithCurrency(price));
        }

        function editDutyPrice(mass){
            var post_data = {
                'mass': parseInt(mass)
            };

            $.post('/customer/shipping/shipping_duty.post.php', post_data, function(data){
                var json = JSON.parse(data);
                var payload = json.payload;
                var duty = payload.duty;

                $('#cart-duty-price').text(formatWithCurrency(parseFloat(duty)));
            });
        }

        function editProductFactor(selector, quantity){
            selector.val(quantity);
        }

        function editProductPriceFactor(selector, response){
            selector.parent().parent()[0].cells[4].children[0].innerHTML = response;
        }

        function updateProductFromBill(selector, quantitySelector, products, reference){
            products.forEach(function (item) {
                var ref = item.ref;
                if (ref == reference) {
                    var quantity = item.quantity;
                    var unity_price = item.unity_price;
                    editProductFactor(quantitySelector, quantity);
                    editProductPriceFactor(selector, formatWithCurrency((quantity * unity_price)));
                    return true;
                }
            });
        }

        function getBill(callback){
            $.get('/customer/purchase/panier/REST_bill.php', function (response) {
                var x = JSON.parse(response);
                switch (x.status) {
                    case 'ok':
                        var payload = x.payload;
                        var price = payload.price;
                        var mass = payload.mass;
                        var products = payload.products;
                        var total = payload.total;

                        var array = {
                            'products' : products,
                            'price' : price,
                            'mass' : mass,
                            'total' : total
                        };

                        callback(array);
                        break;

                    case 'error':
                        return null;
                }
            }).done(function(){

            }).fail(function(){

            });
        }

        function updateCart(){
            getBill(function(bill){
                var price = bill['price'];
                var mass = bill['mass'];
                var products = bill['products'];
                var total = bill['total'];

                var quantity = 0;
                for (var i = 0; i < products.length; i++){
                    quantity += products[i].quantity;
                }

                resetCartCountTo(quantity);

                editProductsPrice(price);
                editTotalPrice(total);
                editDutyPrice(mass);
            });
        }

        function formatWithCurrency(real){
            return new Intl.NumberFormat("fr", {style: "currency", currency: "EUR", minimumFractionDigits: 2}).format(real);
        }
    });

    disableSidebarOrderLink("sidebar-auth");
    disableSidebarOrderLink("sidebar-delivery");
    disableSidebarOrderLink("sidebar-payment");
    disableSidebarOrderLink("sidebar-confirm");
</script>