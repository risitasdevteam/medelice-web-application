<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/11/2017
 * Time: 19:27
 */
//include_once(__DIR__."/../../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../CartUtil.php');
require_once(__DIR__.'/../../../utility/JsonResponse.class.php');

try{
    if (CartUtil::isEmpty()){
        throw new MedeliceException('Empty cart');
    }

    $bill = CartUtil::bill();
    $response = new JsonResponse(JsonResponseStatusType::OK, null);
    $response->add_payload(new JsonResponsePayload('price', $bill['price']));
    $response->add_payload(new JsonResponsePayload('products', $bill['products']));
    $response->add_payload(new JsonResponsePayload('mass', $bill['mass']));
    $response->add_payload(new JsonResponsePayload('taxes', $bill['taxes']));
    $response->add_payload(new JsonResponsePayload('discount', $bill['discount']));
    $response->add_payload(new JsonResponsePayload('total', $bill['total']));
    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}