<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 10/08/2017
 * Time: 15:23
 */


include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__.'/CartUtil.php');
require_once(__DIR__.'/../../includes/data_handler.php');

try {
    post_thrown();

    $ref = RegexUtil::classic_filter_in_with_error_thrower(INPUT_POST, 'ref', FILTER_SANITIZE_STRING, true);

    $quantity = filter_input(INPUT_POST, 'quant', FILTER_VALIDATE_INT);
    if ($quantity == null || $quantity == false) {
        $quantity = 1;
    }

    $type = filter_input(INPUT_POST, 'type', FILTER_SANITIZE_STRING);
    switch ($type) {
        case 'set':
            if (!CartUtil::set($ref, $quantity)){
                throw new Exception('');
            }
            break;

        default:
            if (!CartUtil::insert($ref, $quantity)) {
                throw new Exception('Erreur panier');
            }
            break;
    }

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("msg", ($quantity >= 1 ? L::notif_added_to_cart : "Panier mis à jour")));
    $response->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}