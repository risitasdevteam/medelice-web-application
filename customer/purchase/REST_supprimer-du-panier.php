<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/10/2017
 * Time: 22:23
 */

include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");

require_once(__DIR__ . '/../../initializer.inc.php');
require_once(__DIR__.'/CartUtil.php');
require_once(__DIR__.'/../../utility/JsonResponse.class.php');

/*if ($_SERVER['REQUEST_METHOD'] != 'POST'){
    header('Location: ../../index.php');
}*/

try {
    $ref = filter_input(INPUT_POST, 'ref', FILTER_SANITIZE_STRING);
    if ($ref == false){
        throw new Exception('ERROR');
    }

    $quantity = CartUtil::get_item($ref);
    if (!CartUtil::remove($ref)){
        throw new Exception('ERROR');
    }

    $response = new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("quantity", $quantity));
    $response = $response->encode();

    echo $response;
}
catch(Exception $e){
    $response = new JsonResponse(JsonResponseStatusType::ERROR, new JsonResponsePayload("error", $e->getMessage()));
    $response = $response->encode();
    echo $response;
}