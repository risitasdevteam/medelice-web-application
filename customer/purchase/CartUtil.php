<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 23/10/2017
 * Time: 18:46
 *
 * Utilitaires multiples pour la gestion du panier des clients.
 * Le panier est sauvegardé dans la session sous la forme d'un tableau :
 *  clé : référence du produit
 *  valeur : quantité
 */
require_once(__DIR__.'/../../produits/Valisette.php');
require_once(__DIR__ . '/../../produits/Product.php');
require_once(__DIR__ . '/../../customer/User.class.php');
require_once(__DIR__.'/../../customer/misc/Coupon.php');
require_once(__DIR__.'/../shipping/ShippingUtil.php');
include(__DIR__ . '/../../includes/data_handler.php');

const COUPON_MODE_OWN_SESSION_STORAGE_NAME = 'cart_coupon_own';
const VALISETTE_DRY_MASS = 355;
const VALISETTE_FROUFROU_DRY_MASS = 0;
class CartUtil
{
    /**
     * Check si une référence correspond à une valisette.
     * @return bool
     */
    public static function is_a_valisette($ref){
        return strcmp(substr($ref, 0, 2), "V_")  === 0 && strpos($ref, Valisette::SUB_VALISETTE_DELIMITER) !== false;
    }

    public static function is_alcohol($ref){
        return strcmp(substr($ref, 0, 2), "AL")  === 0;
    }

    public static function valisette_default_mass(){
        return  (VALISETTE_DRY_MASS + VALISETTE_FROUFROU_DRY_MASS);
    }

    //TODO le sys ne peut recevoir pour le moment que des produits à l'unité et non des boxes
    /**
     * Récupérer tout le contenu du panier.
     * @return null
     */
    public static function contents(){
        if (!isset($_SESSION['panier'])){
            return null;
        }

        return $_SESSION['panier'];
    }

    /**
     * Retourne true si le panier est vide.
     * @return bool
     */
    public static function isEmpty(){
        return count(self::contents()) == 0;
    }

    /**
     * Récupérer un item en particulier dans le panier.
     * @param $ref
     * @return null
     */
    public static function get_item($ref){
        if (!isset($_SESSION['panier'])){
            return null;
        }

        return $_SESSION['panier'][$ref];
    }

    /**
     * Récupérer le nombre total de produits.
     * @return int
     */
    public static function total_items(){
        if (!isset($_SESSION['panier'])){
            return 0;
        }

        $cart = $_SESSION['panier'];
        $sum = 0;

        foreach ($cart as $quantity){
            $sum += intval($quantity);
        }

        return $sum;
    }

    /**
     * Ajouter un produit au panier en fonction de la référence et de sa quantité (optionnelle).
     * @param $ref
     * @param $quantity int
     * @param $without_saving bool
     * @return bool Faux si le panier est bloqué par une commande.
     */
    public static function insert($ref, $quantity = 1, $without_saving = false){
        if ($quantity > 2000){ //need2becheked in the future for relative n
            return false;
        }

        if (!isset($_SESSION['panier'])){
            $_SESSION['panier'] = array();
        }

        $product_already_contained_in_cart = false;
        if (array_key_exists($ref, $_SESSION['panier'])) {
            $product_already_contained_in_cart = true;
        }

        if (!self::isLocked()) {
            if (self::is_a_valisette($ref)){
                $b = new Valisette($ref);
                if (!$b->exists){
                    throw new MedeliceException('Box doesn\'t exists');
                }

                $valisette_id = explode(Valisette::SUB_VALISETTE_DELIMITER, $ref)[1];
                $sub_valisette = $b->get_specific_sub_valisette_from_id($valisette_id);
                if (!$sub_valisette instanceof SubValisette){
                    throw new MedeliceException("");
                }

                $product_provider = new DBProductProvider();

                foreach ($sub_valisette->products as $sub_ref){

                    $product = $product_provider->get($sub_ref);

                    if ($product->getStock() === 0){
                        throw new MedeliceException("Malheureusement, certains produits contenus dans cette valisette ne sont plus en stock... Revenez dans quelques jours !");
                    }

                    if (($product->getStock() - $quantity) <= 0)
                    {
                        throw new MedeliceException("Vous ne pouvez pas commander autant de valisettes.");
                    }

                    if (($product->getStock() - $quantity) < 0 || $product_already_contained_in_cart && $product->getStock() - $_SESSION['panier'][$ref] <= 0 && $quantity >= 1)
                    {
                        throw new MedeliceException("Vous ne pouvez pas commander autant de valisettes.");
                    }
                }

            }else{
                $p = (new DBProductProvider)->get($ref);
                if ($p === null){
                    throw new MedeliceException('Product doesn\'t exist');
                }

                if ($p->getStock() == 0){
                    throw new MedeliceException("Le produit n'est plus en stock");
                }

                if (($p->getStock() - $quantity) < 0 || $product_already_contained_in_cart && $p->getStock() - $_SESSION['panier'][$ref] <= 0 && $quantity >= 1)
                {
                    throw new MedeliceException("Vous ne pouvez pas commander autant de produits");
                }
            }

            if ($product_already_contained_in_cart) {
                $_SESSION['panier'][$ref] += $quantity;
            } else {
                $_SESSION['panier'][$ref] = $quantity;
            }
            if (!$without_saving)
                self::saveCustomerCart();

            return true;
        }
        return false;
    }

    private static function getCustomerUniqueId(){
        if ($_SESSION['loggedIn'] !== true){
            return false;
        }

        $mail = $_SESSION['mail'];
        $profile = new User($mail);

        return $profile->id;
    }

    private static function saveCustomerCart(){
        $id = self::getCustomerUniqueId();
        if ($id != false)
            return User::saveCart(self::getCustomerUniqueId(), self::encodeCartAsJSON());
        return $id;
    }

    private static function deleteCustomerCart(){
        $id = self::getCustomerUniqueId();
        if ($id != false)
            return User::destroySavedCart(self::getCustomerUniqueId());
        return $id;
    }

    /**
     * Assigner une quantité fixe à une référence.
     * @param $ref
     * @param int $quantity
     * @param $without_saving bool
     * @return bool
     */
    public static function set($ref, $quantity = 1, $without_saving = false){
        if ($quantity > 2000){ //need2becheked in the future for relative n
            return false;
        }

        if (!self::isLocked()) {
            $_SESSION['panier'][$ref] = $quantity;
            if (!$without_saving)
                self::saveCustomerCart();

            return true;
        }
        return false;
    }

    /**
     * Enlever un produit du panier en fonction de la référence.
     * @param $ref
     * @return bool
     */
    public static function remove($ref, $without_saving = false){
        if (!isset($_SESSION['panier'])){
            return false;
        }

        if (!isset($_SESSION['panier'][$ref])){
            return false;
        }

        if (self::isLocked()){
            return false;
        }

        unset($_SESSION['panier'][$ref]);
        if (!$without_saving) {
            if (count($_SESSION['panier']) > 0)
                self::saveCustomerCart();
            else
                self::deleteCustomerCart();
        }
        return true;
    }

    /**
     * Détruire l'intégralité du panier.
     * @return bool
     */
    public static function destroy($without_saving = false){
        if (!isset($_SESSION['panier'])){
            return false;
        }

        $_SESSION['panier'] = array();
        if (!$without_saving)
            self::deleteCustomerCart();
        //reset cart count with medelice.js, need to be replaced

        return true;
    }

    /**
     * Récupérer la facture totale...
     * @return array|int
     */
    public static function bill()
    {
        if(!isset($_SESSION['panier']))
        {
            return null;
        }

        if(empty($_SESSION['panier'])) // séparé car après vérification de isset
        {
            return null;
        }
        
        $MASS = 0;
        $products_price = 0;
        $products = array();

        foreach (array_keys($_SESSION['panier']) as $key) //get all quantities associated with product
        {
            $factor = $_SESSION['panier'][$key]; //get quantity associated with product $key
            $price = 0;

            if (self::is_a_valisette($key)) //ref is declared as (ref+subref), but we can get box only by the main non variable ref
            {
                $BOX_MASS_G = 355;
                $FROUFROU_MASS_G = 0;
                $constants_masses_added = false;

                $splitted_ref = explode(Valisette::SUB_VALISETTE_DELIMITER, $key);
                $non_variable_ref = $splitted_ref[0]; //eg. V_BRE
                $variable_ref = $splitted_ref[1]; //eg. V_BRE$SM

                $valisette = new Valisette($non_variable_ref); //get box by it's non variable ref
                if($valisette->exists) //if box exists
                {
                    $box_content = $valisette->get_specific_sub_valisette_from_id($variable_ref);
                    if ($box_content instanceof SubValisette)
                    {
                        $sub_products = $box_content->products; //get products

                        foreach ($sub_products as $ref) //loop to get products mass
                        {
                            $product = (new DBProductProvider())->get($ref); //construct a product object from reference
                            if ($product === null) //check if the product exist
                            {
                                throw new MedeliceException("The specified product doesn't exist");
                            }

                            $MASS += $product->getTotalMass(); //add product unity mass to total mass (g)
                            $price += $product->getPrice();
                        }
                    }

                    if (!$constants_masses_added) //check if constants masses has been added
                    {
                        $MASS += ($BOX_MASS_G + $FROUFROU_MASS_G) * $factor; //add constants masses to total mass
                        $constants_masses_added = true;
                    }
                }
                else
                {
                    unset($_SESSION['panier'][$key]);
                    continue; // afficher une alerte produits invalides détectés et supprimés
                }
            }
            else //if it's a product
            {
                $product = (new DBProductProvider())->get($key); //construct a product object from reference
                if ($product !== null) //check if the product exist
                {
                    $price = $product->getPrice(); //get product price from product object
                    $MASS += $product->getTotalMass() * $factor;
                }
                else
                {
                    unset($_SESSION['panier'][$key]);
                    continue; // afficher une alerte produits invalides détectés et supprimés
                }
            }

            $products_price += ($price * $factor);
            $product_array = array('ref' => $key, 'unity_price' => $price, 'quantity' => $factor);
            array_push($products, $product_array);
        }

        $shipping_taxes = ShippingUtil::estimate_shipping_price($MASS);
        $total_bill = ($products_price + $shipping_taxes);

        $discount = 0;
        if (self::has_applied_coupon_mode_own()){
            $coupon = new Coupon([
                'name' => self::get_applied_coupon_mode_own()
            ]);

            $coupon->exists(true);
            $discount_factor = (1 - ($coupon->discount / 100));
            $discount = $total_bill - ($total_bill*$discount_factor);

            $total_bill *= $discount_factor;
        }

        return array('price' => $products_price, 'products' => $products, 'mass' => $MASS, 'discount' => $discount, 'taxes' => $shipping_taxes, 'total' => $total_bill);
    }

    /**
     * Bloquer les mises à jours du panier qd le client s'apprête à passer la commande, évite les fraudes d'ajout de produits.
     */
    public static function lock(){
        $_SESSION['panier-hash'] = md5(implode(';', self::contents()));
    }

    /**
     * Dévérouiller le panier.
     */
    public static function unlock(){
        unset($_SESSION['panier-hash']);
    }

    /**
     * Récupérer le hash correspondant au verrouillage du panier.
     * @return mixed
     */
    public static function lockHash(){
        return self::isLocked() ? $_SESSION['panier-hash'] : null; //hash or null
    }

    public static function isLocked(){
        $hash = isset($_SESSION['panier-hash']); //PHP tweak hax
        return $hash; //bool
    }

    /**
     * Vérifier que le client n'a pas essayé de nous entourlouper en ajoutant des produits.
     * @return bool Si faux le client est un enculé => Nous n'aimons pas les enculés => Nous devons l'anéantir dans des conditions d'atroces souffrances => Obligation d'écouter les 10 premiers épisodes de l'âne trotro.
     */
    public static function verifyLockHash(){
        //var_dump(self::lockHash());
        //var_dump(md5(implode(';', self::contents())));
        return (self::lockHash() === md5(implode(';', self::contents())));
    }

    public static function empty_cart_in_user_interface(){
        echo '<script>
                setCartCount(0);
              </script>';
    }

    /**
     * @return json
     */
    public static function encodeCartAsJSON($products_array_source_default = true){
        require_once(__DIR__ . '/../../orders/OrderProduct.php');

        $json = array();
        if (isset($_SESSION['gift_message'])){
            require_once(__DIR__.'/../../orders/OrderProperty.php');

            $gift_message = new OrderProperty();
            $gift_message->key = 'gift_message';
            $gift_message->value = $_SESSION['gift_message'];

            array_push($json, $gift_message);

            unset($_SESSION['gift_message']);
        }

        $products_source = is_bool($products_array_source_default) ? self::contents() : $products_array_source_default;
        if (!isset($products_source)){
            throw new MedeliceException("Cart error", 500);
        }

        foreach ($products_source as $reference => $quantity){
            $product = new OrderProduct();

            $product->_product_ref = $reference;
            $product->_product_quantity = $quantity;

            array_push($json, $product);
        }

        return json_encode($json);
    }

    public static function decode_cart_from_json($json){
        $cart = array();
        $raw_cart = json_decode($json);
        if (count($raw_cart) < 1){
            return null;
        }

        foreach ($raw_cart as $index => $value){
            if (isset($value->{'_product_ref'}) && isset($value->{'_product_quantity'})){
                $product_ref = $value->{'_product_ref'};
                $product_quantity = $value->{'_product_quantity'};

                $cart[$product_ref] = $product_quantity;
            }
        }

        return $cart;
    }

    public static function has_applied_coupon_mode_own(){
        return isset($_SESSION[COUPON_MODE_OWN_SESSION_STORAGE_NAME]);
    }

    public static function get_applied_coupon_mode_own(){
        return $_SESSION[COUPON_MODE_OWN_SESSION_STORAGE_NAME];
    }

    public static function apply_coupon($name){
        if (!Coupon::apply_mode_own($name)){
            return false;
        }

        $_SESSION[COUPON_MODE_OWN_SESSION_STORAGE_NAME] = $name;
        return true;
    }
}