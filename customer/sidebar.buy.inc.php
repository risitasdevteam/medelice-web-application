<?php
/**
 * Created by PhpStorm.
 * User: Romain
 * Date: 15/11/2017
 * Time: 18:56
 */

// ici sidebar générale pour l'achat (auth, panier, livraison/facturation, paiement, confirmation)
include_once(__DIR__."/../utility/PageDirectAccessGuardThrow.php");
?>


<div id="sidebar" class="black-bg colored-font-opposite">
    <div class="sidebar-content" style="width: 70%; margin: 0 auto; color: white">
        <a href="/index.php?page=panier" class="sidebar-link quicksand" id="sidebar-panier">
            <div class="sidebar-link-dot" style="width: 1.093vw">
                <?php include(__DIR__ . '/../images/logo_icon_inverted_color.svg.php'); ?>
            </div>
            <span class="sidebar-link-text"><?php echo L::checkout_step_cart; ?></span>
        </a>
        <br>
        <br>
        <br>
        <a href="/index.php?page=passer-commande" class="sidebar-link quicksand" id="sidebar-auth">
            <div class="sidebar-link-dot" style="width: 1.093vw">
                <?php include(__DIR__ . '/../images/logo_icon_inverted_color.svg.php'); ?>
            </div>
            <span class="sidebar-link-text"><?php echo L::checkout_step_auth; ?></span>
        </a>
        <br>
        <br>
        <br>
        <a href="/index.php?page=passer-commande&sub=delivery" class="sidebar-link quicksand" id="sidebar-delivery">
            <div class="sidebar-link-dot" style="width: 1.093vw">
                <?php include(__DIR__ . '/../images/logo_icon_inverted_color.svg.php'); ?>
            </div>
            <span class="sidebar-link-text"><?php echo L::checkout_step_delivery; ?></span>
        </a>
        <br>
        <br>
        <br>
        <a href="/index.php?page=passer-commande&sub=payment" class="sidebar-link quicksand" id="sidebar-payment">
            <div class="sidebar-link-dot" style="width: 1.093vw">
                <?php include(__DIR__ . '/../images/logo_icon_inverted_color.svg.php'); ?>
            </div>
            <span class="sidebar-link-text"><?php echo L::checkout_step_payment; ?></span>
        </a>
        <br>
        <br>
        <br>
        <a href="/index.php?page=passer-commande&sub=confirm" class="sidebar-link quicksand" id="sidebar-confirm">
            <div class="sidebar-link-dot" style="width: 1.093vw">
                <?php include(__DIR__ . '/../images/logo_icon_inverted_color.svg.php'); ?>
            </div>
            <span class="sidebar-link-text"><?php echo L::checkout_step_confirm; ?></span>
        </a>
        <br>
        <br>
        <br>
    </div>
    <script>
        function disableSidebarOrderLink(id)
        {
            $("#" + id).find('.sidebar-link-text').css("color", "#9b9b9b");
            $("#" + id).css("pointer-events", "none");
        }

        function enableSidebarOrderLink(id)
        {
            $("#" + id).find('.sidebar-link-text').css("color", "white");
            $("#" + id).css("pointer-events", "auto");
        }
    </script>
</div>

