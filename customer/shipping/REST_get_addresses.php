<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 26/08/2018
 * Time: 21:36
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/../User.class.php');
require_once(__DIR__.'/../../includes/data_handler.php');
require_once(__DIR__.'/../misc/UserUtil.php');
require_once(__DIR__."/../shipping/AddressManager.php");

try{
    include(__DIR__."/../../includes/need_to_be_logged_throw.php");

    $customer_provider = new DBCustomerProvider();
    $customer = $customer_provider->get_on_email(UserUtil::get_mail());
    if ($customer === null){
        throw new MedeliceException("Customer obj error");
    }

    $addresses = AddressManager::get_addresses_from_user_id($customer->getId()) ?? array();
    (new JsonResponse(JsonResponseStatusType::OK, new JsonResponsePayload("addresses", $addresses)))->transmit();
}
catch(Exception $e){
    exception_to_json_payload($e);
}