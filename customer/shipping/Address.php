<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 12/07/2018
 * Time: 20:14
 */
include(__DIR__ . '/../../includes/data_handler.php');
require_once(__DIR__.'/AddressManager.php');

/**
 * Class Address
 * LR : 15/7/18
 */
class Address
{
    public $signature; //string
    public $emitter_name; //string, encrypted
    public $phone; //string, encrypted
    public $address1; //string, encrypted
    public $address2; //string
    public $zip; //string
    public $city; //string
    public $state; //string
    public $country; //string, ISO 3166-1

    public $weight = 1; //usage coefficient

    public static $columns = array("signature", "emitter_name", "phone", "address1", "address2", "zip", "city", "state", "country", "weight");

    /**
     * @param $signature
     * @param $name
     * @param $address
     * @param $zip
     * @param $city
     * @param $country
     * @param $state
     * @param $weight
     * @throws DataBadException
     */
    public function __construct($name, $phone, $address1, $address2, $zip, $city, $state, $country, $weight, $generate_signature = true){
        if (!is_numeric($zip) || !is_numeric($weight)){
            throw new DataBadException("Constructor handle only numeric values for zip and weight");
        }

        $this->emitter_name = $name;
        $this->phone = $phone;
        $this->address1 = $address1;
        $this->address2 = $address2;
        $this->zip = intval($zip);
        $this->city = $city;
        $this->state = $state;
        $this->country = $country;
        $this->weight = intval($weight);

        if ($generate_signature)
            $this->signature = $this->signature();
    }

    /**
     * @return array
     */
    public function toArray(){
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function signature(){
        return hash("sha1", implode(get_object_vars($this)));
    }

    /**
     * @param $address1
     * @param $address2
     * @return array
     * @throws DataBadException
     */
    public static function array_difference($address1, $address2){
        //var_dump($address1);
        //var_dump($address2);
        if (!$address1 instanceof Address || !$address2 instanceof Address){
            throw new DataBadException("Address 1 or 2 must be Address objects");
        }

        $address1_matrix = $address1->toArray();
        $address2_matrix = $address2->toArray();

        $sql_update_sets_matrix = [];

        foreach($address1_matrix as $key => $value){
            $address2_matrix_value = $address2_matrix[$key];

            if ($value != $address2_matrix_value){
                $sql_update_sets_matrix[$key] = $address2_matrix_value;
            }
        }

        return $sql_update_sets_matrix;
    }

    public static function deobfuscate_field($field){
        $parts = explode(':', $field);
        $bin_salt = hex2bin(AddressManager::HEX_SALT);
        $decrypted = openssl_decrypt($parts[0], 'aes-128-cbc', $bin_salt, 0, base64_decode($parts[1]));

        return $decrypted;
    }

    public static function obfuscate_field($field){
        $bin_salt = hex2bin(AddressManager::HEX_SALT);
        $iv = openssl_random_pseudo_bytes(16);
        $encrypted = openssl_encrypt($field,'aes-128-cbc', $bin_salt, 0, $iv).":".base64_encode($iv);

        return $encrypted;
    }

    private static function swap_encrypted_fields_from_array($array, $encrypt = false){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        $encrypted_vars_name = ["emitter_name", "phone", "address1"];
        foreach ($array as $key => $value){
            if (in_array($key, $encrypted_vars_name)){
                $array[$key] = ($encrypt ? self::obfuscate_field($value) : self::deobfuscate_field($value));
            }
        }

        return $array;
    }

    public static function deobfuscate_encrypted_fields_from_array($array){
        return self::swap_encrypted_fields_from_array($array);
    }

    public static function obfuscate_encrypted_fields_from_array($array){
        return self::swap_encrypted_fields_from_array($array, true);
    }

    public static function get_order_customer_addresses_columns_array(){
        $useless_vars = ['weight', 'signature'];
        return array_keys(array_diff_key(array_flip(self::$columns), array_flip($useless_vars)));
    }

    public static function from_array($array, $used_in_order_customer_addresses = false){
        if (!is_array($array)){
            throw new DataBadException("array must be an array...");
        }

        $order_customer_addresses_columns = $used_in_order_customer_addresses ? array_flip(self::get_order_customer_addresses_columns_array()) : array_flip(Address::$columns);

        if (count(array_intersect_key($array, $order_customer_addresses_columns)) != count($order_customer_addresses_columns)) {
            throw new DataBadException("false arrays intersection");
        }

        //fuzzy way to cast
        $address = new Address(
            $array["emitter_name"],
            $array["phone"],
            $array["address1"],
            $array["address2"],
            $array["zip"],
            $array["city"],
            $array["state"],
            $array["country"],
            (!$used_in_order_customer_addresses ? $array["weight"] : 0),
            false
        );

        $address->signature = ($used_in_order_customer_addresses ? $address->signature() : $array["signature"]);

        return $address;
    }
}