<?php
require_once(__DIR__.'/../../utility.inc.php');

require_once(__DIR__.'/../../exceptions/data/DataException.php');
require_once(__DIR__.'/../../exceptions/data/DataBadException.php');

/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 25/10/2017
 * Time: 21:00
 */
class ShippingUtil
{
    const LAPOSTE_API_URL = 'https://api.laposte.fr';
    const LAPOSTE_API_PVTKEY = '1+pvEhbBSJtBkkQej5/zmSTkKXx5jVIHl7GY6Az3iXtVU9pct3iZRYEAGnvJuzmf';

    /**
     * Récupérer auprès de la Poste (France) les frais de livraison en fonction de la masse du colis.
     * @param $mass float La masse du colis en grammes.
     * @return float Le prix.
     * @throws DataBadException Lorsque la masse saisie n'est pas numérique.
     */
    public static function estimateShippingPriceInBorder($mass){
        if (!is_numeric($mass)){
            throw new DataBadException('mass must be numeric and in gram');
        }

        $type = 'colis';
        $response = get(
            self::LAPOSTE_API_URL.'/tarifenvoi/v1?type='.$type.'&poids='.$mass,
            array('X-Okapi-Key: '.self::LAPOSTE_API_PVTKEY));

        $array = json_decode($response);
        var_dump($response);
        return floatval($array[1]->{'price'});
    }

    /**
     * Récupérer auprès de la Poste (France) le dernier statut du colis en fonction de son numéro de tracking.
     * @param $tracking_code string Le numéro de tracking du colis.
     * @return mixed Un tableau avec les objets code, date, status, message, link, type.
     */
    public static function lastStatusOfPackageInBorder($tracking_code){
        $response = get(
            self::LAPOSTE_API_URL.'/suivi/v1?code='.$tracking_code,
            array('X-Okapi-Key: '.self::LAPOSTE_API_PVTKEY));

        return json_decode($response);
    }

    /**
     * 6000
     * @param $mass
     * @return float
     */
    public static function estimate_shipping_price($mass){
        if ($mass <= 250){
            return 4.95;
        }
        else if ($mass <= 500){
            return 6.15;
        }
        else if ($mass <= 750){
            return 7.0;
        }
        else if ($mass <= 1000){
            return 7.65;
        }
        else if ($mass <= 2000){
            return 8.65;
        }
        else if ($mass <= 3000){
            return 13.15;
        }
        else if ($mass <= 5000){
            return 13.15;
        }
        else if ($mass <= 10000){
            return 19.20;
        }
        else if ($mass <= 20000){
            return 27.30;
        }
        else{
            if ($mass % 20000 === 0){
                return 27.30 * ($mass/20000);
            }else{
                while ($mass % 20000 !== 0){
                    $mass++;
                }

                return 27.30 * ($mass/20000);
            }
        }
    }
}