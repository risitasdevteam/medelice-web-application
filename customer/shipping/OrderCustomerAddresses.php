<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 03/02/2018
 * Time: 19:55
 */
require_once(__DIR__.'/Address.php');

/**
 * Class OrderCustomerAddresses
 * LR : 15/7/18
 */
class OrderCustomerAddresses
{
    public $billing; //billing
    public $delivery; //delivery

    public function __construct(){
    }

    public function encode_to_json(){
        if ($this->billing instanceof Address && $this->delivery instanceof Address){
            $bill_arr = $this->billing->toArray();
            $del_arr = $this->delivery->toArray();
        }
        else{
            $bill_arr = &$this->billing;
            $del_arr = &$this->delivery;
        }

        if ((count(array_intersect_key($bill_arr, $del_arr)) != count(Address::$columns))){
                throw new MedeliceException("");
        }

        $useless_vars = ['weight', 'signature'];
        foreach ($bill_arr as $var_name => $var){ //anti-duplication system
            if (strcmp($del_arr[$var_name], $var) == 0){
                unset($bill_arr[$var_name]);
            }

            if (in_array($var_name, $useless_vars)){
                unset($bill_arr[$var_name]);
                unset($del_arr[$var_name]);
            }
        }

        $bill_arr = Address::obfuscate_encrypted_fields_from_array($bill_arr);
        $del_arr = Address::obfuscate_encrypted_fields_from_array($del_arr);

        $this->billing = $bill_arr;
        $this->delivery = $del_arr;

        return json_encode(get_object_vars($this));
    }

    public static function decode_from_json($json){
        $decoded = json_decode($json);

        $order_customer_addresses = new OrderCustomerAddresses();

        if (!isset($decoded->{'billing'}) || !isset($decoded->{'delivery'})){
            throw new MedeliceException("");
        }

        $raw_billing_address = $decoded->{'billing'}; //encoded OrderCustomerAddresses.class variable
        $raw_delivery_address = $decoded->{'delivery'}; //encoded OrderCustomerAddresses.class variable

        if (count($raw_billing_address) < 1 && count($raw_delivery_address) < 1){ //must be
            throw new MedeliceException("");
        }

        $data_columns = Address::get_order_customer_addresses_columns_array();
        $count_data_columns = count($data_columns);

        $billing_address = array();
        $delivery_address = array();

        for ($i = 0; $i < $count_data_columns; $i++){
            $column = $data_columns[$i];

            $raw_delivery_data = $raw_delivery_address->{$column};
            $raw_billing_data = !isset($raw_billing_address->{$column}) ? $raw_delivery_data : $raw_billing_address->{$column};

            $billing_address[$column] = $raw_billing_data;
            $delivery_address[$column] = $raw_delivery_data;
        }

        $billing_address = Address::from_array(Address::deobfuscate_encrypted_fields_from_array($billing_address), true);
        $delivery_address = Address::from_array(Address::deobfuscate_encrypted_fields_from_array($delivery_address), true);

        $order_customer_addresses->billing = $billing_address;
        $order_customer_addresses->delivery = $delivery_address;

        return $order_customer_addresses;
    }
}