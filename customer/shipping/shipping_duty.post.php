<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 24/11/2017
 * Time: 21:27
 */
include_once(__DIR__."/../../utility/PageDirectAccessGuardThrow.php");
require_once(__DIR__.'/ShippingUtil.php');
require_once(__DIR__.'/../../utility/JsonResponse.class.php');
require_once(__DIR__.'/../../initializer.inc.php');

try
{
    if (!isset($_POST['mass'])){
        throw new Exception('mass is not set');
    }

    $mass = intval($_POST['mass']);
    if (!is_int($mass)){
        throw new Exception('must be an integer');
    }

    $duty = ShippingUtil::estimate_shipping_price($mass);

    $response = new JsonResponse(JsonResponseStatusType::OK, null);
    $response->add_payload(new JsonResponsePayload('duty', $duty));
    $response->transmit();
}
catch(Exception $e)
{
    exception_to_json_payload($e);
}