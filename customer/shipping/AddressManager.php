<?php
/**
 * Created by PhpStorm.
 * User: jsanglier
 * Date: 13/07/2018
 * Time: 14:16
 */
require_once(__DIR__.'/Address.php');
require_once(__DIR__.'/../../utility/SQLUtil.php');

class AddressManager
{
    const TABLE = "users_addresses";
    const HEX_SALT = "c64ed09c806d725d9bbc357e6188d1be221eefe10706775bbc0106d144e5a979";

    private static function get_address_from($from_field_name, $from_field_value, $array = false){
        if (!is_string($from_field_name) || !is_string($from_field_value)){
            throw new MedeliceException("field name | value must be a string", 500);
        }

        $result = SQLUtil::select(self::TABLE, ['*'], [$from_field_name => $from_field_value]);
        if (count($result) < 1){
            return null;
        }


        if (!$array) {
            $pointer = $result[0];
            return Address::from_array(Address::deobfuscate_encrypted_fields_from_array($pointer));
        }
        else{
            $returned_array = array();
            for ($i = 0; $i < count($result); $i++){
                $pointer = $result[$i];
                array_push($returned_array, Address::from_array(Address::deobfuscate_encrypted_fields_from_array($pointer)));
            }

            return $returned_array;
        }
    }

    public static function get_address_from_signature($signature){
        return self::get_address_from('signature', $signature);
    }

    public static function get_addresses_from_user_id($user_id){ //array ? should not work
        return self::get_address_from('user_id', $user_id, true);
    }

    public static function range_addresses(&$addresses_array){
        if (!is_array($addresses_array)){
            throw new MedeliceException("");
        }

        if (count($addresses_array) < 2){
            return $addresses_array;
        }

        $weights_arr = array();
        foreach ($addresses_array as $address){
            if (!$address instanceof Address){
                throw new MedeliceException("");
            }

            $weights_arr[$address->signature] = $address->weight;
        }

        uasort($weights_arr, 'cmp');
        $weights_arr = array_reverse($weights_arr);

        $sorted_array = array();
        foreach ($weights_arr as $signature => $weight){
            foreach ($addresses_array as $address_key => $address_object) {
                if (!$address_object instanceof Address) {
                    throw new MedeliceException("");
                }

                if ($address_object->signature == $signature){
                    array_push($sorted_array, $address_object);
                    unset($addresses_array[$address_key]);
                }
            }
        }

        $addresses_array = $sorted_array;
        return $addresses_array;
    }

    public static function remove_address_from_signature($signature, $user_id){
        if (!is_string($signature)){
            throw new MedeliceException("signature must be a string", 500);
        }

        SQLUtil::delete(self::TABLE, ['signature' => $signature, 'user_id' => $user_id]);

        return true;
    }

    public static function update_address_from_signature($signature, $address, $user_id){
        if (!is_string($signature)){
            throw new MedeliceException("signature must be a string", 500);
        }

        if (!$address instanceof Address){
            throw new MedeliceException("address must be an Address object", 500);
        }

        //var_dump($signature);
        $address = Address::from_array(Address::obfuscate_encrypted_fields_from_array($address->toArray()));
        $old_address = self::get_address_from_signature($signature);
        $sets = Address::array_difference($old_address, $address);

        SQLUtil::update(self::TABLE,
            $sets
        , ['signature' => $signature, 'user_id' => $user_id]);

        return true;
    }

    public static function put($user_id, $address){
        if (!$address instanceof Address){
            throw new MedeliceException("address must be an Address object", 500);
        }

        SQLUtil::insert(self::TABLE, array_merge($address->obfuscate_encrypted_fields_from_array($address->toArray()), ["user_id" => $user_id]));

        return true;
    }
}