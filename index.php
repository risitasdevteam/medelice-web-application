<?php require_once('initializer.inc.php'); //need2be here because of session cache limiter warn

if (isset($_GET['page']) && strcmp($_GET['page'], "ma-commande") == 0) {
    include(__DIR__ . '/customer/account/show_order_pdf.php');
    exit;
}

require_once(__DIR__."/utility/PageDirectAccessGuard.php");

?>
<html lang="fr">
    <head>
        <?php
            include('head_data.inc.php');
        ?>
    </head>
    <body>
        <?php
            include('header.inc.php');
        ?>

        <?php
            $include = ''; // surtout pas mettre index.php sinon boucle infinie + laisser vide pour éviter d'avoir un espace vide dégueu quand on a rien à include
            switch ($current_page)
            {
                case '':
                    break;

                case 'home':
                    break;

                case 'deconnexion':
                    $include = 'customer/auth/REST_disconnect.php';
                    break;

                case 'moi':
                    break;

                case 'mes-commandes':
                    break;

                case 'commande':
                    break;

                case 'erreur':
                    break;

                case 'mdp-oublie':
                    break;

                case 'nouveau-mdp':
                    break;

                case 'confirmation-mdp-oublie':
                    $include = 'customer/account/forgotten_password/REST_confirmation-mot-de-passe-oublie.php';
                    break;

                case 'inscription':
                    break;

                case 'inscription-ok':
                    break;

                case 'verification':
                    break;

                case 'mes-parametres':
                    break;

                case "mes-adresses":
                    break;

                case "ma-commande":
                    //$include = 'customer/account/show_order_pdf.php';
                    break;

                case 'mes-coupons':
                    break;

                case 'contact':
                    break;

                case 'confirmation-mes-parametres':
                    $include = 'customer/account/my_settings/my_settings.inc.php';
                    break;

                case 'captcha': //wtf, to delete
                    $include = 'customer/user_services.inc.php';
                    break;

                case 'panier':
                    $include = 'customer/purchase/panier/footerbar_panier.inc.php';
                    break;

                case 'passer-commande':
                    break;

                case 'thankyou':
                    break;

                /*
                 * PRODUCTS DEPT
                 */
                case 'voir':
                    break;

                case 'produits':
                    $include = 'produits/view/liste.inc.php';
                    break;

                case 'valisette':
                    $include = 'produits/view/valisette.inc.php';
                    break;

                case 'chooseregion':
                    break;

                case 'test': //just for debugging todo remove
                    $include = 'test.php';
                    break;

                case 'generic':
                    break;

                default:
                    $include = null;
                    $current_page = '404';
                    break;
            }

        echo '
            <div style="display: block; width: 100%;">
                <div id="top-container">';
                    include('topsection.inc.php');
        echo '  </div>
                <div id="page-container" style="float: left;'.($include == '' ? 'margin-top: 0;': '').'">';
                    if($include != null && $include != '')
                        include($include);
        echo '  
                    <div id="footer-protection"></div>
                </div>
            </div>
        ';

        // doit absolument rester à la fin pour appliquer les nouvelles couleurs en cours de route (changements par les pages importées)
        echo '
            <script>
                $(\'.colored-bg\').css(
                    {
                        "background-color":"#'.$colors[0].'"
                    }
                );
                $(\'.colored-font\').css(
                    {
                        "color":"#'.$colors[0].'"
                    }
                );
                $(\'.colored-font-opposite\').css(
                    {
                        "color":"#'.$colors[1].'"
                    }
                );
                
                var notyf = NotificationFactory.init();
            </script>
        ';

        include (__DIR__ . '/loading_overlay.inc.php');

        ?>
    </body>
    <footer>
        <?php
            include('footer.inc.php');
        ?>
    </footer>
 </html>